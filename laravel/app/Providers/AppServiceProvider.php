<?php

namespace App\Providers;

use Cache;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      # ロカル以外にHTTPSにする
      if (!strpos(gethostname(),'.local') && !strpos($_SERVER['HTTP_HOST'],'.local')) {
          \URL::forceScheme('https');
      }

      $todoufukens = collect($this->todoufuken());
      Cache::forever('todoufukens', $todoufukens);

      // custom Validate of  phone number
      Validator::extend('jp_phone', function($attribute, $value) {
        if( $value == '' )
        {
          return true;
        }
        if ( preg_match("/-/",$value) == false )
        {
          if( preg_match("/^[0-9]{8,15}$/",$value) ) {
            return true;
          }
        }
        // $jp_phone = preg_match("/^([0-9]){2}[-]([0-9]){4}[-]([0-9]){4}$/", $value);
        // $jp_phone99 = preg_match("/^([9]){13}$/", $value);
        $jp_phone = preg_match("/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/", $value);
        return $jp_phone;
        // if($jp_phone){return $jp_phone;}
        // elseif($jp_phone99){return $jp_phone99;}
      });

      // custom Validate of character
      Validator::extend('jp_postcode', function($attribute, $value) {
        if($value == ''){ return true; }
        return  preg_match("/^([0-9]){3}[-]([0-9]){4}$/", $value);
      });

      // zenkaku check
      // 全角文字のみ許可（半角文字が混じるとfalse）
      Validator::extend('zenkaku', function($attribute, $value){
        if($value == ''){ return true; }
        return preg_match('/^[^ -~｡-ﾟ\x00-\x1f\t]+$/u', $value);
      });

      // zenkakukana check
      Validator::extend('zenkakukana', function($attribute, $value){
        if($value == ''){ return true; }
        return preg_match("/^[ァ-ヶー]+$/u",$value);
      });

      // hankaku check
      Validator::extend('hankaku', function($attribute, $value){
        if($value == ''){ return true; }
        //return !preg_match('/^[^ -~｡-ﾟ\x00-\x1f\t]+$/u', $value);
        //return preg_match("/^[a-zA-Z0-9ｱ-ﾝ\w\d\s.,-\/\+ ]+$/u",$value);
        //return preg_match("/^[a-zA-Z0-9ｦ-ﾟ!-~]+$/u", $value);
        return preg_match("/^[a-zA-Z0-9ｦ-ﾟ!-~ ]+$/u", $value);
      });

      // 半角数字、半角英字のみ
      Validator::extend('lixil_password', function($attribute, $value){
        if($value == ''){ return true; }

        if(preg_match('/^[a-zA-Z0-9]+$/u', $value)){
          $number = preg_replace("/[a-zA-Z]/u", "", $value);
          if(strlen($number) < 1) return false;

          $str = preg_replace("/[0-9]/u", "", $value);
          if(strlen($str) < 1) return false;

          return true;
        }

        return false;
      });

      // 半角数字、半角英字のみ
      Validator::extend('lixil_date', function($attribute, $value){
        if($value == '' || request('get_YMD') =='--'){ return true; }
        return checkdate(request('kiboushiteiMM'),request('kiboushiteiDD'),request('kiboushiteiYYYY'));
      });
      // 使用不可文字バリデーション
      Validator::extend('kinsoku',function($attribute, $value){
        if($value == ''){ return true; }
        $ary = array(
                'ⅰ','ⅱ','ⅲ','ⅳ','ⅴ','ⅵ','ⅶ','ⅷ','ⅸ','ⅹ','Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ',
                'Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ','￢','￤','＇','＂','㈱','№','℡','∵',
                '纊','褜','鍈','銈','蓜','俉','炻','昱','棈','鋹','曻','彅','丨','仡','仼','伀',
                '伃','伹','佖','侒','侊','侚','侔','俍','偀','倢','俿','倞','偆','偰','偂','傔',
                '僴','僘','兊','兤','冝','冾','凬','刕','劜','劦','勀','勛','匀','匇','匤','卲',
                '厓','厲','叝','﨎','咜','咊','咩','哿','喆','坙','坥','垬','埈','埇','﨏','塚',
                '增','墲','夋','奓','奛','奝','奣','妤','妺','孖','寀','甯','寘','寬','尞','岦',
                '岺','峵','崧','嵓','﨑','嵂','嵭','嶸','嶹','巐','弡','弴','彧','德',
                '忞','恝','悅','悊','惞','惕','愠','惲','愑','愷','愰','憘','戓','抦','揵','摠',
                '忞','恝','悅','悊','惞','惕','愠','惲','愑','愷','愰','憘','戓','抦','揵','摠',
                '撝','擎','敎','昀','昕','昻','昉','昮','昞','昤','晥','晗','晙','晴','晳','暙',
                '暠','暲','暿','曺','朎','朗','杦','枻','桒','柀','栁','桄','棏','﨓','楨','﨔',
                '榘','槢','樰','橫','橆','橳','橾','櫢','櫤','毖','氿','汜','沆','汯','泚','洄',
                '涇','浯','涖','涬','淏','淸','淲','淼','渹','湜','渧','渼','溿','澈','澵','濵',
                '瀅','瀇','瀨','炅','炫','焏','焄','煜','煆','煇','凞','燁','燾','犱',
                '犾','猤','猪','獷','玽','珉','珖','珣','珒','琇','珵','琦','琪','琩','琮','瑢',
                '璉','璟','甁','畯','皂','皜','皞','皛','皦','益','睆','劯','砡','硎','硤','硺',
                '礰','礼','神','祥','禔','福','禛','竑','竧','靖','竫','箞','精','絈','絜','綷',
                '綠','緖','繒','罇','羡','羽','茁','荢','荿','菇','菶','葈','蒴','蕓','蕙','蕫',
                '﨟','薰','蘒','﨡','蠇','裵','訒','訷','詹','誧','誾','諟','諸','諶','譓','譿',
                '賰','賴','贒','赶','﨣','軏','﨤','逸','遧','郞','都','鄕','鄧','釚',
                '釗','釞','釭','釮','釤','釥','鈆','鈐','鈊','鈺','鉀','鈼','鉎','鉙','鉑','鈹',
                '鉧','銧','鉷','鉸','鋧','鋗','鋙','鋐','﨧','鋕','鋠','鋓','錥','錡','鋻','﨨',
                '錞','鋿','錝','錂','鍰','鍗','鎤','鏆','鏞','鏸','鐱','鑅','鑈','閒','隆','﨩',
                '隝','隯','霳','霻','靃','靍','靏','靑','靕','顗','顥','飯','飼','餧','館','馞',
                '驎','髙','髜','魵','魲','鮏','鮱','鮻','鰀','鵰','鵫','鶴','鸙','黑','・','・'
                );
        $pattern  = '/' . implode('|', array_map('preg_quote', $ary)) . '/i';
        return !preg_match($pattern, $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function todoufuken()
    {
      return [
        '北海道','青森県','岩手県','宮城県','秋田県','山形県',
        '福島県','茨城県','栃木県','群馬県','埼玉県','千葉県',
        '東京都','神奈川県','新潟県','富山県','石川県','福井県',
        '山梨県','長野県','岐阜県','静岡県','愛知県','三重県',
        '滋賀県','京都府','大阪府','兵庫県','奈良県','和歌山県',
        '鳥取県','島根県','岡山県','広島県','山口県','徳島県',
        '香川県','愛媛県','高知県','福岡県','佐賀県','長崎県',
        '熊本県','大分県','宮崎県','鹿児島県','沖縄県'
      ];
    }
}
