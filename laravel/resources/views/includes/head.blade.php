<title>@yield('title')</title>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="The HTML5 Herald">
<meta name="author" content="SitePoint">
<meta name="_token" content="{!! csrf_token() !!}" />
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="/assets/images/favicon.ico">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
<link rel='stylesheet prefetch' href='//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<!-- script jquery -->
<script  type="text/javascript" src="/assets/js/jquery.min.js"></script>
<!-- script datetimepicker -->
<script src='/js/app.js'></script>
<script src='/assets/js/moment.min.js'></script>
<script type="text/javascript" src="/assets/js/bootstrap-datetimepicker.min.js"></script>
<!-- script jquery chosen-->
<script type="text/javascript" src="/assets/js/chosen.jquery.min.js"></script>  
<!-- script jquery autoKana-->
<script type="text/javascript" src="/assets/js/jquery.autoKana.js"></script>  
<link type="text/css" href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
<!-- main style -->
<link type="text/css" href="/assets/css/style.css" type='text/css' rel="stylesheet"/>
<!-- style chosen-->
<link href="/assets/css/chosen.min.css" rel="stylesheet">
@if(Route::current() !== null && Route::current()->getName() == 'home')
<!-- ユーザーIDをGoogle Analyticsに送信する -->
<script>
var dataLayer = dataLayer || [];
dataLayer.push({'repairUserId': '{{Auth::user()->user_id}}'});
</script>
<!-- End ユーザーIDをGoogle Analyticsに送信する -->
@endif
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
