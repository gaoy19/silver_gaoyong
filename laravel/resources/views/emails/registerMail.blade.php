担当者御中

インターネット修理受付センターより、次の内容の利用者登録がありました。
登録を実施ください。

登録日時：{{ $date }}

貴社名：{{ $tokuisaki_sei }}
貴社名（フリガナ）：{{ $tokuisaki_sei_kana }}

郵便番号：{{ $tokuisaki_yubin }}
都道府県：{{ $tokuisakiTodoufuken }}
ご住所：{{ $tokuisakiAddress }}

ご担当者名：{{ $user_simei }}

お電話番号：{{ $tokuisaki_tel_haihun }}
FAX番号：{{ $torokusaki_fax }}

連絡先メールアドレス（メールアドレス2）：{{ $info_mail_address  }}
修理進捗状況 送信用メールアドレス（メールアドレス1）：{{ $mail_address}}

パスワード：{{ $password }}
暗号化後のパスワード：{{ $password_encrypt }}
