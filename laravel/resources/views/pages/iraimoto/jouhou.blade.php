@extends('layouts.master')
@section('request_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理のご依頼[3ご依頼元情報の入力]')
@section('content')
  <!-- * Start section 修理のご依頼 * -->
  <div class="row-sv">
      <!-- * start row top * -->
      <div class="main-vs-12">
          <div class="box-title_sv">
              <h1>修理のご依頼</h1>
          </div>
      </div>
  </div><!--/row-sv-->
  <!-- * End section 修理のご依頼 * -->
  <!-- * Start section お客さま情報の入力 * -->
  <div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-3 active_sv">
                <a href="javascript:void(0);" onclick="@if(Session::has('reachConfirmationFlag')) submitByProgress('iraimoto.showKokyaku') @else submitBack('iraimoto.showKokyaku') @endif" class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">お客さま情報の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <a href="javascript:void(0);" onclick="@if(Session::has('reachConfirmationFlag')) submitByProgress('iraimoto.showNaiyou') @else submitBack('iraimoto.showNaiyou') @endif" class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">修理依頼内容の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力 </div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <a href="javascript:web_customer_info.submit()" class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <div class="box-progress-bar">
                    <div class="box-num-bar">5</div>
                    <div class="box-text-bar">修理のご依頼完了 </div>
                </div>
            </li><!--/box-sv-3-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
  </div><!--/row-sv-->
  <!-- * End section お客さま情報の入力 * -->
  <!-- * Start section 4.3 ご依頼元情報 * -->
  <div class="row-sv">
    <div class="main-vs-12 sv_top20">
        <form id="web_customer_info" method="POST" action="{{ route('iraimoto.jouhou') }}" accept-charset="UTF-8" role="form">
            {{ csrf_field() }}
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>ご依頼元情報<span>貴社登録情報に変更があった場合は<a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a>webinfo-imt@i2.inax.co.jpまでご連絡下さい</span></h5>
                    <ul class="col-nav-bt" style="margin-top:-30px;"><li><a href="javascript:submitReset();"><span style="margin-left:30px;">全てクリア</span></a></li></ul>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv width100">
                        <!--得意先コード-->
                        <div class="tr-form_sv width100">
                            <div class="td-form_sv-4 width100">
                                <div class="col-ft-left">
                                    <div class="td-title">得意先コード</div>
                                </div><!--/col-ft-left-->
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        <div class="span-text-12">{{ Auth::user()->tokuisaki_cd }}</div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv1-->
                        <!--貴社名-->
                        <div class="tr-form_sv width100">
                            <div class="td-form_sv-4">
                                <div class="col-fts-left mid">
                                    <div class="td-title">貴社名</div>
                                </div><!--/col-ft-left-->
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-fts-right">
                                    <div class="col-td-12">
                                        <div class="span-text-12">{{ $goiraimoto_info->tokuisaki_sei }}</div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv1-->
                        <!--ご依頼元担当者様-->
                        <div class="tr-form_sv width100">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご依頼元担当者様</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('goiraimotoTantou'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('goiraimotoTantou') }}</span></div>
                                        @endif
                                        <input type="text" value="{{ old('goiraimotoTantou') }}" name="goiraimotoTantou" class="form-control_sv ime-active{!! $errors->has('goiraimotoTantou') ? ' input-error' : '' !!}" id="customer_Requestor" maxlength="10" style="max-width:500px;">
                                        <div class="label-info-12 col-top10">
                                            <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt="">   </span>全角 1０文字以内で入力してください
                                            </div>
                                        </div>
                                    </div><!--/col-td-12-->
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--貴社住所-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-fts-left mid">
                                    <div class="td-title">貴社住所</div>
                                </div><!--/col-ft-left-->
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-fts-right">
                                    <div class="col-td-12">
                                        <div class="span-text-12">{{ $goiraimoto_info->tokuisaki_jusyo }}</div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv1-->
                        <!--貴社電話番号-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-fts-left">
                                    <div class="td-title">貴社電話番号</div>
                                </div><!--/col-ft-left-->
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-fts-right">
                                    <div class="col-td-12">
                                        <div class="span-text-12">{{ $goiraimoto_info->tokuisaki_tel_haihun }}</div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv1-->
                        <!--貴社FAX-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-fts-left">
                                    <div class="td-title">貴社FAX</div>
                                </div><!--/col-ft-left-->
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-fts-right">
                                    <div class="col-td-12">
                                        <div class="span-text-12">{{ $goiraimoto_info->torokusaki_fax }}</div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv1-->
                        <!--貴社注番-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">貴社注番</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('kisya_chuban'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('kisya_chuban') }}</span></div>
                                      @endif
                                        <input type="text" class="form-control_sv ime-inactive{!! $errors->has('kisya_chuban') ? ' input-error' : '' !!}" id="company_order_number" name="kisya_chuban" value="{{ old('kisya_chuban') }}"  maxlength="20" style="max-width:300px;">
                                    </div><!--/col-td-12-->
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                    </div><!--/tb-info_sv-->
                </div><!--/con-group-sv-->
            </div><!--/box-main-form-->
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>進捗のご連絡
                        <span>FAXにて受付確認票・結果報告(速報・詳細)・進捗報告が送られます</span><span class="des-info">※ご登録状況により送付帳票は異なる場合があります</span>
                    </h5>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv">
                        <input class="sv-checkbox" id="tyohyo_uk" type="hidden" name="tyohyo_uk" @if($goiraimoto_info->fax1 == '1') value="on" @endif>
                        <input class="sv-checkbox" id="tyohyo_gj" type="hidden" name="tyohyo_gj" @if($goiraimoto_info->fax2 == '1') value="on" @endif>
                        <input class="sv-checkbox" id="tyohyo_fax3" type="hidden" name="tyohyo_fax3" @if($goiraimoto_info->fax3 == '1') value="on" @endif>
                        <input class="sv-checkbox" id="tyohyo_fax5" type="hidden" name="tyohyo_fax5" @if($goiraimoto_info->fax5 == '1') value="on" @endif>
                        <input class="sv-checkbox" id="tyohyo_uf" type="hidden" name="tyohyo_uf" @if($goiraimoto_info->fax6 == '1') value="on" @endif>
                         <!--メール送信 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">メール送信</div>
                                    <div class="des-info"><span><br>受付確認・結果速報は、<br>ご登録アドレスに<br>テキストメール連絡<br>することも可能です<br>(FAX併用)</span></div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('mailaddress'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('mailaddress') }}</span></div>
                                        @endif
                                        <div class="span-text-12">修理進捗状況送信用メールアドレス：@if(isset(Auth::user()->mail_address)) <input id="mailaddress" type="text" name="mailaddress" class="form-control_sv {!! $errors->has('mailaddress') ? ' input-error' : '' !!}" value="{{ Auth::user()->mail_address }}" style="background-color:#dddddd;" disabled="disabled"> @else "未登録" @endif</div>
                                    </div>
                                    @if(!isset(Auth::user()->mail_address)) 
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>未登録でメール報告もご希望される場合、登録情報変更から修理進捗状況送信用メールアドレスを登録ください。</div>
                                    </div>
                                    @else
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>テキストメール送信も希望される場合、チェックを入れてください。</div>
                                    </div>
                                    @endif
                                    <div class="col-td-3" style="max-width:125px">
                                        <div class="box-checkbox">
                                        @if(isset(Auth::user()->mail_address)) 
                                            <input class="sv-checkbox" id="kakunin_mail" type="checkbox" value="on" name="kakunin_mail" @if(old('kakunin_mail') == "on") checked @endif>
                                        @else
                                        <input class="sv-checkbox" id="kakunin_mail" type="checkbox" value="off" name="kakunin_mail" style="background-color:#dddddd;" disabled="disabled">
                                        @endif
                                            <label for="kakunin_mail">受付確認</label>
                                        </div>
                                    </div><!--/col-td-3-->
                                    <div class="col-td-3">
                                        <div class="box-checkbox">
                                        @if(isset(Auth::user()->mail_address)) 
                                            <input class="sv-checkbox" id="sokuho_mail" type="checkbox" value="on" name="sokuho_mail" @if(old('sokuho_mail') == "on") checked @endif>
                                        @else
                                            <input class="sv-checkbox" id="sokuho_mail" type="checkbox" value="off" name="sokuho_mail" style="background-color:#dddddd;" disabled="disabled">
                                        @endif
                                            <label for="sokuho_mail">結果速報</label>
                                        </div>
                                    </div><!--/col-td-3-->
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                    </div><!--/tb-form_sv-->
                </div><!--/con-info_sv-->
            </div><!--/box-main-form-->
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>ご請求先</h5>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv">
                        <!--添付資料 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先を選択してください</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      <div class="group_radio">
                                            <input type="radio" id="direct_receipt" name="seikyuuKubun" value="1" @if(old('seikyuuKubun') == "1" ) checked @endif onclick="changeSeikyuukubunFlg(this)">
                                            <label for="direct_receipt">直収</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="your_request" name="seikyuuKubun" value="3" @if(old('seikyuuKubun') == "3" || !Session::has('jouhou')) checked @endif onclick="changeSeikyuukubunFlg(this)">
                                            <label for="your_request">貴社請求</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="Other" name="seikyuuKubun" value="2" @if(old('seikyuuKubun') == "2") checked @endif onclick="changeSeikyuukubunFlg(this)">
                                            <label for="Other">その他</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="site_dicision" name="seikyuuKubun" value="5" @if(old('seikyuuKubun') == "5") checked @endif onclick="changeSeikyuukubunFlg(this)">
                                            <label for="site_dicision">現調後依頼元判断  ※保証期間内の商品を含む</label>
                                        </div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->

                    </div><!--/tb-form_sv-->
                </div><!--/con-info_sv-->
            </div><!--/box-main-form-->
            <!--修理依頼内容の入力へ進む-->
            <div class="row-sv mid">
                <div class="col-sv-6">
                    <a class="btn btn-submit-send" href="javascript:void(0);" onclick="@if(Session::has('reachConfirmationFlag'))  submitByProgress('iraimoto.showNaiyou')  @else  submitBack('iraimoto.showNaiyou')  @endif">
                      <span class="arrow-send_lf"><img src="{{ asset('assets/images/arrow_send_lf.png') }}"></span><span class="arrow-text_lf">2  修理依頼内容の入力に戻る</span></a>
                </div>
                <div class="col-sv-6 btn-submit" style="width: auto;">
                    <div class="col-btn">
                      @if(old('seikyuuKubun') == "2")
                        <button class="btn btn-submit-send" type="submit" name="confirm" value="iraimoto.showGoseikyusaki" ><span class="arrow-text">3  ご請求先の入力に進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
                      @else
                        <button class="btn btn-submit-send" type="submit" name="confirm" value="iraimoto.showConfirmation" ><span class="arrow-text">4  ご依頼内容の確認に進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
                      @endif
                    <div class="col-btn">
                    </div>
                </div>
            </div>
            <input type="hidden" value="" id="progressClick" name="progressClick"><!--/どのステータスをクリックしたか-->
        </form><!--/form-horizontal_sv-->
    </div>
  </div><!--/row-sv--><!--/row-sv-->
  <!-- * End section 4.3 お客さま情報の入力 * -->
  <script type="text/javascript">
    function submitReset() {
        if(confirm("入力した依頼内容が全てクリアされますが、よろしいですか？")){
            var data = $('#web_customer_info');
            data.attr("action",'{{ route('iraimoto.reset') }}');
            data.submit();
        }
    }
    function submitBack(progress) {
      var data = $('#web_customer_info');
      data.attr("action",'{{ route('iraimoto.submitBack') }}');
      // 遷移先を保存
      $("#progressClick").val(progress);
      data.submit();
    }
    function submitByProgress(progress){
      $("#progressClick").val(progress);
      $("#web_customer_info").submit();
    }
  </script>
  <script type="text/javascript">
    $("form").submit(function(e){
    $('input[name=mailaddress]').removeAttr('disabled');
　  });
    $(function () {
      seikyuuKubunFlg = {{ old('seikyuuKubun') ? old('seikyuuKubun'):1 }};
      $("input[name=seikyuuKubun]:radio").change(function (event) {
          if($(this).val() == '2'){
            $("button[name=confirm]:submit").val('iraimoto.showGoseikyusaki');
            $("button[name=confirm]:submit span.arrow-text").text('3  ご請求先の入力に進む');
          } else {
            $("button[name=confirm]:submit").val('iraimoto.showConfirmation');
            $("button[name=confirm]:submit span.arrow-text").text('4  ご依頼内容の確認に進む');
          }
      });
    });
    function changeSeikyuukubunFlg(obj){
      seikyuuKubunFlg = obj.value;
    }
  </script>
@stop
