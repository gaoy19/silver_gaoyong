@extends('layouts.master')
@section('title','LIXILインターネット修理受付センター | 登録情報の変更 | 利用者情報の変更[1利用者情報の変更]')
@section('content')
<!--  * start section container  * -->
<section class="container-silver">
    <div class="row-sv">
        <!-- * start row top * -->
        <div class="main-vs">
            <div class="box-title_sv">
                <h1>利用者情報の変更</h1>
            </div>
        </div>
    </div><!--/row-sv-->
    <div class="row-sv">
        <div class="col-sv-12 box-progress-12">
            <ul class="row-box-bar row-20">
                <li class="box-sv-5 active_sv">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">1</div>
                        <div class="box-text-bar">利用者情報の変更</div>
                    </div>
                </li><!--/box-sv-4-->
                <li class="box-sv-5">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">2</div>
                        <div class="box-text-bar">変更内容のご確認</div>
                    </div>
                </li><!--/box-sv-4-->
                <li class="box-sv-5">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">3</div>
                        <div class="box-text-bar">変更完了</div>
                    </div>
                </li><!--/box-sv-4-->
            </ul><!--/box-bar-->
        </div><!--/box-progress-12-->
    </div><!--/row-sv-->
    <div class="row-sv">
        <div class="main-vs sv_top20" style="margin-top: 0px;">
            <form id="web_customer_info" name="form_changeInformation" method="POST" action="{{ route('user.change.information.confirmation') }}">
              {{ csrf_field() }}
                <div class="box-main-form">
                    <div class="con-info_sv" style="padding-bottom:0px;">
                        <div class="con-info-detail" style="padding-top: 22px;">ご担当者名、メールアドレスが変更できます。表示されている登録情報に変更があった場合は<a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a>webinfo-imt@i2.inax.co.jpまでご連絡下さい</div>
                            <div class="tb-form_sv">
                                <!--貴社名-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">貴社名</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                <input value="{{ old('tokuisaki_sei', $tokuisaki_sei) }}" type="text" class="form-control_sv ime-active" id="tokuisaki_sei" name="tokuisaki_sei" style="max-width:500px;background-color:#dddddd;" disabled="disabled" />
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div>
                                <!--貴社名（フリガナ）-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">貴社名（フリガナ）</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                <input value="{{ old('tokuisaki_sei_kana', $tokuisaki_sei_kana) }}" type="text" class="form-control_sv ime-active" id="tokuisaki_sei_kana" name="tokuisaki_sei_kana" style="max-width:500px;background-color:#dddddd;" disabled="disabled" />
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div>
                                <!--郵便番号-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">郵便番号</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-5">
                                                <input type="text" value="{{ old('tokuisaki_yubin', $tokuisaki_yubin) }}" maxlength="8" class="form-control_sv ime-disabled{!! $errors->has('tokuisaki_yubin') ? ' input-error' : '' !!}; " id="customer_zip_code"  name="tokuisaki_yubin" style="max-width:200px; background-color:#dddddd;" onkeypress="return isNumberKey(event);" disabled="disabled">
                                            </div>
                                            <div class="col-td-7">
                                                 <input type="button" class="btn-control_sv" id="add_zip_code"  style="max-width:200px; display:none;" value="住所検索" onclick="showAddressData()"/>
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div>
                                <!--都道府県-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">都道府県</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                <select class="form-control_sv{!! $errors->has('tokuisaki_jusyo') ? ' input-error' : '' !!}" name="tokuisaki_jusyo" size="1" style="max-width:180px;background-color:#dddddd; " disabled="disabled">
                                                    <option value="">選択してください</option>
                                                    @foreach (Cache::get('todoufukens') as $todoufuken)
                                                      <option value="{{ $todoufuken }}" {{ ( old('tokuisaki_jusyo', $tokuisaki_jusyo) == $todoufuken ) ? ' selected' : '' }}>{{ $todoufuken }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div>
                                <!--ご住所-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">ご住所</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                <input type="text" value="{{ old('tokuisaki_jusyo_address', $tokuisaki_jusyo_address) }}" maxlength="45" class="form-control_sv ime-active{!! $errors->has('tokuisaki_jusyo_address') ? ' input-error' : '' !!}" id="tokuisaki_jusyo_address"  name="tokuisaki_jusyo_address" style="max-width:500px; background-color:#dddddd;" disabled="disabled" >
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                                <!--お電話番号-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">お電話番号</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                <input value="{{ old('tokuisaki_tel_haihun', $tokuisaki_tel_haihun) }}" type="text" class="form-control_sv ime-disabled" id="tokuisaki_tel_haihun" name="tokuisaki_tel_haihun" style="max-width:300px;background-color:#dddddd;" disabled="disabled" >
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                                <!--FAX 番号-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">FAX 番号</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                <input value="{{ old('torokusaki_fax', $torokusaki_fax) }}" type="text" class="form-control_sv ime-disabled" id="torokusaki_fax" name="torokusaki_fax" style="max-width:300px;background-color:#dddddd;" disabled="disabled" >
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                                <!--ご担当者名-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">ご担当者名</div>
                                            <span class="error_label">必須</span>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                @if ($errors->has('user_simei'))
                                                    <div class="error_sv max-100"><span>{{ $errors->first('user_simei') }}</span></div>
                                                @endif
                                                <input value="{{ old('user_simei', $user_simei) }}" type="text" class="form-control_sv" id="user_simei" name="user_simei" style="max-width:300px;" />
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                                <!--連絡先メールアドレス-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">連絡先メールアドレス</div>
                                            <span class="error_label">必須</span>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                @if ($errors->has('info_mail_address'))
                                                    <div class="error_sv max-100"><span>{{ $errors->first('info_mail_address') }}</span></div>
                                                @endif
                                                <input value="{{ old('info_mail_address', $info_mail_address) }}" type="text" class="form-control_sv ime-disabled" id="info_mail_address" name="info_mail_address" style="max-width:500px;" >
                                            </div>
                                            <div class="label-info-12 col-top10" style="margin: 5px 0px 0px 0px;">
                                                <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>修理受付センターからのご連絡を受信するメールアドレスを記入してください</div>
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                                <!--修理進捗状況送信用 メールアドレス-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4 tb-fonm-none">
                                        <div class="col-ft-left">
                                            <div class="td-title">修理進捗状況送信用</div><br/>
                                            <div class="td-title">メールアドレス</div>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8 tb-fonm-none">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                @if ($errors->has('mail_address'))
                                                    <div class="error_sv max-100"><span>{{ $errors->first('mail_address') }}</span></div>
                                                @endif
                                                <input value="{{ old('mail_address', $mail_address) }}" type="text" class="form-control_sv ime-disabled" id="mail_address" name="mail_address" style="max-width:500px;" >
                                            </div>
                                            <div class="label-info-12 col-top10" style="margin: 5px 0px 0px 0px;">
                                                <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>進捗連絡・受付完了メールを受信するメールアドレスを記入してください。不用の場合は空欄で申請ください。</div>
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                            </div><!--/tb-info_sv-->
                       </div><!--/con-group-sv-->
                </div><!--/box-main-form-->
                <div class="btn-center-customer" style="margin-top:60px;">
                   <div class="sv_center">
                        <input type="submit" class="btn-confirm" id="confirm_changes" value="変更内容を確認する">
                   </div>
                   <div class="sv_center" style="margin-top:10px">
                        <a class="btn-confirm-return" id="return_change_registration" href="{{ route('user.change.index') }}">登録情報の変更に戻る</a>
                   </div>
                </div>
            </form><!--/form-horizontal_sv-->
        </div>
    </div><!--/row-sv-->
</section><!--/container-silver-->
<!--  * end section container  * -->
<script type="text/javascript">
$("form").submit(function(e){
    $('select[name=tokuisaki_jusyo]').removeAttr('disabled');
    $('input[name=tokuisaki_sei]').removeAttr('disabled');
    $('input[name=tokuisaki_sei_kana]').removeAttr('disabled');
    $('input[name=tokuisaki_yubin]').removeAttr('disabled');
    $('input[name=tokuisaki_jusyo_address]').removeAttr('disabled');
    $('input[name=tokuisaki_tel_haihun]').removeAttr('disabled');
    $('input[name=torokusaki_fax]').removeAttr('disabled');
});
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    console.log(charCode);
      if (charCode != 45 && charCode > 31
      && (charCode < 48 || charCode > 57))
       return false;
    return true;
  }
  $(document).ready(function() {
    $("input[name=yubinbango]").change(function (e) {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      })
      e.preventDefault();
      var formData = {
          tokuisaki_yubin: $('#customer_zip_code').val()
      }
      $.ajax({
          type: 'POST',
          url: '{{ route('json.yubinbango') }}',
          data: formData,
          dataType: 'json',
          success: function (data) {
              if(data){
                $('select[name=tokuisaki_jusyo]').val(data.ken_mei);
                $('input[name=tokuisaki_jusyo_address]').val(data.sityoson_mei);
              }
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
    });
  });
  // 住所検索ダイアログ表示
  function showAddressData() {
    if($('input[name=tokuisaki_yubin]').val().length == 0 &&
      $('select[name=tokuisaki_jusyo]').val().length == 0 &&
      $('input[name=tokuisaki_jusyo_address]').val().length == 0){
        alert('郵便番号、都道府県、ご住所のいずれかを入力してください。');
        return;
      }
    // 住所検索フォーム
    window.open("{{route('yubinbango.search')}}?windowName=childWin&actionID=search2"
              +"&ken_mei_val=" + $('select[name=tokuisaki_jusyo]').val()
              +"&yubinBango_val=" + $('input[name=tokuisaki_yubin]').val()
              +"&sityoson_mei=" + $('input[name=tokuisaki_jusyo_address]').val()
              +"&form=form_changeInformation"
              +"&_token=" + $('meta[name="_token"]').attr('content'),
              "addressSearchForm",
              "top=0,left=0,height=650,width=850,status=no,scrollbars=yes,toolbar=no,menubar=no");
  }
</script>
@stop
