<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>ＬＩＸＩＬ インターネット修理受付センター - 住所検索画面</title>
<style media="screen">
.col-ft-left {
  padding-top: 15px;
  padding-bottom: 10px;
  text-align: right;
}
.col-ft-right {
    padding: 5px;
}
.text-center{text-align: center;}
.tb-responsive{
  margin: 20px 0;
}
th{
  padding: 10px 0;
  border: 1px solid #dddddd;
  background-color: rgba(231, 84, 0, 0.82);
  color: #ffffff;
}
td{
  padding: 5px 0 0 5px;
  border: 1px solid #dddddd;
}
.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}
input.btn-confirm {
  font-size:14px;
  height:auto;
  border-radius:4px;
  width:55px;
  margin-bottom:5px;
  border-width: 1px;
  font-weight: 100;
  padding: 3px;
}
input.btn-confirm:hover, .btn-confirm:hover {
  border: solid 1px #f79208;
  color: #f79208;
}
</style>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <section class="container-silver">
    <!-- * Start section print * -->
    <div class="row-sv">
        <!-- * start row top * -->
        <div class="box-title_sv">
            <h1>住所検索</h1>
        </div>
    </div><!--/row-sv-->

    <div class="row-sv">
      <div class="box-main-form">
        <div class="modal-header_sv">
            <h2>検索条件</h2>
        </div>
        <div class="con-info_sv">
          <div class="tb-form_sv">
            <form id="web_customer_info"  method="GET" action="{{ route('yubinbango.search') }}" accept-charset="UTF-8" role="form">
              {{ csrf_field() }}
              <input type="hidden" name="form" value="{{isset($input['form'])?$input['form']:''}}">
              <!--連絡先名-->
              <div class="tr-form_sv">
                  <div class="td-form_sv-3 tb-fonm-none">
                      <div class="col-ft-left">
                          <div class="td-title">郵便番号</div>
                      </div>
                  </div><!--/td-form_sv-4-->
                  <div class="td-form_sv-8 tb-fonm-none">
                      <div class="col-ft-right">
                          <div class="col-td-12">
                            <input type="text" name="yubinBango_val" id="yubinBango_val" maxlength="8" size="8" value="{{ isset($input['yubinBango_val'])?$input['yubinBango_val']:'' }}" class="form-control_sv ime-disabled" style="width: 120px;">
                          </div>
                      </div>
                  </div><!--/td-form_sv-8-->
              </div><!--/tr-form_sv-->
              <!--連絡先電話番号-->
              <div class="tr-form_sv">
                  <div class="td-form_sv-3 tb-fonm-none">
                      <div class="col-ft-left">
                          <div class="td-title">都道府県</div>
                      </div>
                  </div><!--/td-form_sv-4-->
                  <div class="td-form_sv-8 tb-fonm-none">
                      <div class="col-ft-right">
                          <div class="col-td-12">
                            <select class="form-control_sv" name="ken_mei_val" size="" style="max-width:180px;">
                                <option value="">選択してください</option>
                                @foreach (Cache::get('todoufukens') as $todoufuken)
                                  <option value="{{ $todoufuken }}" {{ ( (isset($input['ken_mei_val'])?$input['ken_mei_val']:'') == $todoufuken ) ? ' selected' : '' }}>{{ $todoufuken }}</option>
                                @endforeach
                            </select>
                          </div>
                      </div>
                  </div><!--/td-form_sv-8-->
              </div><!--/tr-form_sv-->

              <div class="tr-form_sv">
                  <div class="td-form_sv-3 tb-fonm-none">
                      <div class="col-ft-left">
                          <div class="td-title">市町村</div>
                      </div>
                  </div><!--/td-form_sv-4-->
                  <div class="td-form_sv-8 tb-fonm-none">
                      <div class="col-ft-right">
                          <div class="col-td-8">
                            <input type="text" name="sityoson_mei" size="50" value="{{ isset($input['sityoson_mei'])?$input['sityoson_mei']:'' }}" class="form-control_sv ime-active">
                          </div>
                          <div class="col-td-4">
                            <input type="submit" name="search" class="btn-control_sv" value="検&nbsp;索">
                          </div>
                      </div>
                  </div><!--/td-form_sv-8-->
              </div><!--/tr-form_sv-->
            </form>
          </div><!--/tb-form_sv-->
      </div>
      </div>
    </div>

    @if(isset($yubins))
    <div class="row-sv">
      <div class="box-main-form">
        <div class="modal-header_sv">
          <font style="font-size: 20px"><b>検索結果一覧</b></font>（決定ボタンをクリックすると前画面に郵便番号と住所がセットされます）<br>
          検索結果は最大１００件まで表示されます。
        </div>
        <div class="con-info_sv">
          <div class="tb-form_sv">
            <div class="tb-responsive con-info_sv">
              <table class="detailList table-striped" border="1" cellpadding="0" cellspacing="0">
                <tbody class="lixil_body">
                  <tr>
                    <th class="text-center">選択</th>
                    <th class="text-center">郵便番号</th>
                    <th class="text-center">住所</th>
                    <th class="text-center">旧住所</th>
                  </tr>
                @foreach($yubins as $yubin)
                  <tr>
                    <td class="text-center"><input type="button" class="btn-confirm" name="{{substr($yubin->yubin_bango, 0, 3)."-".substr($yubin->yubin_bango, 3, 4)}},{{$yubin->ken_mei}},{{$yubin->sityoson_mei}}" value="決定" onclick="setYubinAndAddress(this.name)"></td>
                    <td class="text-center">{{substr($yubin->yubin_bango, 0, 3)."-".substr($yubin->yubin_bango, 3, 4)}}</td>
                    <td>{{$yubin->sityoson_mei}}</td>
                    <td>{{$yubin->old_sityoson_mei}}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif
    <div class="row-sv">
      <center>
        <input type="button" value="キャンセル" onclick="javascript:cancel();" class="btn-control_sv">
      </center>
    </div>
  </section>
  <!-- * Start section prind * -->
  <script  type="text/javascript" src="/assets/js/jquery.min.js"></script>
  <script type="text/javascript">
  @if(isset($input['form']) && $input['form'] == 'form_kokyaku')
    function setYubinAndAddress(value) {
      var yubinBango = value.substr(0, value.indexOf(','));
      var values = value.split(",");
      window.opener.document.forms['form_kokyaku'].okyakusamaYuubinNo.value = values[0];
      window.opener.document.forms['form_kokyaku'].okyakusamaAddress.value = values[2];
      window.opener.document.forms['form_kokyaku'].okyakusamaTodoufuken.value = values[1];
      $('.chosen-single span',window.opener.document).text(values[1]);
      self.window.close();
    }
  @elseif(isset($input['form']) && $input['form'] == 'form_goseikyusaki')
    function setYubinAndAddress(value) {
      var yubinBango = value.substr(0, value.indexOf(','));
      var values = value.split(",");
      window.opener.document.forms['form_goseikyusaki'].sonotaYuubinNo.value = values[0];
      window.opener.document.forms['form_goseikyusaki'].sonotaAddress.value = values[2];
      window.opener.document.forms['form_goseikyusaki'].sonotaTodoufuken.value = values[1];
      $('.chosen-single span',window.opener.document).text(values[1]);
      self.window.close();
    }
  @elseif(isset($input['form']) && $input['form'] == 'form_changeInformation')
      function setYubinAndAddress(value) {
        var yubinBango = value.substr(0, value.indexOf(','));
        var values = value.split(",");
        window.opener.document.forms['form_changeInformation'].tokuisaki_yubin.value = values[0];
        window.opener.document.forms['form_changeInformation'].tokuisaki_jusyo.value = values[1];
        window.opener.document.forms['form_changeInformation'].tokuisaki_jusyo_address.value = values[2];
        self.window.close();
      }

  @elseif(isset($input['form']) && $input['form'] == 'form_register')
    function setYubinAndAddress(value) {
      var yubinBango = value.substr(0, value.indexOf(','));
      var values = value.split(",");
      window.opener.document.forms['form_register'].tokuisakiYuubinNo.value = values[0];
      window.opener.document.forms['form_register'].tokuisakiAddress.value = values[2];
      window.opener.document.forms['form_register'].tokuisakiTodoufuken.value = values[1];
      self.window.close();
    }
  @endif
  function cancel() {
    self.window.close();
  }


  </script>
</body>
</html>
