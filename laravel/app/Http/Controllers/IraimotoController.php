<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Iraimoto;
use App\RequestData;
use Session;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use Mail;
use Storage;
use App\Http\Requests;

class IraimotoController extends Controller
{
  private $_session_step1 = 'kokyaku'; // Store data from step 1
  private $_session_restep1 = 'rekokyaku'; // update field
  private $_session_step2 = 'naiyou'; // Store data from step 2
  private $_session_restep2 = 'renaiyou'; // update field
  private $_session_step3 = 'jouhou'; // Store data from step 3
  private $_session_restep3 = 'rejouhou'; // update field
  private $_session_step4 = 'goseikyusaki'; // Store data from step 4
  private $_session_restep4 = 'regoseikyusaki'; // update field
  private $_session_files = 'fileUpload';
  private $_model;

  public function __construct(Iraimoto $model)
  {
    $this->_model = $model;
  }

  /**
   * Get Submit from view then redirect
   */
  public function submitBack(Request $request)
  {
    // 前のページを取得する
    $referer = request()->headers->get('referer');
    if(route('iraimoto.naiyou') == $referer) {
      // 修理依頼内容の入力画面の場合
      $values = $this->_model->charReplaceNaiyou($request->all());
      // 添付ファイルを削除
      for($i=1;$i<=5;$i++){
        if(isset($values["tempsiryoFile$i"])){
          unset($values["tempsiryoFile$i"]);
        }
      }
      // セッションに入力データを保存する
      Session::put($this->_session_step2,$values);

      return Redirect::route('iraimoto.showKokyaku');

    } else if (route('iraimoto.jouhou') == $referer) {
      // ご依頼元情報の入力画面の場合
      $input = $this->_model->charReplaceJouhou($request->all());

      if(!isset($input['tyohyo_uk'])){
        $input['tyohyo_uk'] = 'off';
      }
      if(!isset($input['tyohyo_gj'])){
        $input['tyohyo_gj'] = 'off';
      }
      if(!isset($input['tyohyo_fax3'])){
        $input['tyohyo_fax3'] = 'off';
      }
      if(!isset($input['tyohyo_fax5'])){
        $input['tyohyo_fax5'] = 'off';
      }
      if(!isset($input['tyohyo_uf'])){
        $input['tyohyo_uf'] = 'off';
      }

      if(!isset($input['kakunin_mail'])){
        $input['kakunin_mail'] = 'off';
      }
      if(!isset($input['sokuho_mail'])){
        $input['sokuho_mail'] = 'off';
      }
      // セッションに入力データを保存する
      Session::put($this->_session_step3,$input);

      // ステータス１、２ OR 下部の「修理依頼内容の入力」ボタン をクリックする場合
      return Redirect::route($request->progressClick);

    } else if (route('iraimoto.goseikyusaki') == $referer) {
      // ご請求先情報画面の場合
      $values = $this->_model->charReplaceGoseikyusaki($request->all());
      Session::put($this->_session_step4,$values);

      // ステータス１、２ OR 下部の「ご依頼元情報の入力に戻る」ボタン をクリックする場合
      return Redirect::route($request->progressClick);
    }
  }

  /**
   * Clear all session
   */
  private function _clearSession()
  {
    Session::forget($this->_session_step1);
    Session::forget($this->_session_restep1);
    Session::forget($this->_session_step2);
    Session::forget($this->_session_restep2);
    Session::forget($this->_session_step3);
    Session::forget($this->_session_restep3);
    Session::forget($this->_session_step4);
    Session::forget($this->_session_restep4);
    Session::forget($this->_session_files);
    session()->forget('_old_input');
    session()->forget('brand');
    session()->forget('seihin_daibunrui');
    session()->forget('reachConfirmationFlag');
    }

  public function reset(Request $request)
  {
    $this->_clearSession();

    /*ファイル削除*/
    $src = Storage::disk('tmp_incoming')->url(Session::token());
    $this->_model->deleteTmpfiles($src);
    $this->_model->clearFilenameSession();
    return  redirect()->route('iraimoto.showKokyaku');
  }

  /**
  * Display kokyaku form page
  * お客様情報の入力を表示
  * @return \Illuminate\Http\Response
  */
  public function showKokyaku(Request $request)
  {
    //前画面を取得
    $referer = request()->headers->get('referer');
    $fromConfirmation = false;
    // 確認画面からもどるかをチェックする
    if( route('iraimoto.confirmation')== $referer){
      $fromConfirmation = true;
    }
    if(Session::get($this->_session_step1)){
      $step1 = Session::get($this->_session_restep1) ? Session::get($this->_session_restep1) : Session::get($this->_session_step1);
      foreach ($step1 as $key => $value) {
          Session::put("_old_input.$key", $value);
      }
    }
    if(Session::get($this->_session_restep1)){Session::forget($this->_session_restep1);}

    // 得意先コードが"5"で始まる時は「御社顧客番号 」を表示
    $tokuisaki_cd = Auth::user()->tokuisaki_cd;
    $tokuisaki_cd = isset($tokuisaki_cd[0])?$tokuisaki_cd[0]:null;
    $showKokyakuBangoFlag = null;
    if($tokuisaki_cd == '5'){
      $showKokyakuBangoFlag = true;
    } else { $showKokyakuBangoFlag = false; }
    return view('pages.iraimoto.kokyaku',compact('showKokyakuBangoFlag','fromConfirmation'));
  }
  /**
  * Get post data from kokyaku,validate and set session
  * お客様情報の入力データを取得、バリデーションして、セッションに設置
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function kokyaku(Request $request)
  {
    $rules = (isset($request->telFumeiFlag) && $request->telFumeiFlag == 'on') ? $this->_model->rules(true) : $this->_model->rules();
    // 文字（半角・全角）を変換
    $values = $this->_model->charReplaceKokyaku($request->all());
    // バリデーションを行い
    $validator = Validator::make($values,$rules,$this->_model->errorsMessage());
    // バリデーションが失敗場合
    if ($validator->fails()) {
      if(Session::get($this->_session_step1)){
        Session::put($this->_session_restep1,$values);
      }
      // 入力画面にエラーメッセージを持って戻る
      return redirect()->back()->withInput($values)->withErrors($validator);
    }
    // $values = $this->_model->charBackReplaceKokyaku($request->all());
    Session::put($this->_session_step1,$values);

    // 画面遷移
    if( isset($request->naiyou) ){
      //下部のボタン操作
      return Redirect::route($request->naiyou);
    }elseif( isset($request->progressClick) ){
      // ステータス操作
      return Redirect::route($request->progressClick);
    }
  }

  /**
  * Display naiyou page
  * 修理依頼内容の入力を表示
  * @return \Illuminate\Http\Response
  */
  public function showNaiyou(Request $request)
  {
    $referer = request()->headers->get('referer');
    if( !(route('iraimoto.kokyaku') == $referer || route('iraimoto.naiyou') == $referer || route('iraimoto.jouhou') == $referer
          || route('iraimoto.goseikyusaki') == $referer || route('iraimoto.confirmation') == $referer) ) {
      return  Redirect::route('iraimoto.showKokyaku');
    }

    // 確認画面からもどるかをチェックする
    $fromConfirmation = false;
    if( route('iraimoto.confirmation')== $referer){
      $fromConfirmation = true;
    }
    // check session of step 1 kokyaku
    // お客様情報の入力データのセッションをチェック
    if(!Session::get($this->_session_step1)){
      // redirect to step 1 of Naiyou
      // お客様情報の入力ページに遷移する
      return  Redirect::route('iraimoto.showKokyaku');
    }
    // session store step 2 of Naiyou
    if(Session::get($this->_session_step2)){
      $step2 = Session::get($this->_session_restep2) ? Session::get($this->_session_restep2) : Session::get($this->_session_step2);
      // dd($step2);
      foreach ($step2 as $key => $value) {
          Session::put("_old_input.$key", $value);
      }
    }
    // call naiyou view to show page
    // 修理依頼内容ページを呼び出し、表示する
    if(Session::get($this->_session_restep2)){Session::forget($this->_session_restep2);}
    
    if(Session::get('brand') == null || Session::get('brand') == ''){
    //init商品listLIXIL
       $seihin_daibunrui = \App\WebLxSyohinHinmei::getLists();
    }else{ 
       $seihin_daibunrui =  Session::get('seihin_daibunrui');
      }  
    $toritsukeYYYY = $this->_model->getToritsukeYYYY();
    $toritsukeMM = $this->_model->getToritsukeMM();

    $kiboushiteiYYYY = $this->_model->getkiboushiteiYYYY();
    $kiboushiteiMM = $this->_model->getKiboushiteiMM();
    $kiboushiteiDD = $this->_model->getKiboushiteiDD();
    $kiboushiteiHH = $this->_model->getKiboushiteiHH();
  
    //$wbSmtSeihinbetuCd = \App\WbSmtSeihinbetuCd::getLists(session()->getOldInput('hinmei_select'));
    $fileUpload = $this->_model->getFilenameFromSession();
    return view(
      'pages.iraimoto.naiyou',
      compact('fromConfirmation','seihin_daibunrui','toritsukeYYYY','toritsukeMM','kiboushiteiYYYY','kiboushiteiMM','kiboushiteiDD','kiboushiteiHH','fileUpload')
    );
  }

  public function clearFile(Request $request){
    // dd($step2);
    Session::put($this->_session_step2, $request->all());

    /*ファイル削除*/
    $src = Storage::disk('tmp_incoming')->url(Session::token());
    $this->_model->deleteTmpfiles($src);
    $this->_model->clearFilenameSession();
    return redirect()->back(); 
  }

  /**
  * Post page Request Content
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function naiyou(Request $request)
  { 
    $values = $this->_model->charReplaceNaiyou($request->all());
    //商品名エラーフラグ
    $sizeError = false;
    // ファイルエラーフラグ
    $fileError = false;
    // 同じなファイル名
    $filenameError = false;
    $fileArr = array();
    // 添付資料のあり場合
    if ($request->tempsiryoKbn == '1') {
      // 添付資料あるかどうかチェック
      if(isset($request->tempsiryoFile1) || isset($request->tempsiryoFile2) || isset($request->tempsiryoFile3) ||
          isset($request->tempsiryoFile4) || isset($request->tempsiryoFile5)){
          // 添付資料のサイズをチェック
          $fileSize = 0;
          // セッションファイルを取得
          $fileUpload = $this->_model->getFilenameFromSession();
          for ($i=1; $i <=5 ; $i++) {
              if (!$request->file("tempsiryoFile$i")){ continue; }
              $file = $request->file("tempsiryoFile$i");
              $filename = $file->getClientOriginalName();
              $fileArr[] = $filename;
              $fileSize += $file->getClientSize();    

              // セッションファイルをチェック
              foreach ($fileUpload as $key => $value) {              
                if ($i == $key) {
                  continue;
                }
                if ($filename == $value) {
                  $filenameError = true;
                  break;
                }
              }
              if ($filenameError) {
                break;
              }
          }
          // ファイル名をチェック
          foreach ($fileArr as $key => $value) {
            // アップロードファイルをチェック
            foreach ($fileArr as $key1 => $filename) {
              if ($key == $key1) {
                continue;
              }
              if ($value == $filename) {
                $filenameError = true;
                break;
              }
            }
            if ($filenameError) {
              break;
            }
          }
          if ($filenameError) {
            // 同じファイル名があった場合
            $fileError = "同一ファイル名は指定できません。";
          } else {
            // ファイルサイズ８MB
            $size = 8 * 1024 * 1024;
            // 添付資料のサイズが８MB超える場合、エラーメッセージ
            $fileError = ($fileSize > $size ) ? "合計で8MBを超えるファイルを添付することはできません。" : false;
          }
      } else {
        $files_session = $this->_model->getFilenameFromSession();
        $has = false;
        foreach ($files_session as $value) {
          if($value != ''){
            $has = true;
            break;
          }
        }
        if(!$has){
          // 添付資料フラグがありなのに、添付ファイルがない、エラーメッセージ
          $fileError = "添付資料を１件以上添付してください。";
        }
      }
    } else {
      $src = Storage::disk('tmp_incoming')->url(Session::token());
      $this->_model->deleteTmpfiles($src);
      $this->_model->clearFilenameSession();
    }
    // 添付資料のエラー場合、フラッシュのエラーメッセージ
    if($fileError){ Session::flash('fileError',$fileError); }
   // if($request->hinmei_select == $request->hinmei_directWrite){
    $validation_rule = $this->_model->naiyouRules(/**false**/);
  //  } else {
  //    $validation_rule = $this->_model->naiyouRules(true);
  //  }
    $hinmei_select = \App\Custom\CharReplace::hanKanaTozenKana($values['hinmei_select']);
    $merge =  strlen($hinmei_select) + strlen($values["hinmei_directWrite"]);
    if ($merge > 90) {
        $sizeError = "商品名は:30文字まで入力して下さい。";
    }
    if ($sizeError) {
        Session::flash('sizeError', $sizeError);
    }
    // 全体のバリデーション
    $validator = Validator::make($values, $validation_rule, $this->_model->naiyouErrorsMessage());
    if ($validator->fails()) {
        if (Session::get($this->_session_step2)) {
            Session::put($this->_session_restep2, $values);
        }
        return redirect()->back()->withInput()->withErrors($validator);
    } elseif ($fileError) {
        return redirect()->back()->withInput()->withErrors($validator);
    } elseif ($sizeError) {       
        return redirect()->back()->withInput()->withErrors($validator);
    }
    for ($i=1;$i<=5;$i++) {
        if (isset($values["tempsiryoFile$i"])) {
            unset($values["tempsiryoFile$i"]);
        }
    }
    Session::put($this->_session_step2, $values);
    for ($i=1; $i <=5 ; $i++) {
        if (!$request->file("tempsiryoFile$i")) {
            continue;
        }
        $file = $request->file("tempsiryoFile$i");
        $filename = $file->getClientOriginalName();
        $destinationPath = Storage::disk('tmp_incoming')->url(Session::token());
        $fileSize = round($file->getClientSize()/1024000, 1)."MB";
        $this->_model->deleteTmpExistFileBySession($i);
        $upload = $file->move($destinationPath, $filename);
        if ($upload) {
            $this->_model->setFilenameToSession($i, $filename, $fileSize);
        }else{
            Session::flash('fileError',"アップロードに失敗しました。ファイルを添付し直してください");
            return redirect()->back()->withInput()->withErrors($validator);
        }
    }
    // 画面遷移
    if (isset($request->jouhou)) {
        // 進むボタンをクリックする場合
        // 4.3 ご依頼元情報の入力 or 4.5 ご依頼内容の確認
        return Redirect::route($request->jouhou);
    }elseif (isset($request->progressClick)) {
        //ステータスをクリックする時
        // 4.3 ご依頼元情報の入力 or 4.5 ご依頼内容の確認
        return Redirect::route($request->progressClick);
    }


  }
  /**
  * show page Jouhou
  *
  * @return \Illuminate\Http\Response
  */
  public function showJouhou(Request $request)
  {
    $referer = request()->headers->get('referer');
    if(!(route('iraimoto.kokyaku') == $referer || route('iraimoto.naiyou') == $referer || route('iraimoto.jouhou') == $referer
        || route('iraimoto.goseikyusaki') == $referer || route('iraimoto.confirmation') == $referer) ) {
        return  Redirect::route('iraimoto.showKokyaku');
    }
    // check session of step 1 kokyaku and step 2 of naiyou
    // お客様情報の入力データのセッションをチェック
    if(!Session::get($this->_session_step1)){
        // redirect to step 1  of Kokyaku
        // お客様情報の入力ページに遷移する
        return redirect()->route('iraimoto.showKokyaku');
    }elseif(!Session::get($this->_session_step2)){
      // redirect to step 2 of Naiyou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showNaiyou');
    }
    // session store step 3 of Jouhou
    if(Session::get($this->_session_step3)){
      $step3 = Session::get($this->_session_restep3) ? Session::get($this->_session_restep3) : Session::get($this->_session_step3);
      // dd($step3);
      foreach ($step3 as $key => $value) {
          Session::put("_old_input.$key", $value);
      }
    }

    $goiraimoto_info = \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD();

    // call jouhou view to show page
    // 修理依頼内容ページを呼び出し、表示する
    if(Session::get($this->_session_restep3)){Session::forget($this->_session_restep3);}
    return view('pages.iraimoto.jouhou',compact('goiraimoto_info'));
 }
  /**
  * Post page Request information
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function jouhou(Request $request)
  {

    $input = $this->_model->charReplaceJouhou($request->all());

    if(!isset($input['tyohyo_uk'])){
      $input['tyohyo_uk'] = 'off';
    }
    if(!isset($input['tyohyo_gj'])){
      $input['tyohyo_gj'] = 'off';
    }
    if(!isset($input['tyohyo_fax3'])){
      $input['tyohyo_fax3'] = 'off';
    }
    if(!isset($input['tyohyo_fax5'])){
      $input['tyohyo_fax5'] = 'off';
    }
    if(!isset($input['tyohyo_uf'])){
      $input['tyohyo_uf'] = 'off';
    }

   if(!isset($input['kakunin_mail'])){
      $input['kakunin_mail'] = 'off';
    }
    if(!isset($input['sokuho_mail'])){
      $input['sokuho_mail'] = 'off';
    }
    // 4.4 ご依頼元情報の入力
    $validator = Validator::make($input,$this->_model->jouhouRules($input),$this->_model->jouhouErrorsMessage());
    if ($validator->fails()) {
      if(Session::get($this->_session_step3)){
        Session::put($this->_session_restep3,$input);
      }
      return redirect()->back()->withInput()->withErrors($validator);
    }
    Session::put($this->_session_step3,$input);
    if(isset($request->confirm)){
      return Redirect::route($request->confirm);
    }elseif( isset($request->progressClick) ){
      // ステータス２修理依頼内容の入力をクリックする場合
      return Redirect::route($request->progressClick);
    }elseif( $request->input("seikyuuKubun") =='2' ){
      // ステータスでご依頼内容の確認をクリックする時
      // 4.4 ご請求先の入力
      return Redirect::route('iraimoto.goseikyusaki');
    }else{
     // ステータスでご依頼内容の確認をクリックする時
     // 4.5 ご依頼内容の確認
     return Redirect::route('iraimoto.confirmation');
    }

  }
  /**
  * Display page get Request information addrees
  *
  * @return \Illuminate\Http\Response
  */
  public function showGoseikyusaki(Request $request)
  {
    $referer = request()->headers->get('referer');
    if(!(route('iraimoto.jouhou') == $referer || route('iraimoto.goseikyusaki') == $referer || route('iraimoto.confirmation') == $referer) ) {
       return  Redirect::route('iraimoto.showKokyaku');
    }
    // check session of step 1 kokyaku and step 2 of naiyou
    // お客様情報の入力データのセッションをチェック
    if(!Session::get($this->_session_step1)){
        // redirect to step 1  of Kokyaku
        // お客様情報の入力ページに遷移する
        return redirect()->route('iraimoto.showKokyaku');
    }elseif(!Session::get($this->_session_step2)){
      // redirect to step 2 of Naiyou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showNaiyou');
    }elseif(!Session::get($this->_session_step3) || Session::get($this->_session_step3)['seikyuuKubun'] != '2'){
      // redirect to step 3 of jouhou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showJouhou');
    }

    // session store step 4 of goseikyusaki
    if(Session::get($this->_session_step4)){
      $step3 = Session::get($this->_session_restep4) ? Session::get($this->_session_restep4) : Session::get($this->_session_step4);
      foreach ($step3 as $key => $value) {
          Session::put("_old_input.$key", $value);
      }
    }
    $sonotaShiharaibi = $this->_model->getSonotaShiharaibi();
    // call jouhou view to show page
    // 修理依頼内容ページを呼び出し、表示する
    if(Session::get($this->_session_restep4)){Session::forget($this->_session_restep4);}
    return view(
      'pages.iraimoto.goseikyusaki',
      compact('sonotaShiharaibi')
    );
  }
  /**
  * Post page Request information addrees
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function goseikyusaki(Request $request)
  {
    $values = $this->_model->charReplaceGoseikyusaki($request->all());
    $rules = $this->_model->goseikyusakiRules();
    // 4.5 ご依頼内容の確認
    $validator = Validator::make($values,$rules,$this->_model->goseikyusakiErrorsMessage());
    if ($validator->fails()) {
      if(Session::get($this->_session_step4)){
        Session::put($this->_session_restep4,$values);
      }
      return redirect()->back()->withInput($values)->withErrors($validator);
    }
    Session::put($this->_session_step4,$values);
    // 4.5 インターネット修理受付センター
    if( isset($request->progressClick) ){
      return Redirect::route($request->progressClick);
    }else{
      return Redirect::route('iraimoto.showConfirmation');
    } 
  }
  /**
  * Display page get Request Confirmation
  *
  * @return \Illuminate\Http\Response
  */
  public function showConfirmation()
  {
    $referer = request()->headers->get('referer');
    if (!(route('iraimoto.kokyaku') == $referer || route('iraimoto.naiyou') == $referer || route('iraimoto.jouhou') == $referer || route('iraimoto.goseikyusaki') == $referer || route('iraimoto.submitBack') == $referer)) {
      return  Redirect::route('iraimoto.showKokyaku');
    }

    // check session of step 1 kokyaku and step 2 of naiyou
    // お客様情報の入力データのセッションをチェック
    if(!Session::get($this->_session_step1)){
        // redirect to step 1  of Kokyaku
        // お客様情報の入力ページに遷移する
        return redirect()->route('iraimoto.showKokyaku');
    }elseif(!Session::get($this->_session_step2)){
      // redirect to step 2 of Naiyou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showNaiyou');
    }elseif (!Session::get($this->_session_step3)) {
      return redirect()->route('iraimoto.showJouhou');
    }
    if(Session::get($this->_session_step3)['seikyuuKubun'] == '2' && !Session::get($this->_session_step4)){
      return redirect()->route('iraimoto.showGoseikyusaki');
    }

    //reached the Confirmation page flag
    Session::put('reachConfirmationFlag','reachConfirmationFlag'); 

    $goiraimoto_info = \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD();
    $fileUpload = $this->_model->getFilenameFromSession();

    //ファイル存在チェック
    $fileError=false;
    for ($i=1; $i <=5 ; $i++) {
      if(!$this->_model->validTmpExistFileBySession($i)){
          $fileError = "ファイル添付に問題が発生しました。ファイルを添付し直してください";
          break;
      }
    } 
    if($fileError){ 
      Session::flash('fileError',$fileError); 
    }
    return view('pages.iraimoto.confirmation',compact('goiraimoto_info','fileUpload'));
  }
  /**
  * Display page get Request completed
  *
  * @return \Illuminate\Http\Response
  */
  public function showPrintpreview()
  {
    // check session of step 1 kokyaku and step 2 of naiyou
    // お客様情報の入力データのセッションをチェック
    if(!Session::get($this->_session_step1)){
        // redirect to step 1  of Kokyaku
        // お客様情報の入力ページに遷移する
        return redirect()->route('iraimoto.showKokyaku');
    }elseif(!Session::get($this->_session_step2)){
      // redirect to step 2 of Naiyou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showNaiyou');
    }elseif(!Session::get($this->_session_step3)){
      // redirect to step 2 of Naiyou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showJouhou');
    }

    $goiraimoto_info = \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD();
    $fileUpload = $this->_model->getFilenameFromSession();
    return view('pages.iraimoto.printpreview',compact('goiraimoto_info','fileUpload'));
  }

  /**
   * データ登録を行う関数
   */
  public function completation(Request $request)
  {
    $kokyaku = Session::get('kokyaku');
    $naiyou = Session::get('naiyou');
    $jouhou = Session::get('jouhou');
    $files_session = $this->_model->getFilenameFromSession();
    if(isset($jouhou['seikyuuKubun']) && $jouhou['seikyuuKubun'] == '2'){
        $goseikyusaki = Session::get('goseikyusaki');
    } else {
        $goseikyusaki = array();
    }
    if($kokyaku == null || $naiyou == null || $jouhou == null){
      abort(500);
    }
    $goiraimoto_info = \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD();//5190321
    $yubinBG = \App\YubinBango::searchByKenmei(isset($kokyaku['okyakusamaTodoufuken'])?$kokyaku['okyakusamaTodoufuken']:'');
    // brand_cdを判別
    $brand_cd = $this->_brand_cd($naiyou['brand'], $naiyou['hinmei_select']);

    //BRANDテーブルから、uc_cdとbrand_kbnを取得
    $branddata = $this->_branddata($brand_cd);

    $uc_cd = $branddata[0]->uc_cd;
    $brand_kbn = $branddata[0]->brand_kbn;

    // 受付センターの判別
    if($naiyou['brand'] == '２'|| $uc_cd == '0D'){
      //SW系の処理。ブランドコード一致の受付センターのメールアドレスに添付ファイルメールが送付される。
      $uketukecenter = \App\WebUketukeCenter::where('uc_cd','=',$uc_cd)->first();
    } else {
      //上記以外は、IX系の処理。都道府県を判断した受付センターのメールアドレスに添付ファイルメールが送付される。
      $uc_cd_code = $this->_model->find_uc_cd_by_todofuken($kokyaku['okyakusamaTodoufuken']);
      if(count($uc_cd_code) != 1){
        $uc_cd_code = $this->_model->find_uc_cd_by_yubinbago(str_replace('-','',$kokyaku['okyakusamaYuubinNo']));
        $uketukecenter = \App\WebUketukeCenter::where('uc_cd','=',$uc_cd_code[0]->uc_cd)->first();
        $uc_cd = $uketukecenter->uc_cd;
      } else {
          $uketukecenter = \App\WebUketukeCenter::where('uc_cd', '=', $uc_cd_code[0]->uc_cd)->first();
          $uc_cd = $uketukecenter->uc_cd;
      }
    }

    //WEB受付No.は、ブランドコードによって採番テーブルを変えて採番する。ブランドコードは、画面で選択したブランドのテーブルを参照して取得する。
    $web_uketuke_no = $this->_model->getWebUketukeNo($brand_kbn, $uc_cd);

    //修理対象
    //LIXILの場合
    if($naiyou['brand'] == '0') {
      $syuri_taisyo = '4';
    //INAXの場合
    } elseif ($naiyou['brand'] == '1') {
      $syuri_taisyo = '0';
    //トステムの場合
    } elseif ($naiyou['brand'] == '2') {
      $syuri_taisyo = '5';
    //SWの場合
    } elseif ($naiyou['brand'] == '3') {
      $syuri_taisyo = '1';

    }

    $kibo_sitei_bi = (isset($naiyou['kiboushiteiMM'])?$naiyou['kiboushiteiMM']:'' ) . (isset($naiyou['kiboushiteiDD'])?$naiyou['kiboushiteiDD']:'');

    $values = [
      'web_uketuke_no' => $web_uketuke_no,
      'uketuke_kbn' => '2',
      // Kokyaku
      'kokyaku_sei' => isset($kokyaku['okyakusamaSei'])?$kokyaku['okyakusamaSei']:'',
      'kokyaku_sei_kana' => isset($kokyaku['okyakusamaSeiKana'])?$this->_toHankana($kokyaku['okyakusamaSeiKana']):'',
      'kokyaku_na' => isset($kokyaku['okyakusamaMei'])?$kokyaku['okyakusamaMei']:'',
      'kokyaku_na_kana' => isset($kokyaku['okyakusamaMeiKana'])?$this->_toHankana($kokyaku['okyakusamaMeiKana']):'',
      'kokyaku_tel_haihun' => isset($kokyaku['okyakusamaTel'])?$kokyaku['okyakusamaTel']:'',
      'kokyaku_jusyo' => (isset($kokyaku['okyakusamaTodoufuken']) ? $kokyaku['okyakusamaTodoufuken'] : '') . (isset($kokyaku['okyakusamaAddress'])?$kokyaku['okyakusamaAddress']:''),
      'kokyaku_katagaki' => isset($kokyaku['okyakusamaMansionName'])?$kokyaku['okyakusamaMansionName']:'',
      'kokyaku_yubin' => isset($kokyaku['okyakusamaYuubinNo'])?str_replace('-','',$kokyaku['okyakusamaYuubinNo']):'',
     
      // Naiyou
      'hinmei' => (isset($naiyou['hinmei_select'])?$this->_toZenkaku($naiyou['hinmei_select']):'').(isset($naiyou['hinmei_directWrite'])?$this->_toZenkaku($naiyou['hinmei_directWrite']):''),
      'syohin_hinmoku' => '',
      'syohin_hinban' => isset($naiyou['hinban'])?$naiyou['hinban']:'',
      'torituke_nengetu' => (isset($naiyou['toritsukeYYYY'])?$naiyou['toritsukeYYYY']:'') . (isset($naiyou['toritsukeMM'])?$naiyou['toritsukeMM']:''),
      'irainaiyo' => isset($naiyou['irainaiyou_directWrite'])?$naiyou['irainaiyou_directWrite']:'',
      'koziten' => isset($naiyou['koujitenMei'])?$naiyou['koujitenMei']:'',

      // kokyaku
      'renrakusaki_mei' => isset($kokyaku['renrakusakiMei'])?$kokyaku['renrakusakiMei']:'',
      'renrakusaki_tel_haihun' => isset($kokyaku['renrakusakiTel'])?$kokyaku['renrakusakiTel']:'',
      'hm_kokyaku_no' => isset($kokyaku['kokyakuBango'])?str_replace('-','',$kokyaku['kokyakuBango']):'',

      // Naiyou
      'kibo_sitei_kbn' => ($kibo_sitei_bi == '') ? '0' : '1',
      'kibo_sitei_bi' => $kibo_sitei_bi,
      'kibo_sitei_bi_jikan' => isset($naiyou['kiboushiteiHH'])?$naiyou['kiboushiteiHH']:'',
      'message' => isset($naiyou['renrakujikou'])?$naiyou['renrakujikou']:'',

      // jouhou
      'iraimoto_cd' => Auth::user()->tokuisaki_cd,
      'iraimoto_simei' => $goiraimoto_info->tokuisaki_sei,
      'iraimoto_tanto' => isset($jouhou['goiraimotoTantou'])?$jouhou['goiraimotoTantou']:'',
      'iraimoto_jusyo' => $goiraimoto_info->tokuisaki_jusyo,
      'iraimoto_tel_haihun' => $goiraimoto_info->tokuisaki_tel_haihun,
      'iraimoto_fax' => $goiraimoto_info->torokusaki_fax,

      'fax_sosin_kbn' => '1',//isset($kokyaku[''])?$kokyaku['']:'',
      'fax_iraimoto_cd' => '',//isset($kokyaku[''])?$kokyaku['']:'',
      'fax_iraimoto_simei' => '',//isset($kokyaku[''])?$kokyaku['']:'',
      'fax_iraimoto_tanto' => '',//isset($kokyaku[''])?$kokyaku['']:'',
      'fax_tyumon_bango' => '',//isset($kokyaku[''])?$kokyaku['']:'',
      'fax_iraimoto_tel_haihun' => '',//isset($kokyaku[''])?$kokyaku['']:'',
      'fax_iraimoto_fax' => '',//isset($kokyaku[''])?$kokyaku['']:'',

      'tyohyo_uk' => isset($jouhou['tyohyo_uk'])?($jouhou['tyohyo_uk']=='on'?'1':'0'):'0',
      'tyohyo_gj' => isset($jouhou['tyohyo_gj'])?($jouhou['tyohyo_gj']=='on'?'1':'0'):'0',
      'tyohyo_fax3' => isset($jouhou['tyohyo_fax3'])?($jouhou['tyohyo_fax3']=='on'?'1':'0'):'0',
      'tyohyo_fax5' => isset($jouhou['tyohyo_fax5'])?($jouhou['tyohyo_fax5']=='on'?'1':'0'):'0',
      'tyohyo_uf' => isset($jouhou['tyohyo_uf'])?($jouhou['tyohyo_uf']=='on'?'1':'0'):'0',
      'yuryo_seikyu_kbn' => isset($jouhou['seikyuuKubun'])?$jouhou['seikyuuKubun']:'',

      // 不明
      'muryo_sekinin_kbn' => '',
      'muryo_s_syain_no' => '',

      // $goseikyusaki
      'seikyusaki_simei' => isset($goseikyusaki['sonotaShimei'])?$goseikyusaki['sonotaShimei']:'',
      'seikyusaki_simei_kana' => isset($goseikyusaki['sonotaShimeiKana'])?\App\Custom\CharReplace::zenKanaTohanKana($goseikyusaki['sonotaShimeiKana']):'',
      'seikyusaki_tel_haihun' => isset($goseikyusaki['sonotaTel'])?$goseikyusaki['sonotaTel']:'',
      'seikyusaki_fax' => isset($goseikyusaki['sonotaFax'])?$goseikyusaki['sonotaFax']:'',
      'seikyusaki_jusyo' => (isset($goseikyusaki['sonotaTodoufuken']) ? $goseikyusaki['sonotaTodoufuken'] : '') . (isset($goseikyusaki['sonotaAddress'])?$goseikyusaki['sonotaAddress']:''),
      'seikyusaki_yubin' => isset($goseikyusaki['sonotaYuubinNo'])?str_replace('-','',$goseikyusaki['sonotaYuubinNo']):'',
      'seikyusaki_katagaki' => isset($goseikyusaki['sonotaMansionName'])?$goseikyusaki['sonotaMansionName']:'',
      'seikyusaki_tanto' => (isset($goseikyusaki['sonotaTantousya']) && $jouhou['seikyuuKubun'] == '2')?$goseikyusaki['sonotaTantousya']:(isset($jouhou['goiraimotoTantou'])?$jouhou['goiraimotoTantou']:''),
      'siharaibi' => isset($goseikyusaki['sonotaShiharaibi'])?$goseikyusaki['sonotaShiharaibi']:'',

      'uc_cd' => $uc_cd,
      // 不明
      'iraimoto_bumon' => '',

      'tyumon_bango' => isset($jouhou['kisya_chuban'])?\App\Custom\CharReplace::numberZentoHan($jouhou['kisya_chuban']):'',

      'tempsiryo_umu_flg' => isset($naiyou['tempsiryoKbn'])?$naiyou['tempsiryoKbn']:'',
      'user_id' => Auth::id(),

      'syuri_taisyo' => $syuri_taisyo,
      'brand_cd' => $brand_cd

    ];

    // TEMPORARY SOLUTION: WEBREPAIR-492 
    //DB::beginTransaction();
    //try{

      // 入力されているデータを登録する
    $result = $this->_model->insertData($values);

    if($result){
      // 「受付確認」「経過・結果速報」付ける場合が INSERT実行 WEB受付メールやメール送信を行う
      if((isset($jouhou['kakunin_mail']) && $jouhou['kakunin_mail'] == 'on') || (isset($jouhou['sokuho_mail']) && $jouhou['sokuho_mail'] == 'on')){
        $web_uketuke_mail = [
          'web_uketuke_no' => $web_uketuke_no,
          'edaban' => '1',
          'user_id' => Auth::id(),
          'mail_address' => Auth::user()->mail_address,
          'sokuho_mail' => isset($jouhou['sokuho_mail'])?($jouhou['sokuho_mail']=='on'?'1':'0'):'0',
          'uketuke_mail' => isset($jouhou['kakunin_mail'])?($jouhou['kakunin_mail']=='on'?'1':'0'):'0'
        ];
        // web_uketuke_mailにデータ登録
        $insert_mail = \App\WebUketukeMail::insertData($web_uketuke_mail);
      }
      // 登録したデータを取得してメール送信を行う
      $data = Iraimoto::getUketukeByWebUketukeNo($web_uketuke_no);

      // Move upload file and clear session
      if(isset($naiyou['tempsiryoKbn']) && $naiyou['tempsiryoKbn'] == '1'){
        $src = Storage::disk('tmp_incoming')->url(Session::token());
        $dst = Storage::disk('incoming')->url($web_uketuke_no);
        $this->_model->recurseCopyDirAndFiles($src, $dst);
        // 添付資料のメール送信
        Mail::to($uketukecenter->mail_address)->send(new \App\Mail\UketukeFileMail([
                        'date' => date('Y年m月d日'),
                        'web_uketuke_no' => $web_uketuke_no,
                        'address' => $data->kokyaku_jusyo,
                        'name' => $data->kokyaku_sei . ' ' . $data->kokyaku_na,
                        'filename1' => isset($files_session[1])?$files_session[1]:'',
                        'filename2' => isset($files_session[2])?$files_session[2]:'',
                        'filename3' => isset($files_session[3])?$files_session[3]:'',
                        'filename4' => isset($files_session[4])?$files_session[4]:'',
                        'filename5' => isset($files_session[5])?$files_session[5]:''
          ]));
       Log::info('MailSentLog', [
                             '送信状態' => count(Mail::failures()) == 0 ? 'MailSentSuccessed' : 'MailSentFailed',
                             '受付NO' => $web_uketuke_no,
                             '顧客住所' => $data->kokyaku_jusyo,
                             '顧客姓名' => $data->kokyaku_sei.' '.$data->kokyaku_na,
                             '添付資料の名1 / サイズ1' => (isset($files_session[1]) ? $files_session[1] : '').' / '.(isset($_SESSION['fileSize1']) ? $_SESSION['fileSize1'] : ''),
                             '添付資料の名2 / サイズ2' => (isset($files_session[2]) ? $files_session[2] : '').' / '.(isset($_SESSION['fileSize2']) ? $_SESSION['fileSize2'] : ''),
                             '添付資料の名3 / サイズ3' => (isset($files_session[3]) ? $files_session[3] : '').' / '.(isset($_SESSION['fileSize3']) ? $_SESSION['fileSize3'] : ''),
                             '添付資料の名4 / サイズ4' => (isset($files_session[4]) ? $files_session[4] : '').' / '.(isset($_SESSION['fileSize4']) ? $_SESSION['fileSize4'] : ''),
                             '添付資料の名5 / サイズ5' => (isset($files_session[5]) ? $files_session[5] : '').' / '.(isset($_SESSION['fileSize5']) ? $_SESSION['fileSize5'] : ''),
                             ]);
        //remove files
        $this->_model->deleteTmpfiles($src);
      }
      // メール送信する
      if(isset(Auth::user()->mail_address) && Auth::user()->mail_address != ''){
        Mail::to(Auth::user()->mail_address)->send(new \App\Mail\UketukeMail([
          'date' => date('Y年m月d日'),
          'web_uketuke_no' => $web_uketuke_no,
          'address' => $data->kokyaku_jusyo,
          'name' => $data->kokyaku_sei . ' ' . $data->kokyaku_na
        ]));
      }
      // 入力されているデータのセッションを削除
      $this->_clearSession();
      $this->_model->clearFilenameSession();
      Session::flash('result',$web_uketuke_no);
    } else {
      Session::flash('result',false);
    }

    // TEMPORARY SOLUTION: WEBREPAIR-492 
    //  DB::commit();
    //}catch(\Exception $e){
    //  DB::rollback();
    //  throw $e;
    //}
    return view('pages.iraimoto.completation');

  }


  /**
  * Display page get Request completed
  *
  * @return \Illuminate\Http\Response
  */
  public function showCompletation()
  {
    // check session of step 1 kokyaku and step 2 of naiyou
    // お客様情報の入力データのセッションをチェック
    if(!Session::get($this->_session_step1)){
        // redirect to step 1  of Kokyaku
        // お客様情報の入力ページに遷移する
        return redirect()->route('iraimoto.showKokyaku');
    }elseif(!Session::get($this->_session_step2)){
      // redirect to step 2 of Naiyou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showNaiyou');
    }elseif(!Session::get($this->_session_step3)){
      // redirect to step 3 of jouhou
      // お客様情報の入力ページに遷移する
      return redirect()->route('iraimoto.showJouhou');
    }
    // 4.6 ご依頼内容の確認
    return view('pages.iraimoto.completation');
  }

    public function copyIraiData($webno, Request $request)
    {
      $this->_clearSession();
      $this->_model->clearFilenameSession();
      $webuketuke = DB::table('WEB_UKETUKE')
                ->leftJoin('IRAIUKETUKE', 'IRAIUKETUKE.uketuke_no', '=', 'WEB_UKETUKE.uketuke_no')
                ->select('WEB_UKETUKE.*', 'IRAIUKETUKE.kanryo_flg')
                ->where('WEB_UKETUKE.web_uketuke_no', '=', $webno)
                ->where('WEB_UKETUKE.iraimoto_cd', '=', Auth::user()->tokuisaki_cd)
                ->first();
      $mode = isset($request->mode) ? $request->mode : false;

      if ($webuketuke == null || count($webuketuke) == 0) {
        //uketuke_iraimotoを参照できる場合は表示する。
        $uketuke_iraimoto = DB::table('WEB_UKETUKE')
                    ->join('UKETUKE_IRAIMOTO', 'UKETUKE_IRAIMOTO.uketuke_no', '=', 'WEB_UKETUKE.uketuke_no')
                    ->select('WEB_UKETUKE.*')
                    ->where('WEB_UKETUKE.web_uketuke_no', '=', $webno)
                    ->where('UKETUKE_IRAIMOTO.iraimoto_cd', '=', Auth::user()->tokuisaki_cd)
                    ->first();

        //web_uketuke.iraimoto_cd, uketuke_iraimoto.iraimoto_cd一致が存在しない場合は、申し込みNo.が見つからないと返す。（パラメタを変えた不正アクセス防止）
        if ($uketuke_iraimoto == null || count($uketuke_iraimoto) == 0) {
            $webuketuke = false;
            return view('pages.webuketuke.kakunin', compact('webuketuke', 'mode'));
        } else {
            $webuketuke = DB::table('WEB_UKETUKE')
                    ->leftJoin('IRAIUKETUKE', 'IRAIUKETUKE.uketuke_no', '=', 'WEB_UKETUKE.uketuke_no')
                    ->select('WEB_UKETUKE.*', 'IRAIUKETUKE.kanryo_flg')
                    ->where('WEB_UKETUKE.web_uketuke_no', '=', $webno)
                    ->first();
        }
      }
      //kukyaku
      $values_kokyaku['okyakusamaSei'] = isset($webuketuke->kokyaku_sei) ? $webuketuke->kokyaku_sei : '';
      $values_kokyaku['okyakusamaMei'] = isset($webuketuke->kokyaku_na) ? $webuketuke->kokyaku_na : '';
      $values_kokyaku['okyakusamaSeiKana'] = isset($webuketuke->kokyaku_sei_kana) ? $webuketuke->kokyaku_sei_kana : '';
      $values_kokyaku['okyakusamaMeiKana'] = isset($webuketuke->kokyaku_na_kana) ? $webuketuke->kokyaku_na_kana : '';
      $values_kokyaku['okyakusamaTel'] = isset($webuketuke->kokyaku_tel_haihun) ? $webuketuke->kokyaku_tel_haihun : '';
      $values_kokyaku['okyakusamaYuubinNo'] = isset($webuketuke->kokyaku_yubin) ? substr($webuketuke->kokyaku_yubin, 0, 3).'-'.substr($webuketuke->kokyaku_yubin, 3, 4) : '';
      $ken = \App\YubinBango::find($webuketuke->kokyaku_yubin);
      $values_kokyaku['okyakusamaTodoufuken'] = isset($ken->ken_mei) ? $ken->ken_mei : '';
      $values_kokyaku['okyakusamaAddress'] = isset($webuketuke->kokyaku_jusyo) ? $webuketuke->kokyaku_jusyo : '';
      $values_kokyaku['okyakusamaMansionName'] = isset($webuketuke->kokyaku_katagaki) ? $webuketuke->kokyaku_katagaki : '';
      $values_kokyaku['kokyakuBango'] = isset($webuketuke->hm_kokyaku_no) ? $webuketuke->hm_kokyaku_no : '';
      $values_kokyaku['renrakusakiMei'] = isset($webuketuke->renrakusaki_mei) ? $webuketuke->renrakusaki_mei : '';
      $values_kokyaku['renrakusakiTel'] = isset($webuketuke->renrakusaki_tel_haihun) ? $webuketuke->renrakusaki_tel_haihun : '';
      session::put($this->_session_step1, $values_kokyaku);

      return Redirect::route('iraimoto.showKokyaku');
    }

  /**
   * 全角カタカナ -> 半角カタカナ
   */
  private function _toHankana($value)
  {
    return \App\Custom\CharReplace::zenKanaTohanKana($value);
  }

  private function _toZenkaku($value)
  {
    return \App\Custom\CharReplace::hanKanaTozenKana($value);
  }
  
  //ブランドコードを選択したブランド毎のテーブルから取得
  private function _brand_cd($brand_select, $hinmei_select)
  {
    //LIXILの場合
    if($brand_select == '0') {
      return \App\WebLxSyohinHinmei::getBrandCd($hinmei_select);

    //INAXの場合
    } elseif ($brand_select == '1') {
      return \App\WebIxSyohinHinmei::getBrandCd($hinmei_select);

    //トステムの場合
    } elseif ($brand_select == '2') {
      return \App\WebTsSyohinHinmei::getBrandCd($hinmei_select);

    //SWの場合
    } elseif ($brand_select == '3') {
      return \App\WebSwSyohinHinmei::getBrandCd($hinmei_select);
    }
    }

  //BRANDテーブルからデータを取得
  private function _branddata($brand_cd)
  {
    return \App\Brand::getData($brand_cd);
  }

    //ブランドコードより、商品を取得する
    private function _hinmei($brand_cd)
    {
        //LIXILの場合
        if ($brand_cd == '0') {
            return \App\WebLxSyohinHinmei::getLists();

        //INAXの場合
        } elseif ($brand_cd == '1') {
            return \App\WebIxSyohinHinmei::getLists();

        //トステムの場合
        } elseif ($brand_cd == '2') {
            return \App\WebTsSyohinHinmei::getLists();

        //SWの場合
        } elseif ($brand_cd == '3') {
            return \App\WebSwSyohinHinmei::getLists();
        }
    }
}
