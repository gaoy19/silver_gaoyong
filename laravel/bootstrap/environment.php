<?php
/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
*/

$env = $app->detectEnvironment(function(){
  if(strpos(gethostname(),'.local') || strpos($_SERVER['HTTP_HOST'],'.local') || $_SERVER['HTTP_HOST'] == "silver.dtc-lixil.com"){
    # local ロカル環境
    $env_file = '.env_local';
  } elseif ($_SERVER['HTTP_HOST'] == "repair.t.lixil.co.jp") {
    # staging repair.t.lixil.co.jp ステージング環境
    $env_file = '.env_stg';
  } elseif ($_SERVER['HTTP_HOST'] == "repair.lixil.co.jp") {
    # production 本環境
    $env_file = '.env_pro';
  }
  # get environment path
  $envPath = trim(__DIR__.'/../'.$env_file);
  # env file check
  if (file_exists($envPath)){
    # Get environment info
    $setEnv = trim(file_get_contents($envPath));
    # put environment info to APP_ENV
    putenv("APP_ENV=$setEnv");
    # check put environment and file exist
    if (getenv('APP_ENV') && file_exists(__DIR__.'/../'.$env_file)) {
      # load environment info
      $dotenv = new Dotenv\Dotenv(__DIR__.'/../', $env_file);
      $dotenv->load();
     }
  }
});

?>
