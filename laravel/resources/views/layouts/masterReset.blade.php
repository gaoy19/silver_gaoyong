<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<!-- * start section head * -->
@include('includes.head')
<!-- * end section head* -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrapper_silver">
  <!-- * start section header * -->
  <header id="header_top">
    <div class="box-header">
      <div class="row-sv">
          <div class="col-sv-6">
              <div class="logo-top">
                  <div class="logo_sv">
                      <a href="{{URL::to('/')}}"><img src="{{ asset('assets/images/lixil-logo.png') }}" srcset="{{ asset('assets/images/lixil-logo@2x.png') }} 2x,{{ asset('assets/images/lixil-logo@3x.png') }} 3x" alt="" /></a>
                  </div><!--/logo_sv-->
                  <div class="title_sv">LIXILインターネット修理受付センター</div>
              </div>
          </div><!--/col-sv-6-->
          <div class="col-sv-6">
              <div class="col-nav-right">
                  <ul class="col-nav-top">
                      <li class="nav_left">IMTweb @if(isset($user) || $user != null || count($user) > 0) {{ $user->user_simei }} @else <span>---<span> @endif  様</li>
                      <li>得意先コード：@if(isset($user) || $user != null || count($user) > 0) {{ $user->tokuisaki_cd }}  @else<span>---<span> @endif</li>
                  </ul>
              </div>
          </div><!--/col-sv-6-->
      </div><!--/row-sv-->
  </div><!--/box-header-->
  </header>
  <!-- * end section container  * -->
  <section class="container-silver">
    @yield('content')
  </section>
  <!-- * end section container  * -->
  <!-- * start section footer * -->
  <footer id="footer-silver">
    @include('includes.footer')
  </footer><!--/footer_sv-->
  <!-- * end section footer * -->
</div><!--/wrapper_silver-->
<!--canvas-lixil-menu-->
<div class="canvas-lixil-menu"></div>
<!--script.main.js-->
<script type="text/javascript" src="{{ asset('assets/js/script.main.js') }}"></script>
</body>
</html>
