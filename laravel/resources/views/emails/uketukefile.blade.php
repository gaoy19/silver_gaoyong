担当者御中

インターネット受付センターに添付付依頼が登録されました。

ご依頼日　：{{ $date }}
ご住所　　：{{ $address }}
お客様名　：{{ $name }}
web受付No：{{ $web_uketuke_no }}

添付ファイルを確認してください。

※添付ファイルの文字化け、破損等については依頼元に直接お問い合わせ下さい。

添付ファイル:　{{$filename1}}
@if(isset($filename2) && $filename2 != '')
　　　　　　　{{$filename2}}
@endif
@if(isset($filename3) && $filename3 != '')
　　　　　　　{{$filename3}}
@endif
@if(isset($filename4) && $filename4 != '')
　　　　　　　{{$filename4}}
@endif
@if(isset($filename5) && $filename5 != '')
　　　　　　　{{$filename5}}
@endif
