担当者御中

LIXILインターネット修理受付センターより、次の内容のトップページ自動ログイン登録申請がありました。
変更を実施ください。

登録日時：{{ $date }}

ユーザーID：{{ $user_id }}
AlphasId：{{ $alphas_id}}

貴社名：{{ $tokuisaki_sei }}
ご担当者名：{{ $user_simei }}

連絡先メールアドレス：{{ $info_mail_address }}
連絡先電話番号：{{ $tokuisaki_tel_haihun }}
