<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebAlphasLogin extends Model
{
  protected $table = 'WEB_ALPHAS_LOGIN';
  protected $primaryKey = 'ALPHAS_ID';

}
