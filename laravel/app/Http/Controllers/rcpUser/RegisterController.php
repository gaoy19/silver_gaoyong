<?php

namespace App\Http\Controllers\rcpUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\User;
use App\Tokuisaki;
use App\Iraimoto;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use Mail;
use App\RequestData;
use Session;

class RegisterController extends Controller
{

  public function __construct(User $user)
  {
    $this->_user = $user;
  }
  /**
  * Display 利用者登録
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function showRegister(Request $request)
  {
    //check logo
    if(Auth::check()){
      return Redirect::route('login');
    }
    $data = Session::get('data');
    if(Session::has('data') || $data!=null){
      foreach ($data as $key => $value) {
          Session::put("_old_input.$key", $value);
      }
    }
    //view page 利用者登録
    return view('users.register.register');
  }
  /**
  * Get post register
  * 登録内容の確認
  * @param $request
  * @return \Illuminate\Http\Response
  */
  private $user;
  public function confirmForm(Request $request)
  {
    $values = $this->_user->charReplaceRegister($request->all());
    $rules  = $this->_user->userRules();
    $validator = \Validator::make($values,$rules,$this->_user->userErrorsMessage());
    if($request->joho_flg =='1') {
      $request->session()->forget('_old_input');
      $request->session()->flush();
      return redirect()->back()->withInput($values)->withErrors($validator)
                      ->with('global','弊社での個人情報の取り扱い方法に同意頂けない場合、サービスをご利用いただくことはできません。');
    }
    if($validator->fails() || $request->joho_flg !='0') {
      session(['data'=>$values]);
      return redirect()->back()->withInput($values)->withErrors($validator);
    }
    session(['data'=>$values]);
    return view('users.register.confirm')->with('user', $values);
  }

  /**
  * Display 登録内容の確認
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function register(Request $request)
  {
    //check logo
    if(Auth::check()){
      return Redirect::route('login');
    }

    $values = $request->all();
    $rules  = $this->_user->userRules();
    $validator = \Validator::make($values,$rules,$this->_user->userErrorsMessage());
    if ($validator->fails()) {
      return redirect()->back()->withInput($values)->withErrors($validator);
    }
        do {
            $random_user_id = str_random(9);
            $existence_id = \App\User::getUserId($random_user_id);
        } while (current($existence_id) != []);

        $values = [
            'user_id' => $random_user_id,
            'user_simei' => $request->user_simei,
            'info_mail_address' => $request->info_mail_address,
            'mail_address' => $request->mail_address,
            'passwd' => $request->passwd,
            'password' => bcrypt($request->passwd),
        ];

        $result = \App\User::insertData($values);
    if ($result != 0) {
      $mail_env = \Config::get('mail.from.address');
      date_default_timezone_set("Asia/Tokyo");
      $date_jp = date('Y/m/d H:i');
      Mail::to($mail_env)->send(new \App\Mail\RegisterMail([
        'date'=> $date_jp,
        'tokuisaki_sei'=> $request->tokuisaki_sei,
        'tokuisaki_sei_kana'=> $request->tokuisaki_sei_kana,
        'tokuisaki_yubin'=> $request->tokuisakiYuubinNo,
        'tokuisakiTodoufuken'=> $request->tokuisakiTodoufuken,
        'tokuisakiAddress'=> $request->tokuisakiAddress,
        'user_simei'=> $request->user_simei,
        'tokuisaki_tel_haihun'=> $request->tokuisaki_tel_haihun,
        'torokusaki_fax'=> $request->torokusaki_fax,
        'info_mail_address'=> $request->info_mail_address,
        'mail_address'=> $request->mail_address,
        'password'=> $request->passwd,
        'password_encrypt' => bcrypt($request->passwd),
      ]));
    }
    //view page 登録内容の確認
    $request->session()->forget('_old_input');
    $request->session()->flush();
    return view('users.register.complete');
  }
}
