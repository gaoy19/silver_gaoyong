@extends('layouts.master')

@section('title','インターネット修理受付センター | 利用者登録[1登録内容の入力]')

@section('content')
<div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12" style="margin-top: 12px;">
        <div class="box-title_sv">
            <h1>利用者登録</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">登録内容の入力</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">登録内容のご確認</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4">
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">確認メールの受信</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4">
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">登録受付完了</div>
                </div>
            </li><!--/box-sv-4-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
    <div class="main-vs-12 sv_top20">
        <form id="web_customer_info" name="form_register" method="POST" action="{{ route('user.register.confirm') }}" accept-charset="UTF-8" role="form" autocomplete="off">
            {{ csrf_field() }}
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>利用者情報の入力</h5>
                </div>

                <div class="con-info_sv">
                    <div class="con-info-detail">貴社情報をご入力ください</div>
                        <div class="tb-form_sv">
                            <!--貴社名-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">貴社名</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('tokuisaki_sei'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('tokuisaki_sei') !!}</span></div>
                                          @endif
                                          <input type="text" maxlength="40" value="{{ old('tokuisaki_sei') }}" class="form-control_sv ime-active{!! $errors->has('tokuisaki_sei') ? ' input-error' : '' !!}" id="tokuisaki_sei" name="tokuisaki_sei" style="max-width:500px;">
                                        </div>
                                        <div class="label-info-12">
                                            <div class="label-info">例：株式会社ＬＩＸＩＬ</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div>
                            <!--貴社名（フリガナ）-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">貴社名（フリガナ）</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('tokuisaki_sei_kana'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('tokuisaki_sei_kana') !!}</span></div>
                                          @endif
                                          <input type="text" maxlength="40" value="{{ old('tokuisaki_sei_kana') }}" class="form-control_sv ime-active{!! $errors->has('tokuisaki_sei_kana') ? ' input-error' : '' !!}" id="tokuisaki_sei_kana" name="tokuisaki_sei_kana" style="max-width:500px;">
                                        </div>
                                        <div class="label-info-12">
                                            <div class="label-info">例：カブシキガイシャリクシル</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div>
                            <!--郵便番号-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">郵便番号</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                  <div class="col-ft-right">
                                      @if ($errors->has('tokuisakiYuubinNo'))
                                          <div class="error_sv max-100"><span>{!! $errors->first('tokuisakiYuubinNo') !!}</span></div>
                                      @endif
                                      <div class="col-td-5">
                                          <input type="text" value="{{ old('tokuisakiYuubinNo') }}" maxlength="8" class="form-control_sv ime-disabled{!! $errors->has('tokuisakiYuubinNo') ? ' input-error' : '' !!}" id="customer_zip_code"  name="tokuisakiYuubinNo" style="max-width:200px;" onkeypress="return isNumberKey(event);">
                                      </div>
                                      <div class="col-td-7">
                                           <input type="button" class="btn-control_sv" id="add_zip_code"  style="max-width:200px;" value="住所検索" onclick="showAddressData()"/>
                                      </div>
                                      <div class="label-info-12">
                                          <div class="label-info">例：100-1111</div>
                                      </div>
                                  </div>
                                </div><!--/td-form_sv-8-->
                            </div>
                            <!--都道府県-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">都道府県</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                      <div class="col-td-12">
                                        @if ($errors->has('tokuisakiTodoufuken'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('tokuisakiTodoufuken') !!}</span></div>
                                        @endif
                                          <select class="form-control_sv{!! $errors->has('tokuisakiTodoufuken') ? ' input-error' : '' !!}" name="tokuisakiTodoufuken" size="1" style="max-width:180px;">
                                              <option value="">選択してください</option>
                                              @foreach (Cache::get('todoufukens') as $todoufuken)
                                                <option value="{{ $todoufuken }}" {{ ( old('tokuisakiTodoufuken') == $todoufuken ) ? ' selected' : '' }}>{{ $todoufuken }}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div>
                            <!--ご住所-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">ご住所</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                  <div class="col-ft-right">
                                      <div class="col-td-12">
                                          @if ($errors->has('tokuisakiAddress'))
                                              <div class="error_sv max-100"><span>{!! $errors->first('tokuisakiAddress') !!}</span></div>
                                          @endif
                                          <input type="text" id="customer_address"  name="tokuisakiAddress" value="{{ old('tokuisakiAddress') }}" maxlength="45" class="form-control_sv ime-active{!! $errors->has('tokuisakiAddress') ? ' input-error' : '' !!}" style="max-width:500px;">
                                      </div>
                                  </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">ご担当者名</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('user_simei'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('user_simei') !!}</span></div>
                                          @endif
                                          <input type="text" maxlength="40" id="user_simei" name="user_simei" value="{{ old('user_simei') }}" class="form-control_sv ime-active{!! $errors->has('user_simei') ? ' input-error' : '' !!}" style="max-width:300px;">
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--お電話番号-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">お電話番号</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('tokuisaki_tel_haihun'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('tokuisaki_tel_haihun') !!}</span></div>
                                          @endif
                                          <input type="text"  id="tokuisaki_tel_haihun"  maxlength="13" name="tokuisaki_tel_haihun" value="{{ old('tokuisaki_tel_haihun') }}" class="form-control_sv ime-disabled{!! $errors->has('tokuisaki_tel_haihun') ? ' input-error' : '' !!}" style="max-width:300px;" onkeypress="return isNumberKey(event);">
                                        </div>
                                        <div class="label-info-12">
                                            <div class="label-info">例：03-1234-5678</div>
                                        </div>
                                        <div class="des-info">携帯以外の勤務先電話番号を記入ください</div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--FAX 番号-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">FAX 番号</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('torokusaki_fax'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('torokusaki_fax') !!}</span></div>
                                          @endif
                                          <input type="text" id="torokusaki_fax"  maxlength="13" name="torokusaki_fax" value="{{ old('torokusaki_fax') }}" class="form-control_sv ime-disabled{!! $errors->has('torokusaki_fax') ? ' input-error' : '' !!}" style="max-width:300px;" onkeypress="return isNumberKey(event);">
                                        </div>
                                        <div class="label-info-12">
                                            <div class="label-info">例：03-1234-7890</div>
                                        </div>
                                        <div class="des-info">修理状況を FAX にてお送りします</div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--メールアドレス 1-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">連絡先メールアドレス</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('info_mail_address'))
                                          <div class="error_sv max-100"><span>{!! $errors->first('info_mail_address') !!}</span></div>
                                          @endif
                                          <input type="text" autocomplete="off" maxlength="100" value="{{ old('info_mail_address') }}" class="form-control_sv ime-disabled{!! $errors->has('info_mail_address') ? ' input-error' : '' !!}" id="info_mail_address" name="info_mail_address" style="max-width:500px;">
                                        </div>
                                        <div class="label-info-12 col-top10">
                                            <div class="des-info">ユーザーID連絡用のメールアドレスを記入してください</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--メールアドレス 2-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">修理進捗状況</br>送信用メールアドレス</div>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('mail_address'))
                                          <div class="error_sv max-100"><span>{!! $errors->first('mail_address') !!}</span></div>
                                          @endif
                                          <input type="text" maxlength="100" autocomplete="off" value="{{ old('mail_address') }}" class="form-control_sv ime-disabled{!! $errors->has('mail_address') ? ' input-error' : '' !!}" id="mail_address" name="mail_address" style="max-width:500px;">
                                        </div>
                                        <div class="label-info-12 col-top10">
                                            <div class="des-info">受付確認票、結果・経過速報を受信するメールアドレスを記入してください</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--パスワード-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">パスワード</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('passwd'))
                                          	<div class="error_sv max-100"><span>{!! $errors->first('passwd') !!}</span></div>
                                          @endif
                                          <input type="password" autocomplete="off" maxlength="20" class="form-control_sv{!! $errors->has('passwd') ? ' input-error' : '' !!}" id="password" name="passwd" style="max-width:200px;">
                                        </div>
                                        <div class="label-info-12 col-top10">
                                            <div class="des-info">半角数字と半角英字を組み合わせ、4〜20文字で指定してください</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--パスワード-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">パスワード（再入力）</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('password_confirmation'))
                                           <div class="error_sv max-100"><span>{!! $errors->first('password_confirmation') !!}</span></div>
                                          @endif
                                          <input type="password" autocomplete="off" maxlength="20" class="form-control_sv{!! $errors->has('password_confirmation') ? ' input-error' : '' !!}" id="password_confirmation" name="password_confirmation" style="max-width:200px;">
                                        </div>
                                        <div class="label-info-12 col-top10">
                                            <div class="des-info">パスワードを再度入力してください</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                            <!--個人情報の取り扱いについて-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4 tb-fonm-none">
                                    <div class="col-ft-left">
                                        <div class="td-title">個人情報の取り扱いについて</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8 tb-fonm-none">
                                    <div class="col-ft-right">
                                        <div class="col-td-12" style="margin-top: -10px;">
                                            <div class="group_radio">
                                                <input type="radio" id="agree_info" value="0" name="joho_flg" {{ old('joho_flg')=="0" ? 'checked' : '' }} >
                                                <label for="agree_info">同意する</label>
                                                <span class="text_offest" style="margin-left:40px;">
                                                  <a class="searchNumber" target="_blank" href="http://www.lixil.co.jp/privacy/" target="_blank">プライバシーポリシー</a>
                                                </span>
                                            </div>
                                            <div class="group_radio">
                                                <input type="radio" id="disagree_info" value="1" name="joho_flg" {{ (old('joho_flg') == "1" || !Session::has('data')) ? 'checked=' : '' }}>
                                                <label for="disagree_info">同意しない</label>
                                            </div>
                                        </div>
                                        @if(session('global'))
                                          <div class="error_sv" style="margin-top: 7px;"><span>{{session('global')}}</span></div>
                                        @endif
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div>
                        </div><!--/tb-info_sv-->
                   </div><!--/con-group-sv-->
            </div><!--/box-main-form-->
            <div class="btn-center-customer">
               <div class="checkbox-18 sv_center">
                    <input type="submit" class="btn-confirm" id="confirm_registration" value="登録内容を確認する" {{ (old('joho_flg')=="1" || !Session::has('data')) ? 'disabled' : '' }}>
               </div>
            </div>
        </form><!--/form-horizontal_sv-->
    </div>
</div><!--/row-sv-->
<script type="text/javascript">
  $(document).ready(function() {
    $("input[name=tokuisakiYuubinNo]").change(function (e) {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      })
      e.preventDefault();
      var formData = {
          okyakusamaYuubinNo: $('#customer_zip_code').val()
      }
      $.ajax({
          type: 'POST',
          url: '{{ route('json.yubinbango') }}',
          data: formData,
          dataType: 'json',
          success: function (data) {
              if(data){
                $('select[name=tokuisakiTodoufuken]').val(data.ken_mei);
                $('input[name=tokuisakiAddress]').val(data.sityoson_mei);
              }
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
    });
  });
  // 住所検索ダイアログ表示
  function showAddressData() {
    if($('input[name=tokuisakiYuubinNo]').val().length == 0 &&
      $('select[name=tokuisakiTodoufuken]').val().length == 0 &&
      $('input[name=tokuisakiAddress]').val().length == 0){
        alert('郵便番号、都道府県、ご住所のいずれかを入力してください。');
        return;
      }
    // 住所検索フォーム
    window.open("{{route('yubinbango.search')}}?windowName=childWin&actionID=search2"
              +"&ken_mei_val=" + $('select[name=tokuisakiTodoufuken]').val()
              +"&yubinBango_val=" + $('input[name=tokuisakiYuubinNo]').val()
              +"&sityoson_mei=" + $('input[name=tokuisakiAddress]').val()
              +"&form=form_register"
              +"&_token=" + $('meta[name="_token"]').attr('content'),
              "addressSearchForm",
              "top=100,left=80,height=650,width=850,status=no,scrollbars=yes,toolbar=no,menubar=no");
  }
  //check policy
  $(function () {
    $("input[name=joho_flg]:radio").change(function (event) {
        if($(this).val() == '0'){
          $('input[type="submit"]').removeAttr('disabled');
        } else {
          $('input[type="submit"]').attr('disabled','disabled');
        }
    });
  });
  //end check
  //validate key number
  function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 45 && charCode > 31
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
  }
</script>
@stop
