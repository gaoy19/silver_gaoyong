<?php

namespace App\Http\Controllers\UserInformation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Redirect;
use App\User;
use Mail;
use App\Mail\UpdatePasswordCompleted;
use Hash;

class PasswordController extends Controller
{
  private $_model;

  public function __construct(User $model)
  {
    $this->_model = $model;
  }
  //View: 9-5_インターネット修理受付センター_パスワードの変更
  public function changePassword()
  {
    return view('users.informations.changePassword');
  }
  public function updatePassword(Request $request){

    $validator = Validator::make(
      $request->all(),
      $this->_model->changePasswordRules(),
      $this->_model->changePasswordErrorMessage()
    );

    $userId = User::find(auth()->user()->user_id);

    if ($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    } elseif (!Hash::check($request->input('current_password'), $userId->password)) {
      if ($userId->passwd !== $request->input('current_password')) {
        return redirect()->back()->with('current_password', $this->_model->currentPasswordMsgNotExists());
      }
    }

    $userId->passwd   = $request->input('new_password');
    $userId->password = bcrypt($userId->passwd);
    $userId->update();

    Auth::loginUsingId(auth()->user()->user_id, true);

    Mail::to($userId->info_mail_address)->send(new UpdatePasswordCompleted(['mail_from_address' => \Config::get('mail.from.address')]));

    return view('users.informations.changePasswordCompleted');

  }
}
