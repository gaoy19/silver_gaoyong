/*
* Menu responsive
*/
$('button.open-canvas.navbar-toggle').click( function(e) {
    e.preventDefault();
    $('body').toggleClass('canvas-size');
    $('#navbarCollapse_sv').toggleClass('active-canvas');
    $('#navbarCollapse_sv').toggleClass('canvas-left');
    $('.navbar-header_sv').toggleClass('btncanvas-top');
    $('#btn-bar').toggleClass('icon-button icon-button-up');
});