@extends('layouts.master')
@section('title','LIXILインターネット修理受付センター | 登録情報の変更 | 利用者情報の変更[2変更内容のご確認]')
@section('content')
<!--  * start section container  * -->
<section class="container-silver">
    <div class="row-sv">
          <!-- * start row top * -->
          <div class="main-vs-12" style="margin-top: 12px;">
              <div class="box-title_sv">
                  <h1>利用者情報の変更</h1>
              </div>
          </div>
    </div><!--/row-sv-->
    <div class="row-sv">
        <div class="col-sv-12 box-progress-12">
            <ul class="row-box-bar">
                <li class="box-sv-5 active_sv">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">1</div>
                        <div class="box-text-bar">利用者情報の変更</div>
                    </div>
                </li><!--/box-sv-4-->
                <li class="box-sv-5 active_sv">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">2</div>
                        <div class="box-text-bar">変更内容のご確認</div>
                    </div>
                </li><!--/box-sv-4-->
                <li class="box-sv-5">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">3</div>
                        <div class="box-text-bar">変更完了</div>
                    </div>
                </li><!--/box-sv-4-->
            </ul><!--/box-bar-->
        </div><!--/box-progress-12-->
    </div><!--/row-sv-->
    <div class="row-sv">
        <div class="main-vs-12 sv_top20">
            <form id="web_customer_info" method="POST" action="{{ route('user.change.information.completed') }}">
                {{ csrf_field() }}
                <div class="box-main-form">
                    <div class="modal-header_sv">
                        <h5>利用者内容のご確認</h5>
                    </div>
                    <div class="con-info_sv" style="padding-top: 2px;">
                        <div class="con-info-detail">登録内容をご確認ください。</div>
                        <div class="tb-responsive" style="margin-top: 0px;">
                            <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                                <tbody class="lixil_body">
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>貴社名</label></div>
                                        </th>
                                        <td class="col-data">
                                            <div class="col-result">{{ $tokuisaki_sei }}</div>
                                            <input type="hidden" value="{{ $tokuisaki_sei }}" name="tokuisaki_sei"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>貴社名（フリガナ）</label></div>
                                        </th>
                                        <td class="col-data">
                                            <div class="col-result">{{ $tokuisaki_sei_kana }}</div>
                                            <input type="hidden" value="{{ $tokuisaki_sei_kana }}" name="tokuisaki_sei_kana"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>郵便番号</label></div>
                                        </th>
                                        <td class="col-data">
                                          <div class="col-result">{{ $tokuisaki_yubin }}</div>
                                          <input type="hidden" value="{{ $tokuisaki_yubin }}" name="tokuisaki_yubin"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>都道府県</label></div>
                                        </th>
                                        <td class="col-data">
                                            <div class="col-result">{{ $tokuisaki_jusyo }}</div>
                                            <input type="hidden" value="{{ $tokuisaki_jusyo }}" name="tokuisaki_jusyo"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>ご住所</label></div>
                                        </th>
                                        <td class="col-data">
                                            <div class="col-result">{{ $tokuisaki_jusyo_address }}</div>
                                            <input type="hidden" value="{{ $tokuisaki_jusyo_address }}" name="tokuisaki_jusyo_address"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>ご担当者名</label></div>
                                        </th>
                                        <td class="col-data">
                                          <div class="col-result">{{ $user_simei }}</div>
                                          <input type="hidden" value="{{ $user_simei }}" name="user_simei"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>お電話番号</label></div>
                                        </th>
                                        <td class="col-data">
                                          <div class="col-result">{{ $tokuisaki_tel_haihun }}</div>
                                          <input type="hidden" value="{{ $tokuisaki_tel_haihun }}" name="tokuisaki_tel_haihun"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>FAX 番号</label></div>
                                        </th>
                                        <td class="col-data">
                                          <div class="col-result">{{ $torokusaki_fax }}</div>
                                          <input type="hidden" value="{{ $torokusaki_fax }}" name="torokusaki_fax"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>連絡用メールアドレス</label></div>
                                        </th>
                                        <td class="col-data">
                                          <div class="col-result">{{ $info_mail_address }}</div>
                                          <input type="hidden" value="{{ $info_mail_address }}" name="info_mail_address"/>
                                        </td>
                                    </tr><!--/result-->
                                    <tr>
                                        <th class="labelCol">
                                            <div class="col-label"><label>修理進捗状況送信用メールアドレス</label></div>
                                        </th>
                                        <td class="col-data">
                                          <div class="col-result">{{ $mail_address }}</div>
                                          <input type="hidden" value="{{ $mail_address }}" name="mail_address"/>
                                        </td>
                                    </tr><!--/result-->
                                </tbody>
                           </table>
                        </div><!--tb-responsive-->
                    </div><!--con-info_sv-->
                </div><!--/box-main-form-->
                <div class="btn-center-customer">
                   <div class="sv_center" style="padding-bottom: 12px;">
                        <input type="submit" class="btn-confirm" id="membership_registration" value="この内容で情報を変更する">
                   </div>
                </div>
            </form><!--/form-horizontal_sv-->
            <form id="web_customer_info" action="{{ route('user.change.information') }}">
              {{ csrf_field() }}
              <input type="hidden" value="{{ $tokuisaki_sei }}" name="tokuisaki_sei"/>
              <input type="hidden" value="{{ $tokuisaki_sei_kana }}" name="tokuisaki_sei_kana"/>
              <input type="hidden" value="{{ $tokuisaki_yubin }}" name="tokuisaki_yubin"/>
              <input type="hidden" value="{{ $tokuisaki_jusyo }}" name="tokuisaki_jusyo"/>
              <input type="hidden" value="{{ $tokuisaki_jusyo_address }}" name="tokuisaki_jusyo_address"/>
              <input type="hidden" value="{{ $user_simei }}" name="user_simei"/>
              <input type="hidden" value="{{ $tokuisaki_tel_haihun }}" name="tokuisaki_tel_haihun"/>
              <input type="hidden" value="{{ $torokusaki_fax }}" name="torokusaki_fax"/>
              <input type="hidden" value="{{ $info_mail_address }}" name="info_mail_address"/>
              <input type="hidden" value="{{ $mail_address }}" name="mail_address"/>
              <div class="btn-center-customer" style="margin-top: 10px;">
                 <div class="sv_center" style="padding-bottom: 12px;">
                   <input type="submit" class="btn-confirm-small" id="to_correct" value="前に戻る"/>
                 </div>
              </div>
            </form>
        </div>
    </div><!--/row-sv-->
</section><!--/container-silver-->
<!--  * end section container  * -->
@stop
