@extends('layouts.master')
@section('repair_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理の状況[修理依頼内容の確認]')
@section('content')
  <div class="row-sv">
   <div class="main-vs-12 sv_top20" style="margin-bottom:70px;">
      <div class="col-rs-title border-bot">
        <div class="col-sv-7">
           <h2>修理依頼内容の確認</h2>
        </div>
        <div class="col-sv-5">
            <div class="col-print-5_rs row-15">
                <button class="btn-print" type="button" value="報告書を印刷する" onclick="showPrintForm()"><span class="icon-print"><img src="/assets/images/print.png" width="18"></span>依頼内容を印刷する</button>
            </div>
        </div>
      </div><!--/border-bot-->
      <!-- * start section 1 ステータス * -->
      <div class="box-main-form">
       <div class="box-data-12">
                <div class="tb-responsive_rs">
                   <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                       <tbody class="lixil_body">
                           <tr>
                               <td class="Col-data-2 color-blue">
                                   <div class="col-label-rs"><label>ステータス</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs"><a href="#">完了</a></div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>お申し込み NO</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">WB12032628038</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>登録日</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">2017/6/1</div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>お客さま名</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">伊藤邸</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>お客さま住所</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">北海道札幌市</div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>商品品番</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">BF-HT856TLX-PU/FW1</div>
                               </td>
                           </tr><!--/result-->
                       </tbody>
                    </table>
               </div><!--/tb-responsive-->
           </div><!--/box-data-12-->
      </div><!--/box-main-form1-->
      <!-- * end section 1 ステータス * -->
      <!-- * start section 2 お客さま情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>お客さま情報</h5>
           </div>
         </div>
        <div class="con-info_sv">
           <div class="tb-responsive">
               <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                   <tbody class="lixil_body">
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さま氏名</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さま氏名（フリガナ）</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さま電話番号</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さま郵便番号</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さま都道府県</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さまご住所</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>お客さま建物名・部屋番号</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>貴社顧客番号</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>連絡先名</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>連絡先電話番号</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>製品ブンド</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                   </tbody>
              </table>
        </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_2-->
      <!-- * end section 2 お客さま情報 * -->
      <!-- * start section 3 修理依頼内容 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>修理依頼内容</h5>
           </div>
        </div>
        <div class="con-info_sv">
          <div class="tb-responsive">
           <table class="detailList" border="0" cellpadding="0" cellspacing="0">
               <tbody class="lixil_body">
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>商品名</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>品番</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>取り付け年月</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>工事店名</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>ご依頼内容</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>希望日</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>ご要望または連絡事項</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>添付資料</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>連絡先電話番号</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>製品ブランド</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
               </tbody>
          </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_3--><!--/box-main-form-2-->
      <!-- * end section 3 修理依頼内容 * -->
      <!-- * start section 4 ご依頼元情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>ご依頼元情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
             <div class="tb-responsive">
               <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                   <tbody class="lixil_body">
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>得意先コード</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>貴社名</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>ご依頼元担当者様</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>部署住所</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>部署電話番号</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>部署FAX</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>ご要望または連絡事項</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>貴社注番</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>FAX送信</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>メール送信</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>ご請求先</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                   </tbody>
              </table>
         </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_4-->
      <!-- * end section 4 ご依頼元情報 * -->
      <!-- * start section 5 ご請求先情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>ご請求先情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
                 <div class="tb-responsive">
                   <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                       <tbody class="lixil_body">
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先氏名</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先氏名（フリガナ）</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先電話番号</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先FAX番号</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先郵便番号</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先都道府県</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先建物名・部屋番号</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>ご請求先担当者</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                         <tr>
                             <th class="labelCol">
                                 <div class="col-label"><label>お支払い予定日</label></div>
                             </th>
                             <td class="col-data">
                                 <div class="col-result">入力内容を表示</div>
                             </td>
                         </tr><!--/result-->
                       </tbody>
                  </table>
             </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_5-->
      <!-- * end section 5 ご請求先情報 * -->
      <div class="read-info_center center">
          <a class="btn-info_rs" href="{{ url()->previous() }}">前に戻る</a>
      </div><!--/read-info_center-->
   </div><!--/main-vs-12 sv_top20-->
  </div><!--/row-sv-->
  <script type="text/javascript">
    //-------------------- 追加　2007/03/12　start --------------------//
    function showPrintForm() {
      window.open("{{ route('shurinojokyo.irainaiyonokakuninprintpreview') }}", 'irainaiyonokakuninprintpreview', "top=0,left=0,height=650,width=950,location=no,scrollbars=yes,toolbar=yes,menubar=yes,status=no");
    }
    //-------------------- 追加　2007/03/12　end   --------------------//
  </script>
@stop
