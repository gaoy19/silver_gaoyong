  <div class="con-footer">
      <div class="row-sv">
          <div class="col-sv-5">
              <div class="user_menu">
                  <a href="/assets/images/repair_water_manual.pdf" target="_blank"><img src="{{ asset('assets/images/user_menu.png') }}" alt="ご利用マニュアル" /></a>
              </div>
          </div>
          <div class="col-sv-7">
              <ul class="menu-footer">
                  <li class="bot-rt"><a href="{{ route('shiyofukamoji') }}" target="_blank"><span class="rt-text">使用不可文字一覧</span></a></li>
                  <li class="bot-rt"><a href="http://www.lixil.co.jp/privacy/" target="_blank"><span class="rt-text">プライバシーポリシー</span></a></li>
                  <li><a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a></li>
              </ul>
          </div>
          <div class="col-sv-12">
              <div class="footer-copyright">
                  <p>Copyrights © LIXIL Corporation. All Rights Reserved.</p>
              </div>
          </div>
      </div>
    </div><!--/con-footer-->

<script type="text/javascript">
function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode;
console.log(charCode);
  if (charCode != 45 && charCode > 31
  && (charCode < 48 || charCode > 57))
   return false;

return true;
}
</script>
