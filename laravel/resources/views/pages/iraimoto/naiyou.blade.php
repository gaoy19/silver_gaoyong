﻿@extends('layouts.master')
@section('request_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理のご依頼[2修理依頼内容の入力]')
@section('content')
  <!-- * Start section 修理のご依頼 * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>修理のご依頼</h1>
        </div>
    </div>
  </div><!--/row-sv-->
  <!-- * End row top * -->
  <!-- * Start section お客さま情報の入力 * -->
  <div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-3 active_sv">
                <a href="javascript:void(0);" onclick="if(checkFileSize()){ @if(Session::has('reachConfirmationFlag')) submitByProgress('iraimoto.showKokyaku') @else submitBack() @endif }" class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">お客さま情報の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">修理依頼内容の入力</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <a href="javascript:void(0);" onclick="if(checkFileSize()){ submitByProgress('iraimoto.showJouhou'); }" class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
            @if(Session::has('reachConfirmationFlag'))
                <a href="javascript:void(0);" onclick="if(checkFileSize()){ submitByProgress('iraimoto.showConfirmation'); }" class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </a>
            @else
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </div>
            @endif
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <div class="box-progress-bar">
                    <div class="box-num-bar">5</div>
                    <div class="box-text-bar">修理のご依頼完了 </div>
                </div>
            </li><!--/box-sv-3-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
  </div><!--/row-sv-->
  <!-- * End section お客さま情報の入力 * -->
  <!-- * Start section ご依頼内容の入力 * -->
  <div class="row-sv">
    <div class="main-vs-12 sv_top20">
        <form id="web_customer_info" method="POST" action="{{ route('iraimoto.naiyou') }}" accept-charset="UTF-8" role="form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>ご依頼内容の入力</h5><ul class="col-nav-bt" style="margin-top:-30px;"><li><a href="javascript:submitReset();"><span style="margin-left:30px;">全てクリア</span></a></li></ul>
                </div>
                <div class="con-info_sv" >
                    <div class="tb-form_sv" >
                        <!--連絡先名-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">製品のブランド</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('brand'))
                                            <div class="error_sv max-100"><span>{{ $errors->first('brand') }}</span></div>
                                        @endif
                                        <div class="group_radio">
                                            <input type="radio" id="brand_product1" value="0" name="brand" @if(old('brand') == "0" || !Session::has('naiyou')) checked) checked @endif>
                                            <label for="brand_product1">LIXIL</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="brand_product2" value="1"  name="brand" @if(old('brand') == "1") checked @endif>
                                            <label for="brand_product2">INAX(TS共同開発品含む)</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="brand_product4" value="3"  name="brand" @if(old('brand') == "3") checked @endif>
                                          <label for="brand_product4">サンウエーブ</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="brand_product3" value="2"  name="brand" @if(old('brand') == "2") checked @endif>
                                            <label for="brand_product3">トステム商品(住器) </label>
                                        </div>
                                    </div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>トステム(住器)の場合には「ご要望または連絡事項」欄に製品ラベル記載の“登録No”の記載を頂けますようお願い致します。</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>※非木造(=マンション)に納材されているトステム住器商品は本システムからご依頼できません。弊社営業所にご相談ください。</div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/box-main-form--> 
                        <!--商品名-->
                            <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">商品名</div>
                                <span class="error_label">必須</span>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                        <div class="col-ft-right">
                            <div class="col-td-12">
                                  @if ($errors->has('hinmei_select'))
                                      <div class="error_sv max-100"><span>{{ $errors->first('hinmei_select') }}</span></div>
                                  @endif
                                 @if (Session::has('sizeError'))
                                        <div class="error_sv max-100"><span>{{ Session::get('sizeError') }}</span></div>
                                      @endif
                                 <select class="form-control_sv jqhinmei_select ime-active{!! $errors->has('hinmei_select') || Session::has('sizeError')? ' input-error' : '' !!}" name="hinmei_select" style="max-width:300px;" id="hinmei_select" onchange="selectHinmei()">                                 
                                        <option value="{{ Session::get('hinmei_select') }}">{{ Session::get('hinmei_select') }}</option>
                                        @foreach ($seihin_daibunrui as $item)
                                        <option value="{{ $item['hinmei'] }}" {{ ( old('hinmei_select') == $item['hinmei'] ) ? ' selected' : '' }}>{{ $item['hinmei'] }}</option>
                                        @endforeach
                                  </select>
                                    <div class="box-sv-item">
                                    <input type="text" class="form-control_sv jqhinmei_directWrite ime-active{!! Session::has('sizeError')? ' input-error' : '' !!}" id="item_name" name="hinmei_directWrite" value="{{ old('hinmei_directWrite') }}" maxlength="60" size="80" style="max-width:500px;">                                    </div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>こちらは、水回り商品の修理依頼です</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>選択項目に該当の製品がない場合は、テキストボックスに全角 <span></span><span id="word">30</span>文字以内で入力してください</div>                                </div>
                          </div><!--/td-form_sv-8-->
                        </div><!--tr-form_sv-->   
                        <!--品番-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">品番</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-8">
                                      @if ($errors->has('hinban'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('hinban') }}</span></div>
                                      @endif
                                      <input type="text" class="form-control_sv ime-inactive{!! $errors->has('hinban') ? ' input-error' : '' !!}" id="part_number" name="hinban" value="{{ old('hinban') }}" maxlength="40" size="4" style="max-width:300px;">
                                    </div>
                                    <div class="col-td-4"><span class="text-td-2"><a class="searchNumber" target="_blank" href="http://www.lixil.co.jp/support/purpose/owner_registration/registration_number/">品番の確認方法</a></span></div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--取付年月-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">取付年月</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('toritsukeYYYY') || $errors->has('toritsukeMM_S'))
                                        <div class="error_sv max-100"><span>取付年、取付月の選択を行うか、「不明」「未入居」の指定を行ってください。</span></div>
                                      @endif
                                      <div class="col-td-6">
                                         <select name="toritsukeYYYY_S" class="form-control_sv{!! $errors->has('toritsukeYYYY') ? ' input-error' : '' !!}" value="{{ old('toritsukeYYYY_S') }}" style="max-width:180px;@if(old('toritsukeYYYY_S') == '@@@@' || old('toritsukeYYYY_S') == '8888') background-color:#dddddd; @endif">
                                            <option value="" selected>選択してください</option>
                                            @foreach ($toritsukeYYYY as $item)
                                              <option value="{{ $item['value'] }}" {{ ( old('toritsukeYYYY') == $item['value'] ) ? ' selected' : '' }} @if($item['value'] == '@@@@' || $item['value'] == '8888' ) class="hide_unknow"  @endif>{{ $item['text'] }}</option>
                                            @endforeach
                                          </select>
                                      </div><!--/col-td-6-->
                                      <div class="col-td-1">
                                          <span class="text-td-2">年</span>
                                      </div>
                                      <div class="col-td-6">
                                          <select name="toritsukeMM_S" class="form-control_sv{!! $errors->has('toritsukeMM') ? ' input-error' : '' !!}" value="{{ old('toritsukeMM_S') }}" style="max-width:180px;@if(old('toritsukeMM_S') == '@@' || old('toritsukeMM_S') == '88') background-color:#dddddd; @endif">
                                            <option value="">選択してください</option>
                                            @foreach ($toritsukeMM as $item)
                                              <option value="{{ $item['value'] }}" {{ ( old('toritsukeMM') == $item['value'] ) ? ' selected' : '' }} @if($item['value'] == '@@' || $item['value'] == '88' ) style="hide_notyet"  @endif>{{ $item['text'] }}</option>
                                            @endforeach
                                          </select>
                                      </div><!--/col-td-6-->
                                      <div class="col-td-1">
                                          <span class="text-td-2">月</span>
                                      </div>
                                      <input id="toritsukeYYYY" type="hidden" name="toritsukeYYYY" value="{{ old('toritsukeYYYY') }}">
                                      <input id="toritsukeMM" type="hidden" name="toritsukeMM" value="{{ old('toritsukeMM') }}">
                                    </div>
                                    <div class="col-td-12">
                                        <div class="group_rd_chk">
                                            <input type="checkbox" id="fumeiFlag" name="fumeiFlag" @if(old('fumeiFlag') == "on") checked @endif >
                                            <label for="fumeiFlag">不明</label>
                                        </div>
                                        <div class="group_rd_chk">
                                            <input type="checkbox" id="minyukyoFlag" name="minyukyoFlag" @if(old('minyukyoFlag') == "on") checked @endif >
                                            <label for="minyukyoFlag">未入居</label>
                                        </div>
                                    </div><!--/col-td-12-->
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--工事店名 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">工事店名 </div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('koujitenMei'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('koujitenMei') }}</span></div>
                                      @endif
                                      <input type="text" class="form-control_sv ime-active{!! $errors->has('koujitenMei') ? ' input-error' : '' !!}" id="construction_store_name" name="koujitenMei" value="{{ old('koujitenMei') }}" maxlength="20" style="max-width:500px;">
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご依頼内容 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご依頼内容 </div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('irainaiyou_directWrite'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('irainaiyou_directWrite') }}</span></div>
                                      @endif
                                      <input type="text" class="form-control_sv ime-active{!! $errors->has('irainaiyou_directWrite') ? ' input-error' : '' !!}" id="content_request" name="irainaiyou_directWrite" value="{{ old('irainaiyou_directWrite') }}" maxlength="30" style="max-width:500px;">
                                    </div>
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>全角 30 文字で入力してください。全角 30 文字を超える場合は「ご要望または連絡事項」にご記入ください</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--希望日 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">希望日</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                  <div id="datetimepicker_kiboushitei" class="date form_datetime" style=" max-width:180px">
                                    <input id="get_YMD" type="hidden" name="get_YMD" class="form-control_sv get_YMDH" value="{{ old('get_YMDH') }}" style="max-width:180px;">
                                     <div class="col-td-12">
                                          <a class="btn-calendar "><span class="icon-calendar"><img src="{{ asset('assets/images/calendar.png') }}"></span><span class="calendar-text">カレンダー</span></a>
                                     </div>
                                 </div><!--datetimepicker_kiboushitei-->
                                 @if ($errors->has('get_YMD'))
                                   <div class="error_sv max-100"><span>{{ $errors->first('get_YMD') }}</span></div>
                                 @endif
                                 <div class="col-td-4">
                                 <select id="kiboushiteiYYYY" name="kiboushiteiYYYY" class="form-control_sv{!! $errors->has('get_YMD') ? ' input-error' : '' !!}" data-format="yyyy" style="max-width:180px;">                                        
                                 <option value="">選択してください</option>
                                        @foreach ($kiboushiteiYYYY as $item)
                                          <option value="{{ $item['value'] }}" {{ ( old('kiboushiteiYYYY') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @endforeach
                                      </select>
                                  </div><!--/col-td-4-->
                                  <div class="col-tds-1">
                                      <span class="text-td-2">年</span>
                                  </div>
                                  <div class="col-td-4">
                                  <select name="kiboushiteiMM" id="kiboushiteiMM" class="form-control_sv{!! $errors->has('get_YMD') ? ' input-error' : '' !!}" style="max-width:180px;">                                        
                                  <option value="">選択してください</option>
                                        @foreach ($kiboushiteiMM as $item)
                                          <option value="{{ $item['value'] }}" {{ ( old('kiboushiteiMM') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @endforeach
                                      </select>
                                  </div><!--/col-td-4-->
                                  <div class="col-tds-1">
                                      <span class="text-td-2">月</span>
                                  </div>
                                  <div class="col-td-4">
                                      <select name="kiboushiteiDD" id="kiboushiteiDD" class="form-control_sv{!! $errors->has('get_YMD') ? ' input-error' : '' !!}" style="max-width:180px;">
                                        <option value="">選択してください</option>
                                        @foreach ($kiboushiteiDD as $item)
                                          <option value="{{ $item['value'] }}" {{ ( old('kiboushiteiDD') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @endforeach
                                      </select>
                                  </div><!--/col-td-4-->
                                  <div class="col-tds-1">
                                      <span class="text-td-2">日</span>
                                  </div>
                                  <div class="col-td-4" style="margin-bottom:15px;">
                                      <select name="kiboushiteiHH" id="kiboushiteiHH" class="form-control_sv" value="{{ old('kiboushiteiHH') }}" style="max-width:180px;">
                                        <option value="">選択してください</option>
                                        @foreach ($kiboushiteiHH as $item)
                                          <option value="{{ $item['value'] }}" {{ ( old('kiboushiteiHH') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @endforeach
                                      </select>
                                  </div><!--/col-td-4-->
                                  <div class="col-tds-1">
                                      <span class="text-td-2">時頃</span>
                                  </div>
                                  <div class="label-info-12">
                                      <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>部品・商品の手配状況、また混雑時は希望にそえない場合がございます。あらかじめご了承ください。</div>
                                      <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>ご希望日をもとに修理担当者よりあらためてご訪問日とお時間のお打合せのご連絡をさせていただきます。</div>
                                      <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>なお、お急ぎの場合はお電話にてご依頼ください。※お電話の受付時間 9：00～19：00</div>
                                  </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご要望または連絡事項 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">ご要望または連絡事項 </div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('renrakujikou'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('renrakujikou') }}</span></div>
                                      @endif
                                       <textarea  name="renrakujikou" maxlength="200" class="form-control_sv ime-active" style="max-width:500px;height:240px;">{{ old('renrakujikou') }}</textarea>
                                    </div>
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>全角 200 文字以内で入力してください</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                    </div><!--/tb-info_sv-->
                </div><!--/con-group-sv-->
            </div><!--/box-main-form-->
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>添付資料<span>5 ファイルまで送信できます</span></h5>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv">
                        <!--添付資料 -->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">添付資料 </div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      <div class="group_radio">
                                            <input type="radio" id="statu_none" name="tempsiryoKbn" value="0" @if((old('tempsiryoKbn') == '0' || !Session::get('naiyou')) && !Session::has('fileError')) checked @endif onchange="changeTempsiryoKbnFlg(this)">
                                            <label for="statu_none">なし</label>
                                        </div>
                                        <div class="group_radio">
                                            <input type="radio" id="statu_there" name="tempsiryoKbn" value="1" @if(old('tempsiryoKbn') == '1' || Session::has('fileError')) checked @endif onchange="changeTempsiryoKbnFlg(this)">
                                            <label for="statu_there">あり</label>
                                        </div>
                                    </div>
                                    <div class="col-td-12">
                                    @if (Session::has('fileError'))
                                        <div class="error_sv max-100"><span>{{ Session::get('fileError') }}</span></div>
                                    @endif
                                      <div class="error_sv" hidden id="fileErrMsg"></div>
                                      <div>
                                        <label for="file_img1" class="button-file-upload">
                                          <span class="fake-upload-button-no-hover">ファイルを選択</span>
                                          <input type="file" id="file_img1" name="tempsiryoFile1" value="" class="button-file-upload" onchange="fileChange(this,'file_img1_size');">
                                        </label>
                                        <span class="js-button-upload-file1 button-upload-file">{{ isset($fileUpload[1])?$fileUpload[1]:'' }}</span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span id="file_img1_size">{{ isset($_SESSION['fileSize1'])?$_SESSION['fileSize1']:'' }}</span>
                                      </div>
                                      <div>
                                        <label for="file_img2" class="button-file-upload">
                                          <span class="fake-upload-button-no-hover">ファイルを選択</span>
                                          <input type="file" id="file_img2" name="tempsiryoFile2" value="" class="button-file-upload" onchange="fileChange(this,'file_img2_size');">
                                        </label>
                                        <span class="js-button-upload-file2 button-upload-file">{{ isset($fileUpload[2])?$fileUpload[2]:'' }}</span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span id="file_img2_size">{{ isset($_SESSION['fileSize2'])?$_SESSION['fileSize2']:'' }}</span>
                                      </div>

                                      <div>
                                        <label for="file_img3" class="button-file-upload">
                                          <span class="fake-upload-button-no-hover">ファイルを選択</span>
                                          <input type="file" id="file_img3" name="tempsiryoFile3" value="" class="button-file-upload" onchange="fileChange(this,'file_img3_size');">
                                        </label>
                                        <span class="js-button-upload-file3 button-upload-file">{{ isset($fileUpload[3])?$fileUpload[3]:'' }}</span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span id="file_img3_size">{{ isset($_SESSION['fileSize3'])?$_SESSION['fileSize3']:'' }}</span>
                                      </div>
                                      <div>
                                        <label for="file_img4" class="button-file-upload">
                                          <span class="fake-upload-button-no-hover">ファイルを選択</span>
                                          <input type="file" id="file_img4" name="tempsiryoFile4" value="" class="button-file-upload" onchange="fileChange(this,'file_img4_size');">
                                        </label>
                                        <span class="js-button-upload-file4 button-upload-file">{{ isset($fileUpload[4])?$fileUpload[4]:'' }}</span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span id="file_img4_size">{{ isset($_SESSION['fileSize4'])?$_SESSION['fileSize4']:'' }}</span>
                                      </div>
                                      <div>
                                        <label for="file_img5" class="button-file-upload">
                                          <span class="fake-upload-button-no-hover">ファイルを選択</span>
                                          <input type="file" id="file_img5" name="tempsiryoFile5" value="" class="button-file-upload" onchange="fileChange(this,'file_img5_size');">
                                        </label>
                                        <span class="js-button-upload-file5 button-upload-file">{{ isset($fileUpload[5])?$fileUpload[5]:'' }}</span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<span id="file_img5_size">{{ isset($_SESSION['fileSize5'])?$_SESSION['fileSize5']:'' }}</span>
                                      </div>
                                    </div>
                                </div><!--/col-ft-right-->
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                    </div><!--/tb-form_sv-->
                </div><!--/con-info_sv-->
            </div><!--/box-main-form-->
            <!--修理依頼内容の入力へ進む-->
            <div class="row-sv mid">
                <div class="col-sv-6">
                    <a href="javascript:void(0);" onclick="if(checkFileSize()){ @if(Session::has('reachConfirmationFlag'))  submitByProgress('iraimoto.showKokyaku')  @else  submitBack()  @endif }" class="btn btn-submit-send" >
                      <span class="arrow-send_lf"><img src="{{ asset('assets/images/arrow_send_lf.png') }}"></span><span class="arrow-text_lf">１  お客さま入力情報に戻る</span></a>
                </div>
                <div class="col-sv-6 btn-submit" style="width: auto;">
                @if($fromConfirmation)
                    <button class="btn btn-submit-send filesize-check" name="jouhou" value="iraimoto.showConfirmation"><span class="arrow-text">4  ご依頼内容の確認に進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
                @else
                    <button class="btn btn-submit-send filesize-check" name="jouhou" value="iraimoto.showJouhou"><span class="arrow-text">３  ご依頼元情報入力に進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
                @endif
                </div>
            </div>
            <input type="hidden" value="" id="progressClick" name="progressClick"><!--/どのステータスをクリックしたか-->
        </form><!--/form-horizontal_sv-->
    </div>
</div><!--/row-sv--><!--row-sv-->
<!-- * End section お客さま情報の入力 * -->
<!--upload file-->
<script type="text/javascript">

function submitReset() {
    if(confirm("入力した依頼内容が全てクリアされますが、よろしいですか？")){
        var data = $('#web_customer_info');
        data.attr("action",'{{ route('iraimoto.reset') }}');
        data.submit();
    }
}

function submitBack() {
    var data = $('#web_customer_info');
    data.attr("action",'{{ route('iraimoto.submitBack') }}');
    data.submit();
}

//添付ファイルサイズは８M以上のチェック
function checkFileSize(){
    var fileSize = 0,allfilessize = 0,filePath,file;
    if (window.attachEvent) {
        var fso = new ActiveXObject("Scripting.FileSystemObject");
        for(var i = 1; i<=5; i++){
            filePath = $("#file_img"+ i).val();
            if( filePath != "" ){
                file = fso.GetFile (filePath);
                fileSize = file.size;
                allfilessize = allfilessize + parseFloat(fileSize);
            }
        }
    } else {
        for(var i = 1; i<=5; i++){
            filePath = $("#file_img"+ i).val();
            if( filePath != "" ){
                fileSize = $("#file_img"+ i)[0].files[0].size;
                allfilessize = allfilessize + parseFloat(fileSize);
            }
        }
    }

    var size = allfilessize / 1024000;
    if( size > 8 ){
        $("#fileErrMsg").html("<span>合計で8MBを超えるファイルを添付することはできません。</span>").removeAttr("hidden");
        return false;
    }else{
        return true;
    }
}
//ステータスをクリックする時
function submitByProgress(progress){
    $("#progressClick").val(progress);
    $("#web_customer_info").submit();
}
$(".filesize-check").click(function(){
	var str= $(".filesize-check").val();
    document.getElementByName('jouhou').value = str;
    if( checkFileSize() ){
        $("#web_customer_info").submit();
    }else{
        return false;
    }
});
//添付資料 size
function fileChange(obj,fileSizeObj) {
    var fileSize = 0;
    if (window.attachEvent) {
        var filePath = obj.value;
        var fso = new ActiveXObject("Scripting.FileSystemObject");
        var file = fso.GetFile (filePath);
        fileSize = file.size;
    } else {
        fileSize = obj.files[0].size;
    }
    var size = fileSize / 1024000;
    $("#"+fileSizeObj).html( parseFloat(size.toFixed(1))+"MB" );
}
</script>
<script type="text/javascript">
  $(function() {
    if($('input[name=tempsiryoKbn]:checked').val() == 0){
        document.getElementById("file_img1_size").innerHTML='';
        document.getElementById("file_img2_size").innerHTML='';
        document.getElementById("file_img3_size").innerHTML='';
        document.getElementById("file_img4_size").innerHTML='';
        document.getElementById("file_img5_size").innerHTML='';
        $('.fake-upload-button').addClass('fake-upload-button-no-hover').removeClass('fake-upload-button');
        for (var i = 1; i <= 5; i++) {
            $('#file_img'+i).val('');
            $('#file_img'+i).parent().parent().find("span.button-upload-file").text('');
        }
    }
  })

  tempsiryoKbnFlg = {{ old('tempsiryoKbn')?old('tempsiryoKbn'):0 }};
  function changeTempsiryoKbnFlg(obj){
    if(obj.value == 0){
        if(confirm("添付ファイルが全てクリアされますが、よろしいですか？")){
            var data = $('#web_customer_info');
            data.attr("action",'{{ route('iraimoto.clearfile') }}');
            data.submit();
        } else {
            $($("[name=tempsiryoKbn]").get(1)).prop("checked",true);
            return;
        }
    }

    document.getElementById("file_img1_size").innerHTML='';
    document.getElementById("file_img2_size").innerHTML='';
    document.getElementById("file_img3_size").innerHTML='';
    document.getElementById("file_img4_size").innerHTML='';
    document.getElementById("file_img5_size").innerHTML='';
    tempsiryoKbnFlg = obj.value;
    if(obj.value == 0){
      $('.fake-upload-button').addClass('fake-upload-button-no-hover').removeClass('fake-upload-button');
      for (var i = 1; i <= 5; i++) {
        $('#file_img'+i).val('');
        $('#file_img'+i).parent().parent().find("span.button-upload-file").text('');
      }
    } else {
      $('.fake-upload-button-no-hover').addClass('fake-upload-button').removeClass('fake-upload-button-no-hover');
    }
  }
  $(function() {
    @if(old('tempsiryoKbn') == 1)
      $('.fake-upload-button-no-hover').addClass('fake-upload-button').removeClass('fake-upload-button-no-hover');
    @else
      $('.fake-upload-button').addClass('fake-upload-button-no-hover').removeClass('fake-upload-button');
    @endif
      for (var i = 1; i <= 5; i++) {
        $('#file_img'+i).on('click',function(){
          if($('input[name=tempsiryoKbn]:checked').val() == 0){
            return false;
          }
        });
        $('#file_img'+i).on('change', function () {
            var val = $(this).val().split(/(\\|\/)/g).pop();
            if (val !== "") {
                $(this).parent().parent().find("span.button-upload-file")
                    .text(val)
                    .parent()
                    .removeClass("not-selected");
            } else {
                $(this).parent().parent().find(".span.button-upload-file")
                    .text('')
                    .parent()
                    .addClass("not-selected");
            }
        });
      }
});
</script>
<script type="text/javascript">

    $("input[name=brand]:radio").change(function(){
        $("#hinmei_select").html('<option value =""></option>');
        $("#word").text('30') ;
        $("input[name=brand]:radio").attr("disabled","disabled");
        $("input[name=brand]:radio+label").css("cursor","not-allowed");
    var formData = {
        brand: $("input[name=brand]:radio:checked").val()
      }
    $.ajax({
            type: 'POST',
            url: '{{ route('json.brand') }}',
            data: formData,
            dataType: 'json',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function (data) {
              if(data){
                $.each(data,function(index,val){
                  $("#hinmei_select").append('<option value='+val.hinmei+' >'+val.hinmei+'</option>');
                })
              }
              $("input[name=brand]:radio").removeAttr("disabled");
              $("input[name=brand]:radio+label").css("cursor","pointer");
            },
            error: function (data) {
              console.log('Error:', data);
              $("input[name=brand]:radio").removeAttr("disabled");
              $("input[name=brand]:radio+label").css("cursor","pointer");
            }
        });
       })
//$("form").submit(function(e){
 //   $('input[name=hinmei_directWrite]').removeAttr('disabled');
//});
//selectbox計数
function selectHinmei(){  
    var hs = $("#hinmei_select").val().length;
    if(hs >= 30){
        $("#word").text('0');
    }else {
        $("#word").text( 30 - hs) ;
    }   
}
//textbox計数
//function inputHinmei(){  
//    var hs = $("#hinmei_select").val().length;
//    var hn = $("#item_name").val().length;  
//    var st = hs + hn;

//    if(st >= 30){
//        $("#word").text('0');
//    }else {
//        $("#word").text( 30 - st) ;
//    }
//}
//init計数
$(function(){
    var hs = $("#hinmei_select").val().length;
    if(hs >= 30){
        $("#word").text('0');
    }else {
        $("#word").text( 30 - hs) ;
    }
    });

//function selectHinmei(obj){
// if($(obj).val() == ''){
//    $('input[name=hinmei_directWrite]').val('');
//  } else {
//    $('input[name=hinmei_directWrite]').val($(obj).find("option:selected").text());
//  }

//  if($(obj).val() == ''){
//   $('input[name=hinmei_directWrite]').removeAttr("disabled");
// }else{
//  $('input[name=hinmei_directWrite]').attr("disabled", true);
// }
//}

//obj= $('.jqhinmei_select');
//if($(obj).val() == ''){
//   $('input[name=hinmei_directWrite]').removeAttr("disabled");
// } else {
//   $('input[name=hinmei_directWrite]').attr("disabled", true);
//};

$(function () {
  $("#datetimepicker_kiboushitei.date").datetimepicker({
    format:'yyyy-MM-dd',
    icons: {
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down"
     },
     language: 'ja'
    }).on('dp.change', function (e) {
      var year = e.date.year();
      var month = ("0" + (e.date.month() + 1)).slice(-2);
      var day = ("0" + (e.date.date())).slice(-2);
      var date = month +'-'+ day +'-'+ year;
      $("#get_YMD").val(date);
      $("select#kiboushiteiYYYY").val(year);
      $("select#kiboushiteiMM").val(month);
      $("select#kiboushiteiDD").val(day);

      $('#datetimepicker_kiboushitei').data("DateTimePicker").hide();
    });;
    //check value year and more
    var val1 = $("input[name=toritsukeYYYY]").val();
    var val2 = $("input[name=toritsukeMM]").val();
    if(val1 == '@@@@' && val2 == '@@'){
      $('input[name=minyukyoFlag]').prop('checked' , false);
      $("select[name=toritsukeYYYY_S]").val('@@@@');
      $("select[name=toritsukeMM_S]").val('@@');
      $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , true);
      $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#dddddd');
    }else if (val1 == '8888' || val2 == '88') {
      $("select[name=toritsukeYYYY_S]").val('8888');
      $("select[name=toritsukeMM_S]").val('88');
      $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , true);
      $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#dddddd');
      $('input[name=fumeiFlag]').prop('checked' , false);
    }else{
      $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , false);
      $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#ffffff');
      $('input[name=fumeiFlag],input[name=minyukyoFlag]').prop('checked' , false);
    }
    $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").on('change',function(event){
      var val_year = $("select[name=toritsukeYYYY_S]").val();
      var val_month = $("select[name=toritsukeMM_S]").val();
      $("input[name=toritsukeYYYY]").val(val_year);
      $("input[name=toritsukeMM]").val(val_month);
      if(val_year != '@@@@' || val_year != '8888' || val_month != '88'){
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#ffffff');
        $('input[name=fumeiFlag],input[name=minyukyoFlag]').prop('checked' , false);
      }
    });
    // 取付年月「不明」チェックボックス制御
    $('input[name=fumeiFlag]').on('change', function (event) {
      if(this.checked) {
        $("select#elem").prop('selectedIndex', 0);
        $('input[name=minyukyoFlag]').prop('checked' , false);
        $("input[name=toritsukeYYYY]").val('@@@@');
        $("input[name=toritsukeMM]").val('@@');
        $("select[name=toritsukeYYYY_S]").val('@@@@');
        $("select[name=toritsukeMM_S]").val('@@');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , true);
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#dddddd');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('color','#dddddd');
      } else {
        $("select[name=toritsukeYYYY_S]").val('');
        $("select[name=toritsukeMM_S]").val('');
        $("input[name=toritsukeYYYY]").val('');
        $("input[name=toritsukeMM]").val('');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , false);
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#ffffff');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('color','#555');
      }
    });
    // 取付年月「未入居」チェックボックス制御
    $('input[name=minyukyoFlag]').on('change', function (event) {
      if(this.checked) {
        $('input[name=fumeiFlag]').prop('checked' , false);
        $("input[name=toritsukeYYYY]").val('8888');
        $("input[name=toritsukeMM]").val('88');
        $("select[name=toritsukeYYYY_S]").val('@@@@');
        $("select[name=toritsukeMM_S]").val('@@');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , true);
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#dddddd');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('color','#dddddd');
      } else {
        $("select[name=toritsukeYYYY_S]").val('');
        $("select[name=toritsukeMM_S]").val('');
        $("input[name=toritsukeYYYY]").val('');
        $("input[name=toritsukeMM]").val('');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").prop('disabled' , false);
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('background-color','#ffffff');
        $("select[name=toritsukeYYYY_S],select[name=toritsukeMM_S]").css('color','#555');
      }
    });
    //set date
    var year = $('select#kiboushiteiYYYY').find(":selected").val();
    var month = $('select#kiboushiteiMM').find(":selected").val();
    var day = $('select#kiboushiteiDD').find(":selected").val();
    var date = month +'-'+ day +'-'+ year;
    $("#get_YMD").val(date);
    $('select#kiboushiteiYYYY,select#kiboushiteiMM,select#kiboushiteiDD').on('change', function() {
      var year = $('select#kiboushiteiYYYY').find(":selected").val();
      var month = $('select#kiboushiteiMM').find(":selected").val();
      var day = $('select#kiboushiteiDD').find(":selected").val();
      var date = month +'-'+ day +'-'+ year;
      $("#get_YMD").val(date);
    });
    $("select[name=hinmei_select]").change(function (e) {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      })
      e.preventDefault();
      var formData = {
          SEIHIN_DAIBUNRUI_CD: $(this).val()
      }
      $.ajax({
          type: 'POST',
          url: '{{route('json.seihinbetu')}}',
          data: formData,
          dataType: 'json',
          success: function (data) {
              if(data){
                var combo = $("select[name=hinmei_select2]");
                $("select[name=hinmei_select2] option").remove();
                for (var i = 0; i < data.length; i++) {
                  combo.prepend("<option value="+ data[i].seihinbetu_cd +">" + data[i].seihin_mei + "</option>")
                }
                combo.prepend("<option value=''>選択してください</option>");
                combo.prop('selectedIndex', 0);
              }
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
    });
});
</script>

@stop
