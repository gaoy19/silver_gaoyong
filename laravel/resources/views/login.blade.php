@extends('layouts.master')

@section('title','LIXILインターネット修理受付センター | ログイン')

@section('content')
  <!-- * start section login * -->
    <div id="container-login">
    <div class="form-sv">
      <form class="form-horizontal_sv" method="POST" action="{{ route('login.post') }}" accept-charset="UTF-8" role="form" >
          {{ csrf_field() }}
          <div class="col-sv-6 modal-header_sv">
              <h3>{{ (session('global') || $errors->has('user_id') || $errors->has('password')) || Session::has('loginTimeout')? '再' : '' }}ログイン</h3>
          </div>
          <div class="col-sv-6">
                <div class="des-info underline">
                    <a href="/assets/images/renewal_info.pdf">※インターネット修理受付センターは<br>リニューアルしました。概要はこちら</a>
                </div>
          </div>
          <div class="con-group-sv">
              @if (Session::has('loginTimeout'))
                <style> .session-login_timeout{ font-family: Meiryo;font-size: 14px;line-height: 1.64;color: rgba(84, 88, 90, 0.94); padding: 15px 0 20px 0; } </style>
                <div class="session-login_timeout">
                  <span>{{ Session::get('loginTimeout') }}</span>
                </div>
              @endif
              <div class="form-group_sv {!! $errors->has('user_id') ? 'has-error' : '' !!}">
                <label class="control-label_sv col-sv-3" for="user_id">ユーザー ID:</label>
                <div class="col-sv-9">
                  @if ($errors->has('user_id'))
                  <div class="error_sv"><span>{{ $errors->first('user_id') }}</span></div>
                  @endif
                  <input type="text" class="form-control_sv ime-disabled box-error" id="user_id"  name="user_id" value="{{ old('user_id')?old('user_id'):$user_id }}" />
                </div>
              </div>
              <div class="form-group_sv {!! $errors->has('password') ? 'has-error' : '' !!}">
                <label class="control-label_sv col-sv-3" for="password">パスワード:</label>
                <div class="col-sv-9">
                  @if ($errors->has('password'))
                    <div class="error_sv"><span>{{ $errors->first('password') }}</span></div>
                  @endif
                  <input type="password" class="form-control_sv box-error" id="password" name="password"/>
                </div>
                <div class="col-sv-3"></div>
                <div class="col-sv-9 sv_right">
                  <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>パスワードは大文字/小文字を区別して扱います。</div>
                </div>
              </div>
              <div class="form-group_sv sv_top45">
                <div class="col-sv-12 sv_center">
                  <div class="main-checkbox">
                      <input class="sv-checkbox" id="remember" type="checkbox" @if($user_id != '') checked @endif  name="remember">
                      <label for="remember">ユーザーIDを保存する</label>
                  </div>
                  <button type="submit" class="btn btn-sv">ログイン</button>
                </div>
              </div>
              <div class="form-group_sv sv_top30">
                  <div class="col-sv-12 sv_right">
                      <div class="sv_forgetten"><a href="{{ route('user_password.reset.request') }}">パスワードを忘れた方</a></div>
                  </div>
              </div>
              @if(session('global'))
              <div class="form-group_sv">
                  <div class="col-sv-12">
                        <div class="error_sv" style="margin-top: 7px;"><span>{{session('global')}}</span></div>
                  </div>
              </div>
              @endif
          </div>
      </form><!--/form-horizontal_sv-->
      <div class="form-horizontal_sv sv_top30">
          <div class="modal-header_sv">
              <h4>ご利用にはユーザーID、パスワードが必要です。</h4>
              <p>※従来のシステムのユーザーID、パスワードでご利用できます。</p>
          </div>
          <div class="con-group-sv sv_top5">
              <div class="row-sv">
                  <div class="col-sv-7 sv_left">
                      <p>ユーザーIDをお持ちでないかたは
                          オンラインで申請できます</p>
                  </div>
                  <div class="col-sv-5 sv_center">
                       <a href="{{ route('user.register') }}" class="btn btn-black">新規ユーザー登録</a>
                  </div>
              </div>
          </div>
      </div><!--/form-horizontal_sv-->
      @if(isset($alphasuserid) && $alphasuserid != '')
      <div class="form-horizontal_sv sv_top30">
          <div class="con-group-sv sv_top5">
              <div class="row-sv">
                  <div class="col-sv-12 sv_left">
                      <p>※オプション（トップページ自動ログイン）</p>
                      <p>ユーザーID、パスワード入力を経由せず、</p>
                      <p>トップページに遷移させる登録も可能です。</p>
                      <p>&nbsp;</p>
                  </div>
                  <div class="col-sv-7 sv_center underline">
                      <a href="/assets/images/autologin_detail.pdf">ご注意点はこちら</a>
                  </div>
                  <div class="col-sv-5 sv_center underline">
                      <!-- temporary solution -->
                      <a href="{{ route('user.autologinregister') }}">登録依頼</a>
                  </div>
              </div>
          </div>
        </div><!--/form-horizontal_sv-->
        @endif
    </div>
  </div><!--/container_login-->
  <!-- * End section login * -->
@stop
