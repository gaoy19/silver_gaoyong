<?php

namespace App\Http\Controllers;
use App\Customer;
use DB;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
   //get all the customers
   $search = $request->get('search');
   $customers= Customer::where('irainaiyo_cd','LIKE', '%'.$search.'%')->orWhere('hinmei','LIKE', '%'.$search.'%')->paginate(10);
   return view('customers.index', compact('customers', 'search'));
  }
}
