@extends('layouts.master')
@section('repair_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理の状況一覧')
@section('content')
  <!-- * start row top * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>処理の状況一覧</h1>
        </div>
    </div>
  </div>
  <!-- * end row top * -->
  <div class="row-sv">
    <div id="box_list" class="box_list5_1">
    <!-- * Start div top_content_5-1 * -->
    <div id="top_content">
      <form class="" action="{{ route('webuketuke.index') }}" method="get">
        <div class="row-sv inner">
            <div class="col-sv-7">
                <div class="col-group-3">
                  <div class="title_gradio">
                      <span>絞り込み</span>
                  </div>
                </div>
                <div class="col-group-9">
                  <div class="group_radio float_left mgr30">
                      <input type="radio" id="refine_incomplete" name="mode" value="mikanryo" {{ (isset($input['mode']) && $input['mode'] == 'mikanryo')?'checked':'' }}>
                      <label for="refine_incomplete">未完了</label>
                  </div>
                  <div class="group_radio float_left mgr30">
                      <input type="radio" id="refine_cancel" name="mode" value="cancel" {{ (isset($input['mode']) && $input['mode'] == 'cancel')?'checked':'' }}>
                      <label for="refine_cancel">キャンセル</label>
                  </div>
                  <div class="group_radio float_left mgrl20">
                      <input type="radio" id="refine_done" name="mode" value="kanryo" {{ (isset($input['mode']) && $input['mode'] == 'kanryo')?'checked':'' }}>
                      <label for="refine_done">完了<span class="lbl-small">完了は本日を含め過去10 日分が検索されます</span></label>
                  </div>
                </div>
            </div>
            <div class="col-sv-5">
              <div class="just_content">
                <button type="submit" style="background:none;border:none;"><div class="radius_btn"><span>修理状況を絞り込む</span></div></button>
              </div>
            </div>
        </div>
      </form>

    </div>
     {{--<div class="row-sv-12 pdding_btt39 defualt_w_5-1">
        <div class="box_count">
            <div class="show_count">
                <div class="col-sv-5 count_text padding0 ">
                    <span>受付中</span>
                </div>
                <div class="col-sv-6 padding0 one">
                    <span>{{ $totalUketukechu }} </span>件

                </div>
            </div>
        </div>
        <div class="box_count">
            <div class="col-sv-5 count_text padding0 ">
                <span>完了</span>
                </div>
            <div class="col-sv-6 padding0 two">
                <span>{{ $totalKanryo }} </span>件
            </div>
        </div>
    </div>--}}
    <!-- * End div top_content_5-1 * -->
    <!-- * Start div table data * -->
    <div id="tbl_list" class="mrgl28 mrg_bttm">
      @if(count($webuketuke) > 0)
        <table>
          <tbody>
            <tr>
                <th style="width:80px;" class="head_col1">ステータス</th>
                <th style="width:100px;" class="head_col1">申込No</th>
                <th style="width:80px;" class="head_col1">登録日／<br>修理予定日</th>
                <th style="width:80px;" class="head_col1">受付 No</th>
                <th style="width:80px;" class="head_col5">お客様名</th>
                <th style="width:80px;" class="head_col5">お客さま住所</th>
                <th style="width:80px;" class="head_col4">ご依頼元<br/>担当者様</th>
                <th style="width:80px;" class="head_col1">依頼内容</th>
            </tr>
          </tbody>
          @foreach($webuketuke as $item)
            <tr class="row">
                <td class="center btn_5-1">
                    <div class="tooltip">
                        <div class="btn_intbl active">{{ $item->jyotai }}
                          @if($item->jyotai == '連絡待')
                            <span class="tooltiptext">お客様からの連絡待ちの状態です。</span>
                          @elseif($item->jyotai == '部品待')
                            <span class="tooltiptext">お客様訪問の結果、部品の在庫がなく、その部品を手配中の状態です。</span>
                          @elseif($item->jyotai == '不在')
                            <span class="tooltiptext">お客様がご不在だった時の状態です。</span>
                          @elseif($item->jyotai == '指定日')
                            <span class="tooltiptext">お客様から修理訪問日の指定があった状態です。</span>
                          @elseif($item->jyotai == '打合日')
                            <span class="tooltiptext">お客様とのお打ち合わせ日が決定した状態です。</span>
                          @elseif($item->jyotai == '帰宅遅')
                            <span class="tooltiptext">修理担当者へ連絡中の状態です。</span>
                          @elseif($item->jyotai == '日時待')
                            <span class="tooltiptext">お客様からの訪問日時連絡待ちの状態です。</span>
                          @elseif($item->jyotai == '見積待')
                            <span class="tooltiptext">見積もり提出中でお客様からの連絡待ちの状態です。</span>
                          @elseif($item->jyotai == '納期待')
                            <span class="tooltiptext">部品又は商品発注済で、工場からの納期連絡待ちの状態です。</span>
                          @elseif($item->jyotai == '問合待')
                            <span class="tooltiptext">お客様から技術、部品問い合せ中で確認中の状態です。</span>
                          @elseif($item->jyotai == '製品待')
                            <span class="tooltiptext">製品到着待ちの状態です。</span>
                          @elseif($item->jyotai == '受付中')
                            <span class="tooltiptext">受付センターにて処理中の状態です。</span>
                          @elseif($item->jyotai == '確認中')
                            <span class="tooltiptext">受付センターにて内容を確認中の状態です。</span>
                          @elseif($item->jyotai == '未着手')
                            <span class="tooltiptext">お客様訪問担当者が確定したばかりの状態です。</span>
                          @elseif($item->jyotai == '')
                            <span class="tooltiptext">お客様訪問の結果が特に入力されていない状態です。</span>
                          @endif
                        </div>
                    </div>
                    @if($item->web_no != '' && $item->uketuke_no != '')
                    <a href="{{ route('webuketuke.detail',$item->web_no) }}{{isset($input['mode'])?'?mode='.$input['mode']:''}}"><div style="margin-top:5px;margin-top:5px;cursor: pointer;">詳細</div></a>
                    @endif
                </td>
                <td class="center"><span>@if($item->web_no != $item->uketuke_no){{ $item->web_no }}@endif</span></td>
                <td class="center">{{ date('Y/m/d',strtotime($item->uketukebi)) }}<br>{{ $item->syuriyoteibi }}</td>
                <td class="center">{{ $item->uketuke_no != '' ? substr($item->uketuke_no,0,4).'-'.substr($item->uketuke_no,4,8): '' }}</td>
                <td class="text-left">{{ $item->kokyaku_sei }} {{ $item->kokyaku_na }}</td>
                <td class="text-left">{{ $item->kokyaku_jusyo }}</td>
                <td class="center">{{ $item->iraimoto_tanto }}</td>
                <td class="center btn_5-1">
                  @if($item->web_no != '')
                  <a href="{{ route('webuketuke.kakunin',$item->web_no) }}{{isset($input['mode'])?'?mode='.$input['mode']:''}}"><div style="margin-top:5px;">確認</div></a>
                  @endif
                  @if($item->web_no != $item->uketuke_no)
                  <a href="{{ route('iraimoto.copyIraiData',$item->web_no) }}{{isset($input['mode'])?'?mode='.$input['mode']:''}}"><div class='copydata'>引用登録</div></a>
                  @endif 
                </td>
            </tr>
          @endforeach
        </table>
      @else
        <p class="center" style="color:rgba(230, 84, 0, 0.94);">該当のデータを見つけられませんでした。</p>
      @endif
    </div>
    <!-- * End div table data * -->
    <div id="pagination_wrapper">
        <div class="count">
            <span>{{ $total }} 件中 {{$countPage['countPage']}} 件を表示しています</span>
        </div>
          {{ $pagination }}
    </div>
  </div>
  </div>
  <!-- * End div list head table * -->
@stop
