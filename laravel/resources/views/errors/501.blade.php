@extends('layouts.master')

@section('title','インターネット修理受付センター | エラー')

@section('content')
<div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12 sv_top20">
      <div class="center">
		    <div class="col-not-found">
          <h2>サーバエラーが発生しました。ご迷惑をおかけし、大変申し訳ございません。しばらく経ってご利用いただけない場合は、お問い合わせください。</h2>
  				<div class="share-options">
  					<p><a class="btn btn-success share-options btn-none" href="{{ route('home') }}" title="">ホームページへ戻る</a></p>
  				</div>
        </div>
			</div>
    </div>
</div>
@stop
