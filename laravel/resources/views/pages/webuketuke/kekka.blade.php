
@extends('layouts.master')
@section('search_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理データの条件指定検索[検索結果]')
@section('content')
  <!--  * start section container  * -->
  <div class="width_5_1 padding0">
        <!-- * start row top * -->
        <div class="row-sv">
            <div class="col-sv-12">
                <div class="box-title_sv">
                    <h1>修理データの条件指定検索</h1>
                </div>
            </div>
        </div>
        <!-- * end row top * -->
        <!-- * Start section list head table * -->
        <div id="box_list" class="box_list5_1">
            <!-- * Start section top_content_6-2 * -->
            <div class="row-sv margin0 width965 mrg6-2">
                <div class="col-sv-12 padding0"><span>[ 検索条件 ] に該当する修理データです</span></div>
            </div>

            <!-- * End section top_content_6-2 * -->
            <!-- * Start section table data * -->
            <div id="tbl_list" class="mrgl28 mrg_bttm">
              <div class="row-sv">
                  <div class="text-mid-box_right">
                      <p style="text-align:right;"><span class="col_orang">{{$total}}</span> 件検索されました。</p>
                      <a href="{{ $backUrl }}">
                        <div class="btn_footer">
                            <div class="lbl_radius">
                                <div class="h1">条件指定に戻る</div>
                            </div>
                        </div>
                      </a>
                  </div>
              </div>
              @if(count($webuketuke) > 0)
                <table>
                  <tbody>
                    <tr>
                      <th style="width:80px;" class="head_col1">ステータス</th>
                      <th style="width:100px;" class="head_col1">申込No</th>
                      <th style="width:80px;" class="head_col1">登録日／修理予定日</th>
                      <th style="width:90px;" class="head_col1">受付 No</th>
                      <th style="width:80px;" class="head_col5">商品</th>
                      <th style="width:80px;" class="head_col4">お客様名</th>
                      <th style="width:80px;" class="head_col5">お客さま住所</th>
                      <th style="width:80px;" class="head_col4">ご依頼元<br />担当者樣</th>
                      <th style="width:80px;" class="head_col1">依頼内容</th>
                    </tr>
                  </tbody>
                  @foreach($webuketuke as $item)
                    <tr class="row">
                      <td class="center btn_5-1">
                          <div class="tooltip">
                              <div class="btn_intbl active">{{ $item->jyotai }}
                                @if($item->jyotai == '連絡待')
                                  <span class="tooltiptext">お客様からの連絡待ちの状態です。</span>
                                @elseif($item->jyotai == '部品待')
                                  <span class="tooltiptext">お客様訪問の結果、部品の在庫がなく、その部品を手配中の状態です。</span>
                                @elseif($item->jyotai == '不在')
                                  <span class="tooltiptext">お客様がご不在だった時の状態です。</span>
                                @elseif($item->jyotai == '指定日')
                                  <span class="tooltiptext">お客様から修理訪問日の指定があった状態です。</span>
                                @elseif($item->jyotai == '打合日')
                                  <span class="tooltiptext">お客様とのお打ち合わせ日が決定した状態です。</span>
                                @elseif($item->jyotai == '帰宅遅')
                                  <span class="tooltiptext">修理担当者へ連絡中の状態です。</span>
                                @elseif($item->jyotai == '日時待')
                                  <span class="tooltiptext">お客様からの訪問日時連絡待ちの状態です。</span>
                                @elseif($item->jyotai == '見積待')
                                  <span class="tooltiptext">見積もり提出中でお客様からの連絡待ちの状態です。</span>
                                @elseif($item->jyotai == '納期待')
                                  <span class="tooltiptext">部品又は商品発注済で、工場からの納期連絡待ちの状態です。</span>
                                @elseif($item->jyotai == '問合待')
                                  <span class="tooltiptext">お客様から技術、部品問い合せ中で確認中の状態です。</span>
                                @elseif($item->jyotai == '製品待')
                                  <span class="tooltiptext">製品到着待ちの状態です。</span>
                                @elseif($item->jyotai == '受付中')
                                  <span class="tooltiptext">受付センターにて処理中の状態です。</span>
                                @elseif($item->jyotai == '確認中')
                                  <span class="tooltiptext">受付センターにて内容を確認中の状態です。</span>
                                @elseif($item->jyotai == '未着手')
                                  <span class="tooltiptext">お客様訪問担当者が確定したばかりの状態です。</span>
                                @elseif($item->jyotai == '')
                                  <span class="tooltiptext">お客様訪問の結果が特に入力されていない状態です。</span>
                                @endif
                              </div>
                          </div>
                          @if($item->web_no != '' && $item->uketuke_no != '')
                          <a href="{{ route('webuketuke.detail',$item->web_no) }}{{isset($input['mode'])?'?mode='.$input['mode']:''}}"><div style="margin-top:5px;margin-top:5px;cursor: pointer;">詳細</div></a>
                          @endif
                      </td>
                        <td class="center"><span>@if($item->web_no != $item->uketuke_no){{ $item->web_no }}@endif</span></td>
                        <td class="center">{{ date('Y/m/d',strtotime($item->uketukebi)) }}
                          <br>{{ $item->syuriyoteibi }}
                        </td>
                        <td class="center">{{ $item->uketuke_no != '' ? substr($item->uketuke_no,0,4).'-'.substr($item->uketuke_no,4,8): '' }}</td>
                        <td class="text-left">{{ $item->hinban }}</td>
                        <td class="text-left">{{ $item->kokyaku_sei }}{{ $item->kokyaku_na }}</td>
                        <td class="text-left">{{ $item->kokyaku_jusyo }}</td>
                        <td class="center">{{ $item->iraimoto_tanto }}</td>
                        <td class="center btn_5-1">
                          @if($item->web_no != '')
                          <a href="{{ route('webuketuke.kakunin',$item->web_no) }}{{isset($input['mode'])?'?mode='.$input['mode']:''}}"><div style="margin-top:5px;">確認</div></a>
                          @endif
                          @if($item->web_no != $item->uketuke_no)
                          <a href="{{ route('iraimoto.copyIraiData',$item->web_no) }}{{isset($input['mode'])?'?mode='.$input['mode']:''}}"><div class='copydata'>引用登録</div></a>
                          @endif 
                        </td>
                    </tr>
                  @endforeach
                </table>
              @else
                <p class="center" style="color:rgba(230, 84, 0, 0.94);">該当のデータを見つけられませんでした。</p>
              @endif

            </div>
            <div class="footer_tbl text-mid-box_right">
              <div class="row-12 right">
                <a href="{{ $backUrl }}">
                  <div class="btn_footer">
                      <div class="lbl_radius">
                          <div class="h1">条件指定に戻る</div>
                      </div>
                  </div>
                </a>
              </div>
            </div>

            <!-- * End section table data * -->
            <!-- * End div table data * -->
            <div id="pagination_wrapper">
                <div class="count">
                    <span>{{$total}} 件中 {{$countPage['countPage']}} 件を表示しています</span>
                </div>
                {{ $pagination }}
            </div>
        </div>
        <!-- * End section list head table * -->
    </div>
  <!--  * end section container  * -->
@stop
