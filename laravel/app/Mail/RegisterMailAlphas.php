<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMailAlphas extends Mailable
{
    use Queueable, SerializesModels;

    private $_subject = '【LIXIL】インターネット受付センター ユーザー登録通知';

    private $_name = 'Alphas自動ログイン申請受付';

    private $_from = null;

    private $_data = array();
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = array())
    {
        $this->_data = $data;
        $this->_from = \Config::get('mail.from.address');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $this->_subject = "【LIXIL】インターネット受付センター ユーザー登録通知";
      return $this->from($this->_from, $this->_name)
                  ->subject($this->_subject)
                  ->text('emails.registerMailAlphas')
                  ->with($this->_data);
    }

    /**
     *題名を設定
     */
    public function setFrom($from)
    {
      $this->_from = $from;
    }

    /**
     *題名を設定
     */
    public function setSubject($subject)
    {
      $this->_subject = $subject;
    }

    /**
     *題名を設定
     */
    public function setName($name)
    {
      $this->_name = $name;
    }
}
