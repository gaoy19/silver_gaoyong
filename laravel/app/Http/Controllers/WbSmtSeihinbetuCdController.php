<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WbSmtSeihinbetuCd;
use App\RequestData;
use Session;
use Redirect;
use Validator;

class WbSmtSeihinbetuCdController extends Controller
{
  private $_model;

  public function __construct(WbSmtSeihinbetuCd $model)
  {
    $this->_model = $model;
  }

  public function seihinbetu(Request $request)
  {
    $data = $this->_model->getLists($request->SEIHIN_DAIBUNRUI_CD);
    return response()->json($data);
  }

}
