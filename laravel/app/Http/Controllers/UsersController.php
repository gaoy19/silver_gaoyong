<?php

namespace App\Http\Controllers;
use Mail;
use App\User;
use Validator;
use Redirect;
use Hash;
use URL;
use Auth;
use Illuminate\Support\Facades\Crypt;
use DB;
use Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
  /**
  * Display login.
  *
  * @return \Illuminate\Http\Response
  */
  public function Login(Request $request)
  {

    //view login page
    if(Auth::check()){
      return Redirect::route('home');
    }
    $user_id = '';
    if(isset($_COOKIE['login_id'])) {
      $user_id = Crypt::decrypt($_COOKIE['login_id']);
    }

    //alphasuseridがセットされている場合は、自動ログインの案内を表示する。
    $alphasuserid = '';
    if(isset($request->userid)){
      $alphasuserid = $request->userid;
    }

    return view('login',['user_id'=>$user_id, 'alphasuserid'=>$alphasuserid]);

  }

  /**
   * Login logic
   * @param Request $request
   * @return type
  */
  public function postLogin(Request $request)
  {

    //Check Validator
    $this->validate($request, [
        'user_id' => 'required',
        'password' => 'required',
    ],[
      'user_id.required' => 'ユーザーIDを入力してください。',
      'password.required' => 'パスワードを入力してください。'
    ]);

    //Check remember value
    $remember = ($request->has('remember'))? true:false;
    $auth = Auth::attempt(['user_id' =>$request->user_id,'password' =>$request->password]);
    if($remember){
      $time = time() + (86400 * 30); // 86400 = 1day
      $encrypted = Crypt::encrypt($request->user_id);
      setcookie("login_id", $encrypted, $time);
    } else {
      setcookie("login_id", "", time() - 3600);
    }
    //if login success
    if($auth){
      if(!Auth::user()->ver_flg){
        $userFlg = User::find(Auth::user()->user_id);
        $userFlg->ver_flg = 1;
        $userFlg->update();
      }
      return Redirect::route('home');
    }
    return Redirect::route('login')
            ->withInput()
            ->with('global','ログインできません（ユーザー ID またはパスワードが異なります）')
            ->with('error_flg',true);

  }
  /**
 * Logout Logic
 * @return type
 */
  public function logout()
  {
      Session::flush();
      $iraimoto = new \App\Iraimoto;
      $iraimoto->clearFilenameSession();
      Auth::logout();
      return Redirect::route('login');
  }
}
