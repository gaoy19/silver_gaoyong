@extends('layouts.master')
@section('repair_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理の状況')
@section('content')
  <div class="row-sv">
   <div class="main-vs-12 sv_top20" style="margin-bottom:70px;">
     @if($webuketuke !== false)
        <div class="col-rs-title border-bot">
          <div class="col-sv-7">
             <h2>修理の状況</h2>
          </div>
          <div class="col-sv-5">
              <div class="col-print-5_rs row-15">
                  <button class="btn-print" type="button" value="報告書を印刷する" onclick="showPrintForm()"><span class="icon-print"><img src="/assets/images/print.png" width="18"></span>依頼内容を印刷する</button>
              </div>
          </div>
        </div><!--/border-bot-->
        <!-- * start section 2 お客さま情報 * -->
        <div class="box-main-form">
          <div class="modal-header_rs">
             <div class="col-sv-12">
                 <h5>お客さま情報</h5>
             </div>
           </div>
          <div class="con-info_sv">
             <div class="tb-responsive">
                 <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                     <tbody class="lixil_body">
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さま氏名</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->kokyaku_sei }}{{ $webuketuke->kokyaku_na }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さま氏名（フリガナ）</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->kokyaku_sei_kana }}{{ $webuketuke->kokyaku_na_kana }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さま電話番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">
                                 @if($webuketuke->kokyaku_tel_haihun == '9999999999999')
                                  未入居 / 施主様への連絡が不要
                                 @else
                                  {{ $webuketuke->kokyaku_tel_haihun }}
                                 @endif
                               </div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さま郵便番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">
                                 {{substr($webuketuke->kokyaku_yubin , 0, 3)."-".substr($webuketuke->kokyaku_yubin, 3, 4)}}
                               </div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さま都道府県</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->kokyaku_ken_mei }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さまご住所</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->kokyaku_jusyo }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>お客さま建物名・部屋番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->kokyaku_katagaki }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>貴社顧客番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->hm_kokyaku_no }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>連絡先名</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->renrakusaki_mei }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>連絡先電話番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->renrakusaki_tel_haihun }}</div>
                           </td>
                       </tr><!--/result-->
                     </tbody>
                </table>
          </div><!--/tb-responsive-->
          </div><!--/con-group-sv-->
        </div><!--/box-main-form_2-->
        <!-- * end section 2 お客さま情報 * -->
        <!-- * start section 3 修理依頼内容 * -->
        <div class="box-main-form">
          <div class="modal-header_rs">
             <div class="col-sv-12">
                 <h5>修理依頼内容</h5>
             </div>
          </div>
          <div class="con-info_sv">
            <div class="tb-responsive">
             <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                 <tbody class="lixil_body">
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>商品名</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $webuketuke->hinmei }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>品番</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $webuketuke->syohin_hinban }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>取り付け年月</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">
                             @if($webuketuke->torituke_nengetu == '@@@@@@')
                              不明
                             @elseif($webuketuke->torituke_nengetu == '888888')
                              未入居
                             @else
                              {{substr($webuketuke->torituke_nengetu , 0, 4)."年".substr($webuketuke->torituke_nengetu, 4, 5)}}月
                             @endif
                           </div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>工事店名</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $webuketuke->koziten }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼内容</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $webuketuke->irainaiyo }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>希望日</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">
                             @if($webuketuke->kibo_sitei_bi != null && $webuketuke->kibo_sitei_bi != '')
                                {{ substr($webuketuke->kibo_sitei_bi,0,2) }}月{{ substr($webuketuke->kibo_sitei_bi,2,4) }}日
                             @endif
                             @if($webuketuke->kibo_sitei_bi_jikan != null && $webuketuke->kibo_sitei_bi_jikan != '')
                                @if($webuketuke->kibo_sitei_bi_jikan == '@@')
                                  希望無
                                @else
                                  {{ $webuketuke->kibo_sitei_bi_jikan }}:00ごろ
                                @endif
                             @endif
                           </div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご要望または連絡事項</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{$webuketuke->message}}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>添付資料</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">
                             @if($webuketuke->tempsiryo_umu_flg == '1')
                              @foreach($files as $file)
                               <p>{{ $file['basename'] }}</p>
                              @endforeach
                             @endif
                           </div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>連絡先電話番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $webuketuke->renrakusaki_tel_haihun }}</div>
                       </td>
                   </tr><!--/result-->
                 </tbody>
            </table>
            </div><!--/tb-responsive-->
          </div><!--/con-group-sv-->
        </div><!--/box-main-form_3--><!--/box-main-form-2-->
        <!-- * end section 3 修理依頼内容 * -->
        <!-- * start section 4 ご依頼元情報 * -->
        <div class="box-main-form">
          <div class="modal-header_rs">
             <div class="col-sv-12">
                 <h5>ご依頼元情報</h5>
             </div>
          </div>
          <div class="con-info_sv">
               <div class="tb-responsive">
                 <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                     <tbody class="lixil_body">
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>得意先コード</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->iraimoto_cd }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>貴社名</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->iraimoto_simei }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>ご依頼元担当者様</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->iraimoto_tanto }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>貴社住所</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->iraimoto_jusyo }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>貴社電話番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->iraimoto_tel_haihun }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>貴社FAX</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->iraimoto_fax }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>ご要望または連絡事項</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->message }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>貴社注文番号</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">{{ $webuketuke->tyumon_bango }}</div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>FAX送信</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">
                                 @if($webuketuke->tyohyo_uk == "1") 「受付確認票」　@endif
                                 @if($webuketuke->tyohyo_gj == "1") 「ご依頼状況一覧票」　@endif
                                 @if($webuketuke->tyohyo_fax3 == "1") 「経過・結果速報」　@endif
                                 @if($webuketuke->tyohyo_fax5 == "1") 「確定情報」　@endif
                                 @if($webuketuke->tyohyo_uf == "1") 「受付複票」　@endif
                               </div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>メール送信</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">
                                {{ $webuketuke->mail_sousin }}
                               </div>
                           </td>
                       </tr><!--/result-->
                       <tr>
                           <th class="labelCol">
                               <div class="col-label"><label>ご請求先</label></div>
                           </th>
                           <td class="col-data">
                               <div class="col-result">
                                 @if($webuketuke->yuryo_seikyu_kbn == "1") 直収 @endif
                                 @if($webuketuke->yuryo_seikyu_kbn == "2") その他 @endif
                                 @if($webuketuke->yuryo_seikyu_kbn == "3") 貴社請求 @endif
                                 @if($webuketuke->yuryo_seikyu_kbn == "5") 現調後依頼元判断  ※保証期間内の商品を含む @endif
                               </div>
                           </td>
                       </tr><!--/result-->
                     </tbody>
                </table>
           </div><!--/tb-responsive-->
          </div><!--/con-group-sv-->
        </div><!--/box-main-form_4-->
        <!-- * end section 4 ご依頼元情報 * -->
        @if($webuketuke->yuryo_seikyu_kbn == "2")
        <!-- * start section 5 ご請求先情報 * -->
        <div class="box-main-form">
          <div class="modal-header_rs">
             <div class="col-sv-12">
                 <h5>ご請求先情報</h5>
             </div>
          </div>
          <div class="con-info_sv">
                   <div class="tb-responsive">
                     <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                         <tbody class="lixil_body">
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先名</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{ $webuketuke->seikyusaki_simei }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先名（フリガナ）</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{ $webuketuke->seikyusaki_simei_kana }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先電話番号</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">
                                     @if($webuketuke->seikyusaki_tel_haihun == '9999999999999')
                                      未入居 / 施主様への連絡が不要
                                     @else
                                      {{ $webuketuke->seikyusaki_tel_haihun }}
                                     @endif
                                   </div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先FAX番号</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{ $webuketuke->seikyusaki_fax }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先郵便番号</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{substr($webuketuke->seikyusaki_yubin , 0, 3)."-".substr($webuketuke->seikyusaki_yubin, 3, 4)}}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先住所</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{ $webuketuke->seikyusaki_jusyo }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先建物名・部屋番号</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{ $webuketuke->seikyusaki_katagaki }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>ご請求先担当者</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">{{ $webuketuke->seikyusaki_tanto }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <th class="labelCol">
                                   <div class="col-label"><label>お支払い予定日</label></div>
                               </th>
                               <td class="col-data">
                                   <div class="col-result">
                                     @if($webuketuke->siharaibi != '' && $webuketuke->siharaibi != null)
                                     {{ $webuketuke->siharaibi }}日
                                     @endif
                                   </div>
                               </td>
                           </tr><!--/result-->
                         </tbody>
                    </table>
               </div><!--/tb-responsive-->
          </div><!--/con-group-sv-->
        </div><!--/box-main-form_5-->
        <!-- * end section 5 ご請求先情報 * -->
        @endif
      @else
        <div class="main-vs-12 sv_top20 box-main-form">
          <div class="center">
            <div class="col-not-found">
              <h2>申込Noが空欄の依頼は参照できません。</h2>
              <div class="share-options">
                <p><a class="btn btn-success share-options btn-none" href="{{ route('home') }}" title="">トップページへ戻る</a></p>
              </div>
            </div>
          </div>
        </div>
      @endif
      <div class="read-info_center center">
          <a class="btn-info_rs" href="{{ url()->previous() }} ">前に戻る</a>
      </div><!--/read-info_center-->
   </div><!--/main-vs-12 sv_top20-->
  </div><!--/row-sv-->
  @if($webuketuke !== false)
  <script type="text/javascript">
    //-------------------- 追加　2007/03/12　start --------------------//
    function showPrintForm() {
      window.open("{{route('webuketuke.kakunin.print',$webuketuke->web_uketuke_no) }}", '受付情報確認印刷画面', "top=0,left=0,height=650,width=950,location=no,scrollbars=yes,toolbar=yes,menubar=yes,status=no");
    }
    //-------------------- 追加　2007/03/12　end   --------------------//
  </script>
  @endif
@stop
