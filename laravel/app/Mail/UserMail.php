<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserMail extends Mailable
{
    use Queueable, SerializesModels;

    private $_subject = '【LIXIL】インターネット受付センターパスワード再設定URL';

    private $_name = '【LIXIL】インターネット修理受付センター';

    private $_from = null;

    private $_data = array();
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = array())
    {
        $this->_data = $data;
        $this->_from = \Config::get('mail.from.address');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->_from, $this->_name)
                    ->subject($this->_subject)
                    ->text('emails.sendlinkMail')
                    ->with($this->_data);
    }

    /**
     *題名を設定
     */
    public function setFrom($from)
    {
      $this->_from = $from;
    }

    /**
     *題名を設定
     */
    public function setSubject($subject)
    {
      $this->_subject = $subject;
    }

    /**
     *題名を設定
     */
    public function setName($name)
    {
      $this->_name = $name;
    }
}
