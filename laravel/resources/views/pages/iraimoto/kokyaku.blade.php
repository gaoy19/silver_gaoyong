@extends('layouts.master')
@section('request_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理のご依頼[1お客さま情報の入力]')
@section('content')
  <!-- * Start row top * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>修理のご依頼</h1>
        </div>
    </div>
  </div><!--/row-sv-->
  <!-- * End row * -->
  <!-- * Start section お客さま情報の入力 * -->
  <div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">お客さま情報の入力</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <a href="javascript:void(0);" onclick="submitByProgress('iraimoto.showNaiyou')" class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">修理依頼内容の入力 </div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
            @if(Session::has('reachConfirmationFlag'))
                <a href="javascript:void(0);" onclick="submitByProgress('iraimoto.showJouhou')" class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力 </div>
                </a>
            @else
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力 </div>
                </div>
            @endif
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
            @if(Session::has('reachConfirmationFlag'))
                <a href="javascript:void(0);" onclick="submitByProgress('iraimoto.showConfirmation')" class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </a>
            @else
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </div>
            @endif
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <div class="box-progress-bar">
                    <div class="box-num-bar">5</div>
                    <div class="box-text-bar">修理のご依頼完了 </div>
                </div>
            </li><!--/box-sv-3-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
  </div><!--/row-sv-->
  <!-- * End section お客さま情報の入力 * -->
  <!-- * Start section お客さま情報（施主様情報） *-->
  <div class="row-sv">
    <div class="main-vs-12 sv_top20">     
    <form id="web_customer_info" name="form_kokyaku" method="POST" action="{{ route('iraimoto.kokyaku') }}" accept-charset="UTF-8" role="form">  
            {{ csrf_field() }}
            <div class="box-main-form">
                <div class="modal-header_sv">
                  <h5>お客さま情報（施主様情報）</h5><ul class="col-nav-bt" style="margin-top:-30px;"><li><a href="javascript:submitReset();"><span style="margin-left:30px;">全てクリア</span></a></li></ul>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv">
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま氏名</div>
                                <span class="error_label">姓のみ必須</span>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                @if ($errors->has('okyakusamaSei'))
                                    <div class="error_sv max-100 col-td-6"><span>{{ $errors->first('okyakusamaSei') }}</span></div>
                                @endif
                                @if ($errors->has('okyakusamaMei'))
                                    <div class="error_sv max-100 col-td-6"><span>{{ $errors->first('okyakusamaMei') }}</span></div>
                                @endif
                                <div class="col-td-12">
                                  <div class="col-td-2">姓</div>
                                  <div class="col-td-10">
                                      <input type="text" maxlength="40" class="form-control_sv ime-active{!! $errors->has('okyakusamaSei') ? ' input-error' : '' !!}" id="last_name"  name="okyakusamaSei" value="{{ old('okyakusamaSei') }}" style="max-width:200px;">
                                  </div>
                                  <div class="col-td-2">名</div>
                                  <div class="col-td-10">
                                      <input type="text" maxlength="10" class="form-control_sv ime-active{!! $errors->has('okyakusamaMei') ? ' input-error' : '' !!}" id="firsr_name"  name="okyakusamaMei" value="{{ old('okyakusamaMei') }}" style="max-width:200px;">
                                  </div>
                                  <div class="col-td-2">様</div>
                                </div>

                                <div class="label-info-12">
                                    <div class="label-info">例：姓）陸知　名）太郎</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>
                                    <span>ご入居がない場合は「未入居」、新築の場合は「○○邸新築」と入力して下さい姓は 40文字まで、名は 10文字までの全角で入力してください</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                     </div><!--/tr-form_sv1-->
                     <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま氏名（フリガナ）</div>
                                <span class="error_label">姓のみ必須</span>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                @if ($errors->has('okyakusamaSeiKana'))
                                    <div class="error_sv max-100 col-td-6"><span>{{ $errors->first('okyakusamaSeiKana') }}</span></div>
                                @endif
                                @if ($errors->has('okyakusamaMeiKana'))
                                    <div class="error_sv max-100 col-td-6"><span>{{ $errors->first('okyakusamaMeiKana') }}</span></div>
                                @endif
                                <div class="col-td-12">
                                  <div class="col-td-2">セイ</div>
                                  <div class="col-td-10">
                                      <input type="text" value="{{ old('okyakusamaSeiKana') }}" maxlength="50" class="form-control_sv ime-active{!! $errors->has('okyakusamaSeiKana') ? ' input-error' : '' !!}" id="last_namekana"  name="okyakusamaSeiKana" style="max-width:200px;">
                                  </div>
                                  <div class="col-td-2">メイ</div>
                                  <div class="col-td-10">
                                      <input type="text" value="{{ old('okyakusamaMeiKana') }}" maxlength="40" class="form-control_sv ime-active{!! $errors->has('okyakusamaMeiKana') ? ' input-error' : '' !!}" id="first_namekana"  name="okyakusamaMeiKana" style="max-width:200px;">
                                  </div>
                                  <div class="col-td-2">様</div>
                                </div>

                                <div class="label-info-12">
                                    <div class="label-info">例：セイ）リクシル　メイ）タロウ</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>セイは 50 文字まで、メイは 40 文字までの全角で入力してください</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv2-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま電話番号</div>
                                <span class="error_label">必須</span>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('okyakusamaTel'))
                                      <div class="error_sv max-100"><span>{{ $errors->first('okyakusamaTel') }}</span></div>
                                  @endif
                                  <input type="text" value="{{ old('okyakusamaTel') }}" maxlength="13" class="form-control_sv ime-disabled{!! $errors->has('okyakusamaTel') ? ' input-error' : '' !!}" id="okyakusamaTel"  name="okyakusamaTel" style="max-width:300px;" onkeypress="return isNumberKey(event);" @if(old('telFumeiFlag') == "on") readonly @endif>
                                </div>
                                <div class="label-info-12">
                                    <div class="label-info">例：03-3333-1111</div>
                                    <div class="main-checkbox">
                                        <input class="sv-checkbox" id="telFumeiFlag" type="checkbox" @if(old('telFumeiFlag') == "on") checked @endif name="telFumeiFlag">
                                        <label for="telFumeiFlag">未入居 / 施主様への連絡が不要</label>
                                    </div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv3-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま郵便番号</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                @if ($errors->has('okyakusamaYuubinNo'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('okyakusamaYuubinNo') }}</span></div>
                                @endif
                                <div class="col-td-5">
                                    <input type="text" value="{{ old('okyakusamaYuubinNo') }}" maxlength="8" class="form-control_sv ime-disabled{!! $errors->has('okyakusamaYuubinNo') ? ' input-error' : '' !!}" id="customer_zip_code"  name="okyakusamaYuubinNo" style="max-width:200px;" onkeypress="return isNumberKey(event);">
                                </div>
                                <div class="col-td-7">
                                     <input type="button" class="btn-control_sv" id="add_zip_code"  style="max-width:200px;" value="住所検索" onclick="showAddressData()"/>
                                </div>
                                <div class="label-info-12">
                                    <div class="label-info">例：100-1111</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>郵便番号７桁か、住所の一部を入力して［住所検索］ボタンを押すと住所一覧画面を表示します。</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv4-->
                    <!--/お客さま都道府県/-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま都道府県</div>
                                <span class="error_label">必須</span>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('okyakusamaTodoufuken'))
                                      <div class="error_sv max-100"><span>{{ $errors->first('okyakusamaTodoufuken') }}</span></div>
                                  @endif
                                    <select class="form-control_sv{!! $errors->has('okyakusamaTodoufuken') ? ' input-error' : '' !!}" name="okyakusamaTodoufuken" size="1" style="max-width:180px;">
                                        <option value="">選択してください</option>
                                        @foreach (Cache::get('todoufukens') as $todoufuken)
                                          <option value="{{ $todoufuken }}" {{ ( old('okyakusamaTodoufuken') == $todoufuken ) ? ' selected' : '' }}>{{ $todoufuken }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="label-info-12">
                                    <div class="label-info"></div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>都道府県は、選択をする他に、手入力をすることも可能です。</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv4-->
                    <!--お客さまご住所-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さまご住所</div>
                                <span class="error_label">必須</span>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                    @if ($errors->has('okyakusamaAddress'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('okyakusamaAddress') }}</span></div>
                                    @endif
                                    <input type="text" value="{{ old('okyakusamaAddress') }}" maxlength="45" class="form-control_sv ime-active{!! $errors->has('okyakusamaAddress') ? ' input-error' : '' !!}" id="customer_address"  name="okyakusamaAddress" style="max-width:500px;">
                                </div>
                                <div class="label-info-12">
                                    <div class="label-info">例：中央区千代田１−１−１</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>45 文字までの全角で入力してください</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv2-->
                    <!--お客さま建物名・部屋番号-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま建物名・部屋番号</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('okyakusamaMansionName'))
                                      <div class="error_sv max-100"><span>{{ $errors->first('okyakusamaMansionName') }}</span></div>
                                  @endif
                                    <input type="text" maxlength="50" value="{{ old('okyakusamaMansionName') }}" class="form-control_sv ime-active{!! $errors->has('okyakusamaMansionName') ? ' input-error' : '' !!}" id="room_number"  name="okyakusamaMansionName" style="max-width:500px;">
                                </div>
                                <div class="label-info-12">
                                    <div class="label-info">例：霞ヶ関ビルディング３６階１０１号室</div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>部屋番号まで入力してください
                                        50 文字までの全角で入力してください
                                    </div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv2-->
                    <!--貴社顧客番号-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-4 tb-fonm-none">
                             <div class="col-ft-left">
                                 <div class="td-title">貴社顧客番号</div>
                             </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-8 tb-fonm-none">
                             <div class="col-ft-right">
                                 <div class="col-td-12">
                        　           @if ($errors->has('kokyakuBango'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('kokyakuBango') }}</span></div>
                                     @endif
                                     @if($showKokyakuBangoFlag)
                                        <input type="text" maxlength="20" value="{{ old('kokyakuBango') }}" class="form-control_sv ime-disabled{!! $errors->has('kokyakuBango') ? ' input-error' : '' !!}" id="your_customer_number"  name="kokyakuBango" style="max-width:300px;">
                                     @else
                                        <input type="hidden" maxlength="20" value="{{ old('kokyakuBango') }}" class="form-control_sv ime-disabled{!! $errors->has('kokyakuBango') ? ' input-error' : '' !!}" id="your_customer_number"  name="kokyakuBango" style="max-width:300px;">
                                     @endif
                                 </div>
                             </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv2-->
                </div><!--/tb-info_sv-->
                </div><!--/con-group-sv-->
            </div><!--/box-main-form-->
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>連絡先　未入居 / 施主様への連絡が不要な場合はご入力ください</h5>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv">
                        <!--連絡先名-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">連絡先名</div>
                                    <span class="error_label renrakuRequired" @if(old('telFumeiFlag') != "on") style="display:none;" @endif>必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('renrakusakiMei'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('renrakusakiMei') }}</span></div>
                                      @endif
                                        <input type="text" value="{{ old('renrakusakiMei') }}" maxlength="20" class="form-control_sv ime-active{!! $errors->has('renrakusakiMei') ? ' input-error' : '' !!}" id="contact_name" name="renrakusakiMei" style="max-width:500px;">
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--連絡先電話番号-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">連絡先電話番号</div>
                                    <span class="error_label renrakuRequired" @if(old('telFumeiFlag') != "on") style="display:none;" @endif>必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('renrakusakiTel'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('renrakusakiTel') }}</span></div>
                                      @endif
                                        <input type="text" value="{{ old('renrakusakiTel') }}"  maxlength="13" class="form-control_sv ime-disabled{!! $errors->has('renrakusakiTel') ? ' input-error' : '' !!}" id="contact_phone_number" name="renrakusakiTel" style="max-width:300px;" onkeypress="return isNumberKey(event);">
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                    </div><!--/tb-form_sv-->
                </div><!--/con-info_sv-->
            </div><!--/box-main-form-->
            <!--修理依頼内容の入力へ進む-->
            <div class="btn-submit">
            @if($fromConfirmation)
                <button class="btn btn-submit-send" name="naiyou" value="iraimoto.showConfirmation" type="submit"><span class="arrow-text">4  ご依頼内容の確認に進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
            @else
                <button class="btn btn-submit-send" name="naiyou" value="iraimoto.showNaiyou" type="submit"><span class="arrow-text">2  修理依頼内容の入力へ進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
            @endif
            </div>
            <input type="hidden" value="" id="progressClick" name="progressClick"><!--/どのステータスをクリックしたか-->
        </form><!--/form-horizontal_sv-->    
    </div>
  </div><!--/row-sv-->
  <script type="text/javascript">
  
    function submitReset() {
        if(confirm("入力した依頼内容が全てクリアされますが、よろしいですか？")){
            var data = $('#web_customer_info');
            data.attr("action",'{{ route('iraimoto.reset') }}');
            data.submit();
        }
    }

    function submitByProgress(progress){
        $("#progressClick").val(progress);
        form_kokyaku.submit();
    }
   
    $(document).ready(function() {
        $('select[name=okyakusamaTodoufuken]').chosen({
            search_contains: true,
            no_results_text: "一致することができない"  
        });
        $('select[name=okyakusamaTodoufuken]').each(function(){
            if($(this).hasClass('input-error')){
                $(".chosen-single").addClass('input-error');
                $(".chosen-drop").addClass('input-error');
            }
        });
        //氏名フリガナ自動入力
        $.fn.autoKana('#last_name', '#last_namekana', { katakana : true });
        $.fn.autoKana('#firsr_name', '#first_namekana', {katakana : true });

      //set initial state.
      $('#telFumeiFlag').change(function() {
          if(this.checked) {
            $('#okyakusamaTel').val('9999999999999');
            $("#okyakusamaTel").prop('readonly', true);
            $(this).attr( 'checked', true );
            $('.renrakuRequired').css('display','inline');
          } else {
            $('#okyakusamaTel').val('');
            $("#okyakusamaTel").prop('readonly', false);
            $(this).attr('checked', false);
            $('.renrakuRequired').css('display','none');
          }
      });

      $("input[name=okyakusamaYuubinNo]").change(function (e) {

        var yuubinNo = $('#customer_zip_code').val();
        yuubinNo = yuubinNo.replace('-','');
        if(yuubinNo.length==7 && yuubinNo.indexOf('-')<0){
            yuubinNo = yuubinNo.substring(0,3) +'-'+ yuubinNo.substring(3,7);
            $('#customer_zip_code').val(yuubinNo);
        }

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
        })
        e.preventDefault();
        var formData = {
            okyakusamaYuubinNo: $('#customer_zip_code').val()
        }
        $.ajax({
            type: 'POST',
            url: '{{ route('json.yubinbango') }}',
            data: formData,
            dataType: 'json',
            success: function (data) {
                if(data){
                  $('select[name=okyakusamaTodoufuken]').val(data.ken_mei);
                  $('input[name=okyakusamaAddress]').val(data.sityoson_mei);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
      });
    });
    // 住所検索ダイアログ表示
    function showAddressData() {
      if($('input[name=okyakusamaYuubinNo]').val().length == 0 &&
        $('select[name=okyakusamaTodoufuken]').val().length == 0 &&
        $('input[name=okyakusamaAddress]').val().length == 0){
          alert('郵便番号、都道府県、ご住所のいずれかを入力してください。');
          return;
        }
      // 住所検索フォーム
      window.open("{{route('yubinbango.search')}}?windowName=childWin&actionID=search2"
                +"&ken_mei_val=" + $('select[name=okyakusamaTodoufuken]').val()
                +"&yubinBango_val=" + $('input[name=okyakusamaYuubinNo]').val()
                +"&sityoson_mei=" + $('input[name=okyakusamaAddress]').val()
                +"&form=form_kokyaku"
                +"&_token=" + $('meta[name="_token"]').attr('content'),
                "addressSearchForm",
                "top=0,left=0,height=650,width=850,status=no,scrollbars=yes,toolbar=no,menubar=no");
    }
  </script>
@stop
