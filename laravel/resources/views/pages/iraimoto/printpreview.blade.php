<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<link rel="shortcut icon" href="/assets/images/favicon.ico">
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>LIXILインターネット修理受付センター | 修理のご依頼：確認印刷画面</title>
<style media="print">
  .col-nav-bt {display: none;}
</style>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <section class="container-silver">
    <!-- * Start section print * -->
    <div class="tabs-content_cf print-content">
      <div class="col-print-btn">
        <ul class="col-nav-bt">
            <li><a href="javascript:void(0)" onclick="window.print()"><span class="icon icon_user"><img src="/assets/images/print.png"></span>印刷</a></li>
            <li class="logout_sv">
             <a style="text-align:center;" href="javascript:void(0)" onclick="window.close();">
                閉じる
             </a>
           </li>
        </ul>
      </div>
      <div class="report-title-kakunin">
        <h1>修理依頼依頼内容確認</h1>
      </div>
      <div class="tabs-control-form">
         <div class="tb-responsive">
            <table class="detailList_print" border="0" cellpadding="0" cellspacing="0">
              <thead class="lixil_header">
                  <tr><th class="lixil_th" colspan="2">
                      <div class="lixil_title">お客様情報</div>
                  </th>
              </tr></thead>
              <tbody class="lixil_body">
                @if(Session::has('kokyaku'))
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客様氏名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['okyakusamaSei'] }} {{ Session::get('kokyaku')['okyakusamaMei'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま氏名（フリガナ）</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['okyakusamaSeiKana'] }} {{ Session::get('kokyaku')['okyakusamaMeiKana'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(isset(Session::get('kokyaku')['telFumeiFlag']) && Session::get('kokyaku')['telFumeiFlag'] == 'on')
                              未入居 / 施主様への連絡が不要
                            @else
                              {{ Session::get('kokyaku')['okyakusamaTel'] }}
                            @endif
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま郵便番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['okyakusamaYuubinNo'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま都道府県</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['okyakusamaTodoufuken'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さまご住所</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['okyakusamaAddress'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま建物名・部屋番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['okyakusamaMansionName'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社顧客番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['kokyakuBango'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>連絡先名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['renrakusakiMei'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>連絡先電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('kokyaku')['renrakusakiTel'] }}
                          </div>
                      </td>
                  </tr>
                @endif
              </tbody><!--/お客さま情報-->
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">修理依頼内容</div>
                  </th>
                </tr>
              </thead>
              <tbody class="lixil_body">
                @if(Session::has('naiyou'))
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>商品名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('naiyou')['hinmei_directWrite'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>品番</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('naiyou')['hinban'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>取付年月</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(isset(Session::get('naiyou')['fumeiFlag']) && Session::get('naiyou')['fumeiFlag'] == 'on')
                              不明
                            @elseif(isset(Session::get('naiyou')['minyukyoFlag']) && Session::get('naiyou')['minyukyoFlag'] == 'on')
                              未入居
                            @else
                              {{ Session::get('naiyou')['toritsukeYYYY'] }}年{{ Session::get('naiyou')['toritsukeMM'] }}月
                            @endif
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>工事店名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('naiyou')['koujitenMei'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご依頼内容</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('naiyou')['irainaiyou_directWrite'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>希望日</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(Session::has('naiyou'))
                              @if(Session::get('naiyou')['kiboushiteiYYYY'] == '' && Session::get('naiyou')['kiboushiteiMM'] == '' &&
                                  Session::get('naiyou')['kiboushiteiDD'] == '' && Session::get('naiyou')['kiboushiteiHH'] == '')
                                  指定なし
                              @else
                                @if(Session::get('naiyou')['kiboushiteiYYYY'] != '')
                                {{ Session::get('naiyou')['kiboushiteiYYYY'] }}年
                                @endif
                                @if(Session::get('naiyou')['kiboushiteiMM'] != '')
                                {{ Session::get('naiyou')['kiboushiteiMM'] }}月
                                @endif
                                @if(Session::get('naiyou')['kiboushiteiDD'] != '')
                                {{ Session::get('naiyou')['kiboushiteiDD'] }}日
                                @endif
                                @if(Session::get('naiyou')['kiboushiteiHH'] != '')
                                  @if(Session::get('naiyou')['kiboushiteiHH'] == '@@')
                                    希望無
　                                @else
                                    {{ Session::get('naiyou')['kiboushiteiHH'] }}:00ごろ
                                  @endif
                                @endif
                              @endif
                            @endif
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご要望または連絡事項</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {!! nl2br(e(Session::get('naiyou')['renrakujikou'])) !!}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>添付資料</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(Session::has('naiyou'))
                                @if(Session::get('naiyou')['tempsiryoKbn']=='1')
                                <p>あり</p>
                                <div class="get-image">
                                  @if(isset($fileUpload))
                                    @foreach ($fileUpload as $key => $value)
                                      <p>{{ $value }}</p>
                                    @endforeach
                                  @endif
                                </div>
                                @else
                                <p>なし</p>
                                @endif
                            @endif
                          </div>
                      </td>
                  </tr>
                @endif
              </tbody><!--/修理依頼内容-->
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">ご依頼元情報</div>
                  </th>
                </tr>
              </thead><!--/ご依頼元情報-->
              <tbody class="lixil_body">
                @if(Session::has('jouhou'))
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>得意先コード</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Auth::user()->tokuisaki_cd }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>所属貴社</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ $goiraimoto_info->tokuisaki_sei }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご依頼元担当者様</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('jouhou')['goiraimotoTantou'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社住所</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ $goiraimoto_info->tokuisaki_jusyo }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ $goiraimoto_info->tokuisaki_tel_haihun }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社 FAX</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ $goiraimoto_info->torokusaki_fax }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社注番</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('jouhou')['kisya_chuban'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>FAX 送信</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(isset(Session::get('jouhou')['tyohyo_uk']) && Session::get('jouhou')['tyohyo_uk'] == "on") 「受付確認票」　@endif
                            @if(isset(Session::get('jouhou')['tyohyo_gj']) && Session::get('jouhou')['tyohyo_gj'] == "on") 「ご依頼状況一覧票」　@endif
                            @if(isset(Session::get('jouhou')['tyohyo_fax3']) && Session::get('jouhou')['tyohyo_fax3'] == "on") 「経過・結果速報」　@endif
                            @if(isset(Session::get('jouhou')['tyohyo_fax5']) && Session::get('jouhou')['tyohyo_fax5'] == "on") 「確定情報」　@endif
                            @if(isset(Session::get('jouhou')['tyohyo_uf']) && Session::get('jouhou')['tyohyo_uf'] == "on") 「受付複票」　@endif
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>メール送信</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(isset(Session::get('jouhou')['kakunin_mail']) && Session::get('jouhou')['kakunin_mail'] == "on") 「受付確認」　@endif
                            @if(isset(Session::get('jouhou')['sokuho_mail']) && Session::get('jouhou')['sokuho_mail'] == "on") 「結果速報」 @endif
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            @if(Session::get('jouhou')['seikyuuKubun'] == "1") 直収 @endif
                            @if(Session::get('jouhou')['seikyuuKubun'] == "3") 貴社請求 @endif
                            @if(Session::get('jouhou')['seikyuuKubun'] == "2") その他 @endif
                            @if(Session::get('jouhou')['seikyuuKubun'] == "5") 現調後依頼元判断  ※保証期間内の商品を含む @endif
                          </div>
                      </td>
                  </tr>
                @endif
              </tbody><!--/ご依頼元情報-->
              @if(Session::has('jouhou') && Session::get('jouhou')['seikyuuKubun'] == '2' && Session::has('goseikyusaki'))
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">ご請求先情報</div>
                  </th>
                </tr>
              </thead><!--/ご依頼元情報-->
              <tbody class="lixil_body">
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaShimei'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先名（フリガナ）</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaShimeiKana'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaTel'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先 FAX 番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaFax'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先郵便番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaYuubinNo'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先都道府県</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaTodoufuken'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先住所</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaAddress'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先建物名・部屋番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaMansionName'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先担当者</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaTantousya'] }}
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お支払い予定日</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            {{ Session::get('goseikyusaki')['sonotaShiharaibi'] }}日
                          </div>
                      </td>
                  </tr>
              </tbody><!--/ご依頼元情報-->
              @endif
            </table><!--/listdetail_print
          </div><!--tb-responsive-->
      </div><!--/tabs-control-form-->
    </div>
  </section>
  <!-- * Start section prind * -->
</body>
</html>
