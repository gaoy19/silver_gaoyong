  <div class="main-menu_sv">
    <div class="navbar-header_sv">
        <div class="menu-nav_sv">
            <button type="button" class="open-canvas navbar-toggle">
                <div id="btn-bar" class="icon-button"></div>
            </button>
            <div class="menu-title">Menu</div>
        </div>
    </div>
    <nav class="navbar_sv navbar-default_sv" role="navigation" >
      <!-- show content menu for toggling -->
      <div id="navbarCollapse_sv" class="navbarCollapse_sv navbar-collapse_sv">
          <ul class="nav_sv navbar-nav_sv">
              <li class="@yield('home_active')"><a href="{!! route('home') !!}">トップページ</a></li>
              <li class="@yield('request_active')"><a href="{!! route('iraimoto.showKokyaku') !!}">修理のご依頼</a></li>
              <li class="@yield('repair_active')"><a href="{!! route('webuketuke.index') !!}">修理の状況</a></li>
              <li class="@yield('search_active')"><a href="{!! route('webuketuke.showSearch') !!}">修理データの検索</a></li>
          </ul>
      </div>
    </nav>
  </div><!--/main-menu_sv-->
