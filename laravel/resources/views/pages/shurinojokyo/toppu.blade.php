@extends('layouts.master')
@section('repair_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理の状況')
@section('content')
  <!-- * start row top * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>修理の状況</h1>
        </div>
    </div>
  </div>
  <!-- * end row top * -->
  <div class="row-sv">
    <div id="box_list" class="box_list5_1">
    <!-- * Start div top_content_5-1 * -->
    <div id="top_content_5-1">
        <div class="top_content_5-1_inner">
            <div class="col-sv-7 padding0">
                <div class="title_gradio float_left">
                    <span>絞り込み</span>
                </div>
                <div class="group_radio float_left mgr30">
                    <input type="radio" id="refine_all" name="refine_item" checked>
                    <label for="refine_all">すべて</label>
                </div>
                <div class="group_radio float_left mgr30">
                    <input type="radio" id="refine_incomplete" name="refine_item">
                    <label for="refine_incomplete">未完了</label>
                </div>
                <div class="group_radio float_left mgr30">
                    <input type="radio" id="refine_cancel" name="refine_item">
                    <label for="refine_cancel">キャンセル</label>
                </div>
                <div class="group_radio float_left mgrl20">
                    <input type="radio" id="refine_done" name="refine_item">
                    <label for="refine_done">完了</label>
                    <label class="lbl-5-1">完了は本日を含め過去10 日分が検索されます</label>
                </div>
            </div>
            <div class="col-sv-5 just_content">
                <a href="#"><div class="radius_btn"><span>修理状況を絞り込む</span></div></a>
            </div>
        </div>
    </div>
    <div class="row-sv-12 pdding_btt39 defualt_w_5-1">
        <div class="box_count">
            <div class="show_count">
                <div class="col-sv-5 count_text padding0 ">
                    <span>受付中</span>
                </div>
                <div class="col-sv-6 padding0 one">
                    <span>25 </span>件

                </div>
            </div>
        </div>
        <div class="box_count">
            <div class="col-sv-5 count_text padding0 ">
                <span>完了</span>
                </div>
            <div class="col-sv-6 padding0 two">
                <span>25 </span>件
            </div>
        </div>
    </div>
    <!-- * End div top_content_5-1 * -->
    <!-- * Start div table data * -->
    <div id="tbl_list" class="mrgl28 mrg_bttm">
        <table>
          <tbody>
            <tr>
                <th class="head_col1">ステータス</th>
                <th class="head_col1">申込No</th>
                <th class="head_col1">登録日</th>
                <th class="head_col1">修理 予定日</th>
                <th class="head_col1">受付 No</th>
                <th class="head_col5">商品</th>
                <th class="head_col5">お客さま住所</th>
                <th class="head_col4">修理 担当者</th>
                <th class="head_col1">依頼内容</th>
            </tr>
          </tbody>
          {{-- @foreach($shurinojokyos as $shurinojokyo) --}}
            <tr class="row">
                <td class="center">
                    <div class="tooltip active">
                        <span class="btn_intbl">受付中</span>
                        <span class="tooltiptext">受付センターにて処理中の状態です。</span>
                    </div>
                </td>
                <td class="center"><span>WB12032628038</span></td>
                <td class="center">2017/6/1</td>
                <td class="center">2017/6/15</td>
                <td class="center">WB12032628038</td>
                <td class="text-left">INAX ディスポーザー</td>
                <td class="text-left">北海道札幌市北区…</td>
                <td class="center">山田</td>
                <td class="center btn_5-1"><a href="#"><div>詳細</div></a></td>
            </tr>
          {{-- @endforeach --}}
        </table>
    </div>
    <!-- * End div table data * -->
    <div id="pagination_wrapper">
        <div class="count">
            <span>100 件中 10 件を表示しています</span>
        </div>
        <div class="pagination">
            <ul>
                <li class="active"><a href="#"><span>1</span></a></li>
                <li><a href="#"><span>2</span></a></li>
                <li><a href="#"><span>3</span></a></li>
            </ul>
        </div>
    </div>
  </div>
  </div>
  <!-- * End div list head table * -->
@stop
