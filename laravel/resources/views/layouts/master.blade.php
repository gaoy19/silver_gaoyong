<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<!-- * start section head * -->
@include('includes.head')
<!-- * end section head* -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrapper_silver">
  <!-- * start section header * -->
  @include('includes.header')
  <!-- * end section container  * -->
  <section class="container-silver">
    @yield('content')
  </section>
  <!-- * end section container  * -->
  <!-- * start section footer * -->
  <footer id="footer-silver">
    @include('includes.footer')
  </footer><!--/footer_sv-->
  <!-- * end section footer * -->
</div><!--/wrapper_silver-->
<!--canvas-lixil-menu-->
<div class="canvas-lixil-menu"></div>
<!--script.main.js-->
<script type="text/javascript" src="{{ asset('assets/js/script.main.js') }}"></script>
</body>
</html>
