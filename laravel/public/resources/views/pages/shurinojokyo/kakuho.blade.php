@extends('layouts.master')
@section('repair_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理の状況[経過・結果報告（確報）]')
@section('content')
  <div class="row-sv">
   <div class="main-vs-12 sv_top20" style="margin-bottom:70px;">
      <div class="col-rs-title border-bot">
        <div class="col-sv-7">
           <h2>経過・結果報告（確報）</h2>
        </div>
        <div class="col-sv-5">
            <div class="col-print-5_rs row-15">
                <button class="btn-print" type="button" onclick="showPrintForm()"><span class="icon-print"><img src="/assets/images/print.png" width="18"></span>報告書を印刷する</button>
            </div>
        </div>
      </div><!--/border-bot 経過・結果報告（詳細）-->
      <!-- * start section 1 訪問予定報告 * -->
      <div class="box-main-form">
       <div class="box-data-12">
                <div class="tb-responsive_rs">
                   <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                       <tbody class="lixil_body">
                           <tr>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>登録日</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">2017/6/1</div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>受付 No</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">WB12032628038</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>担当者</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">北海道札幌市</div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label></label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs"></div>
                               </td>
                           </tr><!--/result-->
                       </tbody>
                    </table>
               </div><!--/tb-responsive-->
           </div><!--/box-data-12-->
      </div><!--/box-main-form1-->
      <!-- * end section 1 訪問予定報告 * -->
      <!-- * start section 2 経過・結果情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>確定情報</h5>
           </div>
         </div>
        <div class="con-info_sv">
           <div class="tb-responsive">
               <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                   <tbody class="lixil_body">
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>結果報告</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>訪問実施日</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>簡易修理内容</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>明細</label></div>
                         </th>
                         <td class="col-data">
                            <div class="col-result">
                              <table class="list-kakuho">
                                  <thead>
                                    <tr>
                                      <th style="width:138px;">部品品番</th>
                                      <th style="width:168px;">部品名</th>
                                      <th style="width:150px;">単価</th>
                                      <th style="width:128px;">個数</th>
                                      <th style="width:150px;">金額</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>金額</td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>金額</td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>金額</td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                  </tbody>
                                  <tfoot>
                                    <tr>
                                      <td class="kakuho_result" colspan="2">
                                        <div class="result"></div>
                                      </td>
                                      <td>合計</td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                  </tfoot>
                              </table>
                            </div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>処置部位・内容</label></div>
                         </th>
                         <td class="col-data">
                            <div class="col-result">
                              <table class="list-kakuho">
                                <tbody>
                                  <tr>
                                    <td style="width:50%">額</td>
                                    <td style="width:50%"></td>
                                  </tr>
                                  <tr>
                                    <td style="width:50%">金</td>
                                    <td style="width:50%"></td>
                                  </tr>
                                </tbody>
                              </table><!--/list-kakuho-->
                            </div>
                         </td>
                     </tr><!--/result-->
                   </tbody>
              </table>
        </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_2-->
      <!-- * end section 2 経過・結果情報 * -->
      <!-- * start section 3 お客さま情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>お客さま情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
          <div class="tb-responsive">
           <table class="detailList" border="0" cellpadding="0" cellspacing="0">
               <tbody class="lixil_body">
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま氏名</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま住所</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま電話番号</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま建物名・部屋番号</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">入力内容を表示</div>
                     </td>
                 </tr><!--/result-->
               </tbody>
          </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_3--><!--/box-main-form-2-->
      <!-- * end section 3 お客さま情報 * -->
      <!-- * start section 4 修理依頼内容 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>修理依頼内容</h5>
           </div>
        </div>
        <div class="con-info_sv">
             <div class="tb-responsive">
               <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                   <tbody class="lixil_body">
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>商品名</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>品番</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>ご依頼内容</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>完・未完の別</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">入力内容を表示</div>
                         </td>
                     </tr><!--/result-->
                   </tbody>
              </table>
         </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_4-->
      <!-- * end section 4 修理依頼内容 * -->
      <!-- * start section 5 ご依頼元情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>ご依頼元情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
          <div class="tb-responsive">
            <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                 <tbody class="lixil_body">
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>得意先コード</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>貴社名</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼元担当者様</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>部署住所</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>部署電話番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>部署FAX</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>お得意先顧客No</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                 </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_5-->
      <!-- * end section 5 ご依頼元情報 * -->
      <!-- * start section 5 ご依頼元情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>ご請求先情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
          <div class="tb-responsive">
            <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                 <tbody class="lixil_body">
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>得意先コード</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼時ご請求先氏名</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご請求先担当者</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼時ご請求先住所</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼時ご請求先電話番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼時ご請求先FAX番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                 </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_5-->
      <!-- * end section 6 ご請求先情報 * -->
      <!-- * start section 7 工事店 * -->
      <div class="box-main-form">
        <div class="con-info_sv">
          <div class="tb-responsive">
            <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                 <tbody class="lixil_body">
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>注文番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>工事店</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>作業内容</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>お客様へのメッセージ</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">入力内容を表示</div>
                       </td>
                   </tr><!--/result-->
                 </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_5-->
      <!-- * end section 7 ご請求先情報 * -->
      <div class="read-info_center center">
          <a class="btn-info_rs" href="{{ url()->previous() }}">前に戻る</a>
      </div><!--/read-info_center-->
   </div><!--/main-vs-12 sv_top20-->
  </div><!--/row-sv-->
  <script type="text/javascript">
    //-------------------- 追加　2007/03/12　start --------------------//
    function showPrintForm() {
      window.open("{{ URL::to('shurinojokyo/finalreport') }}", 'preliminaryreport', "top=0,left=0,height=650,width=1050,location=no,scrollbars=yes,toolbar=yes,menubar=yes,status=no");
    }
    //-------------------- 追加　2007/03/12　end   --------------------//
  </script>
@stop
