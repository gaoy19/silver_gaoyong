<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Iraimoto;

class HomesController extends Controller
{
  /**
 * Call Model Iraimoto
 *
 * @param $model
 */
  public function __construct(Iraimoto $model)
  {
    $this->model = $model;
  }
  /**
  * Display a listing of イライモト.
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    //get data
    $iraimotos = $this->model->getIraimoto();
    // $iraimotos['iraimotos'] = $iraimotos;
    $messages = DB::table('WEB_MESSAGE')
                ->where('sakujo_flg','=','0')
                ->whereDate('kaisi','<=',date('Y-m-d').' 00:00:00')
                ->whereDate('syuryo','>',date('Y-m-d').' 00:00:00')
                ->whereIn('MESSAGE_KBN', [2, 3])
                ->select('title','naiyo')
                ->orderBy('message_id','DESC')->limit(5)
                ->get()->toArray();
    return view('home')->with(['iraimotos'=>$iraimotos,'messages'=>$messages]);
  }
  public function showShiyofukamoji()
  {
    return view('pages.shiyofukamoji');
  }
}
