<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class SessionLoginTimeout
{
    protected $session;
    protected $timeout = 3600; //3600 = 1 hour

    /**
   * Call Model Iraimoto
   *
   * @param $model
   */
    public function __construct(Store $session)
    {
      $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isLoggedIn = $request->path() != 'logout';

        if(! session('lastActivityTime')) {
          $this->session->put('lastActivityTime', time());
        } elseif (time() - $this->session->get('lastActivityTime') > $this->timeout) {
            $this->session->forget('lastActivityTime');
            \Auth::logout();
            \Session::flash('loginTimeout', 'セッションは中断されました。再度ログインしてください');
            return \Redirect::route('login');
        }
        $this->session->put('lastActivityTime', time());

        return $next($request);
    }
}
