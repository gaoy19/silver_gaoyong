@extends('layouts.master')
@section('home_active','active_sv')
@section('title','LIXILインターネット修理受付センター | トップページ')
@section('content')
  <!-- * start row top * -->
  <div class="row-sv">
      <div class="col-sv-12 pdd61">
          <div class="box-top">
              <div class="col-sv-8 width580 padding0">
                   <div class="top_text_left">
                      インターネット修理受付センターは、LIXIL水まわり商品の修理依頼窓口です。
                  </div>
              </div>
              <div class="col-sv-4 width320 padding0">
                <a href="{!! route('iraimoto.kokyaku') !!}">
                  <div class="top_text_right">
                      <div class="text_inner">
                          修理のご依頼
                      </div>
                  </div>
                </a>
              </div>
          </div>
      </div>
  </div>
  <!-- * end row top * -->
  <!-- * Start block section お知らせ * -->
  <div id="act_footer">
      <div class="row-sv">
          <div class="act_wrapper">
              <div class="act_head_inner">
                  <div class="h2">お知らせ</div>
              </div>
              <ul class="col-msg-12">
              @foreach($messages as $message)
                <li>
                  <div class="sub_title">@php echo $message->title  @endphp</div>
                  <div class="txt">
                      <p>@php echo $message->naiyo @endphp</p>
                  </div>
                </li>
              @endforeach
              </ul>
          </div>
      </div>
  </div>
  <!-- * Start block section お知らせ * -->
  <!-- * Start section list head table * -->
  <section id="box_list">
      <div class="col-sv-12 padding0">
          <div class="header_list_box">
              <div class="col-sv-6 padding0">
                  <div class="text-mid-box">
                      ご依頼の修理の状況
                  </div>
              </div>
              <div class="col-sv-6 padding0">
                  <div class="text-mid-box_right">
                      最新 <span class="col_orang">{{ count($iraimotos) }} </span>件を表示しています
                  </div>
              </div>
          </div>
          <div class="hr"></div>
      </div>
      <!-- * Start section table data * -->
      <div id="tbl_list">
          <table class="table-striped_sv">
            <tr>
                <th class="head_col1 tbl3-1">ステータス</th>
                <th class="head_col2 tbl3-1" style="width:100px;">申込No</th>
                <th class="head_col2 tbl3-1" style="width:100px;">登録日／<br>修理予定日</th>
                <th class="head_col1 tbl3-1" style="width:100px;">受付 No</th>
                <th class="head_col5 tbl3-1" style="width:100px;">商品</th>
                <th class="head_col4 tbl3-1">お客さま名</th>
                <th class="head_col5 tbl3-1">お客さま住所</th>
                <th class="head_col4 tbl3-1">ご依頼元<br />担当者様</th>
            </tr>
            <tbody>
              @foreach($iraimotos as $iraimoto)
              <tr class="row tbl3-1">
                  <td class="col1 tooltip">
                    <div class="btn_intbl active">{{ $iraimoto->jyotai }}
                      @if($iraimoto->jyotai == '連絡待')
                        <span class="tooltiptext">お客様からの連絡待ちの状態です。</span>
                      @elseif($iraimoto->jyotai == '部品待')
                        <span class="tooltiptext">お客様訪問の結果、部品の在庫がなく、その部品を手配中の状態です。</span>
                      @elseif($iraimoto->jyotai == '不在')
                        <span class="tooltiptext">お客様がご不在だった時の状態です。</span>
                      @elseif($iraimoto->jyotai == '指定日')
                        <span class="tooltiptext">お客様から修理訪問日の指定があった状態です。</span>
                      @elseif($iraimoto->jyotai == '打合日')
                        <span class="tooltiptext">お客様とのお打ち合わせ日が決定した状態です。</span>
                      @elseif($iraimoto->jyotai == '帰宅遅')
                        <span class="tooltiptext">修理担当者へ連絡中の状態です。</span>
                      @elseif($iraimoto->jyotai == '日時待')
                        <span class="tooltiptext">お客様からの訪問日時連絡待ちの状態です。</span>
                      @elseif($iraimoto->jyotai == '見積待')
                        <span class="tooltiptext">見積もり提出中でお客様からの連絡待ちの状態です。</span>
                      @elseif($iraimoto->jyotai == '納期待')
                        <span class="tooltiptext">部品又は商品発注済で、工場からの納期連絡待ちの状態です。</span>
                      @elseif($iraimoto->jyotai == '問合待')
                        <span class="tooltiptext">お客様から技術、部品問い合せ中で確認中の状態です。</span>
                      @elseif($iraimoto->jyotai == '製品待')
                        <span class="tooltiptext">製品到着待ちの状態です。</span>
                      @elseif($iraimoto->jyotai == '受付中')
                        <span class="tooltiptext">受付センターにて処理中の状態です。</span>
                      @elseif($iraimoto->jyotai == '確認中')
                        <span class="tooltiptext">受付センターにて内容を確認中の状態です。</span>
                      @elseif($iraimoto->jyotai == '未着手')
                        <span class="tooltiptext">お客様訪問担当者が確定したばかりの状態です。</span>
                      @elseif($iraimoto->jyotai == '')
                        <span class="tooltiptext">お客様訪問の結果が特に入力されていない状態です。</span>
                      @endif
                      </div>
                  </td>
                  <td class="center">{{ $iraimoto->web_no }}</td>
                  <td class="col2 center"><span>{{ $iraimoto->uketukebi != '' ? Carbon\Carbon::parse($iraimoto->uketukebi)->format('Y/m/d') : '' }}
                    <br>{{ $iraimoto->syuriyoteibi != '' ? Carbon\Carbon::parse($iraimoto->syuriyoteibi)->format('Y/m/d') : '' }}</span></td>
                  <td class="center">{{ $iraimoto->uketuke_no != '' ? substr($iraimoto->uketuke_no,0,4).'-'.substr($iraimoto->uketuke_no,4,8): '' }}</td>
                  <td class="text-left">{{ $iraimoto->hinban }}</td>
                  <td class="text-left">{{ $iraimoto->kokyaku_sei }} {{ $iraimoto->kokyaku_na }}</td>
                  <td class="text-left" >{{ $iraimoto->kokyaku_jusyo }}</td>
                  <td class="center">{{ $iraimoto->iraimoto_tanto }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <!-- * Start section table data * -->
          <div class="footer_tbl">
              <div class="row-12 right">
                <a href="{!! route('webuketuke.index') !!}">
                  <div class="btn_footer">
                      <div class="lbl_radius">
                          <div class="h1">修理状況の一覧を見る </div>
                      </div>
                  </div>
                </a>
              </div>
          </div>
      </div>
  </section>
  <!-- * End section list head table * -->
@stop
