<?php

namespace App\Http\Controllers\rcpUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\User;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use App\RequestData;
use Session;
class ConvertOldPasswordController extends Controller
{
    public function convertOldPassword()
    {
        $users = User::whereNull('password')
            ->orderBy('user_id')
            ->take(100)
            ->get();

        foreach ($users as $user){

                $user->password = bcrypt($user->passwd);
                echo($user->user_id);
                echo nl2br("\n");
                $user->update();
        }
    }
}
