@extends('layouts.masterReset')

@section('title','LIXILインターネット修理受付センター | パスワードを忘れた方へ')

@section('content')
  <div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>パスワードの変更</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-6 active_sv" style="padding-right:20px;">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">パスワードの再設定</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-6" style="padding-left:20px;">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">パスワードの再設定完了</div>
                </div>
            </li><!--/box-sv-3-->

        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
  <div class="main-vs-12 sv_top20">
    @if(isset($changePassword) || $changePassword != null || count($changePassword) > 0)
    <form id="web_customer_info" method="POST" action="{{ route('user_password.resetpassword') }}" accept-charset="UTF-8" role="form">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $changePassword->hash }}">
        <div class="box-main-form">
            <div class="modal-header_sv">
                <h5>パスワードを設定してください。</h5>
            </div>
            <div class="con-info_sv">
                <div class="tb-form_sv width100">
                  <div class="tr-form_sv width100">
                     <div class="td-form_sv-4">
                         <div class="col-ft-left">
                             <div class="td-title">新しいパスワード</div>
                             <span class="error_label">必須</span>
                         </div>
                     </div><!--/td-form_sv-4-->
                     <div class="td-form_sv-8">
                         <div class="col-ft-right">
                             <div class="col-td-12">
                                @if ($errors->has('password'))
                                 <div class="error_sv max-100"><span>{{ $errors->first('password') }}</span></div>
                                @endif
                                <input type="password" class="form-control_sv{!! $errors->has('password') ? ' input-error' : '' !!}" id="password" name="password" style="max-width:300px;" required>
                                <div class="label-info-12 col-top10">
                                   <div class="des-info"><span><img src="/assets/images/note-title.png" alt="">   </span>半角数字と半角英字を組み合わせ、4〜20文字で指定してください
                                   </div>
                                </div>
                             </div><!--/col-td-12-->
                         </div>
                     </div><!--/td-form_sv-8-->
                  </div><!--/tr-form_sv-->
                  <div class="tr-form_sv width100">
                      <div class="td-form_sv-4 tb-fonm-none">
                          <div class="col-ft-left">
                              <div class="td-title">新しいパスワード（再入力）</div>
                              <span class="error_label">必須</span>
                          </div>
                      </div><!--/td-form_sv-4-->
                      <div class="td-form_sv-8 tb-fonm-none">
                          <div class="col-ft-right">
                              <div class="col-td-12">
                                @if ($errors->has('password_confirmation'))
                                 <div class="error_sv max-100"><span>{{ $errors->first('password_confirmation') }}</span></div>
                                @endif
                                <input type="password" class="form-control_sv" id="password-confirm" name="password_confirmation" style="max-width:300px;" required>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt="">   </span>新しいパスワードを再度入力してください
                                    </div>
                                </div>
                              </div><!--/col-td-12-->
                          </div>
                      </div><!--/td-form_sv-8-->
                  </div><!--/tr-form_sv-->
                </div>
            </div>
        </div><!--/box-main-form-->
        <div class="col-sv-12 sv_center">
          <button type="submit" class="btn btn-sv" style="width: 320px;">パスワードを再設定する</button>
        </div>
    </form><!--/web_customer_info-->
    @else
    <div class="box-main-form">
        <div class="modal-header_sv">
            <h5>エラーメッセージ</h5>
        </div>
        <div class="con-info_sv col-top10">
          <div class="error_sv max-100" style="max-width: 500px;margin: 30px auto;">
            <span>不可能なトーケンコードです。</span>
          </div>
        </div>
      </div>
    @endif
  </div>
</div>
@stop
