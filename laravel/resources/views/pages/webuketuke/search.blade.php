@extends('layouts.master')
@section('search_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理データの検索[修理データの条件指定検索]')
@section('content')
  <!--  * start section container  * -->
  <div class="row-sv">
    <div class="main-vs-12 sv_top20">
      <form id="web_customer_info" action="{{ route('webuketuke.search') }}" method="GET" accept-charset="UTF-8" role="form">
        <div class="box-title_sv">
            <h1>修理データの条件指定検索</h1>
        </div>
        <div class="col-show-info">
            <p>本日より 6 ヶ月前までの修理データが検索できます </p>
        </div>
       <div class="box-main-form">
            <div class="modal-header_rs">
                <div class="col-sv-10">
                    <h5>修理の状況から探す</h5>
                </div>
            </div>
            <!--選択してください-->
            <div class="con-info_sv col-toggle-12">
                <div class="tb-form_sv">
                    <!--選択してください -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2 tb-fonm-none">
                            <div class="col-ft-left">
                                <div class="td-title">選択してください</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10 tb-fonm-none">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  <div class="group_radio">
                                      <input type="radio" id="product_incomplete" name="kubun" value="0" @if(isset($request['kubun']) && $request['kubun'] == '0') checked="" @endif>
                                      <label for="product_incomplete">未完了</label>
                                    </div>
                                    <div class="group_radio">
                                        <input type="radio" id="product_done" name="kubun" value="1" @if(isset($request['kubun']) && $request['kubun'] == '1') checked="" @endif>
                                        <label for="product_done">完了</label>
                                    </div>
                                    <div class="group_radio">
                                        <input type="radio" id="product_cancel" name="kubun" value="3" @if(isset($request['kubun']) && $request['kubun'] == '3') checked="" @endif>
                                        <label for="product_cancel">キャンセル</label>
                                    </div>
                                    <div class="group_radio">
                                        @if(isset($request['kubun']) && $request['kubun'] == '2' || !isset($request['kubun']))
                                        <input type="radio" id="product_all" name="kubun" value="2" checked="">
                                        @else
                                        <input type="radio" id="product_all" name="kubun" value="2">
                                        @endif
                                        <label for="product_all">すべて</label>
                                    </div>
                                </div>
                            </div><!--/col-ft-right-->
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                </div><!--/tb-form_sv-->
            </div>
        </div><!--/box-main-form-->
        <div class="row-sv">
           <div class="col-sv-12">
               <div class="btn-center">
                    <input type="submit" class="btn-confirm" id="inquiry_customer" value="検索する">
                </div>
            </div>
        </div>
        <!--受付状況から探す-->
        <div class="box-main-form">
            <div class="modal-header_rs">
                <div class="col-sv-10">
                    <h5>受付状況から探す</h5>
                </div>
            </div>
            <!--選択してください-->
            <div class="con-info_sv col-toggle-12">
                <div class="tb-form_sv">
                    <!--選択してください -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">担当者</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                    @if ($errors->has('tanto'))
                                      <div class="error_sv max-100"><span>{{ $errors->first('tanto') }}</span></div>
                                    @endif
                                    @if(old('tanto'))
                                    <input type="text" value="{{ old('tanto') }}" class="form-control_sv ime-active{!! $errors->has('tanto') ? ' input-error' : '' !!}" id="tanto" name="tanto" style="max-width:500px;" maxlength="40">
                                    @else
                                    <input type="text" value="@if(isset($request['tanto'])){{ $request['tanto'] }}@endif" class="form-control_sv ime-active{!! $errors->has('tanto') ? ' input-error' : '' !!}" id="tanto" name="tanto" style="max-width:500px;" maxlength="40">
                                    @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>姓から可能な部分まで全角で入力してください</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                    <!--受付 No-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">受付 No</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('uketuke_no'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('uketuke_no') }}</span></div>
                                  @endif
                                  @if(old('uketuke_no'))
                                  <input type="text" value="{{ old('uketuke_no') }}" class="form-control_sv ime-disabled{!! $errors->has('uketuke_no') ? ' input-error' : '' !!}" id="uketuke_no" name="uketuke_no" maxlength="12" style="max-width:300px;">
                                  @else
                                  <input type="text" value="@if(isset($request['uketuke_no'])){{ $request['uketuke_no'] }}@endif" class="form-control_sv ime-disabled{!! $errors->has('uketuke_no') ? ' input-error' : '' !!}" id="uketuke_no" name="uketuke_no" maxlength="12" style="max-width:300px;">
                                  @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>半角で入力してください。ハイフン（-）は不要です</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                    <!--受付日-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">受付日</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-item-3" style="max-width:120px;width:120px;">
                                    <select name="uketukebiFromYYYY" class="form-control_sv" style="width:110px;">
                                      @foreach ($uketukebiYYYY as $item)
                                        @if(old('uketuke_no'))
                                          <option value="{{ $item['value'] }}" {{ ( old('uketukebiFromYYYY') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @else
                                          <option value="{{ $item['value'] }}" @if(isset($request['uketukebiFromYYYY']) && $request['uketukebiFromYYYY'] == $item['value']) selected @endif>{{ $item['text'] }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                <div class="col-item-1">
                                    <span>年</span>
                                </div>
                                <div class="col-item-2">
                                    <select name="uketukebiFromMM" class="form-control_sv" style="max-width:71px;">
                                      @foreach ($uketukebiMM as $item)
                                        @if(old('uketukebiFromMM'))
                                          <option value="{{ $item['value'] }}" {{ ( old('uketukebiFromMM') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @else
                                          <option value="{{ $item['value'] }}" @if(isset($request['uketukebiFromMM']) && $request['uketukebiFromMM'] == $item['value']) selected @endif>{{ $item['text'] }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                 <div class="col-item-1">
                                    <span>月</span>
                                </div>
                                <div class="col-item-2">
                                    <select name="uketukebiFromDD" class="form-control_sv" style="max-width:71px;">
                                      @foreach ($uketukebiDD as $item)
                                        @if(old('uketukebiFromDD'))
                                          <option value="{{ $item['value'] }}" {{ ( old('uketukebiFromDD') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @else
                                          <option value="{{ $item['value'] }}" @if(isset($request['uketukebiFromDD']) && $request['uketukebiFromDD'] == $item['value']) selected @endif>{{ $item['text'] }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                <div class="col-item-2">
                                    <span>日～</span>
                                </div>
                                 <div class="col-item-3" style="max-width:120px;width:120px;">
                                    <select name="uketukebiToYYYY" class="form-control_sv" style="width:110px;">
                                      @foreach ($uketukebiYYYY as $item)
                                        @if(old('uketukebiToYYYY'))
                                          <option value="{{ $item['value'] }}" {{ ( old('uketukebiToYYYY') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @else
                                          <option value="{{ $item['value'] }}" @if(isset($request['uketukebiToYYYY']) && $request['uketukebiToYYYY'] == $item['value']) selected @endif>{{ $item['text'] }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                <div class="col-item-1">
                                    <span>年</span>
                                </div>
                                <div class="col-item-2">
                                    <select name="uketukebiToMM" class="form-control_sv" style="max-width:71px;">
                                      @foreach ($uketukebiMM as $item)
                                        @if(old('uketukebiToMM'))
                                          <option value="{{ $item['value'] }}" {{ ( old('uketukebiToMM') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @else
                                          <option value="{{ $item['value'] }}" @if(isset($request['uketukebiToMM']) && $request['uketukebiToMM'] == $item['value']) selected @endif>{{ $item['text'] }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                 <div class="col-item-1">
                                    <span>月</span>
                                </div>
                                <div class="col-item-2">
                                    <select name="uketukebiToDD" class="form-control_sv" style="max-width:71px;">
                                      @foreach ($uketukebiDD as $item)
                                        @if(old('uketukebiToDD'))
                                          <option value="{{ $item['value'] }}" {{ ( old('uketukebiToDD') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                        @else
                                          <option value="{{ $item['value'] }}" @if(isset($request['uketukebiToDD']) && $request['uketukebiToDD'] == $item['value']) selected @endif>{{ $item['text'] }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                 <div class="col-item-1">
                                    <span>日</span>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                    <!--依頼元注番-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">貴社注文番号</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('tyumon_bango'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('tyumon_bango') }}</span></div>
                                  @endif
                                  @if(old('tyumon_bango'))
                                    <input type="text" value="{{ old('tyumon_bango') }}" class="form-control_sv ime-inactive{!! $errors->has('tyumon_bango') ? ' input-error' : '' !!}" id="tyumon_bango" name="tyumon_bango" maxlength="50" style="max-width:300px;">
                                  @else
                                    <input type="text" value="@if(isset($request['tyumon_bango'])){{ $request['tyumon_bango'] }}@endif" class="form-control_sv ime-inactive{!! $errors->has('tyumon_bango') ? ' input-error' : '' !!}" id="tyumon_bango" name="tyumon_bango" maxlength="50" style="max-width:300px;">
                                  @endif
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                    <!--お申し込み No-->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2 tb-fonm-none">
                            <div class="col-ft-left">
                                <div class="td-title">お申し込み No</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10 tb-fonm-none">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('web_uketuke_no'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('web_uketuke_no') }}</span></div>
                                  @endif
                                  @if(old('web_uketuke_no'))
                                    <input type="text" value="{{ old('web_uketuke_no') }}" class="form-control_sv ime-disabled{!! $errors->has('web_uketuke_no') ? ' input-error' : '' !!}" id="web_uketuke_no" name="web_uketuke_no"  maxlength="13" style="max-width:300px;">
                                  @else
                                    <input type="text" value="@if(isset($request['web_uketuke_no'])){{ $request['web_uketuke_no'] }}@endif" class="form-control_sv ime-disabled{!! $errors->has('web_uketuke_no') ? ' input-error' : '' !!}" id="web_uketuke_no" name="web_uketuke_no"  maxlength="13" style="max-width:300px;">
                                  @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>半角で入力してください。ハイフン（-）は不要です</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                </div><!--/tb-form_sv-->
            </div>
        </div><!--/box-main-form-->
        <!--お客さま情報から探す-->
        <div class="box-main-form">
            <div class="modal-header_rs">
                <div class="col-sv-10">
                    <h5>お客さま情報から探す</h5>
                </div>
            </div>
            <!--選択してください-->
            <div class="con-info_sv col-toggle-12">
                <div class="tb-form_sv">
                    <!--選択してください -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま名（漢字）</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('kokyaku_sei_kanji'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('kokyaku_sei_kanji') }}</span></div>
                                  @endif
                                  @if(old('kokyaku_sei_kanji'))
                                    <input type="text" value="{{ old('kokyaku_sei_kanji') }}" class="form-control_sv ime-active{!! $errors->has('kokyaku_sei_kanji') ? ' input-error' : '' !!}" id="kokyaku_sei_kanji" name="kokyaku_sei_kanji" style="max-width:500px;">
                                  @else
                                    <input type="text" value="@if(isset($request['kokyaku_sei_kanji'])){{ $request['kokyaku_sei_kanji'] }}@endif" class="form-control_sv ime-active{!! $errors->has('kokyaku_sei_kanji') ? ' input-error' : '' !!}" id="kokyaku_sei_kanji" name="kokyaku_sei_kanji" style="max-width:500px;">
                                  @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>姓のみ全角で入力してください</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                    <!--選択してください -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま名（フリガナ）</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('kokyaku_sei_kana'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('kokyaku_sei_kana') }}</span></div>
                                  @endif
                                  @if(old('kokyaku_sei_kana'))
                                    <input type="text" value="{{ old('kokyaku_sei_kana') }}" class="form-control_sv ime-active{!! $errors->has('kokyaku_sei_kana') ? ' input-error' : '' !!}" id="kokyaku_sei_kana" name="kokyaku_sei_kana" style="max-width:500px;">
                                  @else
                                    <input type="text" value="@if(isset($request['kokyaku_sei_kana'])){{ $request['kokyaku_sei_kana'] }}@endif" class="form-control_sv ime-active{!! $errors->has('kokyaku_sei_kana') ? ' input-error' : '' !!}" id="kokyaku_sei_kana" name="kokyaku_sei_kana" style="max-width:500px;">
                                  @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>姓のみ全角で入力してください</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                     <!--お客さま都道府県 -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま都道府県</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                    <select name="pref" class="form-control_sv" style="max-width:180px;">
                                        <option value="0">選択してください</option>
                                        @foreach (Cache::get('todoufukens') as $todoufuken)
                                        @if(old('pref'))
                                          <option value="{{ $todoufuken }}" {{ ( old('pref') == $todoufuken ) ? ' selected' : '' }}>{{ $todoufuken }}</option>
                                        @else
                                          <option value="{{ $todoufuken }}" @if(isset($request['pref']) && $request['pref'] == $todoufuken) selected @endif>{{ $todoufuken }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                    <!--お客さま住所 -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま住所</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('kokyaku_jusyo'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('kokyaku_jusyo') }}</span></div>
                                  @endif
                                  @if(old('kokyaku_jusyo'))
                                    <input type="text" value="{{ old('kokyaku_jusyo') }}" class="form-control_sv ime-active{!! $errors->has('kokyaku_jusyo') ? ' input-error' : '' !!}" id="kokyaku_jusyo" name="kokyaku_jusyo" style="max-width:500px;">
                                  @else
                                    <input type="text" value="@if(isset($request['kokyaku_jusyo'])){{ $request['kokyaku_jusyo'] }}@endif" class="form-control_sv ime-active{!! $errors->has('kokyaku_jusyo') ? ' input-error' : '' !!}" id="kokyaku_jusyo" name="kokyaku_jusyo" style="max-width:500px;">
                                  @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>市町村名から可能な部分まで全角で入力して下さい</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                     <!--お客さま電話番号 -->
                    <div class="tr-form_sv">
                        <div class="td-form_sv-2 tb-fonm-none">
                            <div class="col-ft-left">
                                <div class="td-title">お客さま電話番号</div>
                            </div>
                        </div><!--/td-form_sv-4-->
                        <div class="td-form_sv-10 tb-fonm-none">
                            <div class="col-ft-right">
                                <div class="col-td-12">
                                  @if ($errors->has('tel'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('tel') }}</span></div>
                                  @endif
                                  @if(old('tel'))
                                    <input type="text" value="{{ old('tel') }}" class="form-control_sv ime-disabled{!! $errors->has('tel') ? ' input-error' : '' !!}" id="tel" name="tel" maxlength="13" style="max-width:300px;" onkeypress="return numberOnly(event);">
                                  @else
                                    <input type="text" value="@if(isset($request['tel'])){{ $request['tel'] }}@endif" class="form-control_sv ime-disabled{!! $errors->has('tel') ? ' input-error' : '' !!}" id="tel" name="tel" maxlength="13" style="max-width:300px;" onkeypress="return numberOnly(event);">
                                  @endif
                                </div>
                                <div class="label-info-12 col-top10">
                                    <div class="des-info"><span><img src="/assets/images/note-title.png" alt=""></span>市外局番から可能な部分まで半角で入力して下さい。ハイフン（-）は不要です</div>
                                </div>
                            </div>
                        </div><!--/td-form_sv-8-->
                    </div><!--/tr-form_sv-->
                </div>
            </div><!--/col-toggle-12-->
        </div><!--/box-main-form-->
        <div class="row-sv">
           <div class="col-sv-12">
               <div class="btn-center">
                    <input type="submit" class="btn-confirm" id="inquiry_customer" value="検索する">
                </div>
            </div>
        </div>
        {{ csrf_field() }}
      </form><!--form-->
    </div><!--/main-vs-12 sv_top20-->
  </div><!--/row-sv-->
  <script type="text/javascript">
  function numberOnly(evt)
    {
    var charCode = (evt.which) ? evt.which : event.keyCode;
      if (charCode > 31
      && (charCode < 48 || charCode > 57))
       return false;

    return true;
    }
  </script>
  <!--  * end section container  * -->
@stop
