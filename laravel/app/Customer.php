<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $primaryKey = 'seq_no';
  public $table = 'D_HINMEIBETU_IRAINAIYO';
  public $timestamps = false;
  protected $fillable=[
    'hinmei',
    'seq_no',
    'irainaiyo_cd',
    'torokubi',
    'torokusya_cd',
    'kosinbi',
    'kosinsya_cd',
    'sakujo_flg'
  ];
  public static function boot()
  {
    parent::boot();
  }
}
