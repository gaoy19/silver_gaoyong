<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebLxSyohinHinmei;
use App\WebIxSyohinHinmei;
use App\WebTsSyohinHinmei;
use App\WebSwSyohinHinmei;
use App\RequestData;
use Session;
use Redirect;
use Validator;

class BrandController extends Controller
{
    public function brand(Request $request)
    {
      $json_parameter = $_POST['brand'];
        // ajaxで商品名を取得
        if ($json_parameter == '0') {
            $seihin_daibunrui = \App\WebLxSyohinHinmei::getLists();
        } elseif ($json_parameter == '1') {
            $seihin_daibunrui = \App\WebIxSyohinHinmei::getLists();
        } elseif ($json_parameter == '2') {
            $seihin_daibunrui = \App\WebTsSyohinHinmei::getLists();
        } elseif ($json_parameter == '3') {
            $seihin_daibunrui = \App\WebSwSyohinHinmei::getLists();
          }
          Session::put('brand',$json_parameter);
          Session::put('seihin_daibunrui',$seihin_daibunrui);
            return response()->json($seihin_daibunrui);
    }
}