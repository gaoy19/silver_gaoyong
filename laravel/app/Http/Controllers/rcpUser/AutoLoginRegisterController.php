<?php

namespace App\Http\Controllers\rcpUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\User;
use App\Tokuisaki;
use App\Iraimoto;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use Mail;
use App\RequestData;
use Session;
use Hash;

class AutoLoginRegisterController extends Controller
{

  public function __construct(User $user)
  {
    $this->_user = $user;
  }
  /**
  * Display Alphas自動登録申請画面
  * @param $request
  * @return \Illuminate\Http\Response
  */
  public function showRegister(Request $request)
  {
    //check logo
    if(Auth::check()){
      return Redirect::route('login');
    }
    $data = Session::get('data');

    if(Session::has('data') || $data!=null){
      foreach ($data as $key => $value) {
          Session::put("_old_input.$key", $value);
      }
    }
    //view page Alphas自動登録画面
    return view('users.autologinregister.register.register');
  }

  private $user;

  public function register(Request $request)
  {
    //check logo
    if(Auth::check()){
      return Redirect::route('login');
    }

    $values = $request->all();
    $rules  = $this->_user->userRulesAlphas();
    $validator = \Validator::make($values,$rules,$this->_user->userErrorsMessageAlphas());

    if ($validator->fails()) {
      return redirect()->back()->withInput($values)->withErrors($validator);
    }

    $user = DB::table('WEB_RCP_USER')
      ->where('user_id','=',$request->user_id_Alphas)
      ->select('WEB_RCP_USER.user_simei','WEB_RCP_USER.tokuisaki_cd','WEB_RCP_USER.alphas_id','WEB_RCP_USER.info_mail_address', 'WEB_RCP_USER.password')
      ->first();

    //パスワード不一致の場合、エラーを返す。
    if (!Hash::check($request->passwd_Alphas, $user->password)) {
      $message = $this->_user->currentPasswordMsgNotExists();
      return Redirect::back()->withErrors(array('passwd_Alphas'=>$message))->withInput($values);
    }

    $tokuisaki = \App\Tokuisaki::TOKUISAKI_INFO_BY_TOKUISAKI_CD($user->tokuisaki_cd);
    $tokuisaki->tokuisaki_mei = $tokuisaki->tokuisaki_sei . $tokuisaki->tokuisaki_na;

    $alphas_id = 'ERROR:NO AlphasID from Alphas'; //セッションからAlphasIDが取得できない場合を想定
    if(session('alphasId')){
      $alphas_id = session('alphasId');
    }

    $mail_env = \Config::get('mail.from.address');
    date_default_timezone_set("Asia/Tokyo");
    $date_jp = date('Y/m/d H:i');
    Mail::to($mail_env)->send(new \App\Mail\RegisterMailAlphas([
      'date'=> $date_jp,
      'user_id'=> $request->user_id_Alphas,
      'user_simei'=> $user->user_simei,
      'tokuisaki_sei'=> $tokuisaki->tokuisaki_mei,
      'alphas_id'=> $alphas_id,
      'info_mail_address'=> $user->info_mail_address,
      'tokuisaki_tel_haihun'=> $tokuisaki->tokuisaki_tel_haihun
    ]));

    //view page Alphas自動登録申請完了画面
    $request->session()->forget('_old_input');
    $request->session()->flush();
    return view('users.autologinregister.register.complete');
  }
}
