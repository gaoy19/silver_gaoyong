<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebAlphasInfUser extends Model
{
  protected $table = 'WEB_ALPHAS_INF_USER';
  protected $primaryKey = 'ALPHAS_ID';
}
