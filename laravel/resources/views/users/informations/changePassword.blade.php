@extends('layouts.master')
@section('title','LIXILインターネット修理受付センター | 登録情報の変更 | パスワードの変更[1パスワードの変更]')
@section('content')
<!--  * start section container  * -->
<section class="container-silver">

    <div class="row-sv">
        <!-- * start row top * -->
        <div class="main-vs">
            <div class="box-title_sv">
                <h1>パスワードの変更</h1>
            </div>
        </div>
    </div><!--/row-sv-->
    <div class="row-sv">
        <div class="col-sv-12 box-progress-12">
            <ul class="row-box-bar row-20">
                <li class="box-sv-6 active_sv" style="padding-right: 20px;">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">1</div>
                        <div class="box-text-bar">パスワードの変更</div>
                    </div>
                </li><!--/box-sv-4-->
                <li class="box-sv-6" style="padding-left: 20px;">
                    <div class="box-progress-bar">
                        <div class="box-num-bar">2</div>
                        <div class="box-text-bar">パスワードの変更完了</div>
                    </div>
                </li><!--/box-sv-4-->
            </ul><!--/box-bar-->
        </div><!--/box-progress-12-->
    </div><!--/row-sv-->
    <div class="row-sv">
        <div class="main-vs sv_top20" style="margin-top: 0px;">
            <form id="web_customer_info" method="POST" action="{{ route('user.change.password') }}">
                {{ csrf_field() }}
                <div class="box-main-form">
                    <div class="con-info_sv" style="padding-bottom:0px;">
                        <div class="con-info-detail" style="padding-top: 22px;">パスワードが変更できます</div>
                            <div class="tb-form_sv">
                                <!--現在のパスワード-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">現在のパスワード</div>
                                            <span class="error_label">必須</span>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                @if ($errors->has('current_password'))
                                                 <div class="error_sv max-100"><span>{{ $errors->first('current_password') }}</span></div>
                                                @elseif (session()->has('current_password'))
                                                  <div class="error_sv max-100"><span>{{ session()->get('current_password') }}</span></div>
                                                @endif
                                                <input type="password" class="form-control_sv" id="current_password" name="current_password" style="max-width:300px;" />
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div>
                                <!--メールアドレス 1-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4">
                                        <div class="col-ft-left">
                                            <div class="td-title">新しいパスワード</div>
                                            <span class="error_label">必須</span>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                @if ($errors->has('new_password'))
                                                 <div class="error_sv max-100"><span>{{ $errors->first('new_password') }}</span></div>
                                                @endif
                                                <input type="password" class="form-control_sv" id="new_password" name="new_password" style="max-width:300px;">
                                            </div>
                                            <div class="label-info-12 col-top10">
                                                <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>半角数字と半角英字を組み合わせ、4〜20文字で指定してください</div>
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                                <!--メールアドレス 2-->
                                <div class="tr-form_sv">
                                    <div class="td-form_sv-4 tb-fonm-none">
                                        <div class="col-ft-left">
                                            <div class="td-title">新しいパスワード（再入力)</div>
                                            <span class="error_label">必須</span>
                                        </div>
                                    </div><!--/td-form_sv-4-->
                                    <div class="td-form_sv-8 tb-fonm-none">
                                        <div class="col-ft-right">
                                            <div class="col-td-12">
                                                @if ($errors->has('new_password_confirmation'))
                                                 <div class="error_sv max-100"><span>{{ $errors->first('new_password_confirmation') }}</span></div>
                                                @endif
                                                <input type="password" class="form-control_sv" id="new_password_re-enter" name="new_password_confirmation" style="max-width:300px;">
                                            </div>
                                            <div class="label-info-12 col-top10">
                                                <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>新しいパスワードを再度入力してください</div>
                                            </div>
                                        </div>
                                    </div><!--/td-form_sv-8-->
                                </div><!--/tr-form_sv-->
                            </div><!--/tb-info_sv-->
                       </div><!--/con-group-sv-->
                </div><!--/box-main-form-->
                <div class="btn-center-customer" style="margin-top:60px;">
                   <div class="sv_center">
                        <input type="submit" class="btn-confirm" id="confirm_changes" value="パスワードを変更する">
                   </div>
                   <div class="sv_center" style="margin-top:10px">
                        <a href="{{ route('user.change.index') }}" class="btn-confirm-return" id="return_change_registration" >キャンセル</a>
                   </div>
                </div>
            </form><!--/form-horizontal_sv-->
        </div>
    </div><!--/row-sv-->
</section><!--/container-silver-->
<!--  * end section container  * -->
@stop
