<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>LIXILインターネット修理受付センター | ご依頼中の修理の状況[確報]</title>
<style>
html,body{font-size: 13px;font-weight: 500;background: #fff !important;}
table{font-size: 13px;}
</style>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <!-- * Start section print * -->
  <section class="container-silver">
    <div class="content-report">
      <div class="col-print-btn">
        <ul class="col-nav-bt">
            <li><a href="javascript:void(0)" onclick="window.print()"><span class="icon icon_user"><img src="/assets/images/print.png"></span>印刷</a></li>
            <li class="logout_sv">
             <a href="javascript:void(0)" onclick="window.close();">
                <span class="icon icon_logout"><img src="/assets/images/logout.png"></span>閉じる
             </a>
           </li>
        </ul>
      </div><!--/col-print-btn-->
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="report-title">
            <h1>結果報告書（詳細）</h1>
            <span class="report-date">発行日<span>{{ $kakuhou->hakkoubi }}</span></span>
          </div>
        </div>
      </div>
      <div class="row-sv">
        <div class="col-sv-8">
          <div class="report-note">
            <div class="report-id">コード<span>{{ $kakuhou->iraimoto_cd }}</span></div>
            <div class="report-dear"><span class="dear_set">{{ $kakuhou->t1 }}御中</span></div>
            @if($kakuhou->iraimoto_tanto!=null || $kakuhou->iraimoto_tanto != '')
            <div class="report-name"><span class="name_set">{{ $kakuhou->iraimoto_tanto }}</span></div>
            @endif
            <div class="report-des">
              <p>いつもお世話になっております。
                ご依頼いただきました修理について訪問しました状況を、
                下記の通りご連絡申し上げます。</p>
            </div>
          </div>
        </div>
        <div class="col-sv-4">
          <div class="report-logo">
            <img src="/assets/images/lixil-logo-report.png">
          </div>
          <div class="report-contact">
            <div class="report-lixil">{{ $kakuhou->toiawase_name }}</div>
            <div class="report-lixil">{{ $kakuhou->toiawase_tel1 }}</div>
            <div class="report-lixil">{{ $kakuhou->toiawase_tel2 }}</div>
            <div class="report-lixil">{{ $kakuhou->toiawase_fax }}</div>
          </div>
          </div>
        </div>
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="center">＜記＞</div>
          <div class="tb-responsive table-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">受付No</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->uketuke_no }}</td>
                  <th class="col-ret-title">貴社注文番号</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->tyumon_bango }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">受付年月日</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->uketukebi }}</td>
                  <th class="col-ret-title">工事店名</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->koziten }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">お得意先様顧客No</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->hm_kokyaku_no }}</td>
                </tr>
              </tbody>
            </table>
          </div><!--table-responsive-->
          <div class="tb-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title ft-bold">
                    <div class="tb-report">お客様住所</div>
                  </th>
                  <td class="ft-bold">:&nbsp;
                    {{ $kakuhou->kokyaku_jusyo }}<br />
                    {{ $kakuhou->kokyaku_katagaki }}
                  </td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様氏名</th>
                  <td class="ft-bold">:&nbsp;{{ $kakuhou->kokyaku_mei }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様電話番号</th>
                  <td class="ft-bold">:&nbsp;{{ $kakuhou->kokyaku_tel_haihun }}</td>
                </tr>
              </tbody>
            </table>
          </div><!--/tb-responsive-->
          <div class="tb-responsive table-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">商品品番(名)/ご依頼内容</th>
                  <td class="col-ret-result">:&nbsp;
                    @if(isset($kakuhou->p_hinban1) && $kakuhou->p_hinban1 != '')
                    ①{{ $kakuhou->p_hinban1 }}／{{ $kakuhou->irainaiyo1 }}
                    @endif
                  </td>
                </tr>
                <tr>
                  <th class="col-ret-title">商品品番(名)/ご依頼内容</th>
                  <td class="col-ret-result">:&nbsp;
                    @if(isset($kakuhou->p_hinban2) && $kakuhou->p_hinban2 != '')
                    ②{{ $kakuhou->p_hinban2 }}／{{ $kakuhou->irainaiyo2 }}
                    @endif
                  </td>
                </tr>
                <tr>
                  <th class="col-ret-title">商品品番(名)/ご依頼内容</th>
                  <td class="col-ret-result">:&nbsp;
                    @if(isset($kakuhou->p_hinban3) && $kakuhou->p_hinban3 != '')
                    ③{{ $kakuhou->p_hinban3 }}／{{ $kakuhou->irainaiyo3 }}
                    @endif
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tb-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">訪問日</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->houmonbi }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">担当者</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->ce_cd }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">完・未完の別</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->kan_mikan }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">ご請求先住所</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->seikyusaki_jusyo }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">ご請求先名</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->seikyusaki_simei }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">ご請求先担当者</th>
                  <td class="col-ret-result">:&nbsp;{{ $kakuhou->seikyusaki_tanto }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">
                    <p>交換部品明細
                      ※交換部品は５種類まで
                      しか表示できませんので
                      ご了承ください</p>
                  </th>
                  <td class="col-ret-result">
                    <table class="table-report-border" style="margin-bottom: 30px;">
                      <thead>
                        <tr>
                          <th style="width:50px"></th>
                          <th style="width:350px">部品明細</th>
                          <th style="width:120px">数量</th>
                          <th style="width:120px">単価</th>
                          <th style="width:200px">部品代</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($kakuhou->buhin_detail as $key => $value)
                        <tr>
                          <td class="center">{{ $key + 1 }}</td>
                          <td>{{ $value['buhinmei'] != '' ? $value['buhinmei'] : $value['hinban'] }}&nbsp;</td>
                          <td align="right">{{ $value['kosuu'] }}</td>
                          <td align="right">{{ $value['tanka'] != '' ? '¥' . $value['tanka'] : '' }}&nbsp;</td>
                          <td align="right">{{ $value['kingaku'] != '' ? '¥' . $value['kingaku'] : '' }}&nbsp;</td>
                        </tr>
                        @endforeach
                        @if(isset($kakuhou->buhinmei6))
                        <tr>
                          <td class="center">6</td>
                          <td>{{ $kakuhou->buhinmei6 }}&nbsp;</td>
                          <td align="right">&nbsp;</td>
                          <td align="right">&nbsp;</td>
                          <td align="right">¥{{ $kakuhou->kingaku6 }}&nbsp;</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th class="col-ret-title">交換部品明細</th>
                  <td class="col-ret-result">
                    <table class="table-report-border">
                      <thead>
                        <tr>
                          <th style="width:50px;"></th>
                          <th style="width:395px;">部位部品名称</th>
                          <th style="width:395px;">処置内容</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if( isset($kakuhou->tyosei_bui1))
                        <tr>
                          <td class="center">1</td>
                          <td>{{ $kakuhou->tyosei_bui1 }}</td>
                          <td>{{ $kakuhou->syoti_naiyo1 }}</td>
                        </tr>
                        @endif
                        @if( isset($kakuhou->tyosei_bui2))
                        <tr>
                          <td class="center">2</td>
                          <td>{{ $kakuhou->tyosei_bui2 }}</td>
                          <td>{{ $kakuhou->syoti_naiyo2 }}</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div>
      </div><!--/row-sv-->
      <div class="row-sv">
        <div class="col-sv-7">
          <div class="tb-responsive tb-total">
            <table class="table-report-no">
                <tbody style="border-bottom: 1px solid #000;">
                  <tr>
                    <th class="col-ret-title">部品代計</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->buhindaikei }}</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">技術料</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->gijuturyou }}</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">出張費</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->syutyohi }}</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">処分料</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->syobunryou }}</td>
                  </tr>
                  @if(isset($kakuhou->nebiki) && $kakuhou->nebiki != '0')
                  <tr>
                    <th class="col-ret-title">値引料</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->nebiki }}</td>
                  </tr>
                  @endif
                  <tr>
                    <th class="col-ret-title">消費税</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->zei }}</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th class="col-ret-title">合計</th>
                    <td class="col-result-price" align="right"><span style="float: left;">:&nbsp;</span>¥{{ $kakuhou->goukei }}</td>
                  </tr>
                </tfoot>
              </table>
            </div><!--/tb-responsive-->
        </div>
        <div class="col-sv-5">
          <div class="tb-responsive">
            <ul class="work-detail">
              <li>
                <p>作業内容&nbsp;:&nbsp;<br />{{ $kakuhou->okyakusamakiji }}</p>
              </li>
            </ul>
            <ul class="work-detail">
              <li>
                <p>お客様へのメッセージ&nbsp;:&nbsp;<br />{{ $kakuhou->iraimoto_kiji }}</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div><!--/content-report-->
  </section>
  <!-- * Start section prind * -->
</body>
</html>
