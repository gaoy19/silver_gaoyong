<?php

namespace App;
use DB;
use Auth;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;

class Iraimoto extends Model
{
  protected $table = 'WEB_UKETUKE';

  public $timestamps = false;

  protected $primaryKey = 'web_uketuke_no';
  /**
   * web_uketuke_no を取得する。
   */
  public function getWebUketukeNo($brand_kbn, $uc_cd)
  {
    if($brand_kbn == '2' && $uc_cd = '0D'){
      return DB::select($this->SQL_WEB_UKETUKE_NO_SW())[0]->web_uketuke_no;
    }
    return DB::select($this->SQL_WEB_UKETUKE_NO())[0]->web_uketuke_no;
  }

  /** 受付No採番ＳＱＬ */
  private function SQL_WEB_UKETUKE_NO()
  {
    $sql = <<<SQL
    SELECT
     'WB' || TO_CHAR(sysdate, 'YYMMDD') || TO_CHAR(seq_web_uketuke_no.nextval, 'FM00000') AS web_uketuke_no
    FROM dual
SQL;
    return $sql;
  }
  /** 受付No採番ＳＱＬ サンウエーブの場合*/
  private function SQL_WEB_UKETUKE_NO_SW()
  {
    $sql = <<<SQL
    SELECT
     'SW' || TO_CHAR(sysdate, 'YYMMDD') || TO_CHAR(seq_web_uketuke_no_sw.nextval, 'FM00000') AS web_uketuke_no
    FROM dual
SQL;
    return $sql;
  }

  public function errorsMessage()
  {
    return [
      "okyakusamaSei.required" => "姓の入力は必須です。",
      "okyakusamaSei.max" => "姓は:max文字まで入力して下さい。",
      "okyakusamaMei.max" => "名は:max文字まで入力して下さい。",
      "okyakusamaSeiKana.required" => "セイの入力は必須です。",
      "okyakusamaSeiKana.max" => "セイは:max文字まで入力して下さい。",
      "okyakusamaMeiKana.max" => "メイは:max文字まで入力して下さい。",
      "okyakusamaTel.required" => "電話番号の入力は必須です。",
      "okyakusamaTel.jp_phone" => "電話番号の形式が不正です。",
      "okyakusamaTel.max" => ":max文字まで入力して下さい。",
      "okyakusamaYuubinNo.max" => ":max文字まで入力して下さい。",
      "okyakusamaYuubinNo.jp_postcode" => "郵便番号の形式が不正です。",
      "okyakusamaTodoufuken.required" => "都道府県の選択は必須です。",
      "okyakusamaAddress.required" => "住所の入力は必須です。",
      "okyakusamaAddress.max" => ":max文字まで入力して下さい。",
      "okyakusamaMansionName.max" => ":max文字まで入力して下さい。",
      "renrakusakiMei.required" => "連絡先名の入力は必須です。",
      "renrakusakiMei.max" => "連絡先名は、全角:max文字以内で入力して下さい。",
      "renrakusakiTel.required" => "連絡先電話番号の入力は必須です。",
      "renrakusakiTel.max" => ":max文字まで入力して下さい。",
      "renrakusakiTel.jp_phone" => "連絡先電話番号の形式が不正です。",
//      "brand.required" => "ブランドを選択して下さい。",
      "kokyakuBango.max" => "貴社顧客番号は:max文字まで行って下さい。"
    ];
  }
  /*
  * Rules
  */
  public function rules($renraku_flg = false)
  {
      if($renraku_flg === false){
        return  [
          "okyakusamaSei" => "required|max:40|zenkaku",
          "okyakusamaMei" => "max:10|zenkaku",
          "okyakusamaSeiKana" => "required|max:50|zenkaku",
          "okyakusamaMeiKana" => "max:40|zenkaku",
          "okyakusamaTel" => "required|max:13|jp_phone",
          "okyakusamaYuubinNo" => "max:8|jp_postcode",
          "okyakusamaTodoufuken" => "required ",
          "okyakusamaAddress" => "required|max:45|zenkaku",
          "okyakusamaMansionName" => "max:50|zenkaku",
          "renrakusakiMei" => "max:20|zenkaku",
          "renrakusakiTel" => "max:13|jp_phone",
//         "brand" => "required",
          "kokyakuBango" => "hankaku|max:20",
        ];
      }
      return  [
        "okyakusamaSei" => "required|max:40|zenkaku",
        "okyakusamaMei" => "max:10|zenkaku",
        "okyakusamaSeiKana" => "required|max:50|zenkaku",
        "okyakusamaMeiKana" => "max:40|zenkaku",
        "okyakusamaTel" => "required|max:13",
        "okyakusamaYuubinNo" => "max:8|jp_postcode",
        "okyakusamaTodoufuken" => "required ",
        "okyakusamaAddress" => "required|max:45|zenkaku",
        "okyakusamaMansionName" => "max:50|zenkaku",
        "renrakusakiMei" => "required|max:20|zenkaku",
        "renrakusakiTel" => "required|max:13|jp_phone",
//       "brand" => "required",
      ];
  }

  /**
  * 修理依頼内容の入力のバリデーションのルール
  */
  public function naiyouRules(/**$hinmei_write = false*/)
  {
//    if($hinmei_write){
      return  [
        "brand" => "required",
        "hinmei_select" => "required",
        "hinmei_directWrite" => "zenkaku",
        "hinban" => "max:40|hankaku",
        "toritsukeYYYY" => "required",
        "renrakujikou" => "max:200",
        "toritsukeMM" => "required",
        "koujitenMei" => "max:20",
        "irainaiyou_directWrite" => "required|max:30|zenkaku",
        "get_YMD" => "lixil_date",
      ];
    }
//    return  [
//      "hinmei_select" => "required",
//      "hinmei_directWrite" => "zenkaku",
//      "hinban" => "max:40|hankaku",
//      "toritsukeYYYY" => "required",
//      "renrakujikou" => "max:200",
//      "toritsukeMM" => "required",
//      "koujitenMei" => "max:20",
//      "irainaiyou_directWrite" => "required|max:30|zenkaku",
//      "get_YMD" => "lixil_date",
//    ];
//  }

  /**
  * 修理依頼内容の入力のバリデーションのエラーメッセージ
  */
  public function naiyouErrorsMessage()
  {
    return  [
      "brand.required" => "ブランドを選択して下さい。",
      "hinmei_select.required" => "商品名の入力を行ってください。",
 //  "hinmei_directWrite.required" => "商品名の入力を行ってください。",
      "hinmei_directWrite.max" => "商品名は:max文字まで入力して下さい。",
      "hinban.max" => "品番は:max文字まで入力して下さい。",
      "hinban.hankaku" => "品番は半角で登録を行ってください。",
      "toritsukeYYYY.required" => "取付年を選んで下さい。",
      "toritsukeMM.required" => "取付月を選んで下さい。",
      "koujitenMei.max" => "工事店名は:max文字まで入力して下さい。",
      "irainaiyou_directWrite.required" => "ご依頼内容を入力して下さい。",
      "irainaiyou_directWrite.max" => "ご依頼内容は:max文字まで入力して下さい。",
      "renrakujikou.max" => "全角 :max 文字以内で入力してください",
    ];
  }
  /*
  * Rules jouhou
  */
  public function jouhouRules($input)
  {
    if((isset($input['kakunin_mail']) && $input['kakunin_mail'] == 'on') || (isset($input['sokuho_mail']) && $input['sokuho_mail'] == 'on')){
      return  [
      "goiraimotoTantou" => "required|max:10|zenkaku",
      "kisya_chuban" => "max:20|hankaku",
      "mailaddress" => "email|max:200"
      ];
    } else {
      return  [
      "goiraimotoTantou" => "required|max:10|zenkaku",
      "kisya_chuban" => "max:20|hankaku"
      ];
    }
  }
  /**
  * 修理依頼内容の入力のバリデーションのエラーメッセージ jouhou
  */
  public function jouhouErrorsMessage()
  {
    return  [
      // Errors messages jouhou
      "goiraimotoTantou.required" => "ご依頼元担当者様の入力を行ってください。",
      "goiraimotoTantou.max" => ":max文字まで入力して下さい。",
      "kisya_chuban.max" => "貴社注番は:max文字まで入力して下さい。",
      "kisya_chuban.hankaku" => "貴社注番は、半角20文字以内で入力して下さい。",
      "mailaddress.email" => "進捗状況連絡先アドレスの形式が正しくありません。 (チェックを外せば、依頼はかけられますがメールは送信されません。)登録情報変更よりメールアドレスを修正ください。 修正反映には3営業日程度必要です。"
    ];
  }
  /*
  * Rules goseikyusaki
  */
  public function goseikyusakiRules()
  {
      return  [
        "sonotaShimei"  => "required|max:40|zenkaku",
        "sonotaShimeiKana"  => "required|max:50|zenkaku",
        "sonotaYuubinNo"  => "max:8|jp_postcode",
        "sonotaTel"         => "required|max:13|jp_phone",
        "sonotaFax"         => "max:13|jp_phone",
        "sonotaTodoufuken"  => "required",
        "sonotaAddress"  => "required|max:45|zenkaku",
        "sonotaMansionName" => "max:50|zenkaku",
        "sonotaTantousya"  => "required|max:10|zenkaku",
        "sonotaShiharaibi"  => "required"
      ];
  }
  /**
  * 修理依頼内容の入力のバリデーションのエラーメッセージ goseikyusakiRules
  */
  public function goseikyusakiErrorsMessage()
  {
    return  [
      // Errors messages goseikyusakiRules
      "sonotaShimei.required" => "ご請求先氏名の入力を行ってください。",
      "sonotaShimei.max" => ":max文字まで入力して下さい。",
      "sonotaShimeiKana.required" => "ご請求先氏名（フリガナ）の入力を行ってください。",
      "sonotaShimeiKana.max" => ":max文字まで入力して下さい。",
      "sonotaTel.required" => "ご請求先電話番号の入力は必須です。",
      "sonotaTel.max" => ":max文字まで入力して下さい。",
      "sonotaTel.jp_phone" => "ご請求先電話番号の形式が不正です。",
      "sonotaFax.jp_phone" => "ご請求先 FAX 番号の形式が不正です。",
      "sonotaYuubinNo.jp_postcode" => "ご請求先郵便番号の形式が不正です。",
      "sonotaTodoufuken.required" => "ご請求先都道府県の選択は必須です。",
      "sonotaAddress.required" => "ご請求先住所の入力は必須です。",
      "sonotaAddress.max" => "ご請求先住所は:max文字まで入力して下さい。",
      "sonotaMansionName.max" => ":max文字まで入力して下さい。",
      "sonotaTantousya.required" => "ご請求先担当者の入力は必須です。",
      "sonotaTantousya.max" => ":max文字まで入力して下さい。",
      "sonotaShiharaibi.required" => "お支払い予定日を選択してください。",

    ];
  }
  /**
	 * 現在の日付取得<br>
	 *
	 *@return 現在の日付
	 */
  public function hiduke()
  {
    $date = new DateTime("now", new DateTimeZone('Asia/Tokyo') );
		$hiduke = array();
		$hiduke[0] = $date->format('Y');
		$hiduke[1] = sprintf("%02d", $date->format('m'));
		$hiduke[2] = sprintf("%02d", $date->format('d'));
		$hiduke[3] = sprintf("%02d", $date->format('H'));
		$hiduke[4] = sprintf("%02d", $date->format('i'));
		$hiduke[5] = sprintf("%02d", $date->format('s'));
    return $hiduke;
  }

  public function getkiboushiteiYYYY()
  {
    $kiboushiteiYYYY = array();
    $hiduke = $this->hiduke();
    $kiboushiteiYYYY[0] = array('value'=>$hiduke[0],'text'=>$hiduke[0]);
    $kiboushiteiYYYY[1] = array('value'=>$hiduke[0]+1,'text'=>$hiduke[0]+1);
    return $kiboushiteiYYYY;
  }

  /**
	 * 取り付け年月：年リストボックスの値およびラベル配列設定
	 */
  public function getToritsukeYYYY()
  {
    $nensuu = 31;
		$year = date("Y");
		$toritsukeYYYY = array();

		for ($i=0; $i < $nensuu; $i++) {
			$insYear = $year - $i;
			$toritsukeYYYY[$i]['value'] = $insYear;

			$heiseiNen = $insYear - 1988;
			if($heiseiNen > 0){
				$heiseiNen = sprintf("%02d", $heiseiNen);
				$toritsukeYYYY[$i]['text'] = "$insYear(H$heiseiNen)";
			} else {
				$showaNen = sprintf("%02d", $insYear - 1925);
				$toritsukeYYYY[$i]['text'] = "$insYear(S$showaNen)";
			}
		}
    // $toritsukeYYYY[count($toritsukeYYYY)] = array('value'=>'@@@@','text'=>'');
    // $toritsukeYYYY[count($toritsukeYYYY)] = array('value'=>'8888','text'=>'');
		return $toritsukeYYYY;
  }

  public function getToritsukeMM()
  {
    $toritsukeMM = array();
    $toritsukeMM = $this->monthList();
       $toritsukeMM[count($toritsukeMM)] = array('value'=>'@@','text'=>'不明');
    // $toritsukeMM[count($toritsukeMM)] = array('value'=>'88','text'=>'');
    return $toritsukeMM;
  }

  public function getKiboushiteiMM()
  {
    return $this->monthList();
  }

  public function monthList()
  {
    $monthList = array();
    for ($i=0; $i < 12; $i++) {
      $monthList[$i] = array('value'=>sprintf("%02d", $i+1),'text'=>sprintf("%02d", $i+1));
    }
    return $monthList;
  }

  public function getDateList()
  {
    $dateList = array();
    for ($i=0; $i < 31; $i++) {
      $dateList[$i] = array('value'=>sprintf("%02d", $i+1),'text'=>sprintf("%02d", $i+1));
    }
    return $dateList;
  }

  public function getSonotaShiharaibi()
  {
    $dateList = array();
    $dateList[0] = array('value'=>'','text'=>'選択してください');
    for ($i=1; $i <= 31; $i++) {
      $dateList[$i] = array('value'=>sprintf("%02d", $i),'text'=>sprintf("%02d", $i));
    }
    return $dateList;
  }

  public function getKiboushiteiDD()
  {
    return $this->getDateList();
  }

  public function getHourList()
  {
    $hourList = array();
    for ($i=9; $i <= 18; $i++) {
      $hourList[$i] = array('value'=>sprintf("%02d", $i),'text'=>sprintf("%02d", $i));
    }
  $hourList[19] = array('value'=>'@@', 'text'=>'希望時無');
    return $hourList;
  }

  public function getKiboushiteiHH()
  {
    $kiboushiteiHH = $this->getHourList();
    // $kiboushiteiHH[count($kiboushiteiHH)+1] = array('value'=>'@@','text'=>'@@');
    return $kiboushiteiHH;
  }

  public function getIraimoto()
  {
    if(!Auth::check()){
      return null;
    }
    return DB::select(DB::raw($this->sqlIraimoto()),[Auth::user()->tokuisaki_cd,Auth::user()->tokuisaki_cd,Auth::user()->tokuisaki_cd]);
  }

  /**
  * Sql イライモト
  * @param $sqlIraimoto
  * @return $sql
  */
  public function sqlIraimoto()
  {
    $sql = <<<SQL
    SELECT * FROM (
    SELECT
    um.syuritaisyo_seq         seq,
    case
    when sy.tekiyo_cd IN (dt.code) then dt.setumei
    else
    case
    when sy.kanryo_flg = '0' then '未着手'
    else NULL
    end
    end                        jyotai,
    iu.uketukebi               uketukebi,
    iu.uketuke_no              uketuke_no,
    wu.web_uketuke_no          web_no,
    ir.iraimoto_tanto          iraimoto_tanto,
    uk.kokyaku_sei             kokyaku_sei,
    uk.kokyaku_na              kokyaku_na,
    uk.kokyaku_jusyo           kokyaku_jusyo,
    uk.kokyaku_tel_haihun      kokyaku_tel,
    um.syohin_hinban           hinban,
    um.irainaiyo               irainaiyo,
    sy.ce_cd                   ce_cd,
    ce.ce_sei                  ce_sei,
    iu.uc_cd                   uc_cd,
    sy.kanryo_flg				sy_kanryo_flg,
    sy.syuri_jisekibi	        syurijisekibi,
    iu.kanryo_flg				iu_kanryo_flg,
    DECODE(SY.RENRAKUBI,NULL,NULL,TO_CHAR(SY.SYURI_YOTEIBI,'YYYY/MM/DD')) syuriyoteibi
    ,DECODE(SY.KANRYO_FLG,'0','1','1','1','2') SORT_KBN
    FROM
    (   SELECT
    distinct
    sy.uc_cd uc_cd,
    sy.uketuke_no          uketuke_no,
    sy.uketuke_nendo       uketuke_nendo,
    max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by sy.SYURI_JISEKIBI desc,DECODE(sy.KANRYO_FLG,'8','1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ
    FROM
    sagyoyotei             sy,
    uketuke_iraimoto       ui
    WHERE
    ui.uc_cd      = sy.uc_cd
    AND ui.uketuke_no = sy.uketuke_no

    AND ui.uketuke_nendo = sy.uketuke_nendo
    AND UI.IRAIMOTO_CD = ?
    AND sy.sakujo_flg = '0'
    ) iis,
    iraiuketuke                    iu,
    uketuke_meisai                 um,
    sagyoyotei                     sy,
    syuritantosya                  ce,
    uketuke_iraimoto               ir,
    uketuke_kokyaku                uk,
    web_uketuke                    wu,

    d_tekiyo_cd                    dt

    WHERE
    iu.uc_cd            = um.uc_cd
    AND iu.uketuke_no      = um.uketuke_no
    AND um.uc_cd           = sy.uc_cd
    AND um.uketuke_no      = sy.uketuke_no
    AND um.syuritaisyo_seq = sy.syuritaisyo_seq
    AND iu.uc_cd           = ir.uc_cd
    AND iu.uketuke_no      = ir.uketuke_no
    AND iu.uc_cd           = uk.uc_cd
    AND iu.uketuke_no      = uk.uketuke_no
    AND sy.ce_cd           = ce.ce_cd
    AND ir.iraimoto_cd     = ?
    AND iu.uketuke_no      = wu.uketuke_no(+)
    AND iis.uc_cd          = sy.uc_cd
    AND iis.uketuke_no     = sy.uketuke_no
    AND iis.max_JISIRIREKI_SEQ  = sy.JISIRIREKI_SEQ
    AND sy.tekiyo_cd       = dt.code(+)
    AND iu.uketuke_nendo      = um.uketuke_nendo
    AND um.uketuke_nendo      = sy.uketuke_nendo
    AND iu.uketuke_nendo      = ir.uketuke_nendo
    AND iu.uketuke_nendo      = uk.uketuke_nendo
    AND iu.uketuke_nendo      = wu.uketuke_nendo(+)
    AND iis.uketuke_nendo     = sy.uketuke_nendo
    AND iu.kanryo_flg <> '8'
    AND iu.kanryo_flg <> '9'
    AND ce.sakujo_flg = '0'
    AND um.sakujo_flg = '0'
    AND ir.sakujo_flg = '0'
    AND uk.sakujo_flg = '0'
    AND (IU.C_VOICE_FLG IS NULL OR IU.C_VOICE_FLG<>'1')
    UNION ALL
    SELECT
    '01'                     seq,
    DECODE(wu.web_kanryo_flg,'0','受付中','2','確認中',NULL)   jyotai,
    wu.torokubi              uketukebi,
    wu.uketuke_no            uketuke_no,
    wu.web_uketuke_no        web_no,
    wu.iraimoto_tanto        iraimoto_tanto,
    wu.kokyaku_sei           kokyaku_sei,
    wu.kokyaku_na            kokyaku_na,
    wu.kokyaku_jusyo         kokyaku_jusyo,
    wu.kokyaku_tel_haihun    kokyaku_tel,
    wu.syohin_hinban         hinban,
    wu.irainaiyo             irainaiyo,
    NULL                         ce_cd,
    NULL                         ce_sei,
    wu.uc_cd                    uc_cd,
    wu.web_kanryo_flg   		sy_kanryo_flg,
    TO_DATE(NULL,'YYYY/MM/DD')  syurijisekibi,
    NULL					    iu_kanryo_flg,
    '' SYURIYOTEIBI
    ,'' SORT_KBN
    FROM
    web_uketuke              wu
    WHERE
    wu.web_kanryo_flg <> '9'

    AND (wu.C_VOICE_FLG IS NULL OR wu.C_VOICE_FLG<>'1')

    AND wu.sakujo_flg      = '0'
    AND wu.iraimoto_cd = ?
    ORDER BY
    uketukebi desc,
    uketuke_no,
    sort_kbn,
    seq)
    WHERE ROWNUM <= 10
SQL;
    return $sql;
  }

  public function insertData($value = [])
  {
    return DB::insert($this->SQL_INSERT_WEB_UKETUKE(),$value);
  }

  public function SQL_INSERT_WEB_UKETUKE()
  {
    $sql = <<<SQL
    INSERT INTO
      web_uketuke(
      web_uketuke_no,
      uketuke_kbn,
      kokyaku_sei,
      kokyaku_sei_kana,
      kokyaku_na,
      kokyaku_na_kana,
      kokyaku_tel_haihun,
      kokyaku_jusyo,
      kokyaku_katagaki,
      kokyaku_yubin,
      hinmei,
      syohin_hinmoku,
      syohin_hinban,
      torituke_nengetu,
      irainaiyo,
      koziten,
      renrakusaki_mei,
      renrakusaki_tel_haihun,
      hm_kokyaku_no,
      kibo_sitei_kbn,
      kibo_sitei_bi,
      kibo_sitei_bi_jikan,
      message,
      iraimoto_cd,
      iraimoto_simei,
      iraimoto_tanto,
      iraimoto_jusyo,
      iraimoto_tel_haihun,
      iraimoto_fax,
      fax_sosin_kbn,
      fax_iraimoto_cd,
      fax_iraimoto_simei,
      fax_iraimoto_tanto,
      fax_tyumon_bango,
      fax_iraimoto_tel_haihun,
      fax_iraimoto_fax,
      tyohyo_uk,
      tyohyo_gj,
      tyohyo_fax3,
      tyohyo_fax5,
      tyohyo_uf,
      yuryo_seikyu_kbn,
      muryo_sekinin_kbn,
      muryo_s_syain_no,
      seikyusaki_simei,
      seikyusaki_simei_kana,
      seikyusaki_tel_haihun,
      seikyusaki_fax,
      seikyusaki_jusyo,
      seikyusaki_yubin,
      seikyusaki_katagaki,
      seikyusaki_tanto,
      siharaibi,
      uc_cd,
      iraimoto_bumon,
      web_kanryo_flg,
      torokubi,
      torokusya_cd,
      kosinbi,
      kosinsya_cd,
      sakujo_flg,
      tyumon_bango,
      tempsiryo_umu_flg,
      user_id,
      syuri_taisyo,
      brand_cd
    )VALUES(
      :web_uketuke_no,
      :uketuke_kbn,
      :kokyaku_sei,
      :kokyaku_sei_kana,
      :kokyaku_na,
      :kokyaku_na_kana,
      :kokyaku_tel_haihun,
      :kokyaku_jusyo,
      :kokyaku_katagaki,
      :kokyaku_yubin,
      :hinmei,
      :syohin_hinmoku,
      :syohin_hinban,
      :torituke_nengetu,
      :irainaiyo,
      :koziten,
      :renrakusaki_mei,
      :renrakusaki_tel_haihun,
      :hm_kokyaku_no,
      :kibo_sitei_kbn,
      :kibo_sitei_bi,
      :kibo_sitei_bi_jikan,
      :message,
      :iraimoto_cd,
      :iraimoto_simei,
      :iraimoto_tanto,
      :iraimoto_jusyo,
      :iraimoto_tel_haihun,
      :iraimoto_fax,
      :fax_sosin_kbn,
      :fax_iraimoto_cd,
      :fax_iraimoto_simei,
      :fax_iraimoto_tanto,
      :fax_tyumon_bango,
      :fax_iraimoto_tel_haihun,
      :fax_iraimoto_fax,
      :tyohyo_uk,
      :tyohyo_gj,
      :tyohyo_fax3,
      :tyohyo_fax5,
      :tyohyo_uf,
      :yuryo_seikyu_kbn,
      :muryo_sekinin_kbn,
      :muryo_s_syain_no,
      :seikyusaki_simei,
      :seikyusaki_simei_kana,
      :seikyusaki_tel_haihun,
      :seikyusaki_fax,
      :seikyusaki_jusyo,
      :seikyusaki_yubin,
      :seikyusaki_katagaki,
      :seikyusaki_tanto,
      :siharaibi,
      :uc_cd,
      :iraimoto_bumon,
      '0',
      sysdate,
      'WBUINS',
      sysdate,
      'WBUINS',
      '0',
      :tyumon_bango,
      :tempsiryo_umu_flg,
      :user_id,
      :syuri_taisyo,
      :brand_cd
      )
SQL;
    return $sql;
  }

  public static function getUketukeByWebUketukeNo($web_uketuke_no)
  {
    return self::where('web_uketuke_no','=',$web_uketuke_no)->first();
  }

  public function charReplaceKokyaku($values)
  {
    // 全角番号を半角番号に変換
    $values['okyakusamaTel'] = \App\Custom\CharReplace::replaceNumber($values['okyakusamaTel']);
    $values['renrakusakiTel'] = \App\Custom\CharReplace::replaceNumber($values['renrakusakiTel']);
    $values['okyakusamaYuubinNo'] = \App\Custom\CharReplace::replaceNumber($values['okyakusamaYuubinNo']);
    // 半角を全角に変換
    $values['okyakusamaSei'] = \App\Custom\CharReplace::hanKanaTozenKana($values['okyakusamaSei']);
    $values['okyakusamaMei'] = \App\Custom\CharReplace::hanKanaTozenKana($values['okyakusamaMei']);
    $values['okyakusamaSeiKana'] = \App\Custom\CharReplace::hanKanaTozenKana($values['okyakusamaSeiKana']);
    $values['okyakusamaMeiKana'] = \App\Custom\CharReplace::hanKanaTozenKana($values['okyakusamaMeiKana']);
    $values['okyakusamaAddress'] = \App\Custom\CharReplace::hanKanaTozenKana($values['okyakusamaAddress']);
    $values['okyakusamaMansionName'] = \App\Custom\CharReplace::hanKanaTozenKana($values['okyakusamaMansionName']);
    $values['renrakusakiMei'] = \App\Custom\CharReplace::hanKanaTozenKana($values['renrakusakiMei']);
    return $values;
  }

  public function charBackReplaceKokyaku($values)
  {
    $values['okyakusamaTel'] = \App\Custom\CharReplace::replaceBackNumber($values['okyakusamaTel']);
    $values['renrakusakiTel'] = \App\Custom\CharReplace::replaceBackNumber($values['renrakusakiTel']);
    $values['okyakusamaYuubinNo'] = \App\Custom\CharReplace::replaceBackNumber($values['okyakusamaYuubinNo']);
    // $values['kokyakuBango'] = \App\Custom\CharReplace::replaceBackNumber($values['kokyakuBango']);
    return $values;
  }

  // 修理依頼内容の入力画面に文字変換
  public function charReplaceNaiyou($values)
  {
    // 半角を全角に変換
    $values['hinmei_directWrite'] = \App\Custom\CharReplace::hanKanaTozenKana($values['hinmei_directWrite']);
    $values['irainaiyou_directWrite'] = \App\Custom\CharReplace::hanKanaTozenKana($values['irainaiyou_directWrite']);
    $values['koujitenMei'] = \App\Custom\CharReplace::hanKanaTozenKana($values['koujitenMei']);
    $values['renrakujikou'] = \App\Custom\CharReplace::hanKanaTozenKana($values['renrakujikou']);
    // 全角を半角に変換
    $values['hinban'] = \App\Custom\CharReplace::zenKanaTohanKana($values['hinban']);

    return $values;
  }

  // ご依頼元情報の入力画面に文字変換
  public function charReplaceJouhou($values)
  {
    // 半角を全角に変換
    $values['goiraimotoTantou'] = \App\Custom\CharReplace::hanKanaTozenKana($values['goiraimotoTantou']);
    // 全角を半角に変換
    $values['kisya_chuban'] = \App\Custom\CharReplace::zenKanaTohanKana($values['kisya_chuban']);
    return $values;
  }

  public function charReplaceGoseikyusaki($values)
  {
    $values['sonotaTel'] = \App\Custom\CharReplace::replaceNumber($values['sonotaTel']);
    $values['sonotaFax'] = \App\Custom\CharReplace::replaceNumber($values['sonotaFax']);
    $values['sonotaYuubinNo'] = \App\Custom\CharReplace::replaceNumber($values['sonotaYuubinNo']);
    // 半角を全角に変換
    $values['sonotaShimei'] = \App\Custom\CharReplace::hanKanaTozenKana($values['sonotaShimei']);
    $values['sonotaShimeiKana'] = \App\Custom\CharReplace::hanKanaTozenKana($values['sonotaShimeiKana']);
    $values['sonotaAddress'] = \App\Custom\CharReplace::hanKanaTozenKana($values['sonotaAddress']);
    $values['sonotaMansionName'] = \App\Custom\CharReplace::hanKanaTozenKana($values['sonotaMansionName']);
    $values['sonotaTantousya'] = \App\Custom\CharReplace::hanKanaTozenKana($values['sonotaTantousya']);
    return $values;
  }

  public function charBackReplaceGoseikyusaki($values)
  {
    $values['sonotaTel'] = \App\Custom\CharReplace::replaceBackNumber($values['sonotaTel']);
    $values['sonotaFax'] = \App\Custom\CharReplace::replaceBackNumber($values['sonotaFax']);
    $values['sonotaYuubinNo'] = \App\Custom\CharReplace::replaceBackNumber($values['sonotaYuubinNo']);
    return $values;
  }

  /**
   * 内容入力ページの添付ファイルのセッションを設置
   * Set filename session for Naiyou page
   */
  public function setFilenameToSession($i = 1,$filename)
  {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $_SESSION['fileUpload' . $i] = $filename;
  }

  /**
   * 内容入力ページの添付ファイルのセッションを取得
   * Get filename as array from session for Naiyou page
   */
  public function getFilenameFromSession()
  {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $files = [];
    for($i = 1; $i <= 5; $i++){
      $files[$i] = isset($_SESSION['fileUpload' . $i]) ? $_SESSION['fileUpload' . $i] : '';
    }
    return $files;
  }

  /**
   * Clear filename session for Naiyou page
   */
  public function clearFilenameSession()
  {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    for($i = 1; $i <= 5; $i++){
      if(isset($_SESSION['fileUpload' . $i])){
        unset($_SESSION['fileUpload' . $i]);
      }
    }
  }
  //copy files and folder, if confirmation completed
  public function recurseCopyDirAndFiles($src = NULL, $dst = NULL)
  {
    if (is_dir($src)) {
      /* Returns false if src doesn't exist */
      $dir = @opendir($src);
      /* Make destination directory. False on failure */
      if (!file_exists($dst)) @mkdir($dst);
      /* Recursively copy */
      while (false !== ($file = readdir($dir))) {
          if (( $file != '.' ) && ( $file != '..' )) {
             if ( is_dir($src . '/' . $file) ) recurseCopyDirAndFiles($src . '/' . $file, $dst . '/' . $file);
             else copy($src . '/' . $file, $dst . '/' . $file);
          }
      }
      closedir($dir);

   // $this->deleteDirAndFiles($src);
    }
  }

  // Delete tmp file and folder
  public function deleteTmpfiles($src = NULL)
  {
    if(is_dir($src)){
      $this->deleteDirAndFiles($src);
    }
  }
  //remove files and folder, if confirmation completed
  private function deleteDirAndFiles($dir = NULL)
  {
    if (is_dir($dir)) {
      $files = scandir($dir);
      foreach ($files as $file) {
        if ($file != "." && $file != "..") {
          if (filetype($dir."/".$file) == "dir") {
              $this->deleteDirAndFiles($dir."/".$file);
          } else {
            unlink($dir."/".$file);
          }
        }
      }
      rmdir($dir);
    }
  }

  /**
   * Delete tmp file if file is overrided
   */
  public function deleteTmpExistFileBySession($index)
  {
    if (session_status() == PHP_SESSION_NONE) {
  			session_start();
  	}
    if(!isset($_SESSION['fileUpload'.$index])) return;

    $destinationPath = \Storage::disk('tmp_incoming')->url(\Session::token());
    $filename = $destinationPath . '/' .$_SESSION['fileUpload'.$index];
    // ファイルが保存されるかチェック、あった場合削除
  	if(\File::exists($filename)){
      \File::Delete($filename);
    }
  }

  /**
   * tmp_incomingにファイル存在チェック
   */ 
  public function validTmpExistFileBySession($index)
  {
    if (session_status() == PHP_SESSION_NONE) {
  			session_start();
  	}
    if(!isset($_SESSION['fileUpload'.$index])) return true;

    $destinationPath = \Storage::disk('tmp_incoming')->url(\Session::token());
    $filename = $destinationPath . '/' .$_SESSION['fileUpload'.$index];
    // ファイルが保存されるかチェック、あった場合 return true
    if(\File::exists($filename)){
      return true;
    }else{
      return false;
    }
  }

  /**
   * UC_CDを検索
   */
  public function find_uc_cd_by_todofuken($kenmei,$flg = true)
  {
    return DB::select(DB::raw($this->SQL_FIND_UC_CD_BY_TODOFUKEN($kenmei)));
  }

  /**
   * UC_CDを検索
   */
  public function find_uc_cd_by_yubinbago($yubin_bango,$flg = true)
  {
    return DB::select(DB::raw($this->SQL_FIND_UC_CD_BY_YUBIN_BANGO()),['yubin_bango' => $yubin_bango]);
  }

  /**
   * 郵便番号を条件にUC_CDを検索
   */
  private function SQL_FIND_UC_CD_BY_YUBIN_BANGO()
  {
    $sql = <<<SQL
      SELECT DISTINCT
        EI.UC_CD
      FROM
        SC  SC,
        EIGYOSYO  EI,
        YUBIN_BANGO Y
      WHERE
        EI.EIGYOSYO_CD = SC.EIGYOSYO_CD
        AND EI.EIGYOSYO_CD = Y.EIGYOSYO_CD
        AND SC.SC_CD = Y.SC_CD
        AND Y.YUBIN_BANGO = :yubin_bango
      ORDER BY EI.UC_CD
SQL;
    return $sql;
  }

  /**
   * 都道府県名を条件にUC_CDを検索
   */
  private function SQL_FIND_UC_CD_BY_TODOFUKEN($ken_mei = '')
  {
    $sql = <<<SQL
      SELECT DISTINCT
        EI.UC_CD
      FROM
        SC  SC,
        EIGYOSYO  EI,
        YUBIN_BANGO Y
      WHERE
        EI.EIGYOSYO_CD = SC.EIGYOSYO_CD
        AND EI.EIGYOSYO_CD = Y.EIGYOSYO_CD
        AND SC.SC_CD = Y.SC_CD
        AND Y.KEN_MEI LIKE '$ken_mei%'
      ORDER BY EI.UC_CD
SQL;
    return $sql;
  }
}
