<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class WbSmtSeihinDaibunrui extends Model
{
    protected $table = 'WB_SMT_SEIHIN_DAIBUNRUI';

    public $timestamps = false;

    protected $fillable = [
      "SEIHIN_DAIBUNRUI_CD",
    	"SEIHIN_DAIBUNRUI_MEI",
    	"HYOJI_JUN",
    	"TOROKUBI",
    	"TOROKUSYA_CD",
    	"KOSINBI",
    	"KOSINSYA_CD",
    	"SAKUJO_FLG"
    ];

    public static function getLists()
    {
      $lists = self::select('SEIHIN_DAIBUNRUI_MEI','SEIHIN_DAIBUNRUI_CD')
  									->where('SAKUJO_FLG','=','0')
  									->orderBy('HYOJI_JUN')
  									->get()
                    ->toArray();
      return $lists;
    }
}
