<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;
use DateTimeZone;
class SSOAuthAlphas
{
    protected $limit_login_time = 30; //3600 = 1 hour
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ($request->all() !== NULL) {
        if ( \Auth::check() ) { return \Redirect::route('home'); }

        //alphas連携(GETの場合)は、クエリパラメタからsessionid、useridを取得する。
        $alphasUSER_ID = NULL;
        $alphasUSER_ID = \App\WebAlphasLogin::where('alphas_id', $request->userid)->where('session_id', $request->sessionid)->orderBy('login_time', 'desc')->first();
        
        if ($alphasUSER_ID !== NULL) {
          $alphasId = $alphasUSER_ID->alphas_id;
          session(['alphasId' => $alphasId]);
        }

        //alphas連携(POSTの場合)は、応答本文からsessionid、useridを取得する。
        $posSessionId = '';
        $posUserId = '';
        if ($alphasUSER_ID == NULL){

          $posSessionId = strpos($request, 'sessionid');
          $posUserId = strpos($request, 'userid');

          if($posSessionId != false && $posUserId != false){
            $alphasUSER_ID = \App\WebAlphasLogin::where('alphas_id', substr($request, $posUserId + 7, 10))->where('session_id', substr($request, $posSessionId + 10, 10))->orderBy('login_time', 'desc')->first();

            if ($alphasUSER_ID !== NULL) {
              session(['alphasId' => $alphasUSER_ID]);
            }
          }
        }

        if ($alphasUSER_ID !== NULL) {
          if ($alphasUSER_ID->alphas_id === $request->userid && $alphasUSER_ID->session_id === $request->sessionid) {
            $login_time_jp = new DateTime($alphasUSER_ID->login_time, new DateTimeZone('Asia/Tokyo'));
            if ( time() - $login_time_jp->getTimestamp() < $this->limit_login_time ) {
              $alphasInfUser = \App\WebAlphasInfUser::where('alphas_id', $request->userid)->first();
              if ($alphasInfUser !== NULL) {
                \Auth::loginUsingId($alphasInfUser->user_id);
                return \Redirect::route('home');
              }
            }
          }
        }
      }

      //alphas連携(POSTの場合)でかつ、自動ログイン登録をしていないユーザーに、自動登録案内を表示する。
      if($posSessionId != '' && $posUserId != ''){
        if($posSessionId != false && $posUserId != false){
        return \Redirect::route('login', ['userid' => substr($request, $posUserId + 7, 10)]);
        }
      }
      //alphas連携(POSTの場合)でかつ、自動ログイン登録をしていないユーザーに、自動登録案内を表示する。
      return \Redirect::route('login', ['userid' => $request->userid]);
      return $next($request);
    }
}
