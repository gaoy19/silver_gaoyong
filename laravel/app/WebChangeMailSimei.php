<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class WebChangeMailSimei extends Model
{
    protected $table = 'WEB_RCP_USER';

    protected $primaryKey = 'user_id';

    public static function updateData($values)
    {
      return DB::update(self::SQL_UPDATE_WEB_SIMEI_MAIL(),$values);
    }

    private static function SQL_UPDATE_WEB_SIMEI_MAIL()
    {
      $sql = <<<SQL
      UPDATE  WEB_RCP_USER SET
            USER_SIMEI = ?,
            INFO_MAIL_ADDRESS = ?,
            MAIL_ADDRESS = ? 
            WHERE
            USER_ID = ?
SQL;
      return $sql;
    }       
}
