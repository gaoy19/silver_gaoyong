@extends('layouts.master')
@section('request_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理のご依頼[3ご請求先情報の入力]')
@section('content')
  <!-- * Start section 修理のご依頼 * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>修理のご依頼</h1>
        </div>
    </div>
  </div><!--/row-sv-->
  <!-- * End section 修理のご依頼 * -->
  <!-- * Start section お客さま情報の入力 * -->
  <div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-3 active_sv">
                <a href="javascript:void(0);" onclick="@if(Session::has('reachConfirmationFlag')) submitByProgress('iraimoto.showKokyaku') @else submitBack('iraimoto.showKokyaku') @endif" class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">お客さま情報の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <a href="javascript:void(0);" onclick="@if(Session::has('reachConfirmationFlag')) submitByProgress('iraimoto.showNaiyou') @else submitBack('iraimoto.showNaiyou') @endif" class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">修理依頼内容の入力</div>
                </a>            
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力 </div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <a href="javascript:form_goseikyusaki.submit()" class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <div class="box-progress-bar">
                    <div class="box-num-bar">5</div>
                    <div class="box-text-bar">修理のご依頼完了 </div>
                </div>
            </li><!--/box-sv-3-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
  </div><!--/row-sv-->
  <!-- * End section お客さま情報の入力 * -->
  <!-- * Start section ご請求先情報 * -->
  <div class="row-sv">
    <div class="main-vs-12 sv_top20">
        <form id="web_customer_info" method="POST" name="form_goseikyusaki" action="{{ route('iraimoto.goseikyusaki') }}" accept-charset="UTF-8" role="form">
            {{ csrf_field() }}
            <div class="box-main-form">
                <div class="modal-header_sv">
                    <h5>ご請求先情報</h5><ul class="col-nav-bt" style="margin-top:-30px;"><li><a href="javascript:submitReset();"><span style="margin-left:30px;">全てクリア</span></a></li></ul>
                </div>
                <div class="con-info_sv">
                    <div class="tb-form_sv">
                        <!--ご請求先氏名-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先氏名</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaShimei'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaShimei') }}</span></div>
                                        @endif
                                        <input type="text" value="{{ old('sonotaShimei') }}" class="form-control_sv ime-active{!! $errors->has('sonotaShimei') ? ' input-error' : '' !!}" id="billing_name_full_name1" name="sonotaShimei" maxlength="40" style="max-width:500px;"><span class="span-text">様</span>
                                    </div>
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt="">   </span>40 文字までの全角で入力してください
                                        </div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先氏名（フリガナ）-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先氏名（フリガナ）</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaShimeiKana'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaShimeiKana') }}</span></div>
                                        @endif
                                        <input type="text" value="{{ old('sonotaShimeiKana') }}" class="form-control_sv ime-active{!! $errors->has('sonotaShimeiKana') ? ' input-error' : '' !!}" id="billing_name_full_name2" name="sonotaShimeiKana" maxlength="50" style="max-width:500px;"><span class="span-text">様</span>
                                    </div>
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt="">   </span>50 文字までの全角で入力してください
                                        </div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先電話番号-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先電話番号</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaTel'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaTel') }}</span></div>
                                        @endif
                                        <input type="text" value="{{ old('sonotaTel') }}" class="form-control_sv ime-disabled{!! $errors->has('sonotaTel') ? ' input-error' : '' !!}" id="sonotaTel" name="sonotaTel" maxlength="13" style="max-width:300px;" onkeypress="return isNumberKey(event);">
                                    </div>
                                    <div class="label-info-12">
                                        <div class="label-info">例：03-3333-1111</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先 FAX 番号-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先 FAX 番号</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('sonotaFax'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('sonotaFax') }}</span></div>
                                      @endif
                                        <input type="text" value="{{ old('sonotaFax') }}" class="form-control_sv ime-disabled{!! $errors->has('sonotaFax') ? ' input-error' : '' !!}" id="billing_destination_fax_number" name="sonotaFax" maxlength="13" style="max-width:300px;" onkeypress="return isNumberKey(event);">
                                    </div>
                                    <div class="label-info-12">
                                        <div class="label-info">例：03-3333-2222</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先郵便番号-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先郵便番号</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                  @if ($errors->has('sonotaYuubinNo'))
                                    <div class="error_sv max-100"><span>{{ $errors->first('sonotaYuubinNo') }}</span></div>
                                  @endif
                                    <div class="col-td-5">
                                        <input type="text" value="{{ old('sonotaYuubinNo') }}" class="form-control_sv ime-disabled{!! $errors->has('sonotaYuubinNo') ? ' input-error' : '' !!}" id="billing_postal_code"  name="sonotaYuubinNo" maxlength="8" value="" style="max-width:200px;" onkeypress="return isNumberKey(event);">
                                    </div>
                                    <div class="col-td-7">
                                         <input type="button" class="btn-control_sv" id="add_postal_code" name="add_postal_code" style="max-width:200px;" value="住所検索" onclick="showAddressData()">
                                    </div>
                                    <div class="label-info-12">
                                        <div class="label-info">例：100-1111</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先都道府県-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先都道府県</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaTodoufuken'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaTodoufuken') }}</span></div>
                                        @endif
                                        <select class="form-control_sv{!! $errors->has('sonotaTodoufuken') ? ' input-error' : '' !!}" name="sonotaTodoufuken" size="1" style="max-width:180px;">
                                            <option value="">選択してください</option>
                                            @foreach (Cache::get('todoufukens') as $todoufuken)
                                              <option value="{{ $todoufuken }}" {{ ( old('sonotaTodoufuken') == $todoufuken ) ? ' selected' : '' }}>{{ $todoufuken }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="label-info-12">
                                    <div class="label-info"></div>
                                    <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>都道府県は、選択をする他に、手入力をすることも可能です。</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先住所-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先住所</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaAddress'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaAddress') }}</span></div>
                                        @endif
                                        <input type="text" value="{{ old('sonotaAddress') }}" class="form-control_sv ime-active{!! $errors->has('sonotaAddress') ? ' input-error' : '' !!}" id="billing_address" name="sonotaAddress" maxlength="45" style="max-width:500px;">
                                    </div>
                                    <div class="label-info-12">
                                        <div class="label-info">例：中央区千代田１－１－１</div>
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>45 文字までの全角で入力してください</div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先建物名・部屋番号-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先建物名・部屋番号</div>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                      @if ($errors->has('sonotaMansionName'))
                                        <div class="error_sv max-100"><span>{{ $errors->first('sonotaMansionName') }}</span></div>
                                      @endif
                                        <input type="text" value="{{ old('sonotaMansionName') }}" class="form-control_sv ime-active{!! $errors->has('sonotaMansionName') ? ' input-error' : '' !!}" id="billing_address_building_name" name="sonotaMansionName" maxlength="50" style="max-width:500px;">
                                    </div>
                                    <div class="label-info-12">
                                        <div class="label-info">例：霞ヶ関ビルディング 36 階 101 号室</div>
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>部屋番号まで入力してください
                                            50 文字までの全角で入力してください
                                        </div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                        <!--ご請求先担当者-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4">
                                <div class="col-ft-left">
                                    <div class="td-title">ご請求先担当者</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaTantousya'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaTantousya') }}</span></div>
                                        @endif
                                        <input type="text" value="{{ old('sonotaTantousya') }}" class="form-control_sv ime-active{!! $errors->has('sonotaTantousya') ? ' input-error' : '' !!}" id="contact_person_in_charge" name="sonotaTantousya" maxlength="10" style="max-width:300px;"><span class="span-text">様</span>
                                    </div>
                                    <div class="label-info-12 col-top10">
                                        <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt="">   </span>10 文字までの全角で入力してください
                                        </div>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                         <!--お支払い予定日-->
                        <div class="tr-form_sv">
                            <div class="td-form_sv-4 tb-fonm-none">
                                <div class="col-ft-left">
                                    <div class="td-title">お支払い予定日</div>
                                    <span class="error_label">必須</span>
                                </div>
                            </div><!--/td-form_sv-4-->
                            <div class="td-form_sv-8 tb-fonm-none">
                                <div class="col-ft-right">
                                    <div class="col-td-12">
                                        @if ($errors->has('sonotaShiharaibi'))
                                          <div class="error_sv max-100"><span>{{ $errors->first('sonotaShiharaibi') }}</span></div>
                                        @endif
                                        <select name="sonotaShiharaibi" class="form-control_sv{!! $errors->has('sonotaShiharaibi') ? ' input-error' : '' !!}" id="payment_schedule_date"  style="max-width:180px;">
                                          @foreach ($sonotaShiharaibi as $item)
                                            <option value="{{ $item['value'] }}" {{ ( old('sonotaShiharaibi') == $item['value'] ) ? ' selected' : '' }}>{{ $item['text'] }}</option>
                                          @endforeach
                                        </select><span class="span-text">日</span>
                                    </div>
                                </div>
                            </div><!--/td-form_sv-8-->
                        </div><!--/tr-form_sv-->
                    </div><!--/tb-info_sv-->
                </div><!--/con-group-sv-->
            </div><!--/box-main-form-->
            <!--修理依頼内容の入力へ進む-->
            <div class="row-sv">
                <div class="col-sv-6">
                    <a class="btn btn-submit-send" href="javascript:void(0);" onclick="@if(Session::has('reachConfirmationFlag'))  submitByProgress('iraimoto.showJouhou')  @else  submitBack('iraimoto.showJouhou')  @endif">
                      <span class="arrow-send_lf"><img src="{{ asset('assets/images/arrow_send_lf.png') }}"></span><span class="arrow-text_lf">3  ご依頼元情報の入力に戻る</span></a>
                </div>
                <div class="col-sv-6 btn-submit" style="width: auto;">
                    <button class="btn btn-submit-send" type="submit" value="4  ご依頼内容の確認に進む"><span class="arrow-text">4  ご依頼内容の確認に進む</span><span class="arrow-send"><img src="{{ asset('assets/images/arrow_send.png') }}"></span></button>
                </div>
            </div>
            <input type="hidden" value="" id="progressClick" name="progressClick"><!--/どのステータスをクリックしたか-->
        </form><!--/form-horizontal_sv-->
    </div>
  </div><!--/row-sv--><!--/row-sv-->
  <!-- * End section ご請求先情報 * -->
  <script type="text/javascript">
    function submitReset() {
        if(confirm("入力した依頼内容が全てクリアされますが、よろしいですか？")){
            var data = $('#web_customer_info');
            data.attr("action",'{{ route('iraimoto.reset') }}');
            data.submit();
        }
    }
    $(document).ready(function() {
        $('select[name=sonotaTodoufuken]').chosen({
            search_contains: true,
            no_results_text: "一致することができない"  
        });
        $('select[name=sonotaTodoufuken]').each(function(){
            if($(this).hasClass('input-error')){
                $(".chosen-single").addClass('input-error');
                $(".chosen-drop").addClass('input-error');
            }
        });

      $('#sonotaTelFlg').change(function() {
          if(this.checked) {
            $('#sonotaTel').val('9999999999999');
            $("#sonotaTel").prop('readonly', true);
            $(this).attr( 'checked', true );
          } else {
            $('#sonotaTel').val('');
            $("#sonotaTel").prop('readonly', false);
            $(this).attr('checked', false);
          }
      });

      $("input[name=sonotaYuubinNo]").change(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault();
        var formData = {
            okyakusamaYuubinNo: $('input[name=sonotaYuubinNo]').val()
        }
        $.ajax({
            type: 'POST',
            url: '{{ route('json.yubinbango') }}',
            data: formData,
            dataType: 'json',
            success: function (data) {
                if(data){
                  $('select[name=sonotaTodoufuken]').val(data.ken_mei);
                  $('input[name=sonotaAddress]').val(data.sityoson_mei);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
      });
    });
    // 住所検索ダイアログ表示
    function showAddressData() {
      if($('input[name=sonotaYuubinNo]').val().length == 0 &&
        $('select[name=sonotaTodoufuken]').val().length == 0 &&
        $('input[name=sonotaAddress]').val().length == 0){
          alert('郵便番号、都道府県、ご住所のいずれかを入力してください。');
          return;
        }
      // 住所検索フォーム
      window.open("{{route('yubinbango.search')}}?windowName=childWin&actionID=search2"
                +"&ken_mei_val=" + $('select[name=sonotaTodoufuken]').val()
                +"&yubinBango_val=" + $('input[name=sonotaYuubinNo]').val()
                +"&sityoson_mei=" + $('input[name=sonotaAddress]').val()
                +"&form=form_goseikyusaki"
                +"&_token=" + $('meta[name="_token"]').attr('content'),
                "addressSearchForm",
                "top=0,left=0,height=650,width=850,status=no,scrollbars=yes,toolbar=no,menubar=no");
    }
  </script>
  <script type="text/javascript">
  
    function submitBack(progress) {
      var data = $('#web_customer_info');
      data.attr("action",'{{ route('iraimoto.submitBack') }}');
      //どのステータスをクリックしたかを保存する
      $("#progressClick").val(progress);
      data.submit();
    }

    function submitByProgress(progress){
      $("#progressClick").val(progress);
      $("#web_customer_info").submit();
    }
  </script>
@stop
