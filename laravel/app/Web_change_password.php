<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Web_change_password extends Model
{
  // use Notifiable;
  /**
   * The database table used by the model.
   *
   * @var string
  */
  protected $table = 'WEB_CHANGE_PASSWORD';
  // protected $table = 'USERS';
  protected $primaryKey = 'user_id';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  public $timestamps = false;
  public $incrementing = false;
  protected $fillable = [
    'user_id', 'hash', 'del_flg','create_datetime','update_datetime'
  ];
  public function getAuthIdentifier()
  {
    return $this->original['user_id'];
  }
}
