@extends('layouts.master')

@section('title','LIXILインターネット修理受付センター | 利用者登録[2登録内容のご確認]')

@section('content')
<div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12" style="margin-top: 12px;">
        <div class="box-title_sv">
            <h1>利用者登録</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">利用者情報の入力</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">登録内容のご確認</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4">
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">確認メールの受信</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4">
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">登録受付完了</div>
                </div>
            </li><!--/box-sv-4-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
    <div class="main-vs-12 sv_top20">
      <form id="web_customer_info" method="POST" action="{{ route('user.register.post') }}" accept-charset="UTF-8" role="form">
        {{ csrf_field() }}
        <div class="box-main-form">
            <div class="modal-header_sv">
                <h5>登録内容のご確認</h5>
            </div>
            <div class="con-info_sv" style="padding-top: 2px;">
                <div class="con-info-detail">登録内容をご確認ください。登録には2営業日かかります。弊社にて得意先登録が必要な場合には更に3日営業日かかる場合があります。ご了承ください。</div>
                <div class="tb-responsive" style="margin-top: 0px;">
                    <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                        <tbody class="lixil_body">
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>貴社名</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                        <input type="hidden" name="tokuisaki_sei" value="{{ $user['tokuisaki_sei'] }}">
                                        {{ $user['tokuisaki_sei'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>貴社名（フリガナ）</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="tokuisaki_sei_kana" value="{{ $user['tokuisaki_sei_kana'] }}">
                                      {{ $user['tokuisaki_sei_kana'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>郵便番号</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="tokuisakiYuubinNo" value="{{ $user['tokuisakiYuubinNo'] }}">
                                      {{ $user['tokuisakiYuubinNo'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>都道府県</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="tokuisakiTodoufuken" value="{{ $user['tokuisakiTodoufuken'] }}">
                                      {{ $user['tokuisakiTodoufuken'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>ご住所</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="tokuisakiAddress" value="{{ $user['tokuisakiAddress'] }}">
                                      {{ $user['tokuisakiAddress'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>ご担当者名</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="user_simei" value="{{ $user['user_simei'] }}">
                                      {{ $user['user_simei'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>お電話番号</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="tokuisaki_tel_haihun" value="{{ $user['tokuisaki_tel_haihun'] }}">
                                      {{ $user['tokuisaki_tel_haihun'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>FAX 番号</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="torokusaki_fax" value="{{ $user['torokusaki_fax'] }}">
                                      {{ $user['torokusaki_fax'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>連絡先メールアドレス</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="info_mail_address" value="{{ $user['info_mail_address'] }}">
                                      {{ $user['info_mail_address'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol">
                                    <div class="col-label"><label>修理状況送信用</br>メールアドレス</label></div>
                                </th>
                                <td class="col-data">
                                    <div class="col-result">
                                      <input type="hidden" name="mail_address" value="{{ $user['mail_address'] }}">
                                      {{ $user['mail_address'] }}
                                    </div>
                                </td>
                            </tr><!--/result-->
                            <tr>
                                <th class="labelCol tb-fonm-none">
                                    <div class="col-label"><label>パスワード</label></div>
                                </th>
                                <td class="col-data tb-fonm-none">
                                    <div class="col-result">・・・・・・・・
                                      <input type="hidden" name="passwd" value="{{ $user['passwd'] }}">
                                      <input type="hidden" name="password_confirmation" value="{{ $user['password_confirmation'] }}">
                                      <input type="hidden" name="joho_flg" value="{{ $user['joho_flg'] }}">
                                      <span class="col-result-note">セキュリティーの保護のため表示しません</span>
                                    </div>
                                </td>
                            </tr><!--/result-->
                        </tbody>
                   </table>
                </div><!--tb-responsive-->
            </div><!--con-info_sv-->
        </div><!--/box-main-form-->
        <div class="btn-center-customer">
           <div class="sv_center" style="padding-bottom: 12px;">
                <input type="submit" class="btn-confirm" id="membership_registration" value="この内容で登録する">
           </div>
           <div class="sv_center">
                <a href="{{ route('user.register') }}" class="btn-confirm-small" id="to_correct">修正する</a>
           </div>
        </div>
      </form><!--/form-horizontal_sv-->
    </div>
</div><!--/row-sv-->
@stop
