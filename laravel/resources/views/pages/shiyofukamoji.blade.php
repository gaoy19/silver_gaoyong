@extends('layouts.master')
@section('home_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 使用不可文字一覧')
@section('content')
  <!-- * Start section 修理のご依頼 * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>使用不可文字一覧</h1>
        </div>
    </div>
  </div><!--/row-sv-->
  <!-- * End section 修理のご依頼 * -->
  <!-- * Start section 修理のご依頼完了 * -->
  <div class="row-sv">
      <div class="main-vs-12 sv_top20">
         <div class="box-main-form">
              <div class="con-info_sv">
                  <div class="col-unspecified-title">以下の文字は入力には使用出来ません。ご注意下さい</div>
                  <div class="col-unspecified-des">
                    <img src="{{ asset('assets/images/unspecified_character_list.png')}}" alt="仕様不可文字一覧"/>
                  </div>
              </div>
          </div>
          <!--お問い合わせ先-->
          <div class="contact-sv">
              <div class="contact-title">
                  <h5>お問い合わせ先</h5>
              </div>
              <div class="box-sv-12">
                  <div class="box-sv-title">（株）LIXIL 修理受付センター</div>
                  <div class="mail-sv">
                      <div class="mail_cot">
                          <a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a>
                          <div class="mail_des">メールアドレス：webinfo-imt@i2.inax.co.jp</div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-sv-12">
             <div class="btn-center-received">
                 <div class="sv_center">
                      <a class="btn-confirm_status" id="confirm_status" href="/">トップページに戻る</a>
                 </div>
              </div>
          </div>
      </div><!--main-vs-12 sv_top20-->
  </div><!--/row-sv--><!--/row-sv-->
  <!-- * End section 修理のご依頼完了 * -->
@stop
