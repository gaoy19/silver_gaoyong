<?php

namespace App\Http\Controllers\rcpUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\User;
use App\Iraimoto;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use Mail;
use App\RequestData;
use Session;
class ForgotPasswordController extends Controller
{

  public function sendResetLinkToEmail($token=null, Request $request)
  {
    //check logo
    if(Auth::check()){
      return Redirect::route('login');
    }
    // check Validator
    $this->validate($request, [
        'user_id' => 'required',
        'email' => 'required|email|max:255',
    ],[
      'user_id.required' => 'ユーザー IDは必要です。',
      'email.required' => 'メールアドレスは必要です。',
      'email.max' => 'メールアドレスは:max文字まで行って下さい。',
      'email.email' => '正しいメールアドレスを行って下さい。'
    ]);

    $user = User::where('USER_ID','=',$request->user_id)
                  ->where('INFO_MAIL_ADDRESS','=',$request->email)->first();
    if($user === null || count($user) == 0){
      return back()->withInput()->with('global','ユーザー IDとメールアドレスが一致しません。');
    }

    $reset = new \App\Web_change_password;
    $token = str_random(60);
    $reset->user_id = $user->getAttributes()['user_id'];
    $reset->hash = $token;
    $reset->del_flg = '0';
    $reset->create_datetime = Carbon::now();
    $reset->update_datetime = Carbon::now();
    $reset->save();
    // send Reset Link ToEmail
    Mail::to($request->email)->send(new \App\Mail\UserMail([
      'custom_name' => $user->user_simei,
      'mail_address' => route('user_password.reset',$token)
    ]));
    $status = session(['status'=>'success']);
    return Redirect::route('user_password.sendcomplete',$status.$reset->user_id);
  }
  public function showLinkRequestForm()
  {
    if(Auth::check()){
      return Redirect::route('login');
    }
    return view('users.passwords.email');
  }
  public function showsendComplete(Request $request)
  {
    if(Auth::check()){
      return Redirect::route('login');
    }

    if(Session::get('status') != null){
      $request->session()->flush();
      return view('users.passwords.sendcomplete');
    }
    return Redirect::route('login');
  }

}
