<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class Tokuisaki extends Model
{
    protected $table = 'TOKUISAKI';

    public $timestamps = false;

    protected $primaryKey = 'TOKUISAKI_CD';
    public $incrementing = false;
    protected $fillable = [
      "TOKUISAKI_CD",
    	"TOKUISAKI_SEI",
    	"TOKUISAKI_SEI_KANA",
    	"TOKUISAKI_NA" ,
    	"TOKUISAKI_NA_KANA" ,
    	"TOKUISAKI_RYAKUSYO" ,
    	"TOKUISAKI_RYAKUSYO_KANA" ,
    	"TOKUISAKI_JUSYO" ,
    	"TOKUISAKI_JUSYO_KANA" ,
    	"TOKUISAKI_KATAGAKI" ,
    	"TOKUISAKI_KATAGAKI_KANA" ,
    	"TOKUISAKI_YUBIN" ,
    	"TOKUISAKI_TEL" ,
    	"TOKUISAKI_TEL_HAIHUN" ,
    	"TOROKUSAKI_FAX" ,
    	"TOKUISAKI_KBN" ,
    	"TOKUISAKI_RANK" ,
    	"FAX1" ,
    	"FAX2" ,
    	"FAX3" ,
    	"FAX4" ,
    	"FAX5" ,
    	"FAX6" ,
    	"SEIKYU_SIMEBI" ,
    	"SIHARAI_YOTEBI" ,
    	"SIHARAI_KBN" ,
    	"SEIKYU_SYOSIKI" ,
    	"KAISETU_NENGAPI" ,
    	"SENYO_SEIKYUSYO_FLG" ,
    	"SEIKYU_TIKU_CD" ,
    	"PROGURD_SHOP_FLG" ,
    	"HOJO_KAMOKU_CD" ,
    	"TOROKUBI" ,
    	"TOROKUSYA_CD" ,
    	"KOSINBI" ,
    	"KOSINSYA_CD" ,
    	"SAKUJO_FLG" ,
    	"I_MATE_TOUROKUTEN" ,
    	"FAX7" ,
    	"FAX8" ,
    	"FAX1_5_TYOKUSYU_FLG" ,
    	"POST_DEST" ,
    	"POST_ADDR_KBN" ,
    	"OVERLAY_NO" ,
    	"ETC_NAME" ,
    	"ETC_ZIP_CODE" ,
    	"ETC_ADDRESS" ,
    	"ETC_PHONE" ,
    	"SEIKYU_SYOSIKI2" ,
    	"KOBETU_HAKO_KBN" ,
    	"SEIKYUSYO_SOHUSAKI" ,
    	"SIHARAI_TUKI" ,
    	"SOHU_KBN" ,
    	"BUHIN_NEBIKI_FLG" ,
    	"BUHIN_NEBIKI_NAIYO" ,
    	"BUHIN_NEBIKI_JOKEN" ,
    	"FAX9" ,
    	"IEG_FLG"
    ];

    public static function GOIRAIMOTO_INFO_BY_TOKUISAKI_CD()
    {
	  $data = self::select('tokuisaki_sei', 'tokuisaki_na', 'tokuisaki_jusyo', 'tokuisaki_tel_haihun', 'torokusaki_fax',
	  				'fax1', 'fax2', 'fax3', 'fax5', 'fax6')
                    ->where('sakujo_flg','=','0')
                    ->where('tokuisaki_cd','=',Auth::user()->tokuisaki_cd)
                    ->first();
      return $data;
    }
    public static function TOKUISAKI_INFO_BY_TOKUISAKI_CD($tokuisaki_cd)
    {
	  $data = self::select('tokuisaki_sei', 'tokuisaki_na', 'tokuisaki_jusyo', 'tokuisaki_tel_haihun', 'torokusaki_fax',
	  				'fax1', 'fax2', 'fax3', 'fax5', 'fax6')
                    ->where('sakujo_flg','=','0')
                    ->where('tokuisaki_cd','=',$tokuisaki_cd)
                    ->first();
      return $data;
    }
	public static function GOIRAIMOTO_INFO_C_BY_TOKUISAKI_CD()
    {
        $data = self::select('tokuisaki_sei', 'tokuisaki_sei_kana', 'tokuisaki_jusyo', 'tokuisaki_tel_haihun', 'torokusaki_fax','tokuisaki_yubin',
                    'siharai_yotebi', 'tokuisaki_katagaki')
                    ->where('sakujo_flg', '=', '0')
                    ->where('tokuisaki_cd', '=', Auth::user()->tokuisaki_cd)
                    ->first();

        return $data;
    }
    //YUBIN_BANGO: TOKUISAKI = 1:N relationship
    public function yubinbango()
    {
        return $this->hasOne('App\YubinBango', 'yubin_bango', 'tokuisaki_yubin');
    }
}
