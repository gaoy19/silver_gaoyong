<?php

namespace App\Http\Controllers\UserInformation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Redirect;
use App\User;
use App\Tokuisaki;
use Mail;
use App\Mail\UpdateInformationCompleted;
use App\Custom\CharReplace;

class InformationController extends Controller
{
  private $_model;

  public function __construct(User $model)
  {
    $this->_model = $model;
  }

  //9-2_インターネット修理受付センター_利用者情報修正
  public function changeInformation(User $user_id, Request $request)
  {

    if ($request->has('_token')){
      $tokuisaki_sei           = $request->tokuisaki_sei;
      $tokuisaki_sei_kana      = $request->tokuisaki_sei_kana;
      $tokuisaki_yubin         = $request->tokuisaki_yubin;
      $tokuisaki_jusyo         = $request->tokuisaki_jusyo;
      $tokuisaki_jusyo_address = $request->tokuisaki_jusyo_address;
      $user_simei              = $request->user_simei;
      $tokuisaki_tel_haihun    = $request->tokuisaki_tel_haihun;
      $torokusaki_fax          = $request->torokusaki_fax;
      $info_mail_address       = $request->info_mail_address;
      $mail_address            = $request->mail_address;
    } else {
      $user      = $this->_model::find(Auth::user()->user_id);
      $tokuisaki = Tokuisaki::find($user->tokuisaki_cd);

      if ($tokuisaki !== NULL) {
        $tokuisaki_sei           = $tokuisaki->tokuisaki_sei;
        $tokuisaki_sei_kana      = CharReplace::hanKanaTozenKana($tokuisaki->tokuisaki_sei_kana);
        $tokuisaki_yubin         = substr($tokuisaki->tokuisaki_yubin,0,3).'-'.substr($tokuisaki->tokuisaki_yubin,3,4);
        $tokuisaki_jusyo         = $tokuisaki->yubinbango !== NULL ? $tokuisaki->yubinbango->ken_mei : "";
        $tokuisaki_jusyo_address = $tokuisaki->yubinbango !== NULL ? $tokuisaki->yubinbango->sityoson_mei : "";
        $user_simei              = $user->user_simei;
        $tokuisaki_tel_haihun    = $tokuisaki->tokuisaki_tel_haihun;
        $torokusaki_fax          = $tokuisaki->torokusaki_fax;
        $info_mail_address       = $user->info_mail_address;
        $mail_address            = $user->mail_address;
      } else {
        $tokuisaki_sei           = "";
        $tokuisaki_sei_kana      = "";
        $tokuisaki_yubin         = "";
        $tokuisaki_jusyo         = "";
        $tokuisaki_jusyo_address = "";
        $user_simei              = $user->user_simei;
        $tokuisaki_tel_haihun    = "";
        $torokusaki_fax          = "";
        $info_mail_address       = $user->info_mail_address;
        $mail_address            = $user->mail_address;
      }
    }

    return view('users.informations.changeInformation',
                compact('tokuisaki_sei',
                        'tokuisaki_sei_kana',
                        'tokuisaki_yubin',
                        'tokuisaki_jusyo',
                        'tokuisaki_jusyo_address',
                        'user_simei',
                        'tokuisaki_tel_haihun',
                        'torokusaki_fax',
                        'info_mail_address',
                        'mail_address'));
  }

  //9-2_インターネット修理受付センター_利用者情報修正
  public function confirmInformation(Request $request)
  {
    $values    = $request->all();
    //$values['tokuisaki_sei_kana'] = CharReplace::hanKanaTozenKana($values['tokuisaki_sei_kana']);
    $validator = Validator::make($values,
                                 $this->_model->changeInformationRules(),
                                 $this->_model->changeInformationErrorMessage());
    if ($validator->fails()) {
      return Redirect::back()->withErrors($validator)->withInput($values);
    }
    
    //入力内容が、１つも変更がない場合、エラーとする
    $user= $this->_model::find(Auth::user()->user_id);      
    if($user->user_simei == $request->user_simei
      && $user->info_mail_address == $request->info_mail_address 
      && $user->mail_address == $request->mail_address
      ){
      $message = $this->_model->notChangeInformation();
      return Redirect::back()->withErrors(array('user_simei'=>$message))->withInput($values);
      //return redirect()->back()->withErrors('user_simei', $this->_model->notChangeInformation());
    }
    return view('users.informations.confirmInformation', $values);
  }

  public function completeInformation(Request $request)
  {
    $values    = $request->all();
    $validator = Validator::make($values,
                                 $this->_model->changeInformationRules(),
                                 $this->_model->changeInformationErrorMessage());
    if ($validator->fails()) {
      return Redirect::back()->withErrors($validator)->withInput($values);
    }

    //変更があった情報には、★をつける
    $user      = $this->_model::find(Auth::user()->user_id);

    $user_simei_before       = $user->user_simei;
    $user_simei_after       = $request->user_simei;

    if ($user_simei_before == $user_simei_after) {
       $user_simei = $user_simei_after;
      } else {
       $user_simei           = "★".$user_simei_after;
    }

    $info_mail_address_before       = $user->info_mail_address;
 	  $info_mail_address_after       = $request->info_mail_address;

    if ($info_mail_address_before == $info_mail_address_after) {
        $info_mail_address = $info_mail_address_after;
 	    } else {
        $info_mail_address           = "★".$info_mail_address_after;
    }

    $mail_address_before      = $user->mail_address;
    $mail_address_after       = $request->mail_address;

    if ($mail_address_before == $mail_address_after) {
        $mail_address = $mail_address_after;
    } else {
        $mail_address           = "★".$mail_address_after;
    }
    
    $simei_mail_value =[$request->user_simei,$request->info_mail_address,$request->mail_address,Auth::user()->user_id];
    $simei_mail = \App\WebChangeMailSimei::updateData($simei_mail_value);
    
   if ($simei_mail != '') {
       Mail::to(\Config::get('mail.from.address'))->send(new UpdateInformationCompleted([
      'tokuisaki_sei'           => $request->tokuisaki_sei,
      'tokuisaki_sei_kana'      => $request->tokuisaki_sei_kana,
      'tokuisaki_yubin'         => $request->tokuisaki_yubin,
      'tokuisaki_jusyo'         => $request->tokuisaki_jusyo,
      'tokuisaki_jusyo_address' => $request->tokuisaki_jusyo_address,
      'user_simei'              => $user_simei,
      'tokuisaki_tel_haihun'    => $request->tokuisaki_tel_haihun,
      'torokusaki_fax'          => $request->torokusaki_fax,
      'info_mail_address'       => $info_mail_address,
      'mail_address'            => $mail_address
    ]));
   }
    return view('users.informations.completedInformation');
  }

  public function completedInformation(Request $request)
  {
    if (!$request->has('_token')){
      return Redirect::route('user.change.information');
    }
  }
}
