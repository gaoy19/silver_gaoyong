<?php

namespace App\Http\Controllers\UserInformation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeController extends Controller
{
  //View: 9-1_インターネット修理受付センター_利用者情報の変更
  public function index()
  {
    return view('users.informations.index');
  }
}
