<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Brand extends Model
{
  protected $table = 'BRAND';

  public $timestamps = false;

  protected $primaryKey = 'brand_cd';

  public static function getData($brand_cd)
  {
    $data = self::where('SAKUJO_FLG','=','0')
                ->where('BRAND_CD','=', $brand_cd)  
                ->get();
    return $data;
  }

}
