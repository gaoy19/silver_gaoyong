@extends('layouts.master')
@section('title','LIXILインターネット修理受付センター | 利用者情報の変更[3変更完了]')
@section('content')
<!--  * start section container  * -->
<section class="container-silver">
    <div class="row-sv">
          <!-- * start row top * -->
          <div class="main-vs-12" style="margin-top: 12px;">
              <div class="box-title_sv">
                  <h1>利用者情報の変更</h1>
              </div>
          </div>
      </div><!--/row-sv-->
      <div class="row-sv">
          <div class="col-sv-12 box-progress-12">
              <ul class="row-box-bar">
                  <li class="box-sv-5 active_sv">
                      <div class="box-progress-bar">
                          <div class="box-num-bar">1</div>
                          <div class="box-text-bar">利用者登録情報の変更</div>
                      </div>
                  </li><!--/box-sv-4-->
                  <li class="box-sv-5 active_sv">
                      <div class="box-progress-bar">
                          <div class="box-num-bar">2</div>
                          <div class="box-text-bar">変更内容のご確認</div>
                      </div>
                  </li><!--/box-sv-4-->
                  <li class="box-sv-5 active_sv">
                      <div class="box-progress-bar">
                          <div class="box-num-bar">3</div>
                          <div class="box-text-bar">変更完了</div>
                      </div>
                  </li><!--/box-sv-4-->
              </ul><!--/box-bar-->
          </div><!--/box-progress-12-->
      </div><!--/row-sv-->
      <div class="row-sv">
          <div class="main-vs-12 sv_top20" style="padding-bottom: 50px;">
              <div class="box-main-form">
                  <div class="modal-header_sv">
                      <h5>利用者情報の変更完了</h5>
                  </div>
                  <div class="con-info_sv" style="padding-top: 2px;">
                      <div class="con-info-detail">
                          <p>利用者情報の変更が完了しました。</p>
                      </div>
                  </div><!--con-info_sv-->
              </div><!--/box-main-form-->
          </div>
      </div><!--/row-sv-->
</section><!--/container-silver-->
<!--  * end section container  * -->
@stop
