@extends('layouts.master')
@section('title','LIXILインターネット修理受付センター | 登録情報の変更')
@section('content')
<!--  * start section container  * -->
<section class="container-silver">
    <div class="row-sv">
        <div class="main-vs sv_top20" style="padding-bottom: 150px;">
            <div class="col-rs-title">
                <h1>登録情報の変更</h1>
            </div>
            <div class="box-main-form">
                <div class="box-data-12" style="padding: 22px 30px 22px 30px;">
                    <ul class="link-back">
                        <li><a href="{{ route('user.change.password') }}">パスワードの変更</a></li>
                        <li><a href="{{ route('user.change.information') }}">利用者情報の変更</a></li>
                    </ul>
                </div><!--/box-data-12-->
            </div><!--/box-main-form1-->
        </div><!--/main-vs-12 sv_top20-->
    </div><!--/row-sv-->
</section><!--/container-silver-->
<!--  * end section container  * -->
@stop
