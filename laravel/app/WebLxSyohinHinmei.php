<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class WebLxSyohinHinmei extends Model
{
    protected $table = 'WEB_LX_SYOHIN_HINMEI';

    public $timestamps = false;


    public static function getLists()
    {
      $lists = self::where('SAKUJO_FLG','=','0')
  									->get()
                    ->toArray();
      return $lists;
    }

    public static function getBrandCd($hinmei)
    {
      $data = self::where('HINMEI','=', $hinmei)
                   ->get();;
      return $data[0]->brand_cd;
    }
}
