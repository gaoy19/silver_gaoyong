@extends('layouts.master')

@section('title','LIXILインターネット修理受付センター | 利用者登録[4利用者登録完了]')

@section('content')
<div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12" style="margin-top: 12px;">
        <div class="box-title_sv">
            <h1>利用者登録</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">登録内容の入力</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">登録内容のご確認</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">確認メールの受信</div>
                </div>
            </li><!--/box-sv-4-->
            <li class="box-sv-4 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">登録受付完了</div>
                </div>
            </li><!--/box-sv-4-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
    <div class="main-vs-12 sv_top20" style="padding-bottom: 50px;">
        <div class="box-main-form">
            <div class="modal-header_sv">
                <h5>登録受付完了</h5>
            </div>
            <div class="con-info_sv" style="padding-top: 2px;">
                <div class="con-info-detail">
                    <p>利用者登録受付が完了しました。５営業日以内に、修理受付センターより、登録受付完了のご連絡を致します。</p>
                </div>
            </div><!--con-info_sv-->
        </div><!--/box-main-form-->
    </div>
</div><!--/row-sv-->
@stop
