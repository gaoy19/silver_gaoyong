<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shurinojokyo;
use App\iraimoto;
use App\RequestData;
use Session;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
class ShurinojokyoController extends Controller
{
  /**
 * Call Model Iraimoto
 *
 * @param $model
 */
   public function __construct(Iraimoto $model)
   {
     $this->model = $model;
   }
  /**
  * Display  top page
  * ご依頼中の修理の状況[トップ]
  * @return \Illuminate\Http\Response
  */
  public function showToppu(Request $request)
  {
    //get data
    $shurinojokyos = $this->model->getIraimoto();
    $shurinojokyos['shurinojokyos'] = $shurinojokyos;
    return view('pages.shurinojokyo.toppu',$shurinojokyos);

  }
  /**
  * Display  irainaiyonokakunin
  * ご依頼中の修理の状況[依頼内容の確認]
  * @return \Illuminate\Http\Response
  */
  public function showIrainaiyonokakunin()
  {
    //get data
    return view('pages.shurinojokyo.irainaiyonokakunin');
  }
  /**
  * Display page get Irainaiyonokakunin Printpreview
  *
  * @return \Illuminate\Http\Response
  */
  public function showIrainaiyonokakuninPrintpreview()
  {
    return view('pages.shurinojokyo.irainaiyonokakuninprintpreview');
  }
  /**
  * Display  homonyoteihokoku
  * ご依頼中の修理の状況[訪問予定報告]
  * @return \Illuminate\Http\Response
  */
  public function showHomonyoteihokoku()
  {
    //get data
    return view('pages.shurinojokyo.homonyoteihokoku');
  }
  /**
  * Display  subao
  * ご依頼中の修理の状況[速報]
  * @return \Illuminate\Http\Response
  */
  public function showSubao()
  {
    return view('pages.shurinojokyo.subao');
  }
  /**
  * Display  preliminaryreport
  * ご依頼中の修理の状況[速報]
  * @return \Illuminate\Http\Response
  */
  public function showPreliminaryreport()
  {
    return view('pages.shurinojokyo.preliminaryreport');
  }
  /**
  * Display  preliminaryreport
  * ご依頼中の修理の状況[速報]
  * @return \Illuminate\Http\Response
  */
  public function showKakuho()
  {
    return view('pages.shurinojokyo.kakuho');
  }
  /**
  * Display  Final report
  * 印刷イメージ（確報）
  * @return \Illuminate\Http\Response
  */
  public function showFinalreport()
  {
    return view('pages.shurinojokyo.finalreport');
  }

}
