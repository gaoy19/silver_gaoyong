@extends('layouts.master')
@section('repair_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理の状況[経過・結果報告（速報）]')
@section('content')
  <div class="row-sv">
   <div class="main-vs-12 sv_top20" style="margin-bottom:70px;">
      <div class="col-rs-title border-bot">
        <div class="col-sv-7">
           <h2>経過・結果報告（速報）</h2>
        </div>
        <div class="col-sv-5">
            <div class="col-print-5_rs row-15">
                <button class="btn-print" type="button" onclick="showPrintForm();"><span class="icon-print"><img src="/assets/images/print.png" width="18"></span>報告書を印刷する</button>
            </div>
        </div>
      </div><!--/border-bot 経過・結果報告（速報）-->
      <!-- * start section 1 訪問予定報告 * -->
      <div class="box-main-form">
       <div class="box-data-12">
                <div class="tb-responsive_rs">
                   <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                       <tbody class="lixil_body">
                           <tr>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>登録日</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">
                                     {{ $detail['uketukebi'] }}
                                   </div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>受付 No</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">{{ $detail['uketuke_no'] }}</div>
                               </td>
                           </tr><!--/result-->
                           <tr>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label>担当者</label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs">{{ $detail['ce_sei'] }}</div>
                               </td>
                               <td class="Col-data-2">
                                   <div class="col-label-rs"><label></label></div>
                               </td>
                               <td class="col-data-4">
                                   <div class="col-data_rs"></div>
                               </td>
                           </tr><!--/result-->
                       </tbody>
                    </table>
               </div><!--/tb-responsive-->
           </div><!--/box-data-12-->
      </div><!--/box-main-form1-->
      <!-- * end section 1 ステータス * -->
      <!-- * start section 2 経過・結果情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>経過・結果情報</h5>
           </div>
         </div>
        <div class="con-info_sv">
           <div class="tb-responsive">
               <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                   <tbody class="lixil_body">
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>結果報告</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['kekka'] }}</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>訪問実施日</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['houmonbi'] }}</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>簡易修理内容</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['syuri_naiyou'] }}</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>請求内容</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['sekyu_naiyou'] }}</div>
                         </td>
                     </tr><!--/result-->
                   </tbody>
              </table>
        </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_2-->
      <!-- * end section 2 経過・結果情報 * -->
      <!-- * start section 3 お客さま情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>お客さま情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
          <div class="tb-responsive">
           <table class="detailList" border="0" cellpadding="0" cellspacing="0">
               <tbody class="lixil_body">
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま氏名</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">
                           {{ $detail['kokyaku_mei'] }}
                         </div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま住所</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">{{ $detail['kokyaku_jusyo'] }}</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま電話番号</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">{{ $detail['kokyaku_tel_haihun'] }}</div>
                     </td>
                 </tr><!--/result-->
                 <tr>
                     <th class="labelCol">
                         <div class="col-label"><label>お客さま建物名・部屋番号</label></div>
                     </th>
                     <td class="col-data">
                         <div class="col-result">{{ $detail['kokyaku_katagaki'] }}</div>
                     </td>
                 </tr><!--/result-->
               </tbody>
          </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_3--><!--/box-main-form-2-->
      <!-- * end section 3 お客さま情報 * -->
      <!-- * start section 4 修理依頼内容 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>修理依頼内容</h5>
           </div>
        </div>
        <div class="con-info_sv">
             <div class="tb-responsive">
               <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                   <tbody class="lixil_body">
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>商品名</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['hinmei'] }}</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>品番</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['syohin_hinban'] }}</div>
                         </td>
                     </tr><!--/result-->
                     <tr>
                         <th class="labelCol">
                             <div class="col-label"><label>ご依頼内容</label></div>
                         </th>
                         <td class="col-data">
                             <div class="col-result">{{ $detail['irainaiyo'] }}</div>
                         </td>
                     </tr><!--/result-->
                   </tbody>
              </table>
         </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_4-->
      <!-- * end section 4 修理依頼内容 * -->
      <!-- * start section 5 ご依頼元情報 * -->
      <div class="box-main-form">
        <div class="modal-header_rs">
           <div class="col-sv-12">
               <h5>ご依頼元情報</h5>
           </div>
        </div>
        <div class="con-info_sv">
          <div class="tb-responsive">
            <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                 <tbody class="lixil_body">
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>得意先コード</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['iraimoto_cd'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>貴社名</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['iraimoto_simei'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>ご依頼元担当者様</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['iraimoto_tanto'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>貴社住所</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['iraimoto_jusyo'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>貴社電話番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['iraimoto_tel_haihun'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>貴社FAX</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['iraimoto_fax'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>お得意先顧客No</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['hm_kokyaku_no'] }}</div>
                       </td>
                   </tr><!--/result-->
                 </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_5-->
      <!-- * end section 5 ご依頼元情報 * -->
      <!-- * start section 6 工事店 * -->
      <div class="box-main-form">
        <div class="con-info_sv">
          <div class="tb-responsive">
            <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                 <tbody class="lixil_body">
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>貴社注文番号</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['tyumon_bango'] }}</div>
                       </td>
                   </tr><!--/result-->
                   <tr>
                       <th class="labelCol">
                           <div class="col-label"><label>工事店</label></div>
                       </th>
                       <td class="col-data">
                           <div class="col-result">{{ $detail['koziten'] }}</div>
                       </td>
                   </tr><!--/result-->
                 </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div><!--/con-group-sv-->
      </div><!--/box-main-form_5-->
      <!-- * end section 7 ご請求先情報 * -->
      <div class="read-info_center center">
          <a class="btn-info_rs" href="{{ url()->previous() }}">前に戻る</a>
      </div><!--/read-info_center-->
   </div><!--/main-vs-12 sv_top20-->
  </div><!--/row-sv-->
  <script type="text/javascript">
    //-------------------- 追加　2007/03/12　start --------------------//
    function showPrintForm() {
      window.open("{{ route('webuketuke.sokuhou.print',$detail['web_uketuke_no']) }}", 'sokuhou.print', "top=0,left=0,height=650,width=1050,location=no,scrollbars=yes,toolbar=yes,menubar=yes,status=no");
    }
    //-------------------- 追加　2007/03/12　end   --------------------//
  </script>
@stop
