<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class WebUketuke extends Model
{
  protected $table = 'WEB_UKETUKE';

  public $timestamps = false;

  // protected $primaryKey = 'web_uketuke_no';

  /**
   * バリデーションのルール
   */
  public function searchRules()
  {
    return  [
      "tanto" => "required_without_all:tanto,uketuke_no,uketukebiFromYYYY,uketukebiFromMM,uketukebiFromDD,uketukebiToYYYY,uketukebiToMM,uketukebiToDD,tyumon_bango,web_uketuke_no,kokyaku_sei_kanji,kokyaku_sei_kana,todoufuken,kokyaku_jusyo,tel|max:40|zenkaku",
      "uketuke_no" => "hankaku",
      "tyumon_bango" => "hankaku",
      "web_uketuke_no" => "hankaku",
      "kokyaku_sei_kanji" => "zenkaku",
      "kokyaku_sei_kana" => "zenkaku",
      "kokyaku_jusyo" => "zenkaku",
      "tel" => "hankaku|nullable|numeric"
    ];
  }

  /**
  * バリデーションのメッセージエラー
  */
  public function searchErrorMessage()
  {
      return  [
        "tanto.required_without_all" => "検索条件を１つ以上設定してください。",
        "tanto.max" => "担当者の指定は:max文字まで行ってください。",
        "tanto.zenkaku" => "担当者の指定は全角で行ってください。",
        "uketuke_no.hankaku" => "受付NOの指定は半角で行ってください。",
        "tyumon_bango.hankaku" => "貴社注文番号の指定は半角で行って下さい。",
        "web_uketuke_no.hankaku" => "お申し込み Noの指定は半角で行って下さい。",
        "kokyaku_sei_kanji.zenkaku" => "お客さま名（漢字）の指定は全角で行って下さい。",
        "kokyaku_sei_kana.zenkaku" => "お客さま名（フリガナ）の指定は全角で行って下さい。",
        "kokyaku_jusyo.zenkakau" => "お客さま名の指定は全角で行って下さい。",
        "tel.numeric" => "半角数字のみで入力して下さい。"
      ];
  }

  /**
   * 修理の作業状況作業状況を取得
   */
  public function getWebUketukeStatus($webno)
  {
    return DB::table('WEB_UKETUKE')
              ->leftJoin('IRAIUKETUKE', 'WEB_UKETUKE.uketuke_no', '=', 'IRAIUKETUKE.uketuke_no')
              ->leftJoin('SAGYOYOTEI','IRAIUKETUKE.uketuke_no', '=', 'SAGYOYOTEI.uketuke_no')
              ->select(
                'WEB_UKETUKE.web_kanryo_flg',
                'IRAIUKETUKE.uketuke_no',
                'IRAIUKETUKE.UC_CD',
                'SAGYOYOTEI.SYURITAISYO_SEQ AS seq',
                'IRAIUKETUKE.kanryo_flg AS iu_kanryo_flg',
                'SAGYOYOTEI.kanryo_flg AS sy_kanryo_flg',
                'SAGYOYOTEI.syuri_jisekibi'
              )
              ->where('WEB_UKETUKE.web_uketuke_no','=',$webno)
              ->first();
  }

  public function getMikanryoList($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_MIKANRYO()),$search);
  }

  public function getCountMikanryo($search = array())
  {
    return DB::select(DB::raw($this->_SQL_COUNT_MIKANRYO()),$search);
  }

  public function getKanryoList($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_KANRYO()),$search);
  }

  public function getSokuhou($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_SOKUHOU()),$search);
  }

  public function getYotei($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_YOTEI()),$search);
  }

  public function getkakuhouUketukeNo($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_KAKUHOU_UKETUKE_NO()),$search);
  }

  public function getKakuhou($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_KAKUHOU()),$search);
  }

  public function getCountUKETUKECHU($search = array())
  {
    return DB::select(DB::raw($this->_SQL_COUNT_UKETUKECHU()),$search);
  }

  public function getCancelList($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_CANCEL()),$search);
  }

  public function getCountCancel($search = array())
  {
    return DB::select(DB::raw($this->_SQL_COUNT_CANCEL()),$search);
  }

  public function getCountKanryo($search = array())
  {
    return DB::select(DB::raw($this->_SQL_COUNT_KANRYO()),$search);
  }

  public function getCountAll($search = array())
  {
    $sql = "SELECT COUNT(*) cnt FROM ({$this->_SQL_ALL()})";
    return DB::select(DB::raw($sql),$search);
  }

  public function getAllList($search = array())
  {
    $sql = "SELECT * FROM ( SELECT ROWNUM rownumber,tbl.* FROM ({$this->_SQL_ALL()}) tbl )WHERE rownumber > :startrow AND rownumber <= :endrow";
    return DB::select(DB::raw($sql),$search);
  }

  public function getUeketukeByWebNo($search = array())
  {
    $sql = <<<SQL
     SELECT * FROM (
         {$this->_SQL_ALL()}
       )
     WHERE WEB_NO = :web_no
SQL;
    return DB::select(DB::raw($sql),$search);
  }

  /**
   * 受付一覧取得用SQL(全て)
   */
  private function _SQL_ALL()
  {
    $sql = <<<SQL
      SELECT
         case
           when sy.tekiyo_cd IN (dt.code) then dt.setumei
           else
             case
               when sy.kanryo_flg = '0' then '未着手'
               else NULL
             end
         end                       jyotai,
        NVL(wu.web_uketuke_no,iu.uketuke_no)          web_no,
        TO_CHAR(iu.uketukebi, 'YYYY/MM/DD')  uketukebi,
        wu.web_uketuke_no          web_uketuke_no,
        iu.uketuke_no              uketuke_no,
        ir.iraimoto_tanto          iraimoto_tanto,
        uk.kokyaku_sei             kokyaku_sei,
        uk.kokyaku_na              kokyaku_na,
        uk.kokyaku_jusyo           kokyaku_jusyo,
        uk.kokyaku_tel_haihun      kokyaku_tel,
        um.syohin_hinban           hinban,
        um.irainaiyo               irainaiyo,
        sy.ce_cd                   ce_cd,
        ce.ce_sei                  ce_sei,
        iu.uc_cd                   uc_cd,
        um.syuritaisyo_seq         seq,
        sy.kanryo_flg				sy_kanryo_flg,
        iu.kanryo_flg				iu_kanryo_flg,
        sy.syuri_jisekibi	        syurijisekibi,
        DECODE(SY.RENRAKUBI,NULL,NULL,TO_CHAR(SY.SYURI_YOTEIBI,'YYYY/MM/DD')) syuriyoteibi,
        uk.kokyaku_sei_kana        kokyaku_sei_kana,
        ir.tyumon_bango            tyumon_bango,
        uk.kokyaku_tel             tel
        , um.BRAND_CD          BRAND_CD

         ,DECODE(SY.KANRYO_FLG,'0','1','1','1','2')             	SORT_KBN
      FROM
        (   SELECT

        distinct
        sy.uc_cd               uc_cd,
        sy.uketuke_no          uketuke_no,
        sy.uketuke_nendo          uketuke_nendo,
        max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by sy.SYURI_JISEKIBI desc,DECODE(sy.KANRYO_FLG,'8','1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

          FROM
            sagyoyotei             sy,
            uketuke_iraimoto       ui
          WHERE
              ui.uc_cd      = sy.uc_cd
            AND ui.uketuke_no = sy.uketuke_no

            AND ui.uketuke_nendo = sy.uketuke_nendo

            AND UI.IRAIMOTO_CD= :iraimoto_cd

            AND sy.sakujo_flg = '0'

        ) iis,
        iraiuketuke                    iu,
        uketuke_meisai                 um,
        sagyoyotei                     sy,
        syuritantosya                  ce,
        uketuke_iraimoto               ir,
        uketuke_kokyaku                uk,
        web_uketuke                    wu,

         d_tekiyo_cd                   dt

      WHERE
          iu.uc_cd            = um.uc_cd
        AND iu.uketuke_no      = um.uketuke_no
        AND um.uc_cd           = sy.uc_cd
        AND um.uketuke_no      = sy.uketuke_no
        AND um.syuritaisyo_seq = sy.syuritaisyo_seq
        AND iu.uc_cd           = ir.uc_cd
        AND iu.uketuke_no      = ir.uketuke_no
        AND iu.uc_cd           = uk.uc_cd
        AND iu.uketuke_no      = uk.uketuke_no
        AND sy.ce_cd           = ce.ce_cd
        AND ir.iraimoto_cd     = :iraimoto_cd

        AND iu.uketuke_no      = wu.uketuke_no(+)
        AND iis.uc_cd           = sy.uc_cd
        AND iis.uketuke_no      = sy.uketuke_no
        AND iis.max_JISIRIREKI_SEQ  = sy.JISIRIREKI_SEQ

        AND sy.tekiyo_cd       = dt.code(+)

        AND iu.uketuke_nendo   = um.uketuke_nendo
        AND um.uketuke_nendo   = sy.uketuke_nendo
        AND iu.uketuke_nendo   = ir.uketuke_nendo
        AND iu.uketuke_nendo   = uk.uketuke_nendo
        AND iu.uketuke_nendo   = wu.uketuke_nendo(+)
        AND iis.uketuke_nendo  = sy.uketuke_nendo

        AND iu.kanryo_flg <> '8'
        AND iu.kanryo_flg <> '9'

        AND ce.sakujo_flg = '0'
        AND um.sakujo_flg = '0'
        AND ir.sakujo_flg = '0'
        AND uk.sakujo_flg = '0'
         AND (IU.C_VOICE_FLG IS NULL OR IU.C_VOICE_FLG<>'1')
      UNION ALL
      SELECT
        DECODE(wu.web_kanryo_flg,'0','受付中','2','確認中',NULL)   jyotai,
        wu.web_uketuke_no        web_no,
        TO_CHAR(wu.torokubi, 'YYYY/MM/DD')              uketukebi,
        wu.web_uketuke_no        web_uketuke_no,
        wu.uketuke_no            uketuke_no,
        wu.iraimoto_tanto        iraimoto_tanto,
        wu.kokyaku_sei           kokyaku_sei,
        wu.kokyaku_na            kokyaku_na,
        wu.kokyaku_jusyo         kokyaku_jusyo,
        wu.kokyaku_tel_haihun    kokyaku_tel,
        wu.syohin_hinban         hinban,
        wu.irainaiyo             irainaiyo,
        NULL                     ce_cd,
        NULL                     ce_sei,
        wu.uc_cd                 uc_cd,
        '01'                     seq,
        wu.web_kanryo_flg		  sy_kanryo_flg,
        NULL					  iu_kanryo_flg,
        TO_DATE(NULL,'YYYY/MM/DD')					syurijisekibi,
        ''                                         syuriyoteibi,
        wu.kokyaku_sei_kana                        kokyaku_sei_kana,
        wu.tyumon_bango                            tyumon_bango,
        replace( wu.kokyaku_tel_haihun, '-', '' )  tel
        , wu.BRAND_CD        BRAND_CD
         ,''             	SORT_KBN
      FROM
        web_uketuke              wu
      WHERE
          wu.web_kanryo_flg <> '9'

         AND (wu.C_VOICE_FLG IS NULL OR wu.C_VOICE_FLG<>'1')

        AND wu.sakujo_flg      = '0'
         AND WU.UKETUKE_NO    IS NULL
        and wu.iraimoto_cd = :iraimoto_cd
      UNION ALL
      SELECT
                DECODE(SAGYOYOTEI.KANRYO_FLG,
                        '9','完了',NULL) JOTAI,
                NVL(WEB_UKETUKE.WEB_UKETUKE_NO, IRAIUKETUKE.UKETUKE_NO) web_no,
                TO_CHAR(IRAIUKETUKE.UKETUKEBI, 'YYYY/MM/DD')  UKETUKEBI,
                WEB_UKETUKE.WEB_UKETUKE_NO     web_uketuke_no,
                IRAIUKETUKE.UKETUKE_NO     uketuke_no,
                UKETUKE_IRAIMOTO.IRAIMOTO_TANTO        iraimoto_tanto,
                UKETUKE_KOKYAKU.KOKYAKU_SEI    kokyaku_sei,
                UKETUKE_KOKYAKU.KOKYAKU_NA     kokyaku_na,
                UKETUKE_KOKYAKU.KOKYAKU_JUSYO  kokyaku_jusyo,
                UKETUKE_KOKYAKU.KOKYAKU_TEL_HAIHUN     kokyaku_tel,
                UKETUKE_MEISAI.SYOHIN_HINBAN       hinban,
                UKETUKE_MEISAI.IRAINAIYO       irainayo,
                SAGYOYOTEI.CE_CD       ce_cd,
                SYURITANTOSYA.CE_SEI       ce_sei,
                IRAIUKETUKE.UC_CD      uc_cd,
                UKETUKE_MEISAI.SYURITAISYO_SEQ                 seq,
                SAGYOYOTEI.KANRYO_FLG                          sy_kanryo_flg,
                IRAIUKETUKE.KANRYO_FLG                         iu_kanryo_flg,
                SAGYOYOTEI.SYURI_JISEKIBI                      syurijisekibi,
                ''                                             syuriyoteibi,
                UKETUKE_KOKYAKU.kokyaku_sei_kana      kokyaku_sei_kana,
                UKETUKE_IRAIMOTO.TYUMON_BANGO         tyumon_bango,
                UKETUKE_KOKYAKU.KOKYAKU_TEL           TEL
          , UKETUKE_MEISAI.BRAND_CD
         ,DECODE(SAGYOYOTEI.KANRYO_FLG,'9','1','2')             	SORT_KBN
            FROM
                (SELECT

                distinct
                sy.uc_cd 				uc_cd,
                sy.uketuke_no			uketuke_no,
                sy.uketuke_nendo			uketuke_nendo,
                max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by nvl(sy.kanryo_flg,'0') desc,DECODE(SYURI_JISEKIBI,NULL,'1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

                FROM
                    sagyoyotei			sy,
                    uketuke_iraimoto	ui
                WHERE
                        ui.uc_cd = sy.uc_cd
                    AND ui.uketuke_no = sy.uketuke_no

                    AND ui.uketuke_nendo = sy.uketuke_nendo

                    AND ui.IRAIMOTO_CD = :iraimoto_cd

                    AND ui.sakujo_flg = '0'
                    AND sy.sakujo_flg = '0'

                ) iis,
                IRAIUKETUKE,
                UKETUKE_MEISAI,
                SAGYOYOTEI,
                SYURITANTOSYA,
                UKETUKE_IRAIMOTO,
                UKETUKE_KOKYAKU,
                WEB_UKETUKE
            WHERE
                IRAIUKETUKE.UC_CD				= UKETUKE_MEISAI.UC_CD AND
                IRAIUKETUKE.UKETUKE_NO			= UKETUKE_MEISAI.UKETUKE_NO AND
                UKETUKE_MEISAI.UC_CD			= SAGYOYOTEI.UC_CD AND
                UKETUKE_MEISAI.UKETUKE_NO		= SAGYOYOTEI.UKETUKE_NO AND
                UKETUKE_MEISAI.SYURITAISYO_SEQ	= SAGYOYOTEI.SYURITAISYO_SEQ AND
                IRAIUKETUKE.UC_CD				= UKETUKE_IRAIMOTO.UC_CD AND
                IRAIUKETUKE.UKETUKE_NO			= UKETUKE_IRAIMOTO.UKETUKE_NO AND
                IRAIUKETUKE.UC_CD				= UKETUKE_KOKYAKU.UC_CD AND
                IRAIUKETUKE.UKETUKE_NO			= UKETUKE_KOKYAKU.UKETUKE_NO AND
                SAGYOYOTEI.CE_CD				= SYURITANTOSYA.CE_CD AND
                IRAIUKETUKE.UKETUKE_NO			= WEB_UKETUKE.UKETUKE_NO(+) AND

                IIS.UC_CD						= SAGYOYOTEI.UC_CD AND
                IIS.UKETUKE_NO					= SAGYOYOTEI.UKETUKE_NO AND
                IIS.MAX_JISIRIREKI_SEQ			= SAGYOYOTEI.JISIRIREKI_SEQ AND

                IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_MEISAI.UKETUKE_NENDO AND
                UKETUKE_MEISAI.UKETUKE_NENDO	= SAGYOYOTEI.UKETUKE_NENDO AND
                IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_IRAIMOTO.UKETUKE_NENDO AND
                IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_KOKYAKU.UKETUKE_NENDO AND
                IRAIUKETUKE.UKETUKE_NENDO		= WEB_UKETUKE.UKETUKE_NENDO(+) AND
                IIS.UKETUKE_NENDO				= SAGYOYOTEI.UKETUKE_NENDO AND

                UKETUKE_IRAIMOTO.IRAIMOTO_CD	= :iraimoto_cd AND
                UKETUKE_MEISAI.SAKUJO_FLG		= '0' AND


                IRAIUKETUKE.KANRYO_FLG		= '9' AND

                UKETUKE_KOKYAKU.SAKUJO_FLG		= '0' AND
                UKETUKE_IRAIMOTO.SAKUJO_FLG	= '0' AND
                SYURITANTOSYA.SAKUJO_FLG		= '0'
         AND (IRAIUKETUKE.C_VOICE_FLG IS NULL OR IRAIUKETUKE.C_VOICE_FLG<>'1')
            UNION ALL
            SELECT
                DECODE(SAGYOYOTEI.KANRYO_FLG,'8','キャンセル',NULL) JOTAI,
                NVL(WEB_UKETUKE.WEB_UKETUKE_NO, IRAIUKETUKE.UKETUKE_NO) web_no,
                TO_CHAR(IRAIUKETUKE.UKETUKEBI, 'YYYY/MM/DD') AS UKETUKEBI,
                WEB_UKETUKE.WEB_UKETUKE_NO web_uketuke_no,
                IRAIUKETUKE.UKETUKE_NO,
                UKETUKE_IRAIMOTO.IRAIMOTO_TANTO,
                UKETUKE_KOKYAKU.KOKYAKU_SEI,
                UKETUKE_KOKYAKU.KOKYAKU_NA,
                UKETUKE_KOKYAKU.KOKYAKU_JUSYO,
                UKETUKE_KOKYAKU.KOKYAKU_TEL_HAIHUN,
                UKETUKE_MEISAI.SYOHIN_HINBAN,
                UKETUKE_MEISAI.IRAINAIYO,
                SAGYOYOTEI.CE_CD,
                SYURITANTOSYA.CE_SEI,
                IRAIUKETUKE.UC_CD,
                UKETUKE_MEISAI.SYURITAISYO_SEQ,
                SAGYOYOTEI.KANRYO_FLG,
                IRAIUKETUKE.KANRYO_FLG,
                SAGYOYOTEI.SYURI_JISEKIBI,
                ''                                             syuriyoteibi,
                UKETUKE_KOKYAKU.kokyaku_sei_kana       kokyaku_sei_kana,
                UKETUKE_IRAIMOTO.tyumon_bango          tyumon_bango,
                UKETUKE_KOKYAKU.KOKYAKU_TEL            TEL
          , UKETUKE_MEISAI.BRAND_CD
         ,DECODE(SAGYOYOTEI.KANRYO_FLG,'8','1','2')             	SORT_KBN
            FROM
                (SELECT
                    sy.uc_cd		uc_cd,
                    sy.uketuke_no		uketuke_no,
                    SY.SYURITAISYO_SEQ	SYURITAISYO_SEQ,

                    sy.uketuke_nendo	uketuke_nendo,

                    max(JISIRIREKI_SEQ)	max_JISIRIREKI_SEQ
                FROM
                    sagyoyotei		sy,
                    uketuke_iraimoto	ui
                WHERE
                    ui.uc_cd = sy.uc_cd
                    AND ui.uketuke_no = sy.uketuke_no

                    AND ui.uketuke_nendo = sy.uketuke_nendo

                    AND ui.IRAIMOTO_CD = :iraimoto_cd
                    AND ui.sakujo_flg = '0'
                    AND sy.sakujo_flg = '0'
                GROUP BY
                    sy.uc_cd,
                    sy.uketuke_no,
                    SY.SYURITAISYO_SEQ

                    ,sy.uketuke_nendo

                ) iis,
                IRAIUKETUKE,
                UKETUKE_MEISAI,
                SAGYOYOTEI,
                SYURITANTOSYA,
                UKETUKE_IRAIMOTO,
                UKETUKE_KOKYAKU,
                WEB_UKETUKE
            WHERE
                IRAIUKETUKE.UC_CD		= UKETUKE_MEISAI.UC_CD AND
                IRAIUKETUKE.UKETUKE_NO		= UKETUKE_MEISAI.UKETUKE_NO AND
                UKETUKE_MEISAI.UC_CD		= SAGYOYOTEI.UC_CD AND
                UKETUKE_MEISAI.UKETUKE_NO	= SAGYOYOTEI.UKETUKE_NO AND
                UKETUKE_MEISAI.SYURITAISYO_SEQ	= SAGYOYOTEI.SYURITAISYO_SEQ AND
                IRAIUKETUKE.UC_CD		= UKETUKE_IRAIMOTO.UC_CD AND
                IRAIUKETUKE.UKETUKE_NO		= UKETUKE_IRAIMOTO.UKETUKE_NO AND
                IRAIUKETUKE.UC_CD		= UKETUKE_KOKYAKU.UC_CD AND
                IRAIUKETUKE.UKETUKE_NO		= UKETUKE_KOKYAKU.UKETUKE_NO AND
                SAGYOYOTEI.CE_CD		= SYURITANTOSYA.CE_CD AND
                IRAIUKETUKE.UKETUKE_NO		= WEB_UKETUKE.UKETUKE_NO(+) AND

                IIS.UC_CD			= SAGYOYOTEI.UC_CD AND
                IIS.UKETUKE_NO			= SAGYOYOTEI.UKETUKE_NO AND
                IIS.SYURITAISYO_SEQ		= SAGYOYOTEI.SYURITAISYO_SEQ AND
                IIS.MAX_JISIRIREKI_SEQ		= SAGYOYOTEI.JISIRIREKI_SEQ AND

                IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_MEISAI.UKETUKE_NENDO AND
                UKETUKE_MEISAI.UKETUKE_NENDO	= SAGYOYOTEI.UKETUKE_NENDO AND
                IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_IRAIMOTO.UKETUKE_NENDO AND
                IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_KOKYAKU.UKETUKE_NENDO AND
                IRAIUKETUKE.UKETUKE_NENDO		= WEB_UKETUKE.UKETUKE_NENDO(+) AND
                IIS.UKETUKE_NENDO				= SAGYOYOTEI.UKETUKE_NENDO AND

                UKETUKE_IRAIMOTO.IRAIMOTO_CD	= :iraimoto_cd AND
                UKETUKE_MEISAI.SAKUJO_FLG	= '0' AND

                IRAIUKETUKE.KANRYO_FLG		= '8' AND

                UKETUKE_KOKYAKU.SAKUJO_FLG	= '0' AND
                UKETUKE_IRAIMOTO.SAKUJO_FLG	= '0' AND
                SYURITANTOSYA.SAKUJO_FLG	= '0'
         AND (IRAIUKETUKE.C_VOICE_FLG IS NULL OR IRAIUKETUKE.C_VOICE_FLG<>'1')
         ORDER BY uketukebi desc
SQL;
    return $sql;
  }

  /**
   *依頼状況一覧検索ＳＱＬ(カウント受付中部分)
   */
  private function _SQL_COUNT_UKETUKECHU()
  {
    $sql = <<<SQL
    SELECT count(*) cnt FROM (
      {$this->_SQL_MIKANRYO()}
    ) WHERE JYOTAI = '受付中'
SQL;
    return $sql;
  }

  /**
   *依頼状況一覧検索ＳＱＬ(カウント未完了部分)
   */
  private function _SQL_COUNT_MIKANRYO()
  {
    $sql = <<<SQL
    SELECT count(*) cnt FROM (
      {$this->_SQL_MIKANRYO()}
    )
SQL;
    return $sql;
  }

  /**
   *依頼状況一覧検索ＳＱＬ(未完了部分)
   */
  private function _SQL_SELECT_MIKANRYO()
  {
    $sql = <<<SQL
    SELECT * FROM (
SELECT ROWNUM rownumber,tbl.* FROM (
    {$this->_SQL_MIKANRYO()}
     ) tbl
     )
     WHERE rownumber > :startrow AND rownumber <= :endrow
SQL;
    return $sql;
  }

  /**
   *依頼状況一覧検索ＳＱＬ(未完了部分)
   */
  private function _SQL_MIKANRYO()
  {
    $sql = <<<SQL
    SELECT
          um.syuritaisyo_seq         seq,
          case
            when sy.tekiyo_cd IN (dt.code) then dt.setumei
            else
              case
                when sy.kanryo_flg = '0' then '未着手'
                else NULL
              end
          end                        jyotai,
          iu.uketukebi               uketukebi,
          wu.web_uketuke_no          web_uketuke_no,
          iu.uketuke_no              uketuke_no,
          NVL(wu.web_uketuke_no,iu.uketuke_no)          web_no,
          ir.iraimoto_tanto          iraimoto_tanto,
          uk.kokyaku_sei             kokyaku_sei,
          uk.kokyaku_na              kokyaku_na,
          uk.kokyaku_jusyo           kokyaku_jusyo,
          uk.kokyaku_tel_haihun      kokyaku_tel,
          um.syohin_hinban           hinban,
          um.irainaiyo               irainaiyo,
          sy.ce_cd                   ce_cd,
          ce.ce_sei                  ce_sei,
          iu.uc_cd                   uc_cd,
          sy.kanryo_flg				sy_kanryo_flg,
          sy.syuri_jisekibi	        syurijisekibi,
          iu.kanryo_flg				iu_kanryo_flg,
          DECODE(SY.RENRAKUBI,NULL,NULL,TO_CHAR(SY.SYURI_YOTEIBI,'YYYY/MM/DD')) syuriyoteibi
         ,DECODE(SY.KANRYO_FLG,'0','1','1','1','2') SORT_KBN
         FROM
         (   SELECT

            distinct
            sy.uc_cd               uc_cd,
            sy.uketuke_no          uketuke_no,
            sy.uketuke_nendo       uketuke_nendo,
            max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by sy.SYURI_JISEKIBI desc,DECODE(sy.KANRYO_FLG,'8','1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

          FROM
              sagyoyotei             sy,
              uketuke_iraimoto       ui
          WHERE
             ui.uc_cd      = sy.uc_cd
          AND ui.uketuke_no = sy.uketuke_no

          AND ui.uketuke_nendo = sy.uketuke_nendo

          AND UI.IRAIMOTO_CD= :iraimoto_cd

          AND sy.sakujo_flg = '0'

         ) iis,
         iraiuketuke                    iu,
         uketuke_meisai                 um,
         sagyoyotei                     sy,
         syuritantosya                  ce,
         uketuke_iraimoto               ir,
         uketuke_kokyaku                uk,
         web_uketuke                    wu,

         d_tekiyo_cd                    dt

         WHERE
          iu.uc_cd            = um.uc_cd
         AND iu.uketuke_no      = um.uketuke_no
         AND um.uc_cd           = sy.uc_cd
         AND um.uketuke_no      = sy.uketuke_no
         AND um.syuritaisyo_seq = sy.syuritaisyo_seq
         AND iu.uc_cd           = ir.uc_cd
         AND iu.uketuke_no      = ir.uketuke_no
         AND iu.uc_cd           = uk.uc_cd
         AND iu.uketuke_no      = uk.uketuke_no
         AND sy.ce_cd           = ce.ce_cd
         AND ir.iraimoto_cd     = :iraimoto_cd

         AND iu.uketuke_no      = wu.uketuke_no(+)
         AND iis.uc_cd          = sy.uc_cd
                 AND iis.uketuke_no     = sy.uketuke_no
         AND iis.max_JISIRIREKI_SEQ  = sy.JISIRIREKI_SEQ

         AND sy.tekiyo_cd       = dt.code(+)

         AND iu.uketuke_nendo      = um.uketuke_nendo
         AND um.uketuke_nendo      = sy.uketuke_nendo
         AND iu.uketuke_nendo      = ir.uketuke_nendo
         AND iu.uketuke_nendo      = uk.uketuke_nendo
         AND iu.uketuke_nendo      = wu.uketuke_nendo(+)
         AND iis.uketuke_nendo     = sy.uketuke_nendo

         AND iu.kanryo_flg <> '8'
         AND iu.kanryo_flg <> '9'

         AND ce.sakujo_flg = '0'
         AND um.sakujo_flg = '0'
         AND ir.sakujo_flg = '0'
         AND uk.sakujo_flg = '0'
         AND (IU.C_VOICE_FLG IS NULL OR IU.C_VOICE_FLG<>'1')
         UNION ALL
         SELECT
          '01'                     seq,
           DECODE(wu.web_kanryo_flg,'0','受付中','2','確認中',NULL)   jyotai,
          wu.torokubi              uketukebi,
          wu.web_uketuke_no        web_uketuke_no,
          wu.uketuke_no            uketuke_no,
          wu.web_uketuke_no        web_no,
           wu.iraimoto_tanto        iraimoto_tanto,
           wu.kokyaku_sei           kokyaku_sei,
          wu.kokyaku_na            kokyaku_na,
          wu.kokyaku_jusyo         kokyaku_jusyo,
          wu.kokyaku_tel_haihun    kokyaku_tel,
          wu.syohin_hinban         hinban,
           wu.irainaiyo             irainaiyo,
          NULL                         ce_cd,
          NULL                         ce_sei,
           wu.uc_cd                    uc_cd,
           wu.web_kanryo_flg   		sy_kanryo_flg,
           TO_DATE(NULL,'YYYY/MM/DD')  syurijisekibi,
           NULL					    iu_kanryo_flg,
                   '' SYURIYOTEIBI
             ,'' SORT_KBN
          FROM
          web_uketuke              wu
         WHERE
          wu.web_kanryo_flg <> '9'

         AND (wu.C_VOICE_FLG IS NULL OR wu.C_VOICE_FLG<>'1')

         AND wu.sakujo_flg      = '0'
         and wu.iraimoto_cd = :iraimoto_cd

         ORDER BY

         uketukebi desc,
         uketuke_no,
         sort_kbn,
         seq
SQL;
    return $sql;
  }

  /**
   * 依頼状況一覧検索ＳＱＬ(完了部分)
   */
   private function _SQL_SELECT_KANRYO()
   {
     $sql = <<<SQL
     SELECT tbl.* FROM (
      SELECT
        rownum rownumber,
        um.SYURITAISYO_SEQ seq, 			/* 1.修理対象SEQ */
        DECODE(sy.KANRYO_FLG,'9','完了',NULL) jyotai,  /* 2.状態*/
        iu.UKETUKEBI uketukebi, 						/* 3.登録日*/
        wu.WEB_UKETUKE_NO web_uketuke_no,
        iu.UKETUKE_NO uketuke_no, 					/* 4.受付No*/
        NVL(wu.WEB_UKETUKE_NO, iu.UKETUKE_NO) web_no, 				/* 5.お申込み受付日*/
        ir.IRAIMOTO_TANTO iraimoto_tanto, 			/* 6.依頼元担当*/
        uk.KOKYAKU_SEI kokyaku_sei, 				/* 7.お客様姓*/
        uk.KOKYAKU_NA kokyaku_na, 				/* 8.お客様名*/
        uk.KOKYAKU_JUSYO kokyaku_jusyo, 				/* 9.お客様住所*/
        uk.KOKYAKU_TEL_HAIHUN kokyaku_tel, 		/* 10.お客様TEL*/
        um.SYOHIN_HINBAN hinban, 				/* 11.商品(品番)*/
        um.IRAINAIYO irainaiyo, 					/* 12.依頼内容*/
        sy.CE_CD ce_cd, 							/* 13.修理担当者 CEコード*/
        ce.CE_SEI ce_sei, 						/* 14.修理担当者 CE姓*/
        iu.UC_CD uc_cd, 							/* 15.UCコード*/
         sy.KANRYO_FLG sy_kanryo_flg, 					/* 16.作業予定完了フラグ*/
        sy.SYURI_JISEKIBI syurijisekibi, 					/* 17.修理実績日*/
         iu.KANRYO_FLG iu_kanryo_flg, 					/* 18.依頼受付完了フラグ*/
        '' syuriyoteibi    									/* 19.修理予定日*/
         ,DECODE(sy.KANRYO_FLG,'9','1','2') SORT_KBN 	/*ソート用*/
        FROM
          (SELECT
                distinct  +
          sy.uc_cd 				uc_cd,
          sy.uketuke_no			uketuke_no,
          sy.uketuke_nendo		uketuke_nendo,
          max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by nvl(sy.kanryo_flg,'0') desc,DECODE(SYURI_JISEKIBI,NULL,'1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

          FROM
            sagyoyotei			sy,
            uketuke_iraimoto	ui
          WHERE
            ui.uc_cd = sy.uc_cd
            AND ui.uketuke_no = sy.uketuke_no

            AND ui.uketuke_nendo = sy.uketuke_nendo

            AND ui.IRAIMOTO_CD = :iraimoto_cd

            AND ui.sakujo_flg = '0'
            AND sy.sakujo_flg = '0'
            AND TO_DATE(SYSDATE) - sy.syuri_jisekibi <= 9 /* Custom to apply with laravel */
            /* AND TO_DATE(SYSDATE,'RRRR/MM/DD') - sy.syuri_jisekibi <= 9 */
          ) iis,
          IRAIUKETUKE iu,
          UKETUKE_MEISAI um,
          SAGYOYOTEI sy,
          SYURITANTOSYA ce,
          UKETUKE_IRAIMOTO ir,
          UKETUKE_KOKYAKU uk,
          WEB_UKETUKE wu
         WHERE
          iu.UC_CD				= um.UC_CD AND
          iu.UKETUKE_NO			= um.UKETUKE_NO AND
          um.UC_CD			= sy.UC_CD AND
          um.UKETUKE_NO		= sy.UKETUKE_NO AND
          um.SYURITAISYO_SEQ	= sy.SYURITAISYO_SEQ AND
          iu.UC_CD				= ir.UC_CD AND
          iu.UKETUKE_NO			= ir.UKETUKE_NO AND
          iu.UC_CD				= uk.UC_CD AND
          iu.UKETUKE_NO			= uk.UKETUKE_NO AND
          sy.CE_CD				= ce.CE_CD AND
          iu.UKETUKE_NO			= wu.UKETUKE_NO(+) AND
          IIS.UC_CD						= sy.UC_CD AND
          IIS.UKETUKE_NO					= sy.UKETUKE_NO AND
          IIS.MAX_JISIRIREKI_SEQ			= sy.JISIRIREKI_SEQ AND
          iu.UKETUKE_NO			= um.UKETUKE_NO AND
          um.UKETUKE_NO		= sy.UKETUKE_NO AND
          iu.UKETUKE_NO			= ir.UKETUKE_NO AND
          iu.UKETUKE_NO			= wu.UKETUKE_NO(+) AND
          IIS.UKETUKE_NO					= sy.UKETUKE_NO AND
          ir.IRAIMOTO_CD	= :iraimoto_cd AND
          um.SAKUJO_FLG		= '0' AND
          iu.KANRYO_FLG		= '9' AND
          uk.SAKUJO_FLG		= '0' AND
          ir.SAKUJO_FLG	= '0' AND
          ce.SAKUJO_FLG		= '0'
           AND (iu.C_VOICE_FLG IS NULL OR iu.C_VOICE_FLG<>'1')
        ORDER BY
           uketukebi DESC, uketuke_no, sort_kbn, seq
) tbl
      WHERE rownumber > :startrow AND rownumber <= :endrow

SQL;
     return $sql;
   }

   /**
    * 依頼状況一覧検索ＳＱＬ(カウント完了部分)
    */
   private function _SQL_COUNT_KANRYO()
   {
     $sql = <<<SQL
     SELECT
        COUNT(*) CNT
        FROM
          (SELECT
                distinct  +
          sy.uc_cd 				uc_cd,
          sy.uketuke_no			uketuke_no,
          sy.uketuke_nendo		uketuke_nendo,
          max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by nvl(sy.kanryo_flg,'0') desc,DECODE(SYURI_JISEKIBI,NULL,'1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

          FROM
            sagyoyotei			sy,
            uketuke_iraimoto	ui
          WHERE
            ui.uc_cd = sy.uc_cd
            AND ui.uketuke_no = sy.uketuke_no

            AND ui.uketuke_nendo = sy.uketuke_nendo

            AND ui.IRAIMOTO_CD = :iraimoto_cd

            AND ui.sakujo_flg = '0'
            AND sy.sakujo_flg = '0'
            AND TO_DATE(SYSDATE) - sy.syuri_jisekibi <= 9 /* Custom to apply with laravel */
           /* AND TO_DATE(SYSDATE,'RRRR/MM/DD') - sy.syuri_jisekibi <= 9 */
          ) iis,
          IRAIUKETUKE iu,
          UKETUKE_MEISAI um,
          SAGYOYOTEI sy,
          SYURITANTOSYA ce,
          UKETUKE_IRAIMOTO ir,
          UKETUKE_KOKYAKU uk,
          WEB_UKETUKE wu
         WHERE
          iu.UC_CD				= um.UC_CD AND
          iu.UKETUKE_NO			= um.UKETUKE_NO AND
          um.UC_CD			= sy.UC_CD AND
          um.UKETUKE_NO		= sy.UKETUKE_NO AND
          um.SYURITAISYO_SEQ	= sy.SYURITAISYO_SEQ AND
          iu.UC_CD				= ir.UC_CD AND
          iu.UKETUKE_NO			= ir.UKETUKE_NO AND
          iu.UC_CD				= uk.UC_CD AND
          iu.UKETUKE_NO			= uk.UKETUKE_NO AND
          sy.CE_CD				= ce.CE_CD AND
          iu.UKETUKE_NO			= wu.UKETUKE_NO(+) AND
          IIS.UC_CD						= sy.UC_CD AND
          IIS.UKETUKE_NO					= sy.UKETUKE_NO AND
          IIS.MAX_JISIRIREKI_SEQ			= sy.JISIRIREKI_SEQ AND
          iu.UKETUKE_NO			= um.UKETUKE_NO AND
          um.UKETUKE_NO		= sy.UKETUKE_NO AND
          iu.UKETUKE_NO			= ir.UKETUKE_NO AND
          iu.UKETUKE_NO			= wu.UKETUKE_NO(+) AND
          IIS.UKETUKE_NO					= sy.UKETUKE_NO AND
          ir.IRAIMOTO_CD	= :iraimoto_cd AND
          um.SAKUJO_FLG		= '0' AND
          iu.KANRYO_FLG		= '9' AND
          uk.SAKUJO_FLG		= '0' AND
          ir.SAKUJO_FLG	= '0' AND
          ce.SAKUJO_FLG		= '0'
           AND (iu.C_VOICE_FLG IS NULL OR iu.C_VOICE_FLG<>'1')
SQL;
    return $sql;
   }

   /**
    * 依頼状況一覧検索ＳＱＬ(キャンセル部分)
    */
   private function _SQL_SELECT_CANCEL()
   {
    $sql = <<<SQL
    SELECT * FROM (
        SELECT rownum rownumber,tbl.* FROM (
          SELECT
          UM.SYURITAISYO_SEQ seq,
          DECODE(SAG.KANRYO_FLG,'8','キャンセル',NULL) jyotai,
          IRU.UKETUKEBI uketukebi,
          WU.WEB_UKETUKE_NO web_uketuke_no,
          IRU.UKETUKE_NO uketuke_no,
          NVL(WU.WEB_UKETUKE_NO, IRU.UKETUKE_NO) web_no,
          UKI.IRAIMOTO_TANTO iraimoto_tanto,
          UK.KOKYAKU_SEI kokyaku_sei,
          UK.KOKYAKU_NA kokyaku_na,
          UK.KOKYAKU_JUSYO kokyaku_jusyo,
          UK.KOKYAKU_TEL_HAIHUN kokyaku_tel,
          UM.SYOHIN_HINBAN hinban,
          UM.IRAINAIYO irainaiyo,
          SAG.CE_CD ce_cd,
          ST.CE_SEI ce_sei,
          IRU.UC_CD uc_cd,
          SAG.KANRYO_FLG sy_kanryo_flg,
          SAG.SYURI_JISEKIBI syurijisekibi,
          IRU.KANRYO_FLG iu_kanryo_flg,
          '' syuriyoteibi
           ,DECODE(SAG.KANRYO_FLG,'8','1','2') SORT_KBN
          FROM
            (SELECT
              sy.uc_cd		uc_cd,
              sy.uketuke_no		uketuke_no,
              SY.SYURITAISYO_SEQ	SYURITAISYO_SEQ,
              sy.uketuke_nendo	uketuke_nendo,
              max(JISIRIREKI_SEQ)	max_JISIRIREKI_SEQ
            FROM
              sagyoyotei		sy,
              uketuke_iraimoto	ui
            WHERE
              ui.uc_cd = sy.uc_cd
              AND ui.uketuke_no = sy.uketuke_no

              AND ui.uketuke_nendo = sy.uketuke_nendo

              AND ui.IRAIMOTO_CD = :iraimoto_cd
              AND ui.sakujo_flg = '0'
              AND sy.sakujo_flg = '0'
            GROUP BY
              sy.uc_cd,
              sy.uketuke_no,
              SY.SYURITAISYO_SEQ

              ,SY.uketuke_nendo

            ) iis,
            IRAIUKETUKE IRU,
            UKETUKE_MEISAI UM,
            SAGYOYOTEI SAG,
            SYURITANTOSYA ST,
            UKETUKE_IRAIMOTO UKI,
            UKETUKE_KOKYAKU UK,
            WEB_UKETUKE WU
          WHERE
            IRU.UC_CD		= UM.UC_CD AND
            IRU.UKETUKE_NO		= UM.UKETUKE_NO AND
            UM.UC_CD		= SAG.UC_CD AND
            UM.UKETUKE_NO	= SAG.UKETUKE_NO AND
            UM.SYURITAISYO_SEQ	= SAG.SYURITAISYO_SEQ AND
            IRU.UC_CD		= UKI.UC_CD AND
            IRU.UKETUKE_NO		= UKI.UKETUKE_NO AND
            IRU.UC_CD		= UK.UC_CD AND
            IRU.UKETUKE_NO		= UK.UKETUKE_NO AND
            SAG.CE_CD		= ST.CE_CD AND
            IRU.UKETUKE_NO		= WU.UKETUKE_NO(+) AND

            IIS.UC_CD			= SAG.UC_CD AND
            IIS.UKETUKE_NO			= SAG.UKETUKE_NO AND
            IIS.SYURITAISYO_SEQ		= SAG.SYURITAISYO_SEQ AND
            IIS.MAX_JISIRIREKI_SEQ		= SAG.JISIRIREKI_SEQ AND

            IRU.UKETUKE_NENDO		= UM.UKETUKE_NENDO AND
            UM.UKETUKE_NENDO	= SAG.UKETUKE_NENDO AND
            IRU.UKETUKE_NENDO		= UKI.UKETUKE_NENDO AND
            IRU.UKETUKE_NENDO		= UK.UKETUKE_NENDO AND
            IRU.UKETUKE_NENDO		= WU.UKETUKE_NENDO(+) AND
            IIS.UKETUKE_NENDO				= SAG.UKETUKE_NENDO AND

            UKI.IRAIMOTO_CD	= :iraimoto_cd AND
            UM.SAKUJO_FLG	= '0' AND

            IRU.KANRYO_FLG = '8' AND


            UK.SAKUJO_FLG	= '0' AND
            UKI.SAKUJO_FLG	= '0' AND
            ST.SAKUJO_FLG	= '0'
             AND (IRU.C_VOICE_FLG IS NULL OR IRU.C_VOICE_FLG<>'1')
           ORDER BY uketukebi desc,uketuke_no,SORT_KBN,seq
          ) tbl
      )
      WHERE rownumber > :startrow AND rownumber <= :endrow
SQL;
    return $sql;
   }
   /**
    * 依頼状況一覧検索ＳＱＬ(カウントキャンセル部分)
    */
   private function _SQL_COUNT_CANCEL()
   {
     $sql = <<<SQL
      SELECT
       COUNT(*) CNT
      FROM
        (SELECT
          sy.uc_cd		uc_cd,
          sy.uketuke_no		uketuke_no,
          SY.SYURITAISYO_SEQ	SYURITAISYO_SEQ,
          sy.uketuke_nendo	uketuke_nendo,
          max(JISIRIREKI_SEQ)	max_JISIRIREKI_SEQ
        FROM
          sagyoyotei		sy,
          uketuke_iraimoto	ui
        WHERE
          ui.uc_cd = sy.uc_cd
          AND ui.uketuke_no = sy.uketuke_no

          AND ui.uketuke_nendo = sy.uketuke_nendo

          AND ui.IRAIMOTO_CD = :iraimoto_cd
          AND ui.sakujo_flg = '0'
          AND sy.sakujo_flg = '0'
        GROUP BY
          sy.uc_cd,
          sy.uketuke_no,
          SY.SYURITAISYO_SEQ

          ,SY.uketuke_nendo

        ) iis,
        IRAIUKETUKE IRU,
        UKETUKE_MEISAI UM,
        SAGYOYOTEI SAG,
        SYURITANTOSYA ST,
        UKETUKE_IRAIMOTO UKI,
        UKETUKE_KOKYAKU UK,
        WEB_UKETUKE WU
      WHERE
        IRU.UC_CD		= UM.UC_CD AND
        IRU.UKETUKE_NO		= UM.UKETUKE_NO AND
        UM.UC_CD		= SAG.UC_CD AND
        UM.UKETUKE_NO	= SAG.UKETUKE_NO AND
        UM.SYURITAISYO_SEQ	= SAG.SYURITAISYO_SEQ AND
        IRU.UC_CD		= UKI.UC_CD AND
        IRU.UKETUKE_NO		= UKI.UKETUKE_NO AND
        IRU.UC_CD		= UK.UC_CD AND
        IRU.UKETUKE_NO		= UK.UKETUKE_NO AND
        SAG.CE_CD		= ST.CE_CD AND
        IRU.UKETUKE_NO		= WU.UKETUKE_NO(+) AND

        IIS.UC_CD			= SAG.UC_CD AND
        IIS.UKETUKE_NO			= SAG.UKETUKE_NO AND
        IIS.SYURITAISYO_SEQ		= SAG.SYURITAISYO_SEQ AND
        IIS.MAX_JISIRIREKI_SEQ		= SAG.JISIRIREKI_SEQ AND

        IRU.UKETUKE_NENDO		= UM.UKETUKE_NENDO AND
        UM.UKETUKE_NENDO	= SAG.UKETUKE_NENDO AND
        IRU.UKETUKE_NENDO		= UKI.UKETUKE_NENDO AND
        IRU.UKETUKE_NENDO		= UK.UKETUKE_NENDO AND
        IRU.UKETUKE_NENDO		= WU.UKETUKE_NENDO(+) AND
        IIS.UKETUKE_NENDO				= SAG.UKETUKE_NENDO AND

        UKI.IRAIMOTO_CD	= :iraimoto_cd AND
        UM.SAKUJO_FLG	= '0' AND

        IRU.KANRYO_FLG = '8' AND


        UK.SAKUJO_FLG	= '0' AND
        UKI.SAKUJO_FLG	= '0' AND
        ST.SAKUJO_FLG	= '0'
         AND (IRU.C_VOICE_FLG IS NULL OR IRU.C_VOICE_FLG<>'1')

SQL;
    return $sql;
   }

   /**
 	 * 受付日：年リストボックスの値およびラベル配列設定
 	 */
   public function getUketukebiYYYY()
   {
    $nensuu = 31;
 		$year = date("Y");
 		$toritsukeYYYY = array();
    $toritsukeYYYY[0] = array('value'=>'@@@@','text'=>'');
 		for ($i=1,$j=0; $i <= $nensuu; $i++,$j++) {
 			$insYear = $year -$j;
      if($year - 2 >= $insYear){break;}
 			$toritsukeYYYY[$i]['value'] = $insYear;
 			$heiseiNen = $insYear - 1988;
 			if($heiseiNen > 0){
 				$heiseiNen = sprintf("%02d", $heiseiNen);
 				$toritsukeYYYY[$i]['text'] = "$insYear(H$heiseiNen)";
 			} else {
 				$showaNen = sprintf("%02d", $insYear - 1925);
 				$toritsukeYYYY[$i]['text'] = "$insYear(S$showaNen)";
 			}
 		}
 		return $toritsukeYYYY;
   }

   public function getUketukebiMM()
   {
     $monthList = array();
     $monthList[0] = array('value'=>'@@','text'=>'');
     for ($i=1; $i <= 12; $i++) {
       $monthList[$i] = array('value'=>sprintf("%02d", $i),'text'=>sprintf("%02d", $i));
     }
     return $monthList;
   }

   public function getUketukebiDD()
   {
     $dateList = array();
     $dateList[0] = array('value'=>'@@','text'=>'');
     for ($i=1; $i <= 31; $i++) {
       $dateList[$i] = array('value'=>sprintf("%02d", $i),'text'=>sprintf("%02d", $i));
     }
     return $dateList;
   }

   public function getSearchAll($search, $request)
   {
     $sql = <<<SQL
      SELECT * FROM (
        SELECT tbl.*,rownum rownumber FROM (
          {$this->_SQL_ALL()}
        ) tbl
        {$this->_SQL_WHERE($request)}
      )
      WHERE rownumber > :startrow AND rownumber <= :endrow
SQL;
     return DB::select(DB::raw($sql),$search);
   }

   public function getCountSearchAll($search, $request)
   {
     $sql = <<<SQL
        SELECT COUNT(*) cnt FROM (
          {$this->_SQL_ALL()}
        ) tbl
        {$this->_SQL_WHERE($request)}
SQL;
     return DB::select(DB::raw($sql),$search);
   }

   public function getSearchMikanryo($search, $request)
   {
     $sql = <<<SQL
      SELECT * FROM (
        SELECT tbl.*,rownum rownumber FROM (
          {$this->_SQL_SELECT_COMMAND_MIKANRYO()}
        ) tbl
        {$this->_SQL_WHERE($request)}
      )
      WHERE rownumber > :startrow AND rownumber <= :endrow
SQL;
     return DB::select(DB::raw($sql),$search);
   }

   public function getCountSearchMikanryo($search, $request)
   {
     $sql = <<<SQL
        SELECT COUNT(*) cnt FROM (
          {$this->_SQL_SELECT_COMMAND_MIKANRYO()}
        ) tbl
        {$this->_SQL_WHERE($request)}
SQL;
     return DB::select(DB::raw($sql),$search);
   }

   /**
    *受付検索結果一覧取得用SQL(未完了)
    */
    private function _SQL_SELECT_COMMAND_MIKANRYO()
    {
      $sql = <<<SQL
      SELECT * FROM (
           SELECT

             case
               when sy.tekiyo_cd IN (dt.code) then dt.setumei
               else
                 case
                   when sy.kanryo_flg = '0' then '未着手'
                   else NULL
                 end
             end                       jyotai,              /* 1.状態 */
             NVL(wu.web_uketuke_no,iu.uketuke_no)         web_no,              /*  2.Web受付No*/
             TO_CHAR(iu.uketukebi, 'YYYY/MM/DD') uketukebi,  /*  3.登録日*/
             iu.uketuke_no             uketuke_no,          /*  4.お申込み受付No*/
             ir.iraimoto_tanto         iraimoto_tanto,      /*  5.依頼元担当*/
             uk.kokyaku_sei            kokyaku_sei,         /*  6.お客様姓*/
             uk.kokyaku_na             kokyaku_na,          /*  7.お客様名*/
             uk.kokyaku_jusyo          kokyaku_jusyo,       /*  8.お客様住所*/
             uk.kokyaku_tel_haihun     kokyaku_tel,         /*  9.お客様TEL*/
             um.syohin_hinban          hinban,              /*  10.商品(品番)*/
             um.irainaiyo              irainaiyo,           /*  11.依頼内容*/
             sy.ce_cd                  ce_cd,               /*  12.修理担当者 CEコード*/
             ce.ce_sei                 ce_sei,              /*  13.修理担当者 CE姓*/
             iu.uc_cd                  uc_cd,               /*  14.UCコード*/
             um.syuritaisyo_seq        seq,                 /*  15.修理対象SEQ*/
             sy.kanryo_flg				sy_kanryo_flg,   	/*  16.作業予定完了フラグ*/
             iu.kanryo_flg				iu_kanryo_flg, 	    /*  17.依頼受付完了フラグ*/
             sy.syuri_jisekibi	        syurijisekibi,       /*  18.修理実績日*/
             DECODE(SY.RENRAKUBI,NULL,NULL,TO_CHAR(SY.SYURI_YOTEIBI,'YYYY/MM/DD')) syuriyoteibi,  /*  19.修理予定日*/
             uk.kokyaku_sei_kana       kokyaku_sei_kana,    /*  検索用.お客様姓（ふりがな）*/
             ir.tyumon_bango           tyumon_bango,        /*  検索用.依頼元注番*/
             uk.kokyaku_tel            tel          	    /*  検索用顧客TEL(ハイフンなし)*/
            , um.BRAND_CD          BRAND_CD  		/*  ブランド選択対応*/
           FROM
             ( SELECT
        /* 2017/03/01 MOD START	*/
             distinct
             sy.uc_cd               uc_cd,
             sy.uketuke_no          uketuke_no,
             sy.uketuke_nendo          uketuke_nendo,
             max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by sy.SYURI_JISEKIBI desc,DECODE(sy.KANRYO_FLG,'8','1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

             FROM
               sagyoyotei             sy,
               uketuke_iraimoto       ui
             WHERE
                 ui.uc_cd      = sy.uc_cd
               AND ui.uketuke_no = sy.uketuke_no
        /*  2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応 */
               AND ui.uketuke_nendo = sy.uketuke_nendo
        /*  2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応 */
               AND UI.IRAIMOTO_CD= :iraimoto_cd
        /* 2017/03/01 MOD START */
               AND sy.sakujo_flg = '0'

             ) iis,
             iraiuketuke                    iu,
             uketuke_meisai                 um,
             sagyoyotei                     sy,
             syuritantosya                  ce,
             uketuke_iraimoto               ir,
             uketuke_kokyaku                uk,
             web_uketuke                    wu,
            /* 2009.03.02 Hitachi 摘要コード追加 ADD START */
             d_tekiyo_cd                    dt
            /* 2009.03.02 Hitachi 摘要コード追加 ADD END */
           WHERE
               iu.uc_cd            = um.uc_cd
             AND iu.uketuke_no      = um.uketuke_no
             AND um.uc_cd           = sy.uc_cd
             AND um.uketuke_no      = sy.uketuke_no
             AND um.syuritaisyo_seq = sy.syuritaisyo_seq
             AND iu.uc_cd           = ir.uc_cd
             AND iu.uketuke_no      = ir.uketuke_no
             AND iu.uc_cd           = uk.uc_cd
             AND iu.uketuke_no      = uk.uketuke_no
             AND sy.ce_cd           = ce.ce_cd
             AND ir.iraimoto_cd     = :iraimoto_cd

             AND iu.uketuke_no      = wu.uketuke_no(+)
             AND iis.uc_cd           = sy.uc_cd
             AND iis.uketuke_no      = sy.uketuke_no
             AND iis.max_JISIRIREKI_SEQ  = sy.JISIRIREKI_SEQ
            /* 2009.03.02 Hitachi 摘要コード追加 ADD START */
             AND sy.tekiyo_cd       = dt.code(+)
            /* 2009.03.02 Hitachi 摘要コード追加 ADD END */
        /*  2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応 */
             AND iu.uketuke_nendo   = um.uketuke_nendo
             AND um.uketuke_nendo   = sy.uketuke_nendo
             AND iu.uketuke_nendo   = ir.uketuke_nendo
             AND iu.uketuke_nendo   = uk.uketuke_nendo
             AND iu.uketuke_nendo   = wu.uketuke_nendo(+)
             AND iis.uketuke_nendo  = sy.uketuke_nendo

             AND iu.kanryo_flg <> '8'
             AND iu.kanryo_flg <> '9'
        /* 2017/03/01 MOD END	*/
             AND ce.sakujo_flg = '0'
             AND um.sakujo_flg = '0'
             AND ir.sakujo_flg = '0'
             AND uk.sakujo_flg = '0'
             AND (IU.C_VOICE_FLG IS NULL OR IU.C_VOICE_FLG<>'1')
           UNION ALL
           SELECT
             DECODE(wu.web_kanryo_flg,'0','受付中','2','確認中',NULL)   jyotai,
             wu.web_uketuke_no        web_no,
             TO_CHAR(wu.torokubi, 'YYYY/MM/DD')              uketukebi,
             wu.uketuke_no            uketuke_no,
             wu.iraimoto_tanto        iraimoto_tanto,
             wu.kokyaku_sei           kokyaku_sei,
             wu.kokyaku_na            kokyaku_na,
             wu.kokyaku_jusyo         kokyaku_jusyo,
             wu.kokyaku_tel_haihun    kokyaku_tel,
             wu.syohin_hinban         hinban,
             wu.irainaiyo             irainaiyo,
             NULL                     ce_cd,
             NULL                     ce_sei,
             wu.uc_cd                 uc_cd,
             '01'                     seq,
             wu.web_kanryo_flg		  sy_kanryo_flg,
             NULL					  iu_kanryo_flg,
             TO_DATE(NULL,'YYYY/MM/DD') syurijisekibi,  /*  18.修理実績日 */
             ''                        syuriyoteibi,    /*  19.修理予定日 */
             wu.kokyaku_sei_kana       kokyaku_sei_kana,
             wu.tyumon_bango           tyumon_bango,
             replace( wu.kokyaku_tel_haihun, '-', '' )           tel
            , wu.BRAND_CD          BRAND_CD  	/*  ブランド選択対応 */
           FROM
             web_uketuke              wu
           WHERE
               wu.web_kanryo_flg <> '9'
            /*  2012.8.24 Hitachi ADD START */
             AND (wu.C_VOICE_FLG IS NULL OR wu.C_VOICE_FLG<>'1')
            /*  2012.8.24 Hitachi ADD END */
             AND wu.sakujo_flg      = '0'
             AND WU.UKETUKE_NO    IS NULL
             and wu.iraimoto_cd = :iraimoto_cd
           ORDER BY uketukebi desc
           )
SQL;
      return $sql;
    }

    private function _SQL_WHERE($request)
    {
      $sqlWhereSB = "";
      $flg = false;
      // 検索条件に担当者が入力されている場合
      if(isset($request->tanto) && $request->tanto != null && $request->tanto != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " IRAIMOTO_TANTO LIKE '{$request->tanto}%' ";
        $flg = true;
      }
      // 検索条件にお客様名(漢字)が入力されている場合
      if(isset($request->kokyaku_sei_kanji) && $request->kokyaku_sei_kanji != null && $request->kokyaku_sei_kanji != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " KOKYAKU_SEI LIKE '{$request->kokyaku_sei_kanji}%' ";
        $flg = true;
      }

      // 検索条件にお客様名（かな）が入力されている場合
      if(isset($request->kokyaku_sei_kana) && $request->kokyaku_sei_kana != null && $request->kokyaku_sei_kana != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " KOKYAKU_SEI_KANA LIKE '{$request->kokyaku_sei_kana}%' ";
        $flg = true;
      }
      // 検索条件に受付Noが入力されている場合
      if(isset($request->uketuke_no) && $request->uketuke_no != null && $request->uketuke_no != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " UKETUKE_NO LIKE '{$request->uketuke_no}' ";
        $flg = true;
      }
      // 検索条件に住所(都道府県、お客様住所)が入力されている場合
      if(isset($request->pref) && $request->pref != null && $request->pref != '' && $request->pref != '0'
        && isset($request->kokyaku_jusyo) && $request->kokyaku_jusyo != null && $request->kokyaku_jusyo != ''
      ){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " KOKYAKU_JUSYO LIKE '{$request->pref}{$request->kokyaku_jusyo}%' ";
        $flg = true;
      } else if(isset($request->pref) && ($request->pref == null || $request->pref == '0') &&
        isset($request->kokyaku_jusyo) && $request->kokyaku_jusyo != null && $request->kokyaku_jusyo != ''
      ){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " KOKYAKU_JUSYO LIKE '{$request->kokyaku_jusyo}%' ";
        $flg = true;
      }  else if(isset($request->pref) && $request->pref != null && $request->pref != '0'){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " KOKYAKU_JUSYO LIKE '{$request->pref}%' ";
        $flg = true;
      }
      // 検索条件にお客様TELが入力されている場合
      if(isset($request->tel) && $request->tel != null && $request->tel != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " TEL LIKE '{$request->tel}%' ";
        $flg = true;
      }
      // 検索条件に依頼元注番が入力されている場合
      if(isset($request->tyumon_bango) && $request->tyumon_bango != null && $request->tyumon_bango != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " TYUMON_BANGO LIKE '{$request->tyumon_bango}' ";
        $flg = true;
      }
      // 検索条件にお申し込みNoが入力されている場合
      if(isset($request->web_uketuke_no) && $request->web_uketuke_no != null && $request->web_uketuke_no != ''){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " WEB_NO LIKE '{$request->web_uketuke_no}' ";
        $flg = true;
      }

      // 検索条件に受付日 FROM
      $uketukeFrom = $request->uketukebiFromYYYY;
      $uketukeFrom .= $uketukeFrom != '@@@@' ? ($request->uketukebiFromMM == '@@' ? '01' : $request->uketukebiFromMM) : '@@';
      $uketukeFrom .= $uketukeFrom != '@@@@@@' ? ($request->uketukebiFromDD == '@@' ? '01' : $request->uketukebiFromDD) : '@@';
      // 検索条件に受付日 TO
      $uketukeTo = $request->uketukebiToYYYY;
      $uketukeTo .= $uketukeTo != '@@@@' ? ($request->uketukebiToMM == '@@' ? '12' : $request->uketukebiToMM) : '@@';
      $uketukeTo .= $uketukeTo != '@@@@@@' ? ($request->uketukebiToDD == '@@' ? '31' : $request->uketukebiToDD) : '@@';

      // 検索条件に受付日が入力されている場合
      if($uketukeFrom != '@@@@@@@@' && $uketukeTo == '@@@@@@@@'){
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " UKETUKEBI >= TO_DATE('$uketukeFrom') ";
        $flg = true;
      } elseif ($uketukeFrom == '@@@@@@@@' && $uketukeTo != '@@@@@@@@') {
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " UKETUKEBI <= TO_DATE('$uketukeTo') ";
        $flg = true;
      } elseif ($uketukeFrom != '@@@@@@@@' && $uketukeTo != '@@@@@@@@') {
        $sqlWhereSB .= $flg ? " AND " : "";
        $sqlWhereSB .= " UKETUKEBI >= TO_DATE('$uketukeFrom') ";
        $sqlWhereSB .= " AND UKETUKEBI <= TO_DATE('$uketukeTo') ";
        $flg = true;
      }

      // 検索条件にお申し込みNoが入力されている場合
      // if(isset($request->brand) && $request->brand != null && $request->brand != ''){
      //   $sqlWhereSB .= $flg ? " AND " : "";
      //   if($request->brand == '3'){
      //     $sqlWhereSB .= " BRAND_CD = '0301' ";
      //   } elseif ($request->brand == '0') {
      //     $sqlWhereSB .= " (BRAND_CD = '0101' OR BRAND_CD IS NULL) ";
      //   } else {
      //     $sqlWhereSB .= " BRAND_CD = '0101' ";
      //   }
      //   $flg = true;
      // }

      $sqlWhereSB = $sqlWhereSB == '' ? $sqlWhereSB : ' WHERE ' . $sqlWhereSB;
      return $sqlWhereSB;
    }

    /**
     *受付検索結果一覧取得用SQL(完了取得)
     */
    public function getSearchKanryo($search, $request)
    {
      $sql = <<<SQL
       SELECT * FROM (
         SELECT tbl.*,rownum rownumber FROM (
           {$this->_SQL_SELECT_COMMAND_KANRYO()}
         ) tbl
         {$this->_SQL_WHERE($request)}
       )
       WHERE rownumber > :startrow AND rownumber <= :endrow
SQL;
      return DB::select(DB::raw($sql),$search);
    }

    /**
     *受付検索結果一覧取得用SQL(完了カウント取得)
     */
    public function getCountSearchKanryo($search, $request)
    {
      $sql = <<<SQL
         SELECT COUNT(*) cnt FROM (
           {$this->_SQL_SELECT_COMMAND_KANRYO()}
         ) tbl
         {$this->_SQL_WHERE($request)}
SQL;
      return DB::select(DB::raw($sql),$search);
    }

    /**
      *受付検索結果一覧取得用SQL(完了)
      */
    private function _SQL_SELECT_COMMAND_KANRYO()
    {
      $sql = <<<SQL

      SELECT
        DECODE(SAGYOYOTEI.KANRYO_FLG,							/* 1.状態 */
              '9','完了',NULL)                        jyotai,
        NVL(WEB_UKETUKE.WEB_UKETUKE_NO, IRAIUKETUKE.UKETUKE_NO)                       web_no, 							/* 2.お申込み受付日*/
        TO_CHAR(IRAIUKETUKE.UKETUKEBI, 'YYYY/MM/DD') AS uketukebi, 	/* 3.登録日*/
        WEB_UKETUKE.WEB_UKETUKE_NO                      web_uketuke_no,
        IRAIUKETUKE.UKETUKE_NO                          uketuke_no, 								/* 4.受付No*/
        UKETUKE_IRAIMOTO.IRAIMOTO_TANTO                 iraimoto_tanto,/* 5.依頼元担当*/
        UKETUKE_KOKYAKU.KOKYAKU_SEI                     kokyaku_sei,/* 6.お客様姓*/
        UKETUKE_KOKYAKU.KOKYAKU_NA                      kokyaku_na,/* 7.お客様名*/
        UKETUKE_KOKYAKU.KOKYAKU_JUSYO                   kokyaku_jusyo,/* 8.お客様住所*/
        UKETUKE_KOKYAKU.KOKYAKU_TEL_HAIHUN              kokyaku_tel,/* 9.お客様TEL*/
        UKETUKE_MEISAI.SYOHIN_HINBAN                    hinban,/* 10.商品(品番)*/
        UKETUKE_MEISAI.IRAINAIYO                        irainaiyo,/* 11.依頼内容*/
        SAGYOYOTEI.CE_CD                                ce_cd,/* 12.修理担当者 CEコード*/
        SYURITANTOSYA.CE_SEI                            ce_sei,/* 13.修理担当者 CE姓*/
        IRAIUKETUKE.UC_CD                               uc_cd,/* 14.UCコード*/
        UKETUKE_MEISAI.SYURITAISYO_SEQ                  seq,/* 15.修理対象SEQ*/
        SAGYOYOTEI.KANRYO_FLG                           sy_kanryo_flg,/* 16.作業予定完了フラグ*/
        IRAIUKETUKE.KANRYO_FLG                          iu_kanryo_flg,/* 17.依頼受付完了フラグ*/
        SAGYOYOTEI.SYURI_JISEKIBI                       syurijisekibi,/* 18.修理実績日*/
        '' syuriyoteibi,                                /* 19.修理予定日*/
        UKETUKE_KOKYAKU.kokyaku_sei_kana       kokyaku_sei_kana,    /*  検索用.お客様姓（ふりがな）*/
        UKETUKE_IRAIMOTO.tyumon_bango           tyumon_bango,        /*  検索用.依頼元注番*/
        UKETUKE_KOKYAKU.kokyaku_tel            tel,          	    /*  検索用顧客TEL(ハイフンなし)*/
        UKETUKE_MEISAI.BRAND_CD                       BRAND_CD/* ブランド選択対応*/
        FROM
          (SELECT
      /*2017/03/01 MOD START*/
          distinct
          sy.uc_cd 				uc_cd,
          sy.uketuke_no			uketuke_no,
          sy.uketuke_nendo		uketuke_nendo,
          max(JISIRIREKI_SEQ ) keep( DENSE_RANK FIRST order by nvl(sy.kanryo_flg,'0') desc,DECODE(SYURI_JISEKIBI,NULL,'1','0')) over(partition by sy.uc_cd,sy.uketuke_no,sy.uketuke_nendo)  as max_JISIRIREKI_SEQ

          FROM
            sagyoyotei			sy,
            uketuke_iraimoto	ui
          WHERE
            ui.uc_cd = sy.uc_cd
            AND ui.uketuke_no = sy.uketuke_no
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
            AND ui.uketuke_nendo = sy.uketuke_nendo
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
            AND ui.IRAIMOTO_CD = :iraimoto_cd
      /*2017/03/01 MOD START			*/
            AND ui.sakujo_flg = '0'
            AND sy.sakujo_flg = '0'

          ) iis,
          IRAIUKETUKE,
          UKETUKE_MEISAI,
          SAGYOYOTEI,
          SYURITANTOSYA,
          UKETUKE_IRAIMOTO,
          UKETUKE_KOKYAKU,
          WEB_UKETUKE
      WHERE
              IRAIUKETUKE.UC_CD				= UKETUKE_MEISAI.UC_CD AND
              IRAIUKETUKE.UKETUKE_NO			= UKETUKE_MEISAI.UKETUKE_NO AND
              UKETUKE_MEISAI.UC_CD			= SAGYOYOTEI.UC_CD AND
              UKETUKE_MEISAI.UKETUKE_NO		= SAGYOYOTEI.UKETUKE_NO AND
              UKETUKE_MEISAI.SYURITAISYO_SEQ	= SAGYOYOTEI.SYURITAISYO_SEQ AND
              IRAIUKETUKE.UC_CD				= UKETUKE_IRAIMOTO.UC_CD AND
              IRAIUKETUKE.UKETUKE_NO			= UKETUKE_IRAIMOTO.UKETUKE_NO AND
              IRAIUKETUKE.UC_CD				= UKETUKE_KOKYAKU.UC_CD AND
              IRAIUKETUKE.UKETUKE_NO			= UKETUKE_KOKYAKU.UKETUKE_NO AND
              SAGYOYOTEI.CE_CD				= SYURITANTOSYA.CE_CD AND
              IRAIUKETUKE.UKETUKE_NO			= WEB_UKETUKE.UKETUKE_NO(+) AND

              IIS.UC_CD						= SAGYOYOTEI.UC_CD AND
              IIS.UKETUKE_NO					= SAGYOYOTEI.UKETUKE_NO AND
              IIS.MAX_JISIRIREKI_SEQ			= SAGYOYOTEI.JISIRIREKI_SEQ AND
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
              IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_MEISAI.UKETUKE_NENDO AND
              UKETUKE_MEISAI.UKETUKE_NENDO	= SAGYOYOTEI.UKETUKE_NENDO AND
              IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_IRAIMOTO.UKETUKE_NENDO AND
              IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_KOKYAKU.UKETUKE_NENDO AND
              IRAIUKETUKE.UKETUKE_NENDO		= WEB_UKETUKE.UKETUKE_NENDO(+) AND
              IIS.UKETUKE_NENDO				= SAGYOYOTEI.UKETUKE_NENDO AND
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
              UKETUKE_IRAIMOTO.IRAIMOTO_CD	= :iraimoto_cd AND
              UKETUKE_MEISAI.SAKUJO_FLG		= '0' AND

              IRAIUKETUKE.KANRYO_FLG		= '9' AND
      /* 2017/03/01 MOD END			*/
              UKETUKE_KOKYAKU.SAKUJO_FLG		= '0' AND
              UKETUKE_IRAIMOTO.SAKUJO_FLG	= '0' AND
              SYURITANTOSYA.SAKUJO_FLG		= '0'
               AND (IRAIUKETUKE.C_VOICE_FLG IS NULL OR IRAIUKETUKE.C_VOICE_FLG<>'1')
      ORDER BY uketukebi desc
SQL;
      return $sql;
    }

    /**
     * 受付検索結果一覧取得用SQL(キャンセル取得)
     */
    public function getSearchCancel($search, $request)
    {
      $sql = <<<SQL
       SELECT * FROM (
         SELECT tbl.*,rownum rownumber FROM (
           {$this->_SQL_SELECT_COMMAND_CANCEL()}
         ) tbl
         {$this->_SQL_WHERE($request)}
       )
       WHERE rownumber > :startrow AND rownumber <= :endrow
SQL;
      return DB::select(DB::raw($sql),$search);
    }

    /**
     * 受付検索結果一覧取得用SQL(キャンセルカウント取得)
     */
    public function getCountSearchCancel($search, $request)
    {
      $sql = <<<SQL
         SELECT COUNT(*) cnt FROM (
           {$this->_SQL_SELECT_COMMAND_CANCEL()}
         ) tbl
         {$this->_SQL_WHERE($request)}
SQL;
      return DB::select(DB::raw($sql),$search);
    }

    /**
     * 受付検索結果一覧取得用SQL(キャンセル部分)
     */
    private function _SQL_SELECT_COMMAND_CANCEL()
    {
      $sql = <<<SQL
        SELECT
            DECODE(SAGYOYOTEI.KANRYO_FLG,'8','キャンセル',NULL) jyotai,	/* 1.状態*/
            NVL(WEB_UKETUKE.WEB_UKETUKE_NO,IRAIUKETUKE.UKETUKE_NO)                        web_no,/* 2.お申込み受付no*/
            TO_CHAR(IRAIUKETUKE.UKETUKEBI, 'YYYY/MM/DD') AS   uketukebi,	/* 3.登録日*/
            IRAIUKETUKE.UKETUKE_NO                            uketuke_no,/* 4.受付No*/
            UKETUKE_IRAIMOTO.IRAIMOTO_TANTO                   iraimoto_tanto,/* 5.依頼元担当*/
            UKETUKE_KOKYAKU.KOKYAKU_SEI                       kokyaku_sei,/* 6.お客様姓*/
            UKETUKE_KOKYAKU.KOKYAKU_NA                        kokyaku_na,/* 7.お客様名*/
            UKETUKE_KOKYAKU.KOKYAKU_JUSYO                     kokyaku_jusyo,/* 8.お客様住所*/
            UKETUKE_KOKYAKU.KOKYAKU_TEL_HAIHUN                kokyaku_tel,/* 9.お客様TEL*/
            UKETUKE_MEISAI.SYOHIN_HINBAN                      hinban,/* 10.商品(品番)*/
            UKETUKE_MEISAI.IRAINAIYO                          irainaiyo,/* 11.依頼内容*/
            SAGYOYOTEI.CE_CD                                  ce_cd,/* 12.修理担当者 CEコード*/
            SYURITANTOSYA.CE_SEI                              ce_sei,/* 13.修理担当者 CE姓*/
            IRAIUKETUKE.UC_CD                                 uc_cd,/* 14.UCコード*/
            UKETUKE_MEISAI.SYURITAISYO_SEQ                    seq,/* 15.修理対象SEQ*/
            SAGYOYOTEI.KANRYO_FLG                             sy_kanryo_flg,/* 16.作業予定完了フラグ*/
            IRAIUKETUKE.KANRYO_FLG                            iu_kanryo_flg,/* 17.依頼受付完了フラグ*/
            SAGYOYOTEI.SYURI_JISEKIBI                         syurijisekibi,/* 18.修理実績日*/
            '' syuriyoteibi                                   /* 19.修理予定日*/
            , UKETUKE_MEISAI.BRAND_CD                         BRAND_CD/* ブランド選択対応*/
            ,UKETUKE_KOKYAKU.KOKYAKU_SEI_KANA                 KOKYAKU_SEI_KANA    /*21.検索用.お客様姓（ふりがな）*/
            ,UKETUKE_IRAIMOTO.TYUMON_BANGO                    TYUMON_BANGO        /*22.検索用.依頼元注番*/
            ,UKETUKE_KOKYAKU.KOKYAKU_TEL                      TEL                 /*23.検索用.お客さま電話番号*/
            FROM
              (SELECT
                sy.uc_cd		uc_cd,
                sy.uketuke_no		uketuke_no,
                SY.SYURITAISYO_SEQ	SYURITAISYO_SEQ,
            /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                sy.uketuke_nendo	uketuke_nendo,
            /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
                max(JISIRIREKI_SEQ)	max_JISIRIREKI_SEQ
              FROM
                sagyoyotei		sy,
                uketuke_iraimoto	ui
              WHERE
                ui.uc_cd = sy.uc_cd
                AND ui.uketuke_no = sy.uketuke_no
            /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                AND ui.uketuke_nendo = sy.uketuke_nendo
            /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
                AND ui.IRAIMOTO_CD = :iraimoto_cd
                AND ui.sakujo_flg = '0'
                AND sy.sakujo_flg = '0'
              GROUP BY
                sy.uc_cd,
                sy.uketuke_no,
                SY.SYURITAISYO_SEQ
            /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                ,sy.uketuke_nendo
            /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
              ) iis,
              IRAIUKETUKE,
              UKETUKE_MEISAI,
              SAGYOYOTEI,
              SYURITANTOSYA,
              UKETUKE_IRAIMOTO,
              UKETUKE_KOKYAKU,
              WEB_UKETUKE
            WHERE
              IRAIUKETUKE.UC_CD		= UKETUKE_MEISAI.UC_CD AND
              IRAIUKETUKE.UKETUKE_NO		= UKETUKE_MEISAI.UKETUKE_NO AND
              UKETUKE_MEISAI.UC_CD		= SAGYOYOTEI.UC_CD AND
              UKETUKE_MEISAI.UKETUKE_NO	= SAGYOYOTEI.UKETUKE_NO AND
              UKETUKE_MEISAI.SYURITAISYO_SEQ	= SAGYOYOTEI.SYURITAISYO_SEQ AND
              IRAIUKETUKE.UC_CD		= UKETUKE_IRAIMOTO.UC_CD AND
              IRAIUKETUKE.UKETUKE_NO		= UKETUKE_IRAIMOTO.UKETUKE_NO AND
              IRAIUKETUKE.UC_CD		= UKETUKE_KOKYAKU.UC_CD AND
              IRAIUKETUKE.UKETUKE_NO		= UKETUKE_KOKYAKU.UKETUKE_NO AND
              SAGYOYOTEI.CE_CD		= SYURITANTOSYA.CE_CD AND
              IRAIUKETUKE.UKETUKE_NO		= WEB_UKETUKE.UKETUKE_NO(+) AND
            /* 2011.5.06 Hitachi MOD START (WEB_UKETUKEのリンク項目よりUC_CDを削除）
            /*				IRAIUKETUKE.UC_CD		= WEB_UKETUKE.UC_CD(+) AND
            /*2011.5.06 Hitachi MOD END */
              IIS.UC_CD			= SAGYOYOTEI.UC_CD AND
              IIS.UKETUKE_NO			= SAGYOYOTEI.UKETUKE_NO AND
              IIS.SYURITAISYO_SEQ		= SAGYOYOTEI.SYURITAISYO_SEQ AND
              IIS.MAX_JISIRIREKI_SEQ		= SAGYOYOTEI.JISIRIREKI_SEQ AND
            /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
              IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_MEISAI.UKETUKE_NENDO AND
              UKETUKE_MEISAI.UKETUKE_NENDO	= SAGYOYOTEI.UKETUKE_NENDO AND
              IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_IRAIMOTO.UKETUKE_NENDO AND
              IRAIUKETUKE.UKETUKE_NENDO		= UKETUKE_KOKYAKU.UKETUKE_NENDO AND
              IRAIUKETUKE.UKETUKE_NENDO		= WEB_UKETUKE.UKETUKE_NENDO(+) AND
              IIS.UKETUKE_NENDO				= SAGYOYOTEI.UKETUKE_NENDO AND
            /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
              UKETUKE_IRAIMOTO.IRAIMOTO_CD	= :iraimoto_cd AND
              UKETUKE_MEISAI.SAKUJO_FLG	= '0' AND
            /* 2017/03/01 MOD START	*/
              IRAIUKETUKE.KANRYO_FLG		= '8' AND
            /* 2017/03/01 MOD END */
              UKETUKE_KOKYAKU.SAKUJO_FLG	= '0' AND
              UKETUKE_IRAIMOTO.SAKUJO_FLG	= '0' AND
              SYURITANTOSYA.SAKUJO_FLG	= '0'
               AND (IRAIUKETUKE.C_VOICE_FLG IS NULL OR IRAIUKETUKE.C_VOICE_FLG<>'1')
            ORDER BY uketukebi desc
SQL;
      return $sql;
    }

  /**
   * 経過・結果報告(速報)検索ＳＱＬ
   */
  private function _SQL_SELECT_SOKUHOU()
  {
    $sql = <<<SQL
    SELECT
         TO_CHAR(iu.UKETUKEBI, 'YY/MM/DD') AS UKETUKEBI,                                    /*1.登録日*/
         iu.uketuke_no uketuke_no,                                  /*2.受付Ｎｏ*/
         sy.ce_cd,                                                  /*3.担当者(コード)*/
         st.CE_SEI,                                                 /*4.担当者(姓)*/
         sy.mikanriyu_cd,                                           /*5.結果報告*/
         TO_CHAR(sy.SYURI_JISEKIBI, 'YYYY/MM/DD') AS SYURI_JISEKIBI,     /*6.訪問実施日*/
         kc.KANMI_HOKOKU_BUNSYO,                                    /*7.簡易修理内容*/
         sy.YUMURYO_KBN,                                            /*8.請求内容*/
         k.kokyaku_sei kokyaku_sei,                                 /*9.お客様名(姓)*/
         k.kokyaku_na kokyaku_na,                                   /*10.お客様名(名)*/
         k.kokyaku_jusyo kokyaku_jusyo,                             /*11.お客様住所*/
         k.kokyaku_tel_haihun kokyaku_tel,                          /*12.お客様ＴＥＬ*/
         k.kokyaku_katagaki kokyaku_katagaki,                       /*13.マンション名*/
         um.hinmei,                                                 /*14.商品名*/
         um.SYOHIN_HINBAN,                                          /*15.品番*/
         um.irainaiyo,                                              /*16.ご依頼内容*/
         TO_CHAR(sy.SYURI_JISEKIBI, 'YYYY/MM/DD') AS HOUMONBI,      /*17.訪問日*/
         DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_simei,tk.tokuisaki_sei) iraimoto_simei, /*18.ご依頼元氏名*/
         ir.iraimoto_tanto iraimoto_tanto,                                                           /*19.ご担当者様(依頼元)*/
         ir.iraimoto_jusyo,                                                                          /*20.ご依頼元住所*/
         DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_tel_haihun,tk.tokuisaki_tel_haihun) iraimoto_tel, /*21.ご依頼元ＴＥＬ*/
         DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_fax,tk.torokusaki_fax) iraimoto_fax,              /*22.ご依頼元ＦＡＸ*/
    	 um.koziten,                                                                                  /*23.工事店*/
         TO_CHAR(SYSDATE, 'YYYY/MM/DD') AS HAKKOUBI,                                                 /*24.発行日*/
         toi.toiawase_name toiawase_name,                                                            /*25.お問合せ先*/
         DECODE(substrb(toi.toiawase_tel,1,4),'0570',('ナビダイアル ' ||toi.toiawase_tel),DECODE(toi.toiawase_tel,'','',('TEL ' || toi.toiawase_tel)) ) toiawase_tel1 , /*26.お問合せ先ＴＥＬ*/
         DECODE(substrb(toi.toiawase_tel,1,4),'0570',(select '（電話   ' || UC_TEL || '）' from UKETUKECENTER where UC_CD=iu.uc_cd ),'') toiawase_tel2 , /*27.お問合せ先ＦＡＸ*/
         iu.kanryo_flg uketuke_kanryo, /*28.完了フラグ*/
         k.hm_kokyaku_no hm_kokyaku_no, /*29.お得意様顧客Ｎｏ*/
         ir.tyumon_bango tyumon_bango, /*30.注文番号*/
         ir.iraimoto_cd iraimoto_cd,   /*31.依頼元コード*/
         seikyusaki_tyumon_no,          /* 32.請求先注文番号*/
         wu.seikyusaki_cd,             /* 33.請求先コード*/
         sy.kanryo_flg,                /*34.作業予定.完了フラグ*/
         iu.kanryo_flg                 /*35.依頼受付.完了フラグ*/
         ,iu.uketuke_nendo                 /* 36.依頼受付.受付年度*/
         ,DECODE(toi.toiawase_fax,'','','FAX ' || toi.toiawase_fax) AS toiawase_fax/* 37.お問合せ先ＦＡＸ*/
           ,sy.syuritaisyo_seq                 /* 38.作業予定.修理予定SEQ*/
         /*2018.12.06 ADD START	グローエ、災害時特別対応*/
         ,case  when sk.seikyusaki_simei like 'グロ_エ％' then '無償対応'
          when sk.seikyusaki_cd IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') then (SELECT REPLACE(REPLACE(TOKUBETU_TOKUISAKI.FAX3_KANMI_BUNSYO,'（',''),'）','') FROM TOKUBETU_TOKUISAKI WHERE TOKUBETU_TOKUISAKI.TOKUISAKI_CD = sk.seikyusaki_cd)
          else '0' end hisai_kb    /*39.被災区分*/
         /*2018.12.06 ADD END	グローエ、災害時特別対応*/
    FROM
         iraiuketuke iu,
         uketuke_meisai um,
         sagyoyotei sy,
         toiawase_saki toi,
         uketuke_iraimoto ir,
         tokuisaki tk,
         uketuke_kokyaku k,
         web_uketuke wu,
         SYURITANTOSYA st,
         KANMI_CODE kc
         /*2018.12.06 ADD START	グローエ、災害時特別対応*/
         ,( select denpyo_no , edaban , edaban_max_flg from uriage_denpyo union select denpyo_no , edaban , edaban_max_flg from hokokusyo_nyuryoku )  h
		     ,( select denpyo_no , edaban , seikyusaki_cd , seikyusaki_simei from uriage_seikyusaki union select denpyo_no , edaban , seikyusaki_cd , seikyusaki_simei from nyuryoku_seikyusaki ) sk
         /*2018.12.06 ADD END	グローエ、災害時特別対応*/
    WHERE
         iu.uc_cd = um.uc_cd
         AND iu.uketuke_no = um.uketuke_no
         AND um.uc_cd = sy.uc_cd
         AND um.uketuke_no = sy.uketuke_no
         AND um.syuritaisyo_seq = sy.syuritaisyo_seq
         AND iu.uc_cd = ir.uc_cd
         AND iu.uketuke_no = ir.uketuke_no
         AND iu.uc_cd = k.uc_cd
         AND iu.uketuke_no = k.uketuke_no
         AND ir.iraimoto_cd = tk.tokuisaki_cd(+)
         AND iu.uc_cd = :uc_cd
         AND (
                (SUBSTR(sy.mikanriyu_cd,1,1) <> '4' AND sy.mikanriyu_cd NOT IN ('3A','1H','1J','2J') ) OR
                 sy.mikanriyu_cd IS NULL
             )
         AND iu.uketuke_no = :uketuke_no
         AND (SUBSTR(sy.mikanriyu_cd,1,1) <> '4' OR sy.mikanriyu_cd IS NULL)
         AND toi.fax_id = '3'
         AND iu.uc_cd = toi.uc_cd
         AND iu.eigyosyo_cd = toi.eigyosyo_cd
         AND iu.sc_cd = toi.sc_cd
         AND iu.sakujo_flg = '0'
         AND um.sakujo_flg = '0'
         AND sy.sakujo_flg = '0'
         AND iu.uketuke_nendo = um.uketuke_nendo
         AND um.uketuke_nendo = sy.uketuke_nendo
         AND iu.uketuke_nendo = ir.uketuke_nendo
         AND iu.uketuke_nendo = k.uketuke_nendo
         AND iu.uketuke_nendo = wu.uketuke_nendo(+)
         AND sy.syuri_jisekibi IS NOT NULL
         AND iu.UKETUKE_NO = wu.UKETUKE_NO(+)
         AND um.CE_CD = st.CE_CD(+)
         AND sy.MIKANRIYU_CD = kc.KANMI_CD(+)
         /*2018.12.06 ADD START	グローエ、災害時特別対応*/
         AND h.denpyo_no(+) = sy.ce_cd || sy.hokokusyo_no
	       AND h.edaban_max_flg(+) = '1'
         AND sk.denpyo_no(+) = h.denpyo_no
         AND sk.edaban(+) = h.edaban
         /*2018.12.06 ADD END	グローエ、災害時特別対応*/
     ORDER BY
          iraimoto_cd,
          iraimoto_fax,
          uketuke_no,
          jisirireki_seq
SQL;
     return $sql;
  }

  /**
   * 訪問予定報告検索ＳＱＬ
   */
  private function _SQL_SELECT_YOTEI()
  {
    $sql = <<<SQL
    SELECT
      TO_CHAR(IRAIUKETUKE.UKETUKEBI, 'YYYY/MM/DD') AS UKETUKEBI,	/*1.登録日 */
      IRAIUKETUKE.UKETUKE_NO,						/*2.受付Ｎｏ */
      SAGYOYOTEI.CE_CD,								/*3.担当者(コード) */
      SYURITANTOSYA.CE_SEI,							/*4.担当者(姓) */
      TO_CHAR(SAGYOYOTEI.RENRAKUBI, 'YYYY/MM/DD') AS RENRAKUBI,			/*5.連絡日 */
      NVL(SAGYOYOTEI.C_VOICE_YOTEI_BIKO,D_TEKIYO_CD.C_VOICE_SETUMEI) AS RENRAKU_NAIYO,							/*6.連絡内容 */
      TO_CHAR(SAGYOYOTEI.SYURI_YOTEIBI, 'YYYY/MM/DD') AS SYURI_YOTEIBI,	/*7.訪問予定日 */
      TO_CHAR(SAGYOYOTEI.SYURI_YOTEIBI, 'HH24MI') AS YOTEI_JIKAN,	/*8.予定時間 */
      UKETUKE_KOKYAKU.KOKYAKU_SEI,					/*9.お客様名(姓) */
      UKETUKE_KOKYAKU.KOKYAKU_NA,					/*10.お客様名(名) */
      UKETUKE_KOKYAKU.KOKYAKU_JUSYO,				/*11.お客様住所 */
      UKETUKE_KOKYAKU.KOKYAKU_TEL_HAIHUN,			/*12.お客様ＴＥＬ */
      UKETUKE_KOKYAKU.KOKYAKU_KATAGAKI,				/*13.マンション名 */
      UKETUKE_MEISAI.HINMEI,						/*14.商品名 */
      UKETUKE_MEISAI.SYOHIN_HINBAN,					/*15.品番 */
      UKETUKE_MEISAI.IRAINAIYO,						/*16.ご依頼内容 */
      UKETUKE_SEIKYUSAKI.SEIKYUSAKI_CD,				/*17.得意先コード(請求先) */
      UKETUKE_SEIKYUSAKI.SEIKYUSAKI_SIMEI,			/*18.ご依頼時請求先名 */
      UKETUKE_SEIKYUSAKI.SEIKYUSAKI_TANTO,			/*19.ご担当者様(請求先) */
      UKETUKE_SEIKYUSAKI.SEIKYUSAKI_JUSYO,			/*20.ご依頼時請求先住所 */
      UKETUKE_SEIKYUSAKI.SEIKYUSAKI_TEL_HAIHUN,		/*21.ご依頼時請求先ＴＥＬ */
      UKETUKE_SEIKYUSAKI.SEIKYUSAKI_FAX,			/*22.ご依頼時請求先ＦＡＸ */
      UKETUKE_MEISAI.KOZITEN, 						/*23.工事店 */
      UKETUKE_SEIKYUSAKI.TYUMON_BANGO, 	/* 26.ご依頼時貴社注番 */
      IRAIUKETUKE.UKETUKE_NENDO, 					/* 27.受付年度 */
      SAGYOYOTEI.SYURITAISYO_SEQ 					/* 28.修理対象SEQ */
    FROM
      SYURITANTOSYA,
      SAGYOYOTEI,
      UKETUKE_SEIKYUSAKI,
      UKETUKE_KOKYAKU,
      UKETUKE_MEISAI,
      IRAIUKETUKE
     ,D_TEKIYO_CD
    WHERE
          IRAIUKETUKE.UC_CD              = :uc_cd
      AND IRAIUKETUKE.UKETUKE_NO         = :uketuke_no
      AND IRAIUKETUKE.SAKUJO_FLG         = '0'
      AND UKETUKE_MEISAI.UC_CD           = IRAIUKETUKE.UC_CD
      AND UKETUKE_MEISAI.UKETUKE_NO      = IRAIUKETUKE.UKETUKE_NO
      AND UKETUKE_MEISAI.SYURITAISYO_SEQ = :seq
      AND UKETUKE_MEISAI.SAKUJO_FLG      = '0'
      AND UKETUKE_KOKYAKU.UC_CD          = IRAIUKETUKE.UC_CD
      AND UKETUKE_KOKYAKU.UKETUKE_NO     = IRAIUKETUKE.UKETUKE_NO
      AND UKETUKE_KOKYAKU.SAKUJO_FLG     = '0'
      AND UKETUKE_SEIKYUSAKI.UC_CD       = IRAIUKETUKE.UC_CD
      AND UKETUKE_SEIKYUSAKI.UKETUKE_NO  = IRAIUKETUKE.UKETUKE_NO
      AND UKETUKE_SEIKYUSAKI.SAKUJO_FLG  = '0'
      AND SAGYOYOTEI.UC_CD               = UKETUKE_MEISAI.UC_CD
      AND SAGYOYOTEI.UKETUKE_NO          = UKETUKE_MEISAI.UKETUKE_NO
      AND SAGYOYOTEI.SYURITAISYO_SEQ     = UKETUKE_MEISAI.SYURITAISYO_SEQ
      AND UKETUKE_MEISAI.UKETUKE_NENDO   = IRAIUKETUKE.UKETUKE_NENDO
      AND UKETUKE_KOKYAKU.UKETUKE_NENDO  = IRAIUKETUKE.UKETUKE_NENDO
      AND UKETUKE_SEIKYUSAKI.UKETUKE_NENDO = IRAIUKETUKE.UKETUKE_NENDO
      AND SAGYOYOTEI.UKETUKE_NENDO = UKETUKE_MEISAI.UKETUKE_NENDO
      AND SAGYOYOTEI.JISIRIREKI_SEQ in (SELECT
                                MAX(JISIRIREKI_SEQ)
                             FROM
                                SAGYOYOTEI
                               WHERE
                                SAGYOYOTEI.UC_CD           = :uc_cd
                             AND SAGYOYOTEI.UKETUKE_NO      = :uketuke_no
                                        AND SAGYOYOTEI.UKETUKE_NENDO   = UKETUKE_MEISAI.UKETUKE_NENDO
                                        AND SAGYOYOTEI.SYURITAISYO_SEQ = :seq)
      AND SAGYOYOTEI.SAKUJO_FLG          = '0'
      AND UKETUKE_MEISAI.CE_CD           = SYURITANTOSYA.CE_CD(+)
      AND SAGYOYOTEI.TEKIYO_CD = D_TEKIYO_CD.CODE(+)
SQL;
    return $sql;
  }

  /**
   * 受付依頼元テーブル検索
   */
  public function getUketukeIraimoto($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_UKETUKE_IRAIMOTO()),$search);
  }

  /**
   * 受付依頼元テーブル検索ＳＱＬ
   */
  private function _SQL_SELECT_COMMAND_UKETUKE_IRAIMOTO()
  {
    $sql =<<<SQL
    SELECT
      IRAIMOTO_CD, 							/*ご依頼元コード*/
      IRAIMOTO_SIMEI,						/*ご依頼元氏名*/
      IRAIMOTO_TANTO,						/*ご担当者様(依頼元)*/
      IRAIMOTO_JUSYO,						/*ご依頼元住所*/
      IRAIMOTO_TEL_HAIHUN,					/*ご依頼元ＴＥＬ*/
      IRAIMOTO_FAX, 						/*ご依頼元ＦＡＸ*/
      TYUMON_BANGO 							/*ご依頼元貴社注番*/
    FROM
      UKETUKE_IRAIMOTO
    WHERE
          UKETUKE_IRAIMOTO.UC_CD      = :uc_cd
      AND UKETUKE_IRAIMOTO.UKETUKE_NO = :uketuke_no
      AND UKETUKE_IRAIMOTO.UKETUKE_NENDO = :uketuke_nendo
      AND UKETUKE_IRAIMOTO.SAKUJO_FLG = '0'
SQL;
    return $sql;
  }

  /**
   * SQL実行（確報の判別）
   */
  private function _SQL_SELECT_KAKUHOU_UKETUKE_NO()
  {
    $sql = <<<SQL
    SELECT
    	iu.uketuke_no           uketuke_no,
      iu.uc_cd                uc_cd
     FROM
         uriage_denpyo           h,
         uriage_kokyaku          k,
         imt_hinban              hm,
         imt_syuyaku_hinban      shm,
         (
             SELECT DISTINCT
                 s_iu.uc_cd          uc_cd,
                 s_iu.uketuke_no     uketuke_no,
                 s_iu.eigyosyo_cd    eigyosyo_cd,
                 s_iu.sc_cd          sc_cd,
                 s_iu.uketukebi      uketukebi,
                 s_iu.message        message,
                 s_iu.kanryo_flg     kanryo_flg,
                 s_iu.fax5_date      fax5_date,
                 s_iu.uketuke_nendo     uketuke_nendo,
                 s_iu.torokusya_cd   torokusya_cd
             FROM
                 iraiuketuke         s_iu,
                 uriage_denpyo       s_h
             WHERE
                     s_iu.uketuke_no = s_h.uketuke_no
                 AND s_iu.uketuke_nendo = s_h.uketuke_nendo
                 AND s_iu.sakujo_flg = '0'
                 AND s_iu.KANRYO_FLG in ('9' , '8')
                 AND s_h.kanryo_kbn = '1'
                 AND s_h.akakuro_kbn = 'B'
                 AND NOT (s_h.denpyo_no LIKE '____T____'
                       OR s_h.denpyo_no LIKE 'TE%'
                       OR s_h.denpyo_no LIKE 'K%')
                 AND s_h.sakujo_flg = '0'
                 AND s_h.edaban_max_flg = '1'
                 AND s_iu.uketuke_no =  :uketuke_no
             ) iu,
         uketuke_kokyaku uk
     WHERE
         iu.uc_cd = uk.uc_cd
         AND iu.uketuke_no = uk.uketuke_no
         AND iu.uketuke_no = h.uketuke_no
         AND iu.uketuke_nendo = uk.uketuke_nendo
         AND iu.uketuke_nendo = h.uketuke_nendo
         AND h.denpyo_no = k.denpyo_no(+)
         AND h.edaban = k.edaban(+)
         AND h.edaban_max_flg = '1'
         AND h.a_hinmoku = hm.hinmoku(+) AND h.a_hinban = hm.hinban(+)
         AND hm.hinmoku = shm.hinmoku(+) AND hm.syuyaku_hinban = shm.syuyaku_hinban(+)
         AND h.kanryo_kbn = '1'
         AND h.akakuro_kbn = 'B' AND NVL(h.sekinin_kbn,' ') <> 'Q'
         AND NOT EXISTS(SELECT  'X' FROM uriage_buhin_meisai hm
                         WHERE  h.denpyo_no   = hm.denpyo_no
                           AND  h.edaban      = hm.edaban
                           AND  ((hm.service_cd = 'RSSB'
                                 AND  hm.hinmoku    = 'KK'
                                 AND  hm.gyoban = 1)
                                 OR (hm.service_cd = '809C'))
                       )
         AND NOT EXISTS(SELECT  'X' FROM uriage_tyosei_meisai tm
                        WHERE  h.denpyo_no   = tm.denpyo_no
                           AND  h.edaban      = tm.edaban
                          AND  tm.service_cd    = '809C'
                       )
         AND NOT EXISTS(SELECT  'X' FROM hokokusyo_nyuryoku hn
                         WHERE  hn.uketuke_no = iu.uketuke_no
                           AND  hn.uketuke_nendo = iu.uketuke_nendo
                           AND  hn.SYONINJOKYO in ('0','1','2','7')
                       )
SQL;
    return $sql;
  }

  /**
   * 経過・結果報告(確報)検索ＳＱＬ
   */
  private function _SQL_SELECT_KAKUHOU()
  {
    $sql =<<<SQL
      SELECT
           TO_CHAR(IU.UKETUKEBI,'YYYY/MM/DD') UKETUKEBI,/* 1.登録日 */
           iu.uketuke_no           uketuke_no,          /* 2.受付no */
           h.ce_cd                 ce_cd,               /* 3.担当者(コード) */
           ce.ce_sei               ce_sei,              /* 4.担当者(姓) */
        /* 		       TO_CHAR(H.HOMONBI,'YYYY/MM/DD') HOMONBI,     /* 5.訪問日 */
           TO_CHAR(H.HOMONBI,'YYYY/MM/DD') HOMONBI,     /* 5.訪問日 */
        (select s.MIKANRIYU_CD
          from SAGYOYOTEI s
         where s.uketuke_no = h.uketuke_no and s.ce_cd = h.ce_cd and
               s.syuritaisyo_seq = h.UKETUKE_EDABAN and
               s.uketuke_nendo = h.uketuke_nendo and
               s.jisirireki_seq in
               (select max(sy.jisirireki_seq)
                  from sagyoyotei sy
                 where sy.uketuke_no = h.uketuke_no and sy.ce_cd = h.ce_cd and
                       sy.uketuke_nendo = h.uketuke_nendo and
                       sy.syuritaisyo_seq = h.UKETUKE_EDABAN)) MIKANRIYU_CD, /* 6.未完理由コード */
           h.yumuryo_kbn           yumuryo_kbn,         /* 7. */
        (select KANMI_HOKOKU_BUNSYO
          from KANMI_CODE
         where KANMI_CD =
               (select s.MIKANRIYU_CD
                  from SAGYOYOTEI s
                 where s.uketuke_no = h.uketuke_no and s.ce_cd = h.ce_cd and
                       s.syuritaisyo_seq = h.UKETUKE_EDABAN and
                       s.uketuke_nendo = h.uketuke_nendo and
                       s.jisirireki_seq in
                       (select max(sy.jisirireki_seq)
                          from sagyoyotei sy
                         where sy.uketuke_no = h.uketuke_no and sy.ce_cd = h.ce_cd and
                               sy.uketuke_nendo = h.uketuke_nendo and
                               sy.syuritaisyo_seq = h.UKETUKE_EDABAN))) KANMI_HOKOKU_BUNSYO, /* 8.*/
           k.kokyaku_sei           kokyaku_sei,         /* 9.お客様名 */
           k.kokyaku_na            kokyaku_na,          /* 10.お客様名 */
           k.kokyaku_jusyo         kokyaku_jusyo,       /* 11.お客様住所 */
           k.kokyaku_katagaki      kokyaku_katagaki,    /* 12.マンション名 */
           k.kokyaku_tel_haihun    kokyaku_tel,         /* 13.お客様TEL */
           '' hinmei,                                   /* 14.商品名ダミー */
           '' hinban,                                   /* 15.品番ダミー */
           (select max(mm.irainaiyo) from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd and mm.uketuke_nendo = h.uketuke_nendo) irainaiyo,     /* 16.ご依頼内容 */
           ir.tyumon_bango         tyumon_bango,/* 17.注文番号 */
           DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_simei,tk.tokuisaki_sei)   iraimoto_simei,/* 18.ご依頼元氏名 */
           ir.iraimoto_tanto       iraimoto_tanto,     /* 19.ご担当者様(依頼元) */
           ir.iraimoto_jusyo       iraimoto_jusyo,     /* 20.ご依頼元住所 */
           DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_tel_haihun,tk.tokuisaki_tel_haihun)  iraimoto_tel,/* 21.ご依頼元TEL */
           DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_fax,tk.torokusaki_fax)     iraimoto_fax,          /* 22.ご依頼元FAX */
           sk.seikyusaki_cd        seikyusaki_cd,            /* 23.得意先コード(請求先) */
           sk.seikyusaki_simei     seikyusaki_simei,         /* 24.ご依頼時請求先名 */
           sk.seikyusaki_tanto     seikyusaki_tanto,         /* 25.ご担当者様(請求先) */
           sk.seikyusaki_jusyo     seikyusaki_jusyo,         /* 26.ご依頼時請求先住所 */
           sk.seikyusaki_tel_haihun     seikyusaki_tel,      /* 27.ご依頼時請求先TEL */
           SK.SEIKYUSAKI_FAX           SEIKYUSAKI_FAX,       /* 28.ご依頼時請求先FAX */
           (select max(mm.koziten) from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd and mm.uketuke_nendo = h.uketuke_nendo) koziten,/* 29.工事店 */
           tk2.fax1_5_tyokusyu_flg,               /* 30. */
           uk.hm_kokyaku_no        hm_kokyaku_no, /* 31.お得意様顧客Ｎｏ */
           toi.toiawase_name       toiawase_name, /* 32.お問合せ先 */
           DECODE(SUBSTRB(TOI.TOIAWASE_TEL,1 ,4),'0570',('ﾅﾋﾞﾀﾞｲﾔﾙ ' ||TOI.TOIAWASE_TEL),DECODE(TOI.TOIAWASE_TEL,'','',('TEL ' || TOI.TOIAWASE_TEL)) ) TOIAWASE_TEL1, /* 33.お問合せ先ＴＥＬ */
           DECODE(SUBSTRB(TOI.TOIAWASE_TEL,1 ,4),'0570',(SELECT '（電話   ' || UC_TEL || '）' FROM UKETUKECENTER WHERE UC_CD=IU.UC_CD ),'') TOIAWASE_TEL2 ,           /* 34.お問合せ先ＦＡＸ */
           h.nyukin_kbn            nyukin_kbn,        /* 35. */
           TO_CHAR(SYSDATE,'YYYY/MM/DD')  HAKKOUBI,   /* 36.発行日 */
           ir.iraimoto_cd          iraimoto_cd,       /* 37.ご依頼元コード */
           H.KAISI_JIKOKU  			  KAISI_JIKOKU,  /* 38.開始時刻 */
           DECODE((select nvl(min(mm.syutanto_kbn),'2')      from uketuke_meisai mm
                   where mm.uketuke_no = h.uketuke_no
                     and mm.uketuke_nendo = h.uketuke_nendo
                     and mm.ce_cd = h.ce_cd),'1',
                                                  SUBSTR(SYANAI_KIJI,1,
                                                                     DECODE( INSTR(SYANAI_KIJI,'ご入会'), 0, LENGTH(SYANAI_KIJI),
                                                                                                             INSTR(SYANAI_KIJI,'ご入会')-1
                                                                           )
                                                        )
                                        ,'') sagyo_naiyo,/* 39.お客様記事 */
           SEIKYUSAKI_TYUMON_NO,/*  40.請求先注文番号 */
           SK.TYUMON_BANGO          　　　HOKOKUSYO_TYUMON_BANGO,/*  42.報告書請求先注文番号 */
          (select nvl(min(mm.syutanto_kbn),'2') from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd and mm.uketuke_nendo = h.uketuke_nendo) syutanto_kbn
          ,iu.uketuke_nendo          uketuke_nendo           /*  44.受付年度 */
         ,DECODE(h.yumuryo_kbn, 'A',
                 CASE WHEN  ir.iraimoto_cd <> '93041'   AND sk.seikyusaki_cd = '93041'   THEN '2'
                      WHEN  ir.iraimoto_cd <> '10995'   AND sk.seikyusaki_cd = '10995'   THEN '2'
                      /*	2018.12.06 ADD START	//グローエ、災害時特別対応*/
	                    WHEN  sk.seikyusaki_simei like 'グロ_エ％' THEN '2'
                      WHEN  sk.seikyusaki_cd IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') THEN '2'
                      /*2018.12.06 ADD END*/
                      ELSE  '1'
                 END,
                 '2'           )  sort_kbn1,  /*  45. */
          DECODE(h.yumuryo_kbn, 'A',
                 CASE WHEN  DECODE(sk.seikyusaki_cd, '99999', sk.seikyusaki_tel, tk2.tokuisaki_tel) =
                            DECODE(ir.iraimoto_cd,   '99999', ir.iraimoto_tel,   tk.tokuisaki_tel)      THEN '1'
                      WHEN  DECODE(sk.seikyusaki_cd, '99999', sk.seikyusaki_tel, tk2.tokuisaki_tel) =
                            k.kokyaku_tel      THEN '2'
                      ELSE  '8'
                 END,
                 '9' )  sort_kbn2   /*  46.*/
           ,DECODE(TOI.TOIAWASE_FAX,'','','FAX ' || TOI.TOIAWASE_FAX) 		/*  47.お問合せ先ＦＡＸ */
           ,h.IRAIMOTO_KIJI 		/*  48.依頼元記事 */
           /*2018.12.06 ADD START	//グローエ、災害時特別対応*/
           ,case  when sk.seikyusaki_simei like 'グロ_エ％' then '無償対応'
           when sk.seikyusaki_cd IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') then (SELECT REPLACE(REPLACE(TOKUBETU_TOKUISAKI.FAX3_KANMI_BUNSYO,'（',''),'）','') FROM TOKUBETU_TOKUISAKI WHERE TOKUBETU_TOKUISAKI.TOKUISAKI_CD = sk.seikyusaki_cd)
           else '0' end hisai_kb /*49.被災区分*/
           /*2018.12.06 ADD END*/
           ,h.SEKININ_KBN 		/*  49.責任区分 */
           ,iu.kanryo_flg 		/*  50.責任区分 */
        FROM
           eigyosyo                ei,
           sc                      sc,
           syuritantosya           ce,
           toiawase_saki           toi,
           uriage_denpyo        h,
           uriage_seikyusaki    sk,
           uriage_kokyaku       k,
           imt_hinban              hm,
           imt_syuyaku_hinban      shm,
           uketuke_iraimoto        ir,
           tokuisaki               tk,
           tokuisaki               tk2,
           (
               SELECT DISTINCT
                   s_iu.uc_cd          uc_cd,
                   s_iu.uketuke_no     uketuke_no,
                   s_iu.eigyosyo_cd    eigyosyo_cd,
                   s_iu.sc_cd          sc_cd,
                   s_iu.uketukebi      uketukebi,
                   s_iu.message        message,
                   s_iu.kanryo_flg     kanryo_flg,
                   s_iu.fax5_date      fax5_date,
                   s_iu.torokusya_cd   torokusya_cd
                   ,s_iu.uketuke_nendo  uketuke_nendo
               FROM
                   iraiuketuke         s_iu,
                   uriage_denpyo       s_h
                   /* ,fax_rireki          s_fax */
               WHERE
                       s_iu.uketuke_no = s_h.uketuke_no
                   AND s_iu.uketuke_nendo = s_h.uketuke_nendo
                   AND s_iu.sakujo_flg = '0'
                   AND s_iu.KANRYO_FLG in ('9' , '8')
                   AND s_h.kanryo_kbn = '1'
                   AND s_h.akakuro_kbn = 'B'
                   AND NOT (s_h.denpyo_no LIKE '____T____'
                         OR s_h.denpyo_no LIKE 'TE%'
                         OR s_h.denpyo_no LIKE 'K%')
                   AND s_h.sakujo_flg = '0'
                   AND s_iu.uc_cd = :uc_cd
                   AND s_h.edaban_max_flg = '1'
                   AND s_iu.uketuke_no =  :uketuke_no
               ) iu,
           uketuke_kokyaku uk,
           web_uketuke wu
        WHERE
               iu.uc_cd = :uc_cd
           AND iu.uketuke_no =  :uketuke_no AND
               iu.eigyosyo_cd = ei.eigyosyo_cd
           AND iu.eigyosyo_cd = sc.eigyosyo_cd AND iu.sc_cd = sc.sc_cd
           AND iu.uc_cd = ir.uc_cd AND iu.uketuke_no = ir.uketuke_no
           AND iu.uc_cd = uk.uc_cd AND iu.uketuke_no = uk.uketuke_no
           AND ir.iraimoto_cd = tk.tokuisaki_cd(+)
           AND sk.seikyusaki_cd = tk2.tokuisaki_cd(+) AND  tk2.sakujo_flg(+) = '0'
           AND iu.uketuke_no = h.uketuke_no
           AND h.denpyo_no = k.denpyo_no(+) AND h.denpyo_no = sk.denpyo_no(+)
           AND h.edaban = k.edaban(+) AND h.edaban = sk.edaban(+)
           AND h.edaban_max_flg = '1'
           AND toi.fax_id = '5' AND toi.uc_cd = iu.uc_cd
           AND toi.eigyosyo_cd = iu.eigyosyo_cd AND toi.sc_cd = iu.sc_cd
           AND h.a_hinmoku = hm.hinmoku(+) AND h.a_hinban = hm.hinban(+)
           AND hm.hinmoku = shm.hinmoku(+) AND hm.syuyaku_hinban = shm.syuyaku_hinban(+)
           AND h.ce_cd = ce.ce_cd(+)
           AND h.kanryo_kbn = '1'
           AND h.akakuro_kbn = 'B' AND NVL(h.sekinin_kbn,' ') <> 'Q'
           AND iu.uketuke_nendo = h.uketuke_nendo
           AND iu.uketuke_nendo = ir.uketuke_nendo
           AND iu.uketuke_nendo = uk.uketuke_nendo
           AND iu.uketuke_nendo = wu.uketuke_nendo(+)
           AND NOT EXISTS(SELECT  'X' FROM uriage_buhin_meisai hm
                           WHERE  h.denpyo_no   = hm.denpyo_no
                             AND  h.edaban      = hm.edaban
                             AND  ((hm.service_cd = 'RSSB'
                                   AND  hm.hinmoku    = 'KK'
                                   AND  hm.gyoban = 1)
                                   OR (hm.service_cd = '809C'))
                         )
           AND NOT EXISTS(SELECT  'X' FROM uriage_tyosei_meisai tm
                          WHERE  h.denpyo_no   = tm.denpyo_no
                             AND  h.edaban      = tm.edaban
                            AND  tm.service_cd    = '809C'
                         )
           AND NOT EXISTS(SELECT  'X' FROM hokokusyo_nyuryoku hn
                           WHERE  hn.uketuke_no = iu.uketuke_no
                             AND  hn.uketuke_nendo = iu.uketuke_nendo
                             AND  hn.SYONINJOKYO in ('0','1','2','7')
                         )
           AND IU.UKETUKE_NO = wu.UKETUKE_NO(+)
        ORDER BY
           ir.iraimoto_cd,     ir.iraimoto_fax,
           iu.uketuke_no,
           sort_kbn1,     sort_kbn2,
           h.homonbi   DESC,     syutanto_kbn
SQL;
    return $sql;
  }

  public function getMesai($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_MESAI()),$search);
  }

  private function _SQL_SELECT_MESAI()
  {
    $sql =<<<SQL
    SELECT
     HINMEI,
     SYOHIN_HINBAN,
     IRAINAIYO
     FROM UKETUKE_MEISAI
    WHERE
         UC_CD = :uc_cd
     AND UKETUKE_NO = :uketuke_no
     AND UKETUKE_NENDO = :uketuke_nendo
     AND SAKUJO_FLG = '0'
     AND KANRYO_FLG <> '8'
     AND SYUTANTO_KBN = '1'
     ORDER BY
      SYURITAISYO_SEQ
SQL;
    return $sql;
  }

  /**
   * 部品情報の取得・セット
   */
  public function getBuhin($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_BUHIN()),$search);
  }

  /**
   * 部品情報の取得・セット
   */
  private function _SQL_SELECT_COMMAND_BUHIN()
  {
    $sql = <<<SQL
      SELECT
        x.hinban hinban ,
        x.hinmei hinmei ,
        sum(x.suryo) suryo ,
        x.tanka tanka   ,
        sum(x.buhindai) buhindai
      FROM
      (
       SELECT
           DECODE(h.yumuryo_kbn,'A',
                   CASE WHEN  ir.iraimoto_cd <> '93041'   AND sk.seikyusaki_cd = '93041'   THEN 'M'
                        WHEN  ir.iraimoto_cd <> '10995'   AND sk.seikyusaki_cd = '10995'   THEN 'M'
                        WHEN  bm.syori_kbn IN ('K','G','S')  THEN 'M'
                        ELSE  'Y'          END,
                   'M') syori,
          bm.hinban               hinban,
          sh.hinmei_syagai        hinmei,
          NVL(bm.suryo,0)           suryo,
              DECODE(h.yumuryo_kbn,'A',
                      CASE WHEN  ir.iraimoto_cd <> '93041'   AND sk.seikyusaki_cd = '93041'   THEN  0
                           WHEN  ir.iraimoto_cd <> '10995'   AND sk.seikyusaki_cd = '10995'   THEN  0
                           ELSE  NVL(bm.tanka,0)
                      END,
                      0 ) tanka,
              DECODE(h.yumuryo_kbn,'A',
                      CASE WHEN  ir.iraimoto_cd <> '93041'   AND sk.seikyusaki_cd = '93041'   THEN  0
                           WHEN  ir.iraimoto_cd <> '10995'   AND sk.seikyusaki_cd = '10995'   THEN  0
                           ELSE  NVL(bm.yuryo_kingaku,0)
                      END,
                      0 ) buhindai
       FROM
          uriage_denpyo           h,
          uriage_buhin_meisai  bm,
          iraiuketuke             iu,
          uketuke_iraimoto        ir,
          uriage_seikyusaki       sk,
          imt_syuyaku_hinban      sh
       WHERE
            h.denpyo_no = bm.denpyo_no AND h.edaban = bm.edaban
        AND bm.hinmoku = sh.hinmoku(+) AND bm.syuyaku_hinban = sh.syuyaku_hinban(+)
        AND h.uketuke_no = :uketuke_no
        AND h.uketuke_nendo = :uketuke_nendo
        AND h.akakuro_kbn = 'B'
        AND NVL(h.sekinin_kbn,' ') <> 'Q'
        AND h.sakujo_flg = '0'
        AND h.uketuke_no = iu.uketuke_no
        AND h.uketuke_nendo = iu.uketuke_nendo
        AND iu.uc_cd = ir.uc_cd
        AND iu.uketuke_no = ir.uketuke_no
        AND iu.uketuke_nendo = ir.uketuke_nendo
        AND h.denpyo_no = sk.denpyo_no
        AND h.edaban = sk.edaban
        AND NOT (h.denpyo_no LIKE '____T____'       OR h.denpyo_no LIKE 'K________'      OR h.denpyo_no LIKE 'TE_______')
        AND h.edaban_max_flg = '1'
          AND NOT EXISTS(
                 SELECT  'X'
                 FROM uriage_buhin_meisai B
                 WHERE  H.DENPYO_NO   = B.DENPYO_NO        AND
                        B.SERVICE_CD = '809C') ) x
      GROUP BY
      x.syori,x.hinban ,x.hinmei,x.tanka
      ORDER BY
      x.syori DESC , x.tanka DESC
SQL;
    return $sql;
  }

  /**
   * 報告書有料金額集計取得
   */
  public function getYuryokin($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_YURYOKIN()),$search);
  }

  /**
   * 報告書有料金額集計取得ＳＱＬ
   */
  private function _SQL_SELECT_COMMAND_YURYOKIN()
  {
    $sql =<<<SQL
      SELECT
          NVL(SUM(h.yuryo_buhindai_kei),0)  buhindai_kei,
          NVL(SUM(h.yuryo_gijuturyo),0) + NVL(SUM(h.yuryo_toritukeryo),0) gijuturyo_kei,
          NVL(SUM(h.yuryo_syutyoryo),0)    syutyoryo_kei,
          NVL(SUM(h.yuryo_syobunryo),0)    syobunryo_kei,
          NVL(SUM(h.yuryo_buhindai_nebiki),0) + NVL(SUM(h.yuryo_torituke_nebiki),0) nebiki_kei,
          NVL(SUM(h.yuryo_syohizei),0)     syohizei_kei
      FROM
          uriage_denpyo           h,
          iraiuketuke             iu,
          uketuke_iraimoto        ir,
          uriage_seikyusaki       sk
       WHERE
           h.uketuke_no = :uketuke_no
        AND h.uketuke_nendo = :uketuke_nendo
        AND h.akakuro_kbn = 'B'
        AND NVL(h.sekinin_kbn,' ') <> 'Q'
        AND h.sakujo_flg = '0'
        AND h.edaban_max_flg = '1'
        AND h.uketuke_no = iu.uketuke_no
        AND h.uketuke_nendo = iu.uketuke_nendo
        AND iu.uc_cd = ir.uc_cd
        AND iu.uketuke_no = ir.uketuke_no
        AND iu.uketuke_nendo = ir.uketuke_nendo
        AND h.denpyo_no = sk.denpyo_no
        AND h.edaban = sk.edaban
        AND h.yumuryo_kbn = 'A'
        AND NOT (     ir.iraimoto_cd <> '93041'
                  AND NVL(sk.seikyusaki_cd, ' ') = '93041' )
        AND NOT (     ir.iraimoto_cd <> '10995'
                  AND NVL(sk.seikyusaki_cd, ' ') = '10995' )
        AND NOT (h.denpyo_no LIKE '____T____'       OR h.denpyo_no LIKE 'K________'      OR h.denpyo_no LIKE 'TE_______')
        /*  2019.01.16 MOD START  グローエ、災害時特別対応 */
        /*	2018.12.06 ADD STRAT	グローエ、災害時特別対応 */
	      /*
         AND NVL(sk.seikyusaki_cd,' ') NOT IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') 
	      AND NVL(sk.seikyusaki_simei,' ') NOT like 'グロ_エ％'
        */
        /*	2018.12.06 ADD END */       
        AND ((NVL(sk.seikyusaki_cd,' ') NOT IN (SELECT tokubetu_tokuisaki.tokuisaki_cd FROM tokubetu_tokuisaki WHERE sakujo_flg = '0'  )
		    AND NVL(sk.seikyusaki_simei,' ') NOT like 'グロ_エ％' ) 
		    OR  NVL(ir.iraimoto_cd,' ') IN (SELECT tokubetu_tokuisaki.tokuisaki_cd FROM tokubetu_tokuisaki WHERE sakujo_flg = '0')
	      OR  NVL(ir.iraimoto_simei,' ') like 'グロ_エ％' ) 
        /*  2019.01.16 MOD END */
            AND NOT EXISTS(
                   SELECT  'X'
                   FROM uriage_buhin_meisai A
                   WHERE  H.DENPYO_NO   = A.DENPYO_NO        AND
                          A.SERVICE_CD = '809C')
            AND NOT EXISTS(
                   SELECT  'X'
                   FROM uriage_tyosei_meisai B
                   WHERE  H.DENPYO_NO   = B.DENPYO_NO        AND
                          B.SERVICE_CD = '809C')
      GROUP BY
        h.uketuke_no
SQL;
    return $sql;
  }

  public function getTyosei($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_TYOSEI()),$search);
  }

  private function _SQL_SELECT_COMMAND_TYOSEI()
  {
    $sql = <<< SQL
      SELECT DISTINCT
          tm.tyosei_bui           bui,
          sm.syoti_naiyo          syoti_naiyo
        ,h.YUMURYO_KBN
        ,h.KANRYO_KBN
        ,h.DENPYO_NO
        ,tm.GYOBAN
       FROM
          uriage_denpyo        h,
          uriage_tyosei_meisai tm,
          syoti_cd                sm
       WHERE
            h.denpyo_no = tm.denpyo_no AND h.edaban = tm.edaban
        AND tm.syoti_cd = sm.syoti_cd(+)
        AND h.uketuke_no = :uketuke_no
        AND h.uketuke_nendo = :uketuke_nendo
        AND h.kanryo_kbn = '1'
        AND h.akakuro_kbn = 'B'
        AND NVL(h.sekinin_kbn,' ') <> 'Q'
        AND h.sakujo_flg = '0'
        AND h.edaban_max_flg = '1'
        AND NOT EXISTS(
                SELECT  'X'
                FROM uriage_tyosei_meisai B
                WHERE  H.DENPYO_NO   = B.DENPYO_NO        AND
                       B.SERVICE_CD = '809C')
        AND NVL(tm.syoti_cd,' ') <> 'H'
        ORDER BY
        h.YUMURYO_KBN
        ,h.KANRYO_KBN
        ,h.DENPYO_NO
        ,tm.GYOBAN
SQL;
    return $sql;
  }

  public function getIraiNaiyoKakuhou($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_MEISAI_KAKUHOU()),$search);
  }

  private function _SQL_SELECT_COMMAND_MEISAI_KAKUHOU()
  {
    $sql = <<<SQL
      SELECT
       HINMEI,
       SYOHIN_HINBAN,
       IRAINAIYO
       FROM UKETUKE_MEISAI
      WHERE
           UC_CD = :uc_cd
       AND UKETUKE_NO = :uketuke_no
       /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応 */
       AND UKETUKE_NENDO = :uketuke_nendo
       /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応 */
       AND SAKUJO_FLG = '0'
       AND KANRYO_FLG <> '8'
       AND SYUTANTO_KBN = '1'
       ORDER BY
        SYURITAISYO_SEQ
SQL;
    return $sql;
  }

  public function getIraiNaiyo($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_MEISAI()),$search);
  }

  private function _SQL_SELECT_COMMAND_MEISAI()
  {
    $sql = <<<SQL
      SELECT
           UM.SYURITAISYO_SEQ MEISAI_SEQ,
           UM.HINMEI,
           SYOHIN_HINBAN,
           NVL(UM.IRAINAIYO,' ')        IRAINAIYO,
           TO_CHAR(SY.SYURI_JISEKIBI,'YYYY/MM/DD')   HOMONBI
       FROM
           UKETUKE_MEISAI  UM,
           SAGYOYOTEI      SY,
           KANMI_CODE      MC,
           SYURITANTOSYA CE
       WHERE
           UM.UC_CD = :uc_cd
           AND UM.UKETUKE_NO = :uketuke_no
      /*  2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
           AND UM.UKETUKE_NENDO = :uketuke_nendo
           AND UM.UKETUKE_NENDO = SY.UKETUKE_NENDO
      /*  2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
           AND UM.UC_CD = SY.UC_CD
           AND UM.UKETUKE_NO = SY.UKETUKE_NO
           AND UM.SYURITAISYO_SEQ = SY.SYURITAISYO_SEQ
           AND SY.CE_CD = CE.CE_CD(+)
           AND SY.MIKANRIYU_CD = MC.KANMI_CD(+)
      /* 	2016/12/05 MOD START		*/
      /* 	     AND ((SUBSTR(SY.MIKANRIYU_CD,1,1) <> '4' AND SY.MIKANRIYU_CD <> '3A' )
           AND ((SUBSTR(SY.MIKANRIYU_CD,1,1) <> '4' AND SY.MIKANRIYU_CD  NOT IN ('3A','1H','1J','2J') )
                OR SY.MIKANRIYU_CD IS NULL)
      /* 	2016/12/05 MOD END		*/
           AND SY.SYURI_JISEKIBI IS NOT NULL
       ORDER BY
           MEISAI_SEQ,
           HOMONBI DESC,
           JISIRIREKI_SEQ DESC
SQL;
    return $sql;
  }

  public function getMeisaiNaiyo($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_MEISAI2()),$search);
  }

  private function _SQL_SELECT_COMMAND_MEISAI2()
  {
    $sql = <<<SQL
      SELECT
           UM.SYURITAISYO_SEQ MEISAI_SEQ,
           UM.KOZITEN KOZITEN,
           NVL(DECODE(UM.HINMEI, NULL, UM.SYOHIN_HINBAN, UM.HINMEI),' ')
           HINBAN,
           NVL(UM.IRAINAIYO,' ')        IRAINAIYO,
           TO_CHAR(SY.SYURI_JISEKIBI,'YYYY/MM/DD')   HOMONBI,
           NVL(CE.CE_SEI, ' ') CE_SEI,
           SY.YUMURYO_KBN      YUMURYO_KBN,
           SY.MIKANRIYU_CD     MIKANRIYU_CD,
           NVL(MC.KANMI_HOKOKU_BUNSYO, ' ')  MIKAN_RIYU
           /*2018.12.06 ADD START	グローエ、災害時特別対応*/
           ,case  when sk.seikyusaki_simei like 'グロ_エ％' then '無償対応'
           when sk.seikyusaki_cd IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') then (SELECT REPLACE(REPLACE(TOKUBETU_TOKUISAKI.FAX3_KANMI_BUNSYO,'（',''),'）','') FROM TOKUBETU_TOKUISAKI WHERE TOKUBETU_TOKUISAKI.TOKUISAKI_CD = sk.seikyusaki_cd)
           else '0' end hisai_kb /*38.被災区分*/
           /*2018.12.06 ADD END	グローエ、災害時特別対応*/           
       FROM
           UKETUKE_MEISAI  UM,
           SAGYOYOTEI      SY,
           KANMI_CODE      MC,
           SYURITANTOSYA CE
           /*2018.12.06 ADD START	グローエ、災害時特別対応*/
           ,( select denpyo_no , edaban , edaban_max_flg from uriage_denpyo union select denpyo_no , edaban , edaban_max_flg from hokokusyo_nyuryoku )  h
           ,( select denpyo_no , edaban , seikyusaki_cd , seikyusaki_simei from uriage_seikyusaki union select denpyo_no , edaban , seikyusaki_cd , seikyusaki_simei from nyuryoku_seikyusaki ) sk
           /*2018.12.06 ADD END	グローエ、災害時特別対応*/
       WHERE
           UM.UC_CD = :uc_cd
           AND UM.UKETUKE_NO = :uketuke_no
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応 */
           AND UM.UKETUKE_NENDO = :uketuke_nendo
           AND UM.UKETUKE_NENDO = SY.UKETUKE_NENDO
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応 */
           AND UM.UC_CD = SY.UC_CD
           AND UM.UKETUKE_NO = SY.UKETUKE_NO
           AND UM.SYURITAISYO_SEQ = SY.SYURITAISYO_SEQ
           AND SY.CE_CD = CE.CE_CD(+)
           AND SY.MIKANRIYU_CD = MC.KANMI_CD(+)
      /*		2016/12/05 MOD START	*/

           AND ((SUBSTR(SY.MIKANRIYU_CD,1,1) <> '4' AND SY.MIKANRIYU_CD  NOT IN ('3A','1H','1J','2J') )
      /*		2016/12/05 MOD END*/
                OR SY.MIKANRIYU_CD IS NULL)
           AND SY.SYURI_JISEKIBI IS NOT NULL
      /*2018.12.06 ADD START	グローエ、災害時特別対応*/
           AND h.denpyo_no(+) = sy.ce_cd || sy.hokokusyo_no
	         AND h.edaban_max_flg(+) = '1'
	         AND sk.denpyo_no(+) = h.denpyo_no
		       AND sk.edaban(+) = h.edaban
      /*2018.12.06 ADD END	グローエ、災害時特別対応*/
       ORDER BY
           MEISAI_SEQ,
           HOMONBI DESC,
           JISIRIREKI_SEQ DESC
SQL;
    return $sql;
  }

  public function getKakuhouPrint($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_KAKUHOU_PRINT()),$search);
  }

  private function _SQL_SELECT_COMMAND_KAKUHOU_PRINT()
  {
    $sql = <<< SQL
      SELECT
           TO_CHAR(IU.UKETUKEBI,'YYYY/MM/DD') UKETUKEBI,/*1.登録日*/
           iu.uketuke_no           uketuke_no,          /*2.受付no*/
           h.ce_cd                 ce_cd,               /*3.担当者(コード)*/
           ce.ce_sei               ce_sei,              /*4.担当者(姓)*/
           TO_CHAR(H.HOMONBI,'YYYY/MM/DD') HOMONBI,     /*5.訪問日*/
       (select s.MIKANRIYU_CD
          from SAGYOYOTEI s
         where s.uketuke_no = h.uketuke_no and s.ce_cd = h.ce_cd and
               s.syuritaisyo_seq = h.UKETUKE_EDABAN and
      /* 2010.5.17 Hitachi MOD START UKETUKE_NENDO追加対応*/
               s.uketuke_nendo = h.uketuke_nendo and
               s.jisirireki_seq in
               (select max(sy.jisirireki_seq)
                  from sagyoyotei sy
                 where sy.uketuke_no = h.uketuke_no and sy.ce_cd = h.ce_cd and
                       sy.uketuke_nendo = h.uketuke_nendo and
                       sy.syuritaisyo_seq = h.UKETUKE_EDABAN)) MIKANRIYU_CD, /*6.未完理由コード*/

           h.yumuryo_kbn           yumuryo_kbn,         /*7.*/
       (select KANMI_HOKOKU_BUNSYO
          from KANMI_CODE
         where KANMI_CD =
               (select s.MIKANRIYU_CD
                  from SAGYOYOTEI s
                 where s.uketuke_no = h.uketuke_no and s.ce_cd = h.ce_cd and
                       s.syuritaisyo_seq = h.UKETUKE_EDABAN and
      /* 2010.5.17 Hitachi MOD START UKETUKE_NENDO追加対応*/
                       s.uketuke_nendo = h.uketuke_nendo and
                       s.jisirireki_seq in
                       (select max(sy.jisirireki_seq)
                          from sagyoyotei sy
                         where sy.uketuke_no = h.uketuke_no and sy.ce_cd = h.ce_cd and
                               sy.uketuke_nendo = h.uketuke_nendo and
                               sy.syuritaisyo_seq = h.UKETUKE_EDABAN))) KANMI_HOKOKU_BUNSYO, /*8.*/

      /* 2010.5.17 Hitachi MOD END UKETUKE_NENDO追加対応*/
           k.kokyaku_sei           kokyaku_sei,         /*9.お客様名*/
           k.kokyaku_na            kokyaku_na,          /*10.お客様名*/
           k.kokyaku_jusyo         kokyaku_jusyo,       /*11.お客様住所*/
           k.kokyaku_katagaki      kokyaku_katagaki,    /*12.マンション名*/
           k.kokyaku_tel_haihun    kokyaku_tel,         /*13.お客様TEL*/
           '' hinmei,                                   /*14.商品名ダミー*/
           '' hinban,                                   /*15.品番ダミー*/
      /* 2010.5.17 Hitachi MOD START UKETUKE_NENDO追加対応*/
           (select max(mm.irainaiyo) from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd and mm.uketuke_nendo = h.uketuke_nendo) irainaiyo,     /*16.ご依頼内容*/
      /*     (select max(mm.irainaiyo) from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd) irainaiyo,     /*16.ご依頼内容*/
      /* 2010.5.17 Hitachi MOD END UKETUKE_NENDO追加対応*/
           ir.tyumon_bango         tyumon_bango,/*17.注文番号*/
           DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_simei,tk.tokuisaki_sei)   iraimoto_simei,/*18.ご依頼元氏名*/
           ir.iraimoto_tanto       iraimoto_tanto,     /*19.ご担当者様(依頼元)*/
           ir.iraimoto_jusyo       iraimoto_jusyo,     /*20.ご依頼元住所*/
           DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_tel_haihun,tk.tokuisaki_tel_haihun)  iraimoto_tel,/*21.ご依頼元TEL*/
           DECODE(ir.iraimoto_cd,'99999',ir.iraimoto_fax,tk.torokusaki_fax)     iraimoto_fax,          /*22.ご依頼元FAX*/
           sk.seikyusaki_cd        seikyusaki_cd,            /*23.得意先コード(請求先)*/
           sk.seikyusaki_simei     seikyusaki_simei,         /*24.ご依頼時請求先名*/
           sk.seikyusaki_tanto     seikyusaki_tanto,         /*25.ご担当者様(請求先)*/
           sk.seikyusaki_jusyo     seikyusaki_jusyo,         /*26.ご依頼時請求先住所*/
           sk.seikyusaki_tel_haihun     seikyusaki_tel,      /*27.ご依頼時請求先TEL*/
           SK.SEIKYUSAKI_FAX           SEIKYUSAKI_FAX,       /*28.ご依頼時請求先FAX*/
      /* 2010.5.17 Hitachi MOD START UKETUKE_NENDO追加対応*/
           (select max(mm.koziten) from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd and mm.uketuke_nendo = h.uketuke_nendo) koziten,/*29.工事店*/
      /*	       (select max(mm.koziten) from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd) koziten,/*29.工事店*/
      /* 2010.5.17 Hitachi MOD END UKETUKE_NENDO追加対応*/
           tk2.fax1_5_tyokusyu_flg,               /*30.*/
           uk.hm_kokyaku_no        hm_kokyaku_no, /*31.お得意様顧客Ｎｏ*/
           toi.toiawase_name       toiawase_name, /*32.お問合せ先*/
           DECODE(SUBSTRB(TOI.TOIAWASE_TEL,1 ,4),'0570',('ﾅﾋﾞﾀﾞｲﾔﾙ ' ||TOI.TOIAWASE_TEL),DECODE(TOI.TOIAWASE_TEL,'','',('TEL ' || TOI.TOIAWASE_TEL)) ) TOIAWASE_TEL1, /*33.お問合せ先ＴＥＬ*/
           DECODE(SUBSTRB(TOI.TOIAWASE_TEL,1 ,4),'0570',(SELECT '（電話   ' || UC_TEL || '）' FROM UKETUKECENTER WHERE UC_CD=IU.UC_CD ),'') TOIAWASE_TEL2 ,           /*34.お問合せ先ＦＡＸ*/
           h.nyukin_kbn            nyukin_kbn,        /*35.*/
           TO_CHAR(SYSDATE,'YYYY/MM/DD')  HAKKOUBI,   /*36.発行日*/
           H.KAISI_JIKOKU  			  KAISI_JIKOKU,  /*37.開始時刻*/
           DECODE((select nvl(min(mm.syutanto_kbn),'2')      from uketuke_meisai mm
                   where mm.uketuke_no = h.uketuke_no
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                     and mm.uketuke_nendo = h.uketuke_nendo
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
                     and mm.ce_cd = h.ce_cd),'1',
                                                  SUBSTR(SYANAI_KIJI,1,
                                                                     DECODE( INSTR(SYANAI_KIJI,'ご入会'), 0, LENGTH(SYANAI_KIJI),
                                                                                                             INSTR(SYANAI_KIJI,'ご入会')-1
                                                                           )
                                                        )
                                        ,'') sagyo_naiyo,/*38.お客様記事*/
           SEIKYUSAKI_TYUMON_NO,/* 40.請求先注文番号*/
           SK.TYUMON_BANGO          　　　HOKOKUSYO_TYUMON_BANGO,/* 42.報告書請求先注文番号*/
      /* 2010.5.17 Hitachi MOD START UKETUKE_NENDO追加対応*/
          (select nvl(min(mm.syutanto_kbn),'2') from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd and mm.uketuke_nendo = h.uketuke_nendo) syutanto_kbn
          ,iu.uketuke_nendo           uketuke_nendo           /*43.受付年度*/
      /*	      (select nvl(min(mm.syutanto_kbn),'2') from uketuke_meisai mm where mm.uketuke_no = h.uketuke_no and mm.ce_cd = h.ce_cd) syutanto_kbn
      /* 2010.5.17 Hitachi MOD END UKETUKE_NENDO追加対応*/
      /*2011.7.25 MOD START  表示の優先順位指定 　*/
      /*　1:有料 2:無料*/
         ,DECODE(h.yumuryo_kbn, 'A',
                 CASE WHEN  ir.iraimoto_cd <> '93041'   AND sk.seikyusaki_cd = '93041'   THEN '2'
                      WHEN  ir.iraimoto_cd <> '10995'   AND sk.seikyusaki_cd = '10995'   THEN '2'
         /*2018.12.06 ADD START	グローエ、災害時特別対応*/
		     WHEN  sk.seikyusaki_simei like 'グロ_エ％' THEN '2'
	       WHEN  sk.seikyusaki_cd IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') THEN '2'
         /*2018.12.06 ADD END*/
                      ELSE  '1'
                 END,
                 '2'           )  sort_kbn1,
      /*　1:依頼元請求 2:施主請求	 */
          DECODE(h.yumuryo_kbn, 'A',
                 CASE WHEN  DECODE(sk.seikyusaki_cd, '99999', sk.seikyusaki_tel, tk2.tokuisaki_tel) =
                            DECODE(ir.iraimoto_cd,   '99999', ir.iraimoto_tel,   tk.tokuisaki_tel)      THEN '1'
                      WHEN  DECODE(sk.seikyusaki_cd, '99999', sk.seikyusaki_tel, tk2.tokuisaki_tel) =
                            k.kokyaku_tel      THEN '2'
                      ELSE  '8'
                 END,
                 '9' )  sort_kbn2
      /* 2011.7.25 MOD END   表示の優先順位指定*/
           ,DECODE(TOI.TOIAWASE_FAX,'','','FAX ' || TOI.TOIAWASE_FAX) AS toiawase_fax 		/* 46.お問合せ先ＦＡＸ*/
      /*2016/12/05 ADD START*/
           ,h.IRAIMOTO_KIJI 		/* 47.依頼元記事*/
      /*2016/12/05 ADD END*/

      /*2018.12.06 ADD START	//グローエ、災害時特別対応 */
	   ,case  when sk.seikyusaki_simei like 'グロ_エ％' then '無償対応'
	    when sk.seikyusaki_cd IN (SELECT TOKUBETU_TOKUISAKI.TOKUISAKI_CD FROM TOKUBETU_TOKUISAKI WHERE SAKUJO_FLG = '0') then (SELECT REPLACE(REPLACE(TOKUBETU_TOKUISAKI.FAX3_KANMI_BUNSYO,'（',''),'）','') FROM TOKUBETU_TOKUISAKI WHERE TOKUBETU_TOKUISAKI.TOKUISAKI_CD = sk.seikyusaki_cd)
	    else '0' end hisai_kb/*48.被災区分*/
      /*	2018.12.06 ADD END */
       FROM
           eigyosyo                ei,
           sc                      sc,
           syuritantosya           ce,
           toiawase_saki           toi,
           uriage_denpyo        h,
           uriage_seikyusaki    sk,
           uriage_kokyaku       k,
           imt_hinban              hm,
           imt_syuyaku_hinban      shm,
           uketuke_iraimoto        ir,
           tokuisaki               tk,
           tokuisaki               tk2,
           (
               SELECT DISTINCT
                   s_iu.uc_cd          uc_cd,
                   s_iu.uketuke_no     uketuke_no,
                   s_iu.eigyosyo_cd    eigyosyo_cd,
                   s_iu.sc_cd          sc_cd,
                   s_iu.uketukebi      uketukebi,
                   s_iu.message        message,
                   s_iu.kanryo_flg     kanryo_flg,
                   s_iu.fax5_date      fax5_date,
                   s_iu.torokusya_cd   torokusya_cd
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                   ,s_iu.uketuke_nendo uketuke_nendo
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
               FROM
                   iraiuketuke         s_iu,
                   uriage_denpyo       s_h

               WHERE
                       s_iu.uketuke_no = s_h.uketuke_no
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                   AND s_iu.uketuke_nendo = s_h.uketuke_nendo
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
                   AND s_iu.sakujo_flg = '0'
                   AND s_iu.KANRYO_FLG in ('9' , '8')
                   AND s_h.kanryo_kbn = '1'
                   AND s_h.akakuro_kbn = 'B'
                   AND NOT (s_h.denpyo_no LIKE '____T____'
                         OR s_h.denpyo_no LIKE 'TE%'
                         OR s_h.denpyo_no LIKE 'K%')
                   AND s_h.sakujo_flg = '0'
                   AND s_iu.uc_cd = :uc_cd
                   AND s_h.edaban_max_flg = '1'
                   AND s_iu.uketuke_no =  :uketuke_no
               ) iu,
           uketuke_kokyaku uk,
           web_uketuke wu
       WHERE
      /*2016/12/05 ADD START	  'テスト環境でURIAGE_DENPYOがフルスキャンになる為追加*/
               iu.uc_cd = :uc_cd
           AND iu.uketuke_no =  :uketuke_no AND
      /*2016/12/05 ADD END	*/
               iu.eigyosyo_cd = ei.eigyosyo_cd
           AND iu.eigyosyo_cd = sc.eigyosyo_cd AND iu.sc_cd = sc.sc_cd
           AND iu.uc_cd = ir.uc_cd AND iu.uketuke_no = ir.uketuke_no
           AND iu.uc_cd = uk.uc_cd AND iu.uketuke_no = uk.uketuke_no
           AND ir.iraimoto_cd = tk.tokuisaki_cd(+)
           AND sk.seikyusaki_cd = tk2.tokuisaki_cd(+) AND  tk2.sakujo_flg(+) = '0'
           AND iu.uketuke_no = h.uketuke_no
           AND h.denpyo_no = k.denpyo_no(+) AND h.denpyo_no = sk.denpyo_no(+)
           AND h.edaban = k.edaban(+) AND h.edaban = sk.edaban(+)
           AND h.edaban_max_flg = '1'
           AND toi.fax_id = '5' AND toi.uc_cd = iu.uc_cd
           AND toi.eigyosyo_cd = iu.eigyosyo_cd AND toi.sc_cd = iu.sc_cd
           AND h.a_hinmoku = hm.hinmoku(+) AND h.a_hinban = hm.hinban(+)
           AND hm.hinmoku = shm.hinmoku(+) AND hm.syuyaku_hinban = shm.syuyaku_hinban(+)
           AND h.ce_cd = ce.ce_cd(+)
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
           AND iu.uketuke_nendo = ir.uketuke_nendo
           AND iu.uketuke_nendo = uk.uketuke_nendo
           AND iu.uketuke_nendo = h.uketuke_nendo
           AND iu.uketuke_nendo = wu.uketuke_nendo(+)
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
           AND h.kanryo_kbn = '1'
           AND h.akakuro_kbn = 'B' AND NVL(h.sekinin_kbn,' ') <> 'Q'
           AND NOT EXISTS(SELECT  'X' FROM uriage_buhin_meisai hm
                           WHERE  h.denpyo_no   = hm.denpyo_no
                             AND  h.edaban      = hm.edaban
                             AND  ((hm.service_cd = 'RSSB'
                                   AND  hm.hinmoku    = 'KK'
                                   AND  hm.gyoban = 1)
                                   OR (hm.service_cd = '809C'))
                         )
           AND NOT EXISTS(SELECT  'X' FROM uriage_tyosei_meisai tm
                          WHERE  h.denpyo_no   = tm.denpyo_no
                             AND  h.edaban      = tm.edaban
                            AND  tm.service_cd    = '809C'
                         )
           AND NOT EXISTS(SELECT  'X' FROM hokokusyo_nyuryoku hn
                           WHERE  hn.uketuke_no = iu.uketuke_no
      /* 2010.5.17 Hitachi ADD START UKETUKE_NENDO追加対応*/
                             AND  hn.uketuke_nendo = iu.uketuke_nendo
      /* 2010.5.17 Hitachi ADD END UKETUKE_NENDO追加対応*/
                             AND  hn.SYONINJOKYO in ('0','1','2','7')
                         )
           AND IU.UKETUKE_NO = wu.UKETUKE_NO(+)
       ORDER BY
           ir.iraimoto_cd,     ir.iraimoto_fax,
      /*2011.7.25 MOD START  表示の優先順位指定 　*/

           iu.uketuke_no,
           sort_kbn1,     sort_kbn2,
      /*2011.7.25 MOD END  表示の優先順位指定 　*/
           h.homonbi   DESC,     syutanto_kbn
SQL;
    return $sql;
  }

  public function getHojoce($search = array())
  {
    return DB::select(DB::raw($this->_SQL_SELECT_COMMAND_HOJOCE()),$search);
  }

  private function _SQL_SELECT_COMMAND_HOJOCE()
  {
    $sql = <<< SQL
      SELECT COUNT(DISTINCT CE_CD) - 1 HOJONINZU
        FROM UKETUKE_MEISAI UM
        WHERE UM.UKETUKE_NO = :uketuke_no
          AND UM.UKETUKE_NENDO = :uketuke_nendo
        AND NOT EXISTS (
            SELECT 'X'
            FROM
               SAGYOYOTEI  SY
            WHERE
              SY.UC_CD           = UM.UC_CD
          AND SY.UKETUKE_NO      = UM.UKETUKE_NO
          AND SY.UKETUKE_NENDO   = UM.UKETUKE_NENDO
          AND SY.SYURITAISYO_SEQ = UM.SYURITAISYO_SEQ
          AND SY.MIKANRIYU_CD IN ('1J','2J')
        )
SQL;
    return $sql;
  }

  /**
   * Get all filename by web_uketuke_no
   */
  public function getAllFiles($webno)
  {
    $files = [];
    $filePath = \Storage::disk('incoming')->url($webno);
    if(\File::exists($filePath)) {
      $filesInFolder = \File::files($filePath);

      foreach($filesInFolder as $path)
      {
          $files[] = pathinfo($path);
      }
    }
    return $files;
  }
}
