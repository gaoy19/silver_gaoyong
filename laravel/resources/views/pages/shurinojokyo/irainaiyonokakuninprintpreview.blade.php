<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>LIXILインターネット修理受付センター | 印刷イメージ（修理依頼依頼内容確認）</title>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <section class="container-silver">
    <!-- * Start section print * -->
    <div class="tabs-content_cf">
      <div class="col-print-btn">
          <input class="btn-print_view" type="button" value="印刷" onclick="window.print()">
			    <input class="btn-print_view close" type="button" value="閉じる" name="close" onclick="window.close();">
      </div>
      <div class="report-title-kakunin">
         <h1>修理依頼依頼内容確認</h1>
      </div>
      <div class="tabs-control-form">
         <div class="tb-responsive">
            <table class="detailList_print" border="0" cellpadding="0" cellspacing="0">
              <thead class="lixil_header">
                  <tr><th class="lixil_th" colspan="2">
                      <div class="lixil_title">お客様情報</div>
                  </th>
              </tr></thead>
              <tbody class="lixil_body">
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客様氏名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま氏名（フリガナ）</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま郵便番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま都道府県</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さまご住所</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お客さま建物名・部屋番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社顧客番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>連絡先名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>連絡先電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>製品ブランド</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
              </tbody><!--/お客さま情報-->
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">修理依頼内容</div>
                  </th>
                </tr>
              </thead>
              <tbody class="lixil_body">
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>商品名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>品番</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>取付年月</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>工事店名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご依頼内容</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>希望日</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご要望または連絡事項</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>添付資料</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                            <div class="get-image">

                            </div>
                          </div>
                      </td>
                  </tr>
              </tbody><!--/修理依頼内容-->
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">ご依頼元情報</div>
                  </th>
                </tr>
              </thead><!--/ご依頼元情報-->
              <tbody class="lixil_body">
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>得意先コード</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>所属部署</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご依頼元担当者様</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>部署住所</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>部署電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>部署 FAX</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>貴社注番</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>FAX 送信</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>メール送信</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
              </tbody><!--/ご依頼元情報-->
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">ご依頼元情報</div>
                  </th>
                </tr>
              </thead><!--/ご依頼元情報-->
              <tbody class="lixil_body">
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先氏名</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先氏名（フリガナ）</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先電話番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先 FAX 番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先郵便番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先都道府県</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先住所</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先建物名・部屋番号</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>ご請求先担当者</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
                  <tr>
                      <th class="labelCol">
                          <div id="col-label"><label>お支払い予定日</label></div>
                      </th>
                      <td class="col-data">
                          <div id="col-result">

                          </div>
                      </td>
                  </tr>
              </tbody><!--/ご依頼元情報-->
            </table><!--/listdetail_print
          </div><!--tb-responsive-->
      </div><!--/tabs-control-form-->
    </div>
  </section>
  <!-- * Start section prind * -->
</body>
</html>
