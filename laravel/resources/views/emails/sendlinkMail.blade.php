お客様名：{{ $custom_name }}様


LIXILインターネット修理受付センターをご利用いただき、誠にありがとう
ございます。

パスワードの再設定依頼がございましたのでご連絡差し上げます。

パスワード再設定URL: {{ $mail_address }}

■このメールは、LIXILインターネット修理受付センターのご利用に際して

登録いただいたメールアドレス宛に送信されるものです。


■なお、このメールはパスワードの配信依頼があった方のみに

送信しております。当メールの内容に心当たりのない方は、

ご面倒ですが以下までお伝えいただけると幸いです。

webinfo-imt@i2.inax.co.jp


_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
お問い合わせ先:
　（株）ＬＩＸＩＬ
　LIXIL修理受付センター

　TEL:0570-011-794
　お問い合わせ対応時間　9:00～19:00


LIXILインターネット修理受付センター：
　https://repair.lixil.co.jp/water/

_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
