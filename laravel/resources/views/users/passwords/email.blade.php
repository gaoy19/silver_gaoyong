@extends('layouts.master')

@section('title','インターネット修理受付センター_パスワードを忘れたかたへ')

@section('content')
<section id="container-login">
    <div class="form-sv">
        <form class="form-horizontal_sv" method="POST" action="{{ route('user_password.email') }}">
            {{ csrf_field() }}
            <div class="modal-header_sv">
                <h3>パスワードの再設定</h3>
            </div>
            <div class="con-group-sv">
                <div class="form-group_sv">
                    <div class="col-sv-12">
                        <div class="role_login">ご登録のメールアドレス宛にパスワードの再設定 URL をお送りいたします</div>
                    </div>
                </div>
                <div class="sv_top30">
                    <div class="form-group_sv sv_bot30">
                      <label class="control-label_sv col-sv-4" for="user_id" style="">ユーザー ID:</label>
                      <div class="col-sv-8">
                        @if ($errors->has('user_id'))
                          <div class="error_sv max-100"><span>{{ $errors->first('user_id') }}</span></div>
                        @endif
                        <input type="text" id="user_id"  name="user_id" value="{{ old('user_id') }}" class="form-control_sv{!! $errors->has('user_id') ? ' input-error' : '' !!}" >
                      </div>
                    </div>
                    <div class="form-group_sv sv_bot30">
                      <label class="control-label_sv col-sv-4" for="password">メールアドレス:</label>
                      <div class="col-sv-8">
                        @if ($errors->has('email'))
                          <div class="error_sv max-100"><span>{{ $errors->first('email') }}</span></div>
                        @endif
                        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control_sv{!! $errors->has('email') ? ' input-error' : '' !!}" >
                      </div>
                    </div>
                    <div class="form-group_sv">
                        <div class="col-sv-12">
                            @if(session('global'))
                              <div class="error_sv" style="margin-top: 7px;"><span>{{session('global')}}</span></div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group_sv sv_top60">
                      <div class="col-sv-12 sv_center">
                        <button type="submit" class="btn btn-sv" style="width: 320px;">パスワード再設定のURLを送信する</button>
                      </div>
                    </div>
                </div>
            </div>
        </form><!--/form-horizontal_sv-->
        <div class="con-model">
            <div class="error_info">
                <div class="error_title"><span class="icon"><img src="/assets/images/error_info.png" /></span>ユーザー ID、メールアドレスを紛失された場合は、下記修理受付センターまで問い合わせください</div>
            </div>
            <div class="contact-sv">
                <div class="contact-title">
                    <h5>お問い合わせ先</h5>
                </div>
                <div class="box-sv-12">
                    <div class="box-sv-title">（株）LIXIL 修理受付センター</div>
                    <div class="mail-sv">
                        <div class="mail_cot">
                            <a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a>
                            <div class="mail_des">メールアドレス：webinfo-imt@i2.inax.co.jp</div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/con-model-->
    </div>
</section><!--/container_login-->
@stop
