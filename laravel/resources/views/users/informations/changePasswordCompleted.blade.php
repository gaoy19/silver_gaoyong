@extends('layouts.master')
@section('title','LIXILインターネット修理受付センター | パスワードの変更[2パスワードの変更完了]')
@section('content')
<!--  * start section container  * -->
    <section class="container-silver">
        <div class="row-sv">
            <!-- * start row top * -->
            <div class="main-vs-12" style="margin-top: 12px;">
                <div class="box-title_sv">
                    <h1>パスワードの変更</h1>
                </div>
            </div>
        </div><!--/row-sv-->
        <div class="row-sv">
            <div class="col-sv-12 box-progress-12">
                <ul class="row-box-bar">
                    <li class="box-sv-6 active_sv" style="padding-right: 20px;">
                        <div class="box-progress-bar">
                            <div class="box-num-bar">1</div>
                            <div class="box-text-bar">パスワードの変更</div>
                        </div>
                    </li><!--/box-sv-6-->
                    <li class="box-sv-6 active_sv" style="padding-left: 20px;">
                        <div class="box-progress-bar">
                            <div class="box-num-bar">2</div>
                            <div class="box-text-bar">パスワードの変更完了</div>
                        </div>
                    </li><!--/box-sv-6-->
                </ul><!--/box-bar-->
            </div><!--/box-progress-12-->
        </div><!--/row-sv-->
        <div class="row-sv">
            <div class="main-vs-12 sv_top20" style="padding-bottom: 50px;">
                <div class="box-main-form">
                    <div class="con-info_sv" style="padding-top: 2px;">
                        <div class="con-info-detail">
                            <p>パスワードの変更が完了しました。パスワード変更の連絡をご登録の連絡先メールアドレスに送信しています。</p>
                            <p>ユーザー ID と設定したパスワードでログインし、インターネット修理受付センターをご利用ください</p>
                        </div>
                    </div><!--con-info_sv-->
                </div><!--/box-main-form-->
            </div>
        </div><!--/row-sv-->
    </section><!--/container-silver-->
    <!--  * end section container  * -->
@stop
