<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*----------------------------------------------------
* Start Group Route middleware auth_user
-------------------------------------------------------*/
Route::get('/', function () {
	return redirect()->route('login');
});
Route::group(['prefix' => 'water', 'middleware' => ['auth', 'login_timeout']], function() {
	/*------------------------
	* Route トップページ home page
	--------------------------*/
	Route::get('/','HomesController@index')->name('home');

	Route::post('/iraimoto/submitback','IraimotoController@submitBack')->name('iraimoto.submitBack');
	/*------------------------
	* Route 4.1 お客さま情報の入力
	--------------------------*/
	Route::get('/iraimoto/kokyaku','IraimotoController@showKokyaku')->name('iraimoto.showKokyaku');
	Route::post('/iraimoto/kokyaku','IraimotoController@kokyaku')->name('iraimoto.kokyaku');
	
	Route::post('/iraimoto/reset','IraimotoController@reset')->name('iraimoto.reset');
	/*------------------------
	* Route 4.2 修理依頼内容の入力
	--------------------------*/
	Route::get('/iraimoto/naiyou','IraimotoController@showNaiyou')->name('iraimoto.showNaiyou');
	Route::post('/iraimoto/naiyou','IraimotoController@naiyou')->name('iraimoto.naiyou');
	Route::post('/iraimoto/naiyou/clearfile','IraimotoController@clearFile')->name('iraimoto.clearfile');
	/*------------------------
	* Route 4.3 ご依頼元情報の入力
	--------------------------*/
	Route::get('/iraimoto/jouhou','IraimotoController@showJouhou')->name('iraimoto.showJouhou');
	Route::post('/iraimoto/jouhou','IraimotoController@jouhou')->name('iraimoto.jouhou');
	/*------------------------
	* Route 4.4 ご依頼元情報の入力
	--------------------------*/
	Route::get('/iraimoto/goseikyusaki','IraimotoController@showGoseikyusaki')->name('iraimoto.showGoseikyusaki');
	Route::post('/iraimoto/goseikyusaki','IraimotoController@goseikyusaki')->name('iraimoto.goseikyusaki');
	/*------------------------
	* Route 4.5 ご依頼内容の確認
	--------------------------*/
	Route::get('/iraimoto/confirmation','IraimotoController@showConfirmation')->name('iraimoto.showConfirmation');
	Route::post('/iraimoto/confirmation','IraimotoController@confirmation')->name('iraimoto.confirmation');
	/*------------------------
	* Route 4.6 修理のご依頼完了
	--------------------------*/
	Route::get('/iraimoto/completation','IraimotoController@showCompletation')->name('iraimoto.showCompletation');
	Route::post('/iraimoto/completation','IraimotoController@completation')->name('iraimoto.completation');
	/*------------------------
	* Route 4-7 印刷イメージ（修理依頼依頼内容確認）
	--------------------------*/
	Route::get('/iraimoto/printpreview','IraimotoController@showPrintpreview')->name('iraimoto.showPrintpreview');


	Route::post('/json/seihinbetu','WbSmtSeihinbetuCdController@seihinbetu')->name('json.seihinbetu');

	/*------------------------
	* Route 5-1 修理の状況
	--------------------------*/
	Route::get('/webuketuke','WebUketukeController@index')->name('webuketuke.index');
	/*------------------------
	* Route 5-2 依頼内容の確認
	--------------------------*/
    Route::get('/webuketuke/kakunin/{webno}','WebUketukeController@kakunin')->name('webuketuke.kakunin');

    Route::get('/iraimoto/confirmation/{webno}', 'IraimotoController@copyIraiData')->name('iraimoto.copyIraiData');

	Route::get('/webuketuke/detail/{webno}','WebUketukeController@detail')->name('webuketuke.detail');

	Route::get('/shurinojokyo/irainaiyonokakunin','ShurinojokyoController@showIrainaiyonokakunin')->name('shurinojokyo.irainaiyonokakunin');

	Route::get('/webuketuke/detail/sokuhou/print/{webno}','WebUketukeController@sokuhouPrint')->name('webuketuke.sokuhou.print');

	Route::get('/webuketuke/detail/kakuhou/print/{webno}','WebUketukeController@kakuhouPrint')->name('webuketuke.kakuhou.print');
	/*------------------------
	* Route 5-3 修理依頼依頼内容確認
	--------------------------*/
    Route::get('/webuketuke/kakunin/print/{webno}','WebUketukeController@kakuninPrint')->name('webuketuke.kakunin.print');
	Route::get('/shurinojokyo/irainaiyonokakuninprintpreview','ShurinojokyoController@showIrainaiyonokakuninPrintpreview')->name('shurinojokyo.irainaiyonokakuninprintpreview');
	/*------------------------
	* Route 5-4 訪問予定報告
	--------------------------*/
	Route::get('/shurinojokyo/homonyoteihokoku','SShurinojokyoController@showHomonyoteihokoku')->name('shurinojokyo.homonyoteihokoku');
	/*------------------------
	* Route 5-6 速報
	--------------------------*/
	Route::get('/shurinojokyo/subao','ShurinojokyoController@showSubao')->name('shurinojokyo.subao');
	/*------------------------
	* Route 5-7 ご依頼中の修理の状況[速報]
	--------------------------*/
	Route::get('/shurinojokyo/preliminaryreport','ShurinojokyoController@showPreliminaryreport')->name('shurinojokyo.preliminaryreport');
	/*------------------------
	* Route 5-8 ご依頼中の修理の状況[確報]
	--------------------------*/
	Route::get('/shurinojokyo/kakuho','ShurinojokyoController@showKakuho')->name('shurinojokyo.kakuho');
	/*------------------------
	* Route 5-9 印刷イメージ（確報）
	--------------------------*/
	Route::get('/shurinojokyo/finalreport','ShurinojokyoController@showFinalreport')->name('shurinojokyo.finalreport');
	/*------------------------
	* Route 6-1_インターネット修理受付センター_修理データの条件指定検索[トップ①]
	--------------------------*/
	Route::get('/webuketuke/search','WebUketukeController@showSearch')->name('webuketuke.showSearch');
	Route::get('/webuketuke/search/kekka','WebUketukeController@search')->name('webuketuke.search');

	/*------------------------
	* Route 9-1_インターネット修理受付センター_利用者情報の変更
	--------------------------*/
	Route::get('/user/change','UserInformation\ChangeController@index')->name('user.change.index');
	/*------------------------
	* Route 9-5_インターネット修理受付センター_パスワードの変更
	--------------------------*/
	Route::get('/user/change/password','UserInformation\PasswordController@changePassword')->name('user.change.password');
	Route::post('/user/change/password', 'UserInformation\PasswordController@updatePassword');
	/*------------------------
	* Route 9-2_インターネット修理受付センター_パスワードの変更
	--------------------------*/
	Route::get('/user/change/information','UserInformation\InformationController@changeInformation')->name('user.change.information');
	/*------------------------
	* Route 9-2'_インターネット修理受付センター_パスワードの変更
	--------------------------*/
	Route::post('/user/change/information','UserInformation\InformationController@confirmInformation')->name('user.change.information.confirmation');
	/*------------------------
	* Route 9-3_インターネット修理受付センター_パスワードの変更
	--------------------------*/
	Route::get('/user/change/information/completed','UserInformation\InformationController@completedInformation')->name('user.change.information.completed');
	Route::post('/user/change/information/completed','UserInformation\InformationController@completeInformation');
});
/*-----------------------------------------------------
* End Group Route middleware auth_user
-------------------------------------------------------*/
Route::group(['prefix' => 'water'], function() {
/*------------------------
* login lixil silver
--------------------------*/
Route::get('/login','UsersController@Login')->name('login');
//SSO AUTH with Alphas
Route::get('/login/alphas', 'UsersController@Login')->middleware('sso_auth_alphas');
Route::post('/login/alphas', 'UsersController@Login')->middleware('sso_auth_alphas');
/*
* post sign in
*/
Route::post('/login','UsersController@postLogin')->name('login.post');
Route::post('logout','UsersController@logout')->name('logout');
/*------------------------
*	End login
-------------------------*/
/*------------------------
*  Start Fogot password
--------------------------*/
/*------------------------
* Route 2-1 パスワードを忘れたかたへ
--------------------------*/
Route::get('user/password/reset', 'rcpUser\ForgotPasswordController@showLinkRequestForm')->name('user_password.reset.request');
/*------------------------
* Route 2-2 パスワードを忘れたかたへ
--------------------------*/
Route::post('user/password/email', 'rcpUser\ForgotPasswordController@sendResetLinkToEmail')->name('user_password.email');
/*------------------------
* Route 2-3 メール送信（URL通知）
--------------------------*/
Route::get('user/password/reset/{token}', 'rcpUser\ResetPasswordController@showResetForm')->name('user_password.reset');
/*------------------------
* Route 2-4 パスワード再設定メール送信完了
--------------------------*/
Route::get('user/password/sendcomplete', 'rcpUser\ForgotPasswordController@showsendComplete')->name('user_password.sendcomplete');
/*------------------------
* Route 2-5 パスワード再設定
--------------------------*/
Route::post('user/password/reset', 'rcpUser\ResetPasswordController@resetPassword')->name('user_password.resetpassword');
/*------------------------
* Route 2-6 パスワード再設定完了
--------------------------*/
Route::get('user/register/resetcomplete', 'rcpUser\ResetPasswordController@showresetComplete')->name('user_password.resetcomplete');
/*------------------------
*  Start Register password
--------------------------*/
/*------------------------
* Route 7-1 利用者登録
--------------------------*/
Route::get('user/register', 'rcpUser\RegisterController@showRegister')->name('user.register');
/*------------------------
* Route 7-2 登録内容の確認
--------------------------*/
Route::post('user/register/confirmation', 'rcpUser\RegisterController@confirmForm')->name('user.register.confirm');
/*------------------------
* Route 7-4 利用者登録完了
--------------------------*/
Route::post('user/register/completation', 'rcpUser\RegisterController@register')->name('user.register.post');
/*------------------------
*  End Register password
--------------------------*/
Route::get('/yubinbango/search','YubinBangoController@search')->name('yubinbango.search');

Route::post('/json/yubinbango','YubinBangoController@yubinbango')->name('json.yubinbango');

Route::post('/json/brand','BrandController@brand')->name('json.brand');

/*------------------------
*  Start Register password
--------------------------*/
/*------------------------
* Route 9-1 自動ログイン登録
--------------------------*/
//Route::get('user/autologinregister/register', 'rcpUser\AutoLoginRegisterController@showRegister')->name('user.autologinregister');
Route::get('user/autologinregister/register', 'rcpUser\AutoLoginRegisterController@showRegister')->name('user.autologinregister');
/*------------------------
* Route 9-2 自動ログイン確認
--------------------------*/
//Route::post('user/autologinregister/confirmation', 'rcpUser\AutoLoginRegisterController@confirmForm')->name('user.autologinregister.confirm');
/*------------------------
* Route 9-4 自動ログイン登録完了
--------------------------*/
//Route::post('user/autologinregister/completation', 'rcpUser\AutoLoginRegisterController@register')->name('user.autologinregister.post');
Route::post('user/autologinregister/completation', 'rcpUser\AutoLoginRegisterController@register')->name('user.autologinregister.post');
/*------------------------
*  End Register password
--------------------------*/

/*------------------------
* Route 3.2 仕様不可文字
--------------------------*/
Route::get('/shiyofukamoji',['as' => 'shiyofukamoji','uses' => 'HomesController@showShiyofukamoji']);

/*------------------------
* 管理用
--------------------------*/
Route::get('/convertoldpassword', 'rcpUser\ConvertOldPasswordController@convertOldPassword');
});
