<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\YubinBango;
use App\RequestData;
use Session;
use Redirect;
use Validator;
use App\Custom\CharReplace;

class YubinBangoController extends Controller
{
  private $_model;

  public function __construct(YubinBango $model)
  {
    $this->_model = $model;
  }

  public function yubinbango(Request $request)
  {
    $yubinbango = CharReplace::hyphenZentoHan($request->okyakusamaYuubinNo);
    $yubinbango = CharReplace::replaceNumber($request->okyakusamaYuubinNo);
    $yubinbango = str_replace('-','',$yubinbango);
    $data = $this->_model->getJson($yubinbango);
    return response()->json(isset($data[0])?$data[0]:$data);
  }

  public function search(Request $request)
  {
    $input = $request->all();
    $input['yubinBango_val'] = isset($input['yubinBango_val']) ? CharReplace::replaceNumber($input['yubinBango_val']):'';

    if (Session::token() == $request->_token && isset($request->form) && $request->form){
      $yubins = $this->_model->search($input);
      $input['yubinBango_val'] = CharReplace::replaceBackNumber($request->yubinBango_val);
      return view('pages.yubinbango.search',compact('input','yubins'));
    } else { return abort(500); }
    $input['yubinBango_val'] = CharReplace::replaceBackNumber($request->yubinBango_val);
    return view('pages.yubinbango.search',compact('input'));
  }
}
