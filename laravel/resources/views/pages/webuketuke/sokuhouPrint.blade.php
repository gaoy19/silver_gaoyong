<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>LIXILインターネット修理受付センター | ご依頼中の修理の状況[速報]</title>
<style>
html,body{font-size: 13px;font-weight: 500;background: #fff !important;}
table{font-size: 13px;}
.table-report-no th.col-ret-title{
  width: 165px;
}
</style>
<style media="print">
.table-report-no th.col-ret-title {
  width: 150px;
}
.tb-descritoion p{
  font-size: 12px;
}
.col-sv-8 {
    width: 66.66666667%;
}
.col-sv-4 {
    width: 33.333333%;
}
.report-dear {
  width: 220px;
}
.report-dear span.dear_set{
  width: 180px;
}
</style>
<style media="screen">
  .title-soku1{
    width: 15%;
  }
  .col-sv-4 {
    width: 25%;
  }
  .col-sv-8 {
      width: 75%;
  }
</style>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <!-- * Start section print * -->
  <section class="container-silver">
    <div class="content-report">
      <div class="col-print-btn">
        <ul class="col-nav-bt">
            <li><a href="javascript:void(0)" onclick="window.print()"><span class="icon icon_user"><img src="/assets/images/print.png"></span>印刷</a></li>
            <li class="logout_sv">
             <a href="javascript:void(0)" onclick="window.close();" style="text-align: center;">閉じる</a>
           </li>
        </ul>
      </div><!--/col-print-btn-->
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="report-title">
            <span><h1>結果報告書（速報）</h1></span>
            <span class="report-date">発行日<span>{{ $sokuhou->hakkoubi }}</span></span>
          </div>
        </div>
      </div>
      <div class="row-sv">
        <div class="col-sv-8">
          <div class="report-note">
            <div class="report-id">コード<span>{{ $sokuhou->iraimoto_cd }}</span></div>
            <div class="report-dear"><span class="dear_set">{{ $tokuisaki->tokuisaki_mei }}</span><span>御中</span></div>
            @if($sokuhou->iraimoto_tanto !=null || $sokuhou->iraimoto_tanto != "")
            <div class="report-name">
              <span class="name_set">{{ $sokuhou->iraimoto_tanto }}</span>
              <span>@if($sokuhou->iraimoto_tanto !=null || $sokuhou->iraimoto_tanto != "") 様 @endif</span>
            </div>
            @endif         
            <div class="report-des">
              <p>いつもお世話になっております。
                ご依頼いただきました修理について訪問しました状況を、
                下記の通りご連絡申し上げます。</p>
            </div>
          </div>
        </div>
        <div class="col-sv-4">
          <div class="report-logo">
            <img src="/assets/images/lixil-logo-report.png">
          </div>
          <div class="report-contact">
            <div class="report-lixil">{{ \App\Custom\CharReplace::hanKanaTozenKana($sokuhou->toiawase_name) }}</div>
            <div class="report-lixil">{{ \App\Custom\CharReplace::hanKanaTozenKana($sokuhou->toiawase_tel1) }}</div>
            <div class="report-lixil">{{ $sokuhou->toiawase_tel2 }}</div>
            <div class="report-lixil">{{ $sokuhou->toiawase_fax }}</div>
          </div>
          </div>
        </div>
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="center">＜記＞</div>
          <div class="table-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">受付No<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">{{ $sokuhou->uketuke_no }}</td>
                  <th class="col-ret-title">貴社注文番号<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">{{ $sokuhou->tyumon_bango }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">受付年月日<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">{{ $sokuhou->uketukebi }}</td>
                  <th class="col-ret-title">工事店名<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">{{ $sokuhou->koziten }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title">お得意先様顧客No<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">{{ $sokuhou->hm_kokyaku_no }}</td>
                </tr>
              </tbody>
            </table>
          </div><!--table-responsive-->
          <div class="tb-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title ft-bold">
                    <div class="tb-report">
                      お客様住所<span style="text-align:right;">：</span>
                    </div>
                  </th>
                  <td class="ft-bold">{{ $sokuhou->kokyaku_jusyo }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様氏名<span style="text-align:right;">：</span></th>
                  <td class="ft-bold">{{ $sokuhou->kokyaku_mei }}</td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様電話番号<span style="text-align:right;">：</span></th>
                  <td class="ft-bold">{{ $sokuhou->kokyaku_tel }}</td>
                </tr>
              </tbody>
            </table>
          </div><!--/tb-responsive-->
          <div class="tb-responsive" style="margin-top:30px;">
            <table class="table-report-border">
              <thead>
                <tr>
                  <th class="title-soku1">商品品番（名）</th>
                  <th class="title-soku2">ご依頼内容</th>
                  <th class="title-soku3">訪問日</th>
                  <th class="title-soku4">担当者</th>
                  <th class="title-soku5">完・未完の別</th>
                </tr>
              </thead>
              <tbody>
                @foreach($meisai as $value)
                <tr>
                  <td>{{ $value->hinban }}</td>
                  <td>{{ $value->irainaiyo }}</td>
                  <td>{{ $value->homonbi }}</td>
                  <td>{{ $value->ce_sei }}</td>
                  <td>{{ $value->kanmi_hokoku_bunsyo }}<br>({{ $value->yumuryo_kbn }})</td>
                </tr>
                @endforeach

              </tbody>
            </table>
            <div class="tb-descritoion">
              <p>●この報告書は現場担当者からの第１報です。変更内容ありました時はすみやかにご連絡させていただきます。</p>
            </div>
          </div><!--/tb-responsive-->
        </div>
      </div><!--/row-sv-->
    </div><!--/content-report-->
  </section>
  <!-- * Start section prind * -->
</body>
</html>
