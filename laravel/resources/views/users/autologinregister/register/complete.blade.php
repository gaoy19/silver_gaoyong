@extends('layouts.master')

@section('title','LIXILインターネット修理受付センター |  トップページ自動ログイン登録[2トップページ自動ログイン申請受付完了]')

@section('content')
<div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12" style="margin-top: 12px;">
        <div class="box-title_sv">
            <h1>トップページ自動ログイン申請受付完了</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-7-left active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">トップページ自動ログイン登録申請</div>
                </div>
            </li><!--/box-sv-7-left-->
            <li class="box-sv-7-right active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">トップページ自動ログインの申請受付完了</div>
                </div>
            </li><!--/box-sv-7-right-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
    <div class="main-vs-12 sv_top20" style="padding-bottom: 50px;">
        <div class="box-main-form">
            <div class="modal-header_sv">
                <h5>登録受付完了</h5>
            </div>
            <div class="con-info_sv" style="padding-top: 2px;">
                <div class="con-info-detail">
                    <p>トップページ自動ログインの申請を受け付けました。概ね3営業日以内に、LIXIL修理受付センターより、自動ログイン設定完了のご連絡を致します。</p>
                </div>
            </div><!--con-info_sv-->
        </div><!--/box-main-form-->
    </div>
</div><!--/row-sv-->
@stop
