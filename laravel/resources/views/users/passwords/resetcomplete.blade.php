@extends('layouts.masterReset')

@section('title','LIXILインターネット修理受付センター | パスワードを忘れた方へ')

@section('content')
  <div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>パスワードの変更</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-6 active_sv" style="padding-right:20px;">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">パスワードの再設定</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-6 active_sv" style="padding-left:20px;">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">パスワードの再設定</div>
                </div>
            </li><!--/box-sv-3-->

        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
  <div class="main-vs-12 sv_top20">
    <div id="web_customer_info">
        <div class="box-main-form">
            <div class="con-info_sv">
              <div class="con-group-sv">
                @if(isset($status) && $status == 'success')
                <div class="role_login">パスワードの再設定が完了しました。<br/>
ユーザー ID と設定したパスワードでログインし、インターネット修理受付センターをご利用ください</div>
              </div>
                @endif
            </div><!--/con-info_sv-->
        </div><!--/box-main-form-->
    </div><!--/web_customer_info-->
  </div>
</div>
@stop
