  <header id="header_top">
    @if (Auth::check())
    <div class="box-header">
        <div class="row-sv">
            <div class="col-sv-5">
                <div class="logo-top">
                    <div class="logo_sv">
                        <a href="{{URL::to('/')}}"><img src="{{ asset('assets/images/lixil-logo.png') }}" srcset="{{ asset('assets/images/lixil-logo@2x.png') }} 2x,{{ asset('assets/images/lixil-logo@3x.png') }} 3x" alt="" /></a>
                    </div><!--/logo_sv-->
                    <div class="title_sv">インターネット修理受付センター</div>
                </div>
            </div><!--/col-sv-6-->
            <div class="col-sv-7">
                <div class="col-nav-right">
                    <ul class="col-nav-top">
                        <li>貴社名：{{ \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD()->tokuisaki_sei }}</li>
                        <li>得意先コード：{{ Auth::user()->tokuisaki_cd }}</li>
                        <li><a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a></li>
                    </ul>
                    <ul class="col-nav-bt">
                        <li><a href="{{ route('user.change.index') }}"><span class="icon icon_user"><img src="{{ asset('assets/images/icon_user.png') }}" /></span>登録情報変更</a></li>
                        <li class="logout_sv">
                         <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <span class="icon icon_logout"><img src="{{ asset('assets/images/logout.png') }}"></span>ログアウト
                         </a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                         </form>
                       </li>
                    </ul>
                </div>
            </div><!--/col-sv-6-->
        </div><!--/row-sv-->
    </div><!--/box-header-->
    @else
    <div class="header_sv">
        <div class="logo_sv">
              <a href="{{URL::to('/')}}"><img src="{{ asset('assets/images/lixil-logo.png') }}" srcset="{{ asset('assets/images/lixil-logo@2x.png') }} 2x,{{ asset('assets/images/lixil-logo@3x.png') }} 3x" alt="" /></a>
        </div><!--/logo_sv-->
        <div class="title_sv">LIXILインターネット修理受付センター</div>
    </div><!--/header_sv-->
    @endif
  </header><!--/header_top-->
  @if (Auth::check())
  <!-- * start section menu * -->
  @include('includes.menu')
  <!-- * end section menu * -->
  @endif
