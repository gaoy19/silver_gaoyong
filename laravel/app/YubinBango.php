<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class YubinBango extends Model
{
    protected $table = 'YUBIN_BANGO';

    public $timestamps = false;

    protected $primaryKey = 'YUBIN_BANGO';

    protected $fillable = [
      'yubin_bango',
      'TIKU_MEI',
      'KEN_MEI',
      'KEN_MEI_KANA',
      'SITYOSON_MEI',
      'SITYOSON_MEI_KANA',
      'KYORI_SITYOSON_MEI',
      'SC_CD'
    ];

    public function search($input)
    {
      $list = $this->select(
        'YUBIN_BANGO',
        'KEN_MEI',
        'SITYOSON_MEI',
        'OLD_SITYOSON_MEI'
      )->where('SAKUJO_FLG','=','0');
      if(isset($input['yubinBango_val']) && $input['yubinBango_val']){
        $list->where('YUBIN_BANGO','LIKE','%'.str_replace('-','',$input['yubinBango_val']).'%');
      }
      if(isset($input['ken_mei_val']) && $input['ken_mei_val']){
        $list->where('KEN_MEI','LIKE','%'.$input['ken_mei_val'].'%');
      }
      if(isset($input['sityoson_mei']) && $input['sityoson_mei']){
        $list->where(function($query) use ($input){
                return $query->where('SITYOSON_MEI', 'LIKE', '%'.$input['sityoson_mei'].'%')
                    ->orWhere('OLD_SITYOSON_MEI', 'LIKE', '%'.$input['sityoson_mei'].'%');
            });
      }
      $list->orderBy('SITYOSON_MEI_KANA')->limit(100);
      return $list->get();
    }

    public function getJson($yubinbango)
    {
      return DB::select(DB::raw(self::sqlSearchYubinBango()),[$yubinbango]);
    }

    public static function sqlSearchYubinBango()
    {
      $sql = <<<SQL
      SELECT
        SUBSTR(YUBIN_BANGO,1,3) || '-' || SUBSTR(YUBIN_BANGO,4,4) AS YUBIN_BANGO,
        KEN_MEI,
        SITYOSON_MEI,
        OLD_SITYOSON_MEI,
        SC_CD
      FROM
        YUBIN_BANGO
      WHERE
          YUBIN_BANGO = ?
        AND SAKUJO_FLG = '0'
      ORDER BY
        YUBIN_BANGO
SQL;
      return $sql;
    }

    public static function searchByKenmei($kenmei)
    {
      return self::where('KEN_MEI', 'like', $kenmei.'%')
                ->where('SAKUJO_FLG','=','0')
                ->distinct()->get(['SC_CD'])->toArray();
    }

    public static function sqlSearchSCCDKenmei()
    {
      $sql = <<<SQL
      SELECT DISTINCT
        SC_CD
      FROM
        YUBIN_BANGO
      WHERE
          KEN_MEI LIKE '?%'
        AND SAKUJO_FLG = '0'
SQL;
      return $sql;
    }
}
