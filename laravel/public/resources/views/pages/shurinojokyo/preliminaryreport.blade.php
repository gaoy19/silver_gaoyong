<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<title>LIXILインターネット修理受付センター | 修理依頼の状況[結果報告書（速報）]</title>
<style>
html,body{font-size: 14px;font-weight: 500;background: #fff !important;}
table{font-size: 14px;}
</style>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <!-- * Start section print * -->
  <section class="container-silver">
    <div class="content-report">
      <div class="col-print-btn">
        <ul class="col-nav-bt">
            <li><a href="javascript:void(0)" onclick="window.print()"><span class="icon icon_user"><img src="/assets/images/print.png"></span>印刷</a></li>
            <li class="logout_sv">
             <a href="javascript:void(0)" onclick="window.close();">
                <span class="icon icon_logout"><img src="/assets/images/logout.png"></span>閉じる
             </a>
           </li>
        </ul>
      </div><!--/col-print-btn-->
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="report-title">
            <span><h1>結果報告書（速報）</h1></span>
            <span class="report-date">発行日<span>2017/09/01</span></span>
          </div>
        </div>
      </div>
      <div class="row-sv">
        <div class="col-sv-8">
          <div class="report-note">
            <div class="report-id">コード<span>10143</span></div>
            <div class="report-dear"><span class="dear_set"></span><span>御中</span></div>
            <div class="report-name"><span class="name_set"></span><span>様</span></div>
            <div class="report-des">
              <p>いつもお世話になっております。
                ご依頼いただきました修理について訪問しました状況を、
                下記の通りご連絡申し上げます。</p>
            </div>
          </div>
        </div>
        <div class="col-sv-4">
          <div class="report-logo">
            <img src="/assets/images/lixil-logo-report.png">
          </div>
          <div class="report-contact">
            <div class="report-lixil">LIXIL修理受付センター</div>
            <div class="report-lixil">ナビダイアル　0570-01-1794</div>
            <div class="report-lixil">(電話　011-300-51001）</div>
            <div class="report-lixil">FAX　0120-1794-56</div>
          </div>
          </div>
        </div>
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="center">＜記＞</div>
          <div class="table-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">受付No<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">A338-8423</td>
                  <th class="col-ret-title">貴社注文番号<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">受付年月日<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">2017/03/15</td>
                  <th class="col-ret-title">工事店名<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">お得意先様顧客No<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
              </tbody>
            </table>
          </div><!--table-responsive-->
          <div class="tb-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title ft-bold">
                    <div class="tb-report">
                      お客様住所<span style="text-align:right;">：</span>
                    </div>
                  </th>
                  <td class="ft-bold"></td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様氏名<span style="text-align:right;">：</span></th>
                  <td class="ft-bold">xx　xx様</td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様住所<span style="text-align:right;">：</span></th>
                  <td class="ft-bold"></td>
                </tr>
              </tbody>
            </table>
          </div><!--/tb-responsive-->
          <div class="tb-responsive" style="margin-top:30px;">
            <table class="table-report-border">
              <thead>
                <tr>
                  <th style="width:220px">商品品番（名）</th>
                  <th style="width:300px">ご依頼内容</th>
                  <th style="width:120px">訪問日</th>
                  <th style="width:120px">担当者</th>
                  <th style="width:250px">完・未完の別</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>商</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
            <div class="tb-descritoion">
              <p>●この報告書は現場担当者からの第１報です。変更内容ありました時はすみやかにご連絡させていただきます。</p>
            </div>
          </div><!--/tb-responsive-->
        </div>
      </div><!--/row-sv-->
    </div><!--/content-report-->
  </section>
  <!-- * Start section prind * -->
</body>
</html>
