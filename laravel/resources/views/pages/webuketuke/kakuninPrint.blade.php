<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- 全体_IE互換モード動作抑止タグを実装する。 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDXMB');</script>
<!-- End Google Tag Manager -->
<link rel="shortcut icon" href="/assets/images/favicon.ico">
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>LIXILインターネット修理受付センター_印刷イメージ（修理依頼依頼内容確認）</title>
<style media="print">
  .col-nav-bt {display: none;}
</style>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDXMB"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <section class="container-silver">
    <!-- * Start section print * -->
    @if($webuketuke !== false)
    <div class="tabs-content_cf print-content">
      <div class="col-print-btn">
        <ul class="col-nav-bt">
            <li><a href="javascript:void(0)" onclick="window.print()"><span class="icon icon_user"><img src="/assets/images/print.png"></span>印刷</a></li>
            <li class="logout_sv">
             <a style="text-align:center;" href="javascript:void(0)" onclick="window.close();">
                閉じる
             </a>
           </li>
        </ul>
      </div>
      <div class="report-title-kakunin">
        <h1>修理依頼依頼内容確認</h1>
      </div>
      <div class="tabs-control-form">
         <div class="tb-responsive">
            <table class="detailList_print" border="0" cellpadding="0" cellspacing="0">
              <thead class="lixil_header">
                  <tr><th class="lixil_th" colspan="2">
                      <div class="lixil_title">お客様情報</div>
                  </th>
              </tr></thead>
              <tbody class="lixil_body">
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お客さま氏名</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->kokyaku_sei }}{{ $webuketuke->kokyaku_na }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お客さま氏名（フリガナ）</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->kokyaku_sei_kana }}{{ $webuketuke->kokyaku_na_kana }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お客さま電話番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->kokyaku_tel_haihun == '9999999999999')
                           未入居 / 施主様への連絡が不要
                          @else
                           {{ $webuketuke->kokyaku_tel_haihun }}
                          @endif</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お客さま郵便番号</label></div>
                    </th>
                    <td class="col-data">
                      @if($webuketuke->kokyaku_yubin != '')
                        {{substr($webuketuke->kokyaku_yubin , 0, 3)."-".substr($webuketuke->kokyaku_yubin, 3, 4)}}
                      @endif
                    </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お客さまご住所</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->kokyaku_jusyo }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お客さま建物名・部屋番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->kokyaku_katagaki }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>貴社顧客番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->hm_kokyaku_no }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>連絡先名</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->renrakusaki_mei }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>連絡先電話番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->renrakusaki_tel_haihun }}</div>
                    </td>
                </tr><!--/result-->
              </tbody>
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">修理依頼内容</div>
                  </th>
                </tr>
              </thead>
              <tbody class="lixil_body">
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>商品名</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->hinmei }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>品番</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->syohin_hinban }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>取り付け年月</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->torituke_nengetu == '@@@@@@')
                           不明
                          @elseif($webuketuke->torituke_nengetu == '888888')
                           未入居
                          @else
                           {{substr($webuketuke->torituke_nengetu , 0, 4)."年".substr($webuketuke->torituke_nengetu, 4, 5)}}月
                          @endif
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>工事店名</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->koziten }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご依頼内容</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->irainaiyo }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>希望日</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->kibo_sitei_bi != null && $webuketuke->kibo_sitei_bi != '')
                             {{ substr($webuketuke->kibo_sitei_bi,0,2) }}月{{ substr($webuketuke->kibo_sitei_bi,2,4) }}日
                          @endif
                          @if($webuketuke->kibo_sitei_bi_jikan != null && $webuketuke->kibo_sitei_bi_jikan != '')
                             @if($webuketuke->kibo_sitei_bi_jikan == '@@')
                               希望無
                             @else
                               {{ $webuketuke->kibo_sitei_bi_jikan }}:00ごろ
                             @endif
                          @endif
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご要望または連絡事項</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{$webuketuke->message}}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>添付資料</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->tempsiryo_umu_flg == '1')
                           @foreach($files as $file)
                            <p>{{ $file['basename'] }}</p>
                           @endforeach
                          @endif
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>連絡先電話番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->renrakusaki_tel_haihun }}</div>
                    </td>
                </tr><!--/result-->
              </tbody>
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">ご依頼元情報</div>
                  </th>
                </tr>
              </thead><!--/ご依頼元情報-->
              <tbody class="lixil_body">
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>得意先コード</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->iraimoto_cd }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>貴社名</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->iraimoto_simei }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご依頼元担当者様</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->iraimoto_tanto }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>貴社住所</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->iraimoto_jusyo }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>貴社電話番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->iraimoto_tel_haihun }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>貴社FAX</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->iraimoto_fax }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご要望または連絡事項</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->message }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>貴社注文番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->tyumon_bango }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>FAX送信</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->tyohyo_uk == "1") 「受付確認票」　@endif
                          @if($webuketuke->tyohyo_gj == "1") 「ご依頼状況一覧票」　@endif
                          @if($webuketuke->tyohyo_fax3 == "1") 「経過・結果速報」　@endif
                          @if($webuketuke->tyohyo_fax5 == "1") 「確定情報」　@endif
                          @if($webuketuke->tyohyo_uf == "1") 「受付複票」　@endif
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>メール送信</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                         {{ $webuketuke->mail_sousin }}
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->yuryo_seikyu_kbn == "1") 直収 @endif
                          @if($webuketuke->yuryo_seikyu_kbn == "2") その他 @endif
                          @if($webuketuke->yuryo_seikyu_kbn == "3") 貴社請求 @endif
                          @if($webuketuke->yuryo_seikyu_kbn == "5") 現調後依頼元判断  ※保証期間内の商品を含む @endif
                        </div>
                    </td>
                </tr><!--/result-->
              </tbody>
              @if($webuketuke->yuryo_seikyu_kbn == "2")
              <thead class="lixil_header">
                <tr>
                  <th class="lixil_th" colspan="2">
                      <div class="lixil_title lxl-20">ご請求先情報</div>
                  </th>
                </tr>
              </thead><!--/ご依頼元情報-->
              <tbody class="lixil_body">
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先名</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->seikyusaki_simei }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先名（フリガナ）</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->seikyusaki_simei_kana }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先電話番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->seikyusaki_tel_haihun == '9999999999999')
                           未入居 / 施主様への連絡が不要
                          @else
                           {{ $webuketuke->seikyusaki_tel_haihun }}
                          @endif
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先FAX番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->seikyusaki_fax }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先郵便番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->seikyusaki_yubin != '')
                            {{substr($webuketuke->seikyusaki_yubin , 0, 3)."-".substr($webuketuke->seikyusaki_yubin, 3, 4)}}
                          @endif
                        </div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先住所</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->seikyusaki_jusyo }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先建物名・部屋番号</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->seikyusaki_katagaki }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>ご請求先担当者</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">{{ $webuketuke->seikyusaki_tanto }}</div>
                    </td>
                </tr><!--/result-->
                <tr>
                    <th class="labelCol">
                        <div id="col-label"><label>お支払い予定日</label></div>
                    </th>
                    <td class="col-data">
                        <div id="col-result">
                          @if($webuketuke->siharaibi != '' && $webuketuke->siharaibi != null)
                          {{ $webuketuke->siharaibi }}日
                          @endif</div>
                    </td>
                </tr><!--/result-->
              </tbody>
              @endif
            </table><!--/listdetail_print-->
          </div><!--tb-responsive-->
      </div><!--/tabs-control-form-->
    </div>
    @else
    <div class="main-vs-12 sv_top20">
      <div class="center">
        <div class="col-not-found">
          <h2>申込Noが空欄の依頼は参照できません。/h2>
          <div class="share-options">
            <p><a class="btn btn-success share-options btn-none" href="{{ route('home') }}" title="">トップページへ戻る</a></p>
          </div>
        </div>
      </div>
    </div>
    @endif
  </section>
  <!-- * Start section prind * -->
</body>
</html>
