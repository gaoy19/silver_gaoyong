<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class WbSmtSeihinbetuCd extends Model
{
    protected $table = 'WB_SMT_SEIHINBETU_CD';

    public $timestamps = false;

    protected $fillable = [
      'SEIHIN_DAIBUNRUI_CD',
      'SEIHINBETU_CD',
      'SEIHIN_MEI',
      'HYOJI_JUN',
      'TOROKUBI',
      'TOROKUSYA_CD',
      'KOSINBI',
      'KOSINSYA_CD',
      'SAKUJO_FLG'
    ];

    public static function getLists($SEIHIN_DAIBUNRUI_CD)
    {
      $lists = self::select('SEIHIN_MEI','SEIHINBETU_CD')
  									->where('SAKUJO_FLG','=','0')
                    ->where('SEIHIN_DAIBUNRUI_CD','=',$SEIHIN_DAIBUNRUI_CD)
  									->orderBy('HYOJI_JUN','DESC')
  									->get()
                    ->toArray();
      return $lists;
    }
}
