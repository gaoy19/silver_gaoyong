@extends('layouts.master')
@section('title','インターネット修理受付センター | トップページ自動ログイン登録[1トップページ自動ログイン登録申請]')

@section('content')
<div class="row-sv">
    <!-- * start row top * -->
    <div class="main-vs-12" style="margin-top: 12px;">
        <div class="box-title_sv">
            <h1>トップページ自動ログイン登録</h1>
        </div>
    </div>
</div><!--/row-sv-->
<div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-7-left active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">トップページ自動ログイン登録申請</div>
                </div>
            </li><!--/box-sv-7-left-->
            <li class="box-sv-7-right">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">トップページ自動ログインの申請受付完了</div>
                </div>
            </li><!--/box-sv-7-right-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
</div><!--/row-sv-->
<div class="row-sv">
    <div class="main-vs-12 sv_top20">
        <form id="web_customer_info" name="form_register" method="POST" action="{{ route('user.autologinregister.post') }}" accept-charset="UTF-8" role="form" autocomplete="off">
            {{ csrf_field() }}
            <div class="box-main-form">
                <div class="con-info_sv">
                    </br>
                    <div class="con-info-detail">インターネット修理受付センターのユーザーID、パスワードを入力し、自動ログイン申請するを押してください。</div>
                        <div class="tb-form_sv">
                            <!--ユーザーID-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4">
                                    <div class="col-ft-left">
                                        <div class="td-title">ユーザーID</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('user_id_Alphas'))
                                            <div class="error_sv max-100"><span>{!! $errors->first('user_id_Alphas') !!}</span></div>
                                          @endif
                                          <input type="text" maxlength="40" value="{{ old('user_id_Alphas') }}" class="form-control_sv ime-active{!! $errors->has('user_id_Alphas') ? ' input-error' : '' !!}" id="user_id_Alphas" name="user_id_Alphas" style="max-width:300px;">
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div>
                            <!--パスワード-->
                            <div class="tr-form_sv">
                                <div class="td-form_sv-4 tb-fonm-none">
                                    <div class="col-ft-left">
                                        <div class="td-title">パスワード</div>
                                        <span class="error_label">必須</span>
                                    </div>
                                </div><!--/td-form_sv-4-->
                                <div class="td-form_sv-8 tb-fonm-none">
                                    <div class="col-ft-right">
                                        <div class="col-td-12">
                                          @if ($errors->has('passwd_Alphas'))
                                          	<div class="error_sv max-100"><span>{!! $errors->first('passwd_Alphas') !!}</span></div>
                                          @endif
                                          <input type="password" autocomplete="off" maxlength="20" class="form-control_sv{!! $errors->has('passwd_Alphas') ? ' input-error' : '' !!}" id="password" name="passwd_Alphas" style="max-width:300px;">
                                        </div>
                                        <div class="label-info-12 col-top10">
                                            <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>半角数字と半角英字を組み合わせ、4〜20文字で指定してください</div>
                                            <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>インターネット修理受付センターのユーザーIDをお持ちでない方は、ログイン画面より新規ユーザー登録をお願い致します。</div>
                                            <div class="des-info"><span><img src="{{ asset('assets/images/note-title.png') }}" alt=""></span>登録には3営業日程度要します。登録完了後ユーザーID申請時にご登録頂いた、ID連絡用メールアドレスにご連絡致します。</div>
                                        </div>
                                    </div>
                                </div><!--/td-form_sv-8-->
                            </div><!--/tr-form_sv-->
                        </div><!--/tb-info_sv-->
                   </div><!--/con-group-sv-->
            </div><!--/box-main-form-->
            <div class="btn-center-customer">
               <div class="checkbox-18 sv_center">
                    <input type="submit" class="btn-confirm" id="confirm_registration" value="自動ログインを申請する">
               </div>
               <div class="sv_center">
                    <a href="{{ route('login') }}" class="btn-confirm-small" id="to_correct">ログインに戻る</a>
               </div>
            </div>
        </form><!--/form-horizontal_sv-->
    </div>
</div><!--/row-sv-->

@stop
