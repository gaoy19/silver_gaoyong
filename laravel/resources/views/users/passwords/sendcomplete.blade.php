@extends('layouts.master')

@section('title','LIXILインターネット修理受付センター | パスワード再設定[パスワードの再設定メール送信完了]')

@section('content')
<section id="container-login">
    <div class="form-sv">
        <div class="form-horizontal_sv">
            <div class="modal-header_sv">
                <h3>パスワードの再設定メール送信完了</h3>
            </div>
            <div class="con-group-sv">
                <div class="form-group_sv">
                    <div class="col-sv-12">
                        <div class="role_login">ご登録のメールアドレス宛にパスワードの再設定 URL をお送りいたしました。</div>
                    </div>
                </div>
            </div>
        </div><!--/form-horizontal_sv-->
    </div>
</section><!--/container_login-->
@stop
