<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
  protected $primaryKey = 'edaban';

  public $table = 'WEB_UKETUKE_MAIL';

  public $timestamps = false;

  protected $fillable=[
    'web_uketuke_no',
    'edabaN',
    'torokubi',
    'kosinbi',
    'torokusya_cd',
    'user_id',
  ];

}
