<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class WebUketukeMail extends Model
{
    protected $table = 'WEB_UKETUKE_MAIL';

    public $timestamps = false;

    protected $primaryKey = 'WEB_UKETUKE_NO';

    protected $fillable = [
      "WEB_UKETUKE_NO",
      "EDABAN",
      "USER_ID",
      "MAIL_ADDRESS",
      "TOROKUBI",
      "TOROKUSYA_CD",
      "KOSINBI",
      "KOSINSYA_CD",
      "SAKUJO_FLG",
      "SOKUHO_MAIL",
      "UKETUKE_MAIL"
    ];

    public static function insertData($values)
    {
      return DB::insert(self::SQL_INSERT_WEB_UKETUKE_MAIL(),$values);
    }

    private static function SQL_INSERT_WEB_UKETUKE_MAIL()
    {
      $sql = <<<SQL
      INSERT INTO WEB_UKETUKE_MAIL(
        WEB_UKETUKE_NO,
        EDABAN,
        USER_ID,
        MAIL_ADDRESS,
        SOKUHO_MAIL,
        UKETUKE_MAIL,
        TOROKUBI,
        TOROKUSYA_CD,
        KOSINBI,
        KOSINSYA_CD,
        SAKUJO_FLG
       ) VALUES(
         :web_uketuke_no,
         :edaban,
         :user_id,
         :mail_address,
         :sokuho_mail,
         :uketuke_mail,
         SYSDATE,
         'WBUINS',
         sysdate,
         'WBUINS',
         '0'
       )
SQL;
      return $sql;
    }
}
