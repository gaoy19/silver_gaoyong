@extends('layouts.master')
@section('request_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理のご依頼[4ご依頼内容の確認]')
@section('content')
  <!-- * Start section 修理のご依頼 * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>修理のご依頼</h1>
        </div>
    </div>
  </div><!--/row-sv-->
  <!-- * End section 修理のご依頼 * -->
  <!-- * Start section お客さま情報の入力 * -->
  <div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-3 active_sv">
                <a href="{{ route('iraimoto.kokyaku') }}" class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">お客さま情報の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <a href="{{ route('iraimoto.naiyou') }}" class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">修理依頼内容の入力</div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <a href="{{ route('iraimoto.jouhou') }}" class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力 </div>
                </a>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3">
                <div class="box-progress-bar">
                    <div class="box-num-bar">5</div>
                    <div class="box-text-bar">修理のご依頼完了 </div>
                </div>
            </li><!--/box-sv-3-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
  </div><!--/row-sv--><!--/row-sv-->
  <!-- * End section お客さま情報の入力 * -->
  <!-- * Start section 下記の内容にて修理センターへ登録いたします。ご確認ください * -->
  <div class="row-sv">
      <div class="main-vs-12 sv_top20">
          <form id="web_customer_info" method="POST" action="{{ route('iraimoto.completation') }}" accept-charset="UTF-8" role="form">
              {{ csrf_field() }}
              <div class="contern_register">
                  <div class="col-sv-7">
                      <div class="follow_des">下記の内容にて修理受付センターへ登録いたします。ご確認ください</div>
                  </div>
                  <div class="col-sv-5">
                      <div class="col-print-5">
                          <button class="btn-print" type="button" value="お申し込み内容を印刷する" onclick="showPrintForm()"><span class="icon-print"><img src="{{ asset('assets/images/print.png') }}" width="18"></span>お申し込み内容を印刷する</button>
                      </div>
                  </div>
              </div>
              
              @if ( Session::has('fileError') )
                <div class="error_sv max-100"><span>{{ Session::get('fileError') }}</span><a class="btn-info btn-info-err" href="{{ route('iraimoto.naiyou') }}"><span class="pencil-info"><img src="{{ asset('assets/images/pencil-info.png') }}"></span><span class="cus-info">修理依頼内容を修正する</span></a></div>
              @endif
              <div class="box-main-form">
                  <div class="modal-header_sv">
                      <h5>お客さま情報</h5>
                  </div>
                  <div class="con-info_sv">
                      <div class="tb-responsive">
                              <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                                  <tbody class="lixil_body">
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さま氏名</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    姓：{{ Session::get('kokyaku')['okyakusamaSei'] }} 　　　　　名：{{ Session::get('kokyaku')['okyakusamaMei'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さま氏名（フリガナ）</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    セイ：{{ Session::get('kokyaku')['okyakusamaSeiKana'] }} 　　　　　メイ：{{ Session::get('kokyaku')['okyakusamaMeiKana'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さま電話番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    @if(isset(Session::get('kokyaku')['telFumeiFlag']) && Session::get('kokyaku')['telFumeiFlag'] == 'on')
                                                      未入居／施主様への連絡が不要
                                                    @else
                                                      {{ Session::get('kokyaku')['okyakusamaTel'] }}
                                                    @endif
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さま郵便番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['okyakusamaYuubinNo'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さま都道府県</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['okyakusamaTodoufuken'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さまご住所</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['okyakusamaAddress'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お客さま建物名・部屋番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['okyakusamaMansionName'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>貴社顧客番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['kokyakuBango'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>連絡先名</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['renrakusakiMei'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>連絡先電話番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('kokyaku'))
                                                    {{ Session::get('kokyaku')['renrakusakiTel'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                  </tbody>
                             </table>
                             <div class="read-info">
                                  <a class="btn-info" href="{{ route('iraimoto.kokyaku') }}"><span class="pencil-info"><img src="{{ asset('assets/images/pencil-info.png') }}"></span><span class="cus-info">お客さま情報を修正する</span></a>
                             </div>
                      </div><!--/tb-responsive-->
                  </div><!--/con-group-sv-->
              </div><!--/box-main-form_1-->
              <!--修理依頼内容-->
              <div class="box-main-form">
                  <div class="modal-header_sv">
                      <h5>修理依頼内容</h5>
                  </div>
                  <div class="con-info_sv">
                      <div class="tb-responsive">
                              <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                                  <tbody class="lixil_body">
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>商品名</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                    {{ Session::get('naiyou')['hinmei_select'] }} 
                                                    {{ Session::get('naiyou')['hinmei_directWrite'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>品番</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                    {{ Session::get('naiyou')['hinban'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>取付年月</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                    @if(isset(Session::get('naiyou')['fumeiFlag']) && Session::get('naiyou')['fumeiFlag'] == 'on')
                                                      不明
                                                    @elseif (isset(Session::get('naiyou')['minyukyoFlag']) && Session::get('naiyou')['minyukyoFlag'] == 'on')
                                                      未入居
                                                    @else
                                                      {{ Session::get('naiyou')['toritsukeYYYY'] }}年 {{ Session::get('naiyou')['toritsukeMM'] }}月
                                                    @endif
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>工事店名</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                    {{ Session::get('naiyou')['koujitenMei'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご依頼内容</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                    {{ Session::get('naiyou')['irainaiyou_directWrite'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>希望日</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                  @if(Session::get('naiyou')['kiboushiteiYYYY'] == '' && Session::get('naiyou')['kiboushiteiMM'] == '' &&
                                                      Session::get('naiyou')['kiboushiteiDD'] == '' && Session::get('naiyou')['kiboushiteiHH'] == '')
                                                      指定なし
                                                  @else
                                                    @if(Session::get('naiyou')['kiboushiteiYYYY'] != '')
                                                    {{ Session::get('naiyou')['kiboushiteiYYYY'] }}年
                                                    @endif
                                                    @if(Session::get('naiyou')['kiboushiteiMM'] != '')
                                                    {{ Session::get('naiyou')['kiboushiteiMM'] }}月
                                                    @endif
                                                    @if(Session::get('naiyou')['kiboushiteiDD'] != '')
                                                    {{ Session::get('naiyou')['kiboushiteiDD'] }}日
                                                    @endif
                                                    @if(Session::get('naiyou')['kiboushiteiHH'] != '')
                                                     @if(Session::get('naiyou')['kiboushiteiHH'] == '@@')
                                                       希望無
                                                     @else
                                                       {{ Session::get('naiyou')['kiboushiteiHH'] }}:00ごろ
                                                     @endif
                                                    @endif
                                                  @endif
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご要望または連絡事項</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('naiyou'))
                                                    {{ Session::get('naiyou')['renrakujikou'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>添付資料</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                  @if(Session::has('naiyou'))
                                                      @if(Session::get('naiyou')['tempsiryoKbn']=='1')
                                                      <p>あり</p>
                                                      <div class="get-image">
                                                        @if(isset($fileUpload))
                                                          @foreach ($fileUpload as $key => $value)
                                                            <p>{{ $value }}</p>
                                                          @endforeach
                                                        @endif
                                                      </div>
                                                      @else
                                                      <p>なし</p>
                                                      @endif
                                                  @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                  </tbody>
                             </table>
                             <div class="read-info" style="padding-top: 25px;">
                                  <a class="btn-info" href="{{ route('iraimoto.naiyou') }}"><span class="pencil-info"><img src="{{ asset('assets/images/pencil-info.png') }}"></span><span class="cus-info">修理依頼内容を修正する</span></a>
                             </div>
                      </div><!--/tb-responsive-->
                  </div><!--/con-group-sv-->
              </div><!--/box-main-form_2-->
              <!--ご依頼元情報-->
              <div class="box-main-form">
                  <div class="modal-header_sv">
                      <h5>ご依頼元情報</h5>
                  </div>
                  <div class="con-info_sv">
                      <div class="tb-responsive">
                              <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                                  <tbody class="lixil_body">
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>得意先コード</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">{{ Auth::user()->tokuisaki_cd }}</div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>所属貴社</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">{{ $goiraimoto_info->tokuisaki_sei }}</div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご依頼元担当者様</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('jouhou'))
                                                    {{ Session::get('jouhou')['goiraimotoTantou'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>貴社住所</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">{{ $goiraimoto_info->tokuisaki_jusyo }}</div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>貴社電話番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">{{ $goiraimoto_info->tokuisaki_tel_haihun }}</div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>貴社 FAX</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">{{ $goiraimoto_info->torokusaki_fax }}</div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>貴社注番</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('jouhou'))
                                                    {{ Session::get('jouhou')['kisya_chuban'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>FAX 送信</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('jouhou'))
                                                    @if(isset(Session::get('jouhou')['tyohyo_uk']) && Session::get('jouhou')['tyohyo_uk'] == "on") 「受付確認票」　@endif
                                                    @if(isset(Session::get('jouhou')['tyohyo_gj']) && Session::get('jouhou')['tyohyo_gj'] == "on") 「ご依頼状況一覧票」　@endif
                                                    @if(isset(Session::get('jouhou')['tyohyo_fax3']) && Session::get('jouhou')['tyohyo_fax3'] == "on") 「経過・結果速報」　@endif
                                                    @if(isset(Session::get('jouhou')['tyohyo_fax5']) && Session::get('jouhou')['tyohyo_fax5'] == "on") 「確定情報」　@endif
                                                    @if(isset(Session::get('jouhou')['tyohyo_uf']) && Session::get('jouhou')['tyohyo_uf'] == "on") 「受付複票」　@endif
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>メール送信</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('jouhou'))
                                                    @if(isset(Session::get('jouhou')['kakunin_mail']) && Session::get('jouhou')['kakunin_mail'] == "on") 「受付確認」　@endif
                                                    @if(isset(Session::get('jouhou')['sokuho_mail']) && Session::get('jouhou')['sokuho_mail'] == "on") 「結果速報」 @endif
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('jouhou') && isset(Session::get('jouhou')['seikyuuKubun']))
                                                    @if(Session::get('jouhou')['seikyuuKubun'] == "1") 直収 @endif
                                                    @if(Session::get('jouhou')['seikyuuKubun'] == "3") 貴社請求 @endif
                                                    @if(Session::get('jouhou')['seikyuuKubun'] == "2") その他 @endif
                                                    @if(Session::get('jouhou')['seikyuuKubun'] == "5") 現調後依頼元判断  ※保証期間内の商品を含む @endif
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                  </tbody>
                             </table>
                             <div class="read-info" style="padding-top: 31px;">
                                  <a class="btn-info" href="{{ route('iraimoto.jouhou') }}"><span class="pencil-info"><img src="{{ asset('assets/images/pencil-info.png') }}"></span><span class="cus-info">ご依頼元情報を修正する</span></a>
                             </div>
                      </div><!--/tb-responsive-->
                  </div><!--/con-group-sv-->
              </div><!--/box-main-form_3-->
              @if(Session::has('jouhou') && Session::get('jouhou')['seikyuuKubun'] == '2' && Session::has('goseikyusaki'))
              <!--ご請求先情報-->
              <div class="box-main-form">
                  <div class="modal-header_sv">
                      <h5>ご請求先情報</h5>
                  </div>
                  <div class="con-info_sv">
                      <div class="tb-responsive">
                              <table class="detailList" border="0" cellpadding="0" cellspacing="0">
                                  <tbody class="lixil_body">
                                      <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先名</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaShimei'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先名（フリガナ）</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaShimeiKana'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先電話番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaTel'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先 FAX 番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaFax'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先郵便番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaYuubinNo'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先都道府県</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaTodoufuken'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先住所</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaAddress'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先建物名・部屋番号</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaMansionName'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>ご請求先担当者</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaTantousya'] }}
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->
                                       <tr>
                                          <th class="labelCol">
                                              <div class="col-label"><label>お支払い予定日</label></div>
                                          </th>
                                          <td class="col-data">
                                              <div class="col-result">
                                                @if(Session::has('goseikyusaki'))
                                                    {{ Session::get('goseikyusaki')['sonotaShiharaibi'] }}日
                                                @endif
                                              </div>
                                          </td>
                                      </tr><!--/result-->

                                  </tbody>
                             </table>
                             <div class="read-info" style="padding-top: 35px;">
                                  <a class="btn-info" href="{{ route('iraimoto.goseikyusaki') }}"><span class="pencil-info"><img src="{{ asset('assets/images/pencil-info.png') }}"></span><span class="cus-info">ご請求先情報を修正する</span></a>
                             </div>
                      </div><!--/tb-responsive-->
                  </div><!--/con-group-sv-->
              </div><!--/box-main-form_4-->
              @endif
              <!--上記内容で申し込む-->
              <div class="row-sv">
                 <div class="col-sv-3">
                 </div>
                 <div class="col-sv-6">
                     <div class="btn-center">
                          <input type="submit" class="btn-confirm" @if ( Session::has('fileError') ) disabled @endif id="inquiryconfirm" value="上記内容で申し込む" />
                      </div>
                 </div>
                 <div class="col-sv-3">
                     <div class="col-print-5">
                          <button class="btn-print" type="button" value="お申し込み内容を印刷する" onclick="showPrintForm()"><span class="icon-print"><img src="{{ asset('assets/images/print.png') }}" width="18"></span>お申し込み内容を印刷する</button>
                     </div>
            　   </div>
            </div>
          </form><!--/form-horizontal_sv-->
      </div>
  </div><!--/row-sv--><!--row-sv--><!--/row-sv-->
  <!-- * End section 下記の内容にて修理センターへ登録いたします。ご確認ください * -->
  <script type="text/javascript">
    //-------------------- 追加　2007/03/12　start --------------------//
    function showPrintForm() {
      window.open("{{ route('iraimoto.showPrintpreview') }}", 'printpreview', "top=0,left=0,height=650,width=950,location=no,scrollbars=yes,toolbar=yes,menubar=yes,status=no");
    }
    //-------------------- 追加　2007/03/12　end   --------------------//
  </script>
@stop
