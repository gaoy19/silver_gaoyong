<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebUketuke;
use App\RequestData;
use Session;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use URL;
use Log;
class WebUketukeController extends Controller
{
  private $_model;
  // Cont Var DEBUG_FLAG
  const DEBUG_FLAG = TRUE;

  public function __construct(WebUketuke $model)
  {
    $this->_model = $model;
  }

  /**
   * write custom log activity
   */
  private static function logActivity()
  {
    $user_info = array(
      "user_id" => Auth::user()->user_id,
      "user_simei" => Auth::user()->user_simei,
      "tokuisaki_cd" => Auth::user()->tokuisaki_cd,
      "info_mail_address" => Auth::user()->info_mail_address,
      "remember_token" => Auth::user()->remember_token
    );
    $server = array(
      "REMOTE_ADDR"       => $_SERVER['REMOTE_ADDR'],
      "REMOTE_PORT"       => $_SERVER['REMOTE_PORT'],
      "HTTP_USER_AGENT"   => $_SERVER['HTTP_USER_AGENT']
    );
    $log = array(
      'request'   => $_REQUEST,
      'user_info' => $user_info,
      'server'    => $server,
      'token'     => Session::token()
    );
    //if (self::DEBUG_FLAG) Log::debug( 'pagination debug' , $log );
  }

  /**
   * 6-1_インターネット修理受付センター_修理データの条件指定検索[トップ①]
   */
  public function showSearch(Request $request)
  {
    $uketukebiYYYY = $this->_model->getUketukebiYYYY();
    $uketukebiMM = $this->_model->getUketukebiMM();
    $uketukebiDD = $this->_model->getUketukebiDD();
    return view('pages.webuketuke.search',[
      'uketukebiYYYY' => $uketukebiYYYY,
      'uketukebiMM'   => $uketukebiMM,
      'uketukebiDD'   => $uketukebiDD,
      'request'       => $request->all()
    ]);
  }

  public function kakuhouPrint($webno, Request $request)
  {
    $webuketuke = $this->_model->getUeketukeByWebNo(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'web_no'=>$webno]);
    $kakuhou = $this->_model->getKakuhouPrint(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no]);
    if(count($kakuhou)>0){
      $uketuke_nendo = $kakuhou[0]->uketuke_nendo;
      $uketuke_no = $kakuhou[0]->uketuke_no;
      $kakuhou[0]->uketuke_no = $this->setUketukeNo($uketuke_no);
      $hojoce = $this->_model->getHojoce(['uketuke_no'=>$uketuke_no,'uketuke_nendo'=>$uketuke_nendo]);
      $hojoce_cnt = isset($hojoce[0]->hojoninzu) ? $hojoce[0]->hojoninzu : 0;

      if($hojoce_cnt > 0){
        $kakuhou[0]->ce_cd = $kakuhou[0]->ce_sei . "(" . $kakuhou[0]->ce_cd . ")(他" . $hojoce_cnt . "名)" ;
      } else {
        $kakuhou[0]->ce_cd = $kakuhou[0]->ce_sei . "(" . $kakuhou[0]->ce_cd . ")";
      }

      /* 2004/11/26 fjcl 訪問実施日欄に開始時刻を追加 */
      if($kakuhou[0]->kaisi_jikoku != null && $kakuhou[0]->kaisi_jikoku != "0000"){
        $kakuhou[0]->houmonbi = $kakuhou[0]->homonbi . " " . substr($kakuhou[0]->kaisi_jikoku,0,2) . ":" . substr($kakuhou[0]->kaisi_jikoku,2,4);
      } else {
        $kakuhou[0]->houmonbi = $kakuhou[0]->homonbi;
      }

      if($kakuhou[0]->mikanriyu_cd != null && substr($kakuhou[0]->mikanriyu_cd,0,1) == "1"){
        $kakuhou[0]->kekka = "完了";
      } elseif ($kakuhou[0]->mikanriyu_cd != null && substr($kakuhou[0]->mikanriyu_cd,0,1) == "2"){
        $kakuhou[0]->kekka = "未完了";
      } elseif ($kakuhou[0]->mikanriyu_cd != null && substr($kakuhou[0]->mikanriyu_cd,0,1) == "3"){
        $kakuhou[0]->kekka = "キャンセル";
      }

      /*2018.12.06 ADD START	グローエ、災害時特別対応*/
      if($kakuhou[0]->hisai_kb != "0"){
        $kakuhou[0]->sekyu_naiyou = $kakuhou[0]->hisai_kb;
      } else{
      /*2018.12.06 ADD START	グローエ、災害時特別対応*/
      // ｢請求内容｣の編集
      if($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "A"){
        $kakuhou[0]->sekyu_naiyou = "有料";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "B" && $kakuhou[0]->mikanriyu_cd != "1H"){
        $kakuhou[0]->sekyu_naiyou = "無料";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "C"){
        $kakuhou[0]->sekyu_naiyou = "有料直収";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "D"){
        $kakuhou[0]->sekyu_naiyou = "有料施主様請求";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "E"){
        $kakuhou[0]->sekyu_naiyou = "有料施主様以外請求";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "F"){
        $kakuhou[0]->sekyu_naiyou = "有料依頼元請求";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "G" && substr(Auth::user()->tokuisaki_cd,0,2) == "18"){
        $kakuhou[0]->sekyu_naiyou = "有料施主様以外請求";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "G" && substr(Auth::user()->tokuisaki_cd,0,2) != "18"){
        $kakuhou[0]->sekyu_naiyou = "無料";
      } elseif($kakuhou[0]->yumuryo_kbn != null && $kakuhou[0]->yumuryo_kbn == "H"){
        $kakuhou[0]->sekyu_naiyou = "無料";
      } else {
        $kakuhou[0]->sekyu_naiyou = "";
      }
      //2018.12.06 ADD STRAT	グローエ、災害時特別対応
	  }
	  //2018.12.06 ADD END
      $kakuhou[0]->syuri_naiyou = $kakuhou[0]->kanmi_hokoku_bunsyo;

      // お客様名
      $kakuhou[0]->kokyaku_mei = $this->setName($kakuhou[0]->kokyaku_sei,$kakuhou[0]->kokyaku_na);
      // お客様TEL
      if($kakuhou[0]->kokyaku_tel != null && substr($kakuhou[0]->kokyaku_tel,0,1) == "0"){
        $kakuhou[0]->kokyaku_tel_haihun = $kakuhou[0]->kokyaku_tel;
      } else {
        $kakuhou[0]->kokyaku_tel_haihun = "9999999999999";
      }

      $kakuhou[0]->syohin_hinban = $kakuhou[0]->hinban;
      $kakuhou[0]->seikyusaki_tel_haihun = $kakuhou[0]->seikyusaki_tel;
      $kakuhou[0]->iraimoto_cd = Auth::user()->tokuisaki_cd;
      $kakuhou[0]->iraimoto_tanto = ($kakuhou[0]->iraimoto_tanto != null) ? $kakuhou[0]->iraimoto_tanto . ' 様' : $kakuhou[0]->iraimoto_tanto;
      $kakuhou[0]->iraimoto_tel_haihun = $kakuhou[0]->iraimoto_tel;
      $kakuhou[0]->okyakusamakiji = $kakuhou[0]->sagyo_naiyo;
      // 部品明細取得
      $buhin = $this->_model->getBuhin(['uketuke_no'=>$uketuke_no,'uketuke_nendo'=>$uketuke_nendo]);
      $recCnt2 = 0;
      $ta_buhin_dai_kei=0;
      $hinban ="";
      $buhinmei="";
      $tanka="";
      $kosuu="";
      $kingaku="";
      $buhinDetail = [];
      foreach ($buhin as $value) {
        if($recCnt2 > 4){
          if($value->buhindai > 0){
            if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041"){
              //20110719 MOD 保証延長対応
              //「受付依頼元の依頼元CD="10995"でなく、かつ、売上請求先の請求先CD="10995"である」
            } elseif($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995"){
            } else {
              $ta_buhin_dai_kei += $value->buhindai;
            }
          }
        }else {
          // 品番
          $hinban = $value->hinban;
          // 品名
          $buhinmei = ($value->hinmei == null) ? $hinban : $value->hinmei;
          // 数量
          $kosuu = $value->suryo;
          // 単価,部品代
          $kingaku = $value->buhindai;
          if($kingaku > 0){
            if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041"){
              $tanka="0";
              $kingaku="0";
              //20110719 MOD 保証延長対応
              //「受付依頼元の依頼元CD="10995"でなく、かつ、売上請求先の請求先CD="10995"である」
            }elseif($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995"){
              $tanka="0";
              $kingaku="0";
            } else {
              $tanka = $value->tanka;
            }
          } else {
            $tanka = "0";
            $kingaku = "0";
          }
        }
        if($recCnt2 <= 4){
          $buhinDetail[] = [
            'hinban' => $hinban,
            'buhinmei' => $buhinmei,
            'tanka' => $tanka,
            'kosuu' => $kosuu,
            'kingaku' => $kingaku
          ];
        }
        $recCnt2++;
      }

      if($recCnt2 > 5){
        $kakuhou[0]->buhinmei6 = "その他部品代";
        $kakuhou[0]->kingaku6 = $ta_buhin_dai_kei;
      }
      $kakuhou[0]->buhin_detail = $buhinDetail;
      // 調整明細取得
      $tyosei = $this->_model->getTyosei(['uketuke_no'=>$uketuke_no,'uketuke_nendo'=>$uketuke_nendo]);
      $tempBui = "";
      $tempSyotiNaiyo = "";
      $bui = "";
      $syotiNaiyo = "";
      $recCnt2 = 0;
      foreach ($tyosei as $value) {
        $bui = $value->bui;
        $syotiNaiyo = $value->syoti_naiyo;
        if($recCnt2 > 2) break;
        if($value->syoti_naiyo != null && $value->syoti_naiyo == "共同（２名以上）作業の補助（補助のＣＥが記入）"){
          $syotiNaiyo = " ";
        }
        if($recCnt2 == 0){
          // 1件目の場合
          $kakuhou[0]->tyosei_bui1 = $bui;
          $kakuhou[0]->syoti_naiyo1 = $syotiNaiyo;
          $recCnt2++;
        } elseif($recCnt2 > 0 && ($bui != $tempBui || $syotiNaiyo != $tempSyotiNaiyo)){
          // 上記以外の場合
          switch ($recCnt2) {
            case 1:
              $kakuhou[0]->tyosei_bui2 = $bui;
              $kakuhou[0]->syoti_naiyo2 = $syotiNaiyo;
              break;
            case 2:
              $kakuhou[0]->tyosei_bui3 = $bui;
              $kakuhou[0]->syoti_naiyo3 = $syotiNaiyo;
              break;
            default:
              break;
          }
          $recCnt2++;
        }
        $tempBui = $bui;
        $tempSyotiNaiyo = $syotiNaiyo;
      }

      // 報告書有料金額集計取得
      $yuryokin = $this->_model->getYuryokin(['uketuke_no'=>$uketuke_no,'uketuke_nendo'=>$uketuke_nendo]);
      $goukeigaku = 0;
      if(count($yuryokin) > 0){
        $kakuhou[0]->buhindaikei = $yuryokin[0]->buhindai_kei;
        $kakuhou[0]->gijuturyou = $yuryokin[0]->gijuturyo_kei;
        $kakuhou[0]->syutyohi = $yuryokin[0]->syutyoryo_kei;
        $kakuhou[0]->syobunryou = $yuryokin[0]->syobunryo_kei;
        $kakuhou[0]->nebiki = $yuryokin[0]->nebiki_kei;
        $kakuhou[0]->zei = $yuryokin[0]->syohizei_kei;
        $goukeigaku += $yuryokin[0]->buhindai_kei;
        $goukeigaku += $yuryokin[0]->gijuturyo_kei;
        $goukeigaku += $yuryokin[0]->syutyoryo_kei;
        $goukeigaku += $yuryokin[0]->syobunryo_kei;
        $goukeigaku += $yuryokin[0]->nebiki_kei;
        $goukeigaku += $yuryokin[0]->syohizei_kei;
      } else {
        $kakuhou[0]->buhindaikei = "0";
        $kakuhou[0]->gijuturyou = "0";
        $kakuhou[0]->syutyohi = "0";
        $kakuhou[0]->syobunryou = "0";
        $kakuhou[0]->nebiki = "0";
        $kakuhou[0]->zei = "0";
        $goukeigaku = 0;
      }
      if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041"){
        $kakuhou[0]->buhindaikei = "0";
        $kakuhou[0]->gijuturyou = "0";
        $kakuhou[0]->syutyohi = "0";
        $kakuhou[0]->syobunryou = "0";
        $kakuhou[0]->nebiki = "0";
        $kakuhou[0]->zei = "0";
        $goukeigaku = 0;
      }
      //20110719 MOD 保証延長対応
      //「受付依頼元の依頼元CD="10995"でなく、かつ、売上請求先の請求先CD="10995"である」
      if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995"){
        $kakuhou[0]->sekyu_naiyou = "無料";
        $kakuhou[0]->kan_mikan = "完了（無料）";
        $kakuhou[0]->seikyusaki_jusyo = "";
        $kakuhou[0]->seikyusaki_simei = "";
        $kakuhou[0]->seikyusaki_tanto = "";
        $kakuhou[0]->seikyusaki_cd = "";
        $kakuhou[0]->seikyusaki_fax = "";
        $kakuhou[0]->seikyusaki_tel_haihun = "";

        $kakuhou[0]->buhindaikei = "0";
        $kakuhou[0]->gijuturyou = "0";
        $kakuhou[0]->syutyohi = "0";
        $kakuhou[0]->syobunryou = "0";
        $kakuhou[0]->nebiki = "0";
        $kakuhou[0]->zei = "0";
        $goukeigaku = 0;
      }

      $kakuhou[0]->goukei = $goukeigaku;
      if($goukeigaku > 0){
        if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041"){
          $kakuhou[0]->kan_mikan = "完了（無料）";
          $kakuhou[0]->seikyusaki_jusyo = "";
          $kakuhou[0]->seikyusaki_simei = "";
          $kakuhou[0]->seikyusaki_tanto = "";
        } else {
          $kakuhou[0]->kan_mikan = "完了（有料）";
          // ＦＡＸ１＿５直収表示対象の請求先で且つ、依頼元と請求先が違う場合
          if($kakuhou[0]->fax1_5_tyokusyu_flg != null && $kakuhou[0]->fax1_5_tyokusyu_flg == "1" && $kakuhou[0]->seikyusaki_cd != Auth::user()->tokuisaki_cd){
            $kakuhou[0]->seikyusaki_jusyo = "";
            $kakuhou[0]->seikyusaki_simei = "直収";
            $kakuhou[0]->seikyusaki_tanto = "";
          } else {
            //入金区分
            if($kakuhou[0]->nyukin_kbn != null && $kakuhou[0]->nyukin_kbn == "1"){
              $kakuhou[0]->seikyusaki_jusyo = "";
              $kakuhou[0]->seikyusaki_simei = "直収";
            } else {
              // 顧客と請求先電話番号が同一の場合
              if($kakuhou[0]->kokyaku_tel == $kakuhou[0]->seikyusaki_tel ){
                $kakuhou[0]->seikyusaki_jusyo = "";
                $kakuhou[0]->seikyusaki_simei = "直収";
              } else {
                // $kakuhou[0]->seikyusaki_jusyo = $kakuhou[0]->seikyusaki_jusyo;
                if($kakuhou[0]->seikyusaki_simei != null && strlen($kakuhou[0]->seikyusaki_simei)>0){
                  $kakuhou[0]->seikyusaki_simei = $kakuhou[0]->seikyusaki_simei . " 様";
                } else { $kakuhou[0]->seikyusaki_simei = ''; }
              }
            }
            if($kakuhou[0]->seikyusaki_tanto != null && strlen($kakuhou[0]->seikyusaki_tanto) > 0){
              $kakuhou[0]->seikyusaki_tanto = $kakuhou[0]->seikyusaki_tanto . " 様";
            } else { $kakuhou[0]->seikyusaki_tanto = ''; }
          }
        }
      } else {
        $kakuhou[0]->kan_mikan = "完了（無料）";
        $kakuhou[0]->seikyusaki_jusyo = "";
        $kakuhou[0]->seikyusaki_simei = "";
        $kakuhou[0]->seikyusaki_tanto = "";
      }
      // 依頼内容
      $iraiNaiyo = $this->_model->getIraiNaiyoKakuhou(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$uketuke_no,'uketuke_nendo'=>$uketuke_nendo]);

      $hinban = '';
      for($i=0;$i<3;$i++) {

        if(isset($iraiNaiyo[$i]->hinmei)||isset($iraiNaiyo[$i]->syohin_hinban)){
          if(is_null($iraiNaiyo[$i]->hinmei)){
            $hinban = $iraiNaiyo[$i]->syohin_hinban;
          } else {
            $hinban = $iraiNaiyo[$i]->hinmei;
          }
        } else{
          $hinban = '';
        }
        switch ($i) {
          case 0:
            $kakuhou[0]->p_hinban1 = $hinban;
            $kakuhou[0]->irainaiyo1 = isset($iraiNaiyo[$i]->irainaiyo)?$iraiNaiyo[$i]->irainaiyo:'';
            break;
          case 1:
          $kakuhou[0]->p_hinban2 = $hinban;
          $kakuhou[0]->irainaiyo2 = isset($iraiNaiyo[$i]->irainaiyo)?$iraiNaiyo[$i]->irainaiyo:'';
            break;
          case 2:
          $kakuhou[0]->p_hinban3 = $hinban;
          $kakuhou[0]->irainaiyo3 = isset($iraiNaiyo[$i]->irainaiyo)?$iraiNaiyo[$i]->irainaiyo:'';
            break;
          default:
            # code...
            break;
        }
      }
      $kakuhou[0]->data_flg = "1";
      $tokuisaki = \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD();
      $kakuhou[0]->t1 = $tokuisaki->tokuisaki_sei . $tokuisaki->tokuisaki_na;
      return view('pages.webuketuke.kakuhouPrint',['kakuhou' => $kakuhou[0]]);
    }

  }

  /**
   * 速報印刷ページ
   */
  public function sokuhouPrint($webno, Request $request)
  {

    $webuketuke = $this->_model->getUeketukeByWebNo(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'web_no'=>$webno]);

    $sokuhou = $this->_model->getSokuhou(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no]);

    // 依頼内容
    $iraiNaiyo = $this->_model->getIraiNaiyo(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no,'uketuke_nendo'=>$sokuhou[0]->uketuke_nendo]);
    $detail = [];
    $b_syuritaisyo_seq = "";
    foreach ($iraiNaiyo as $value) {
      if($value->meisai_seq != $b_syuritaisyo_seq){
        $detail['iraiNaiyo'][] = $value;
        $b_syuritaisyo_seq = $value->meisai_seq;
      }
    }

    // 明細内容
    $meisaiNaiyo = $this->_model->getMeisaiNaiyo(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no,'uketuke_nendo'=>$sokuhou[0]->uketuke_nendo]);
    $yumuryo_kbn = "";
    $mikanriyu_cd = "";
    $yumuryo = "";
    $kanmi_hokoku_bunsyo = "";
    $b_syuritaisyo_seq = "";
    $index = 0;
    foreach ($meisaiNaiyo as $value) {

      if($index > 2) break;
      if($value->meisai_seq != $b_syuritaisyo_seq){
        $yumuryo_kbn = $value->yumuryo_kbn;
        $mikanriyu_cd = $value->mikanriyu_cd;
        
        $kanmi_hokoku_bunsyo = $value->mikan_riyu;
        $value->kanmi_hokoku_bunsyo = $kanmi_hokoku_bunsyo ;

        if($webuketuke[0]->iu_kanryo_flg != null && ($webuketuke[0]->iu_kanryo_flg == '8' || $webuketuke[0]->iu_kanryo_flg == '9')){
          /*2018.12.06 ADD START	グローエ、災害時特別対応*/
          if($meisaiNaiyo[0]->hisai_kb != "0"){
            $yumuryo = $meisaiNaiyo[0]->hisai_kb;
          }else{
          /*2018.12.06 ADD START	グローエ、災害時特別対応*/
          if($yumuryo_kbn != null && $yumuryo_kbn == 'A'){
            $yumuryo = "有料";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'B' && $mikanriyu_cd != "1H"){
            $yumuryo = "無料";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'C'){
            $yumuryo = "有料直収";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'D'){
            $yumuryo = "有料施主様請求";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'E'){
            $yumuryo = "有料施主様以外請求";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'F'){
            $yumuryo = "有料依頼元請求";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'G' && substr($sokuhou[0]->iraimoto_cd,0,2) == "18"){
            $yumuryo = "有料施主様以外請求";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'G' && substr($sokuhou[0]->iraimoto_cd,0,2) != "18"){
            $yumuryo = "無料";
          } elseif ($yumuryo_kbn != null && $yumuryo_kbn == 'H'){
            $yumuryo = "無料";
          } else {
            $yumuryo = "";
          }
         }
        }
        $value->yumuryo_kbn = $yumuryo;

        $detail['meisai'][] = $value;
        $index++;
        $b_syuritaisyo_seq = $value->meisai_seq;
      }
    }
    $sokuhou[0]->kokyaku_mei = $this->setName($sokuhou[0]->kokyaku_sei,$sokuhou[0]->kokyaku_na);
    $sokuhou[0]->uketuke_no = $this->setUketukeNo($sokuhou[0]->uketuke_no);
    // 得意先名、得意先コードの取得
    $tokuisaki = \App\Tokuisaki::GOIRAIMOTO_INFO_BY_TOKUISAKI_CD();
    $tokuisaki->tokuisaki_mei = $tokuisaki->tokuisaki_sei . $tokuisaki->tokuisaki_na;

    return view('pages.webuketuke.sokuhouPrint',['iraiNayo'=>$detail['iraiNaiyo'],'meisai'=>$detail['meisai'],'sokuhou'=>$sokuhou[0],'tokuisaki'=>$tokuisaki]);
  }

  /**
   * 検索処理
   */
  public function search(Request $request)
  {

    $values = $request->all();
    $validator = Validator::make($values,$this->_model->searchRules(),$this->_model->searchErrorMessage());

    if ($validator->fails()) {
      return redirect()->back()->withInput()->withErrors($validator);
    }

    $webuketuke = [];
    $count = 0;
    $input['page'] = isset($request->page)?$request->page:1;
    // set start row
    $startrow = ($input['page']-1) * 10;
    // set end row
    $endrow = $startrow + 10;
    // テーダベースの上に半角で保存されるため、お客さま名（フリガナ）全角を半角に変更
    $request->kokyaku_sei_kana = \App\Custom\CharReplace::zenKanaTohanKana($request->kokyaku_sei_kana);
    // Where句編集
    switch ($request->kubun) {
      // 未完了検索
      case '0':
        $webuketuke = $this->_model->getSearchMikanryo(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow],
          $request
        );
        $count = $this->_model->getCountSearchMikanryo(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd],
          $request
        );
        break;
      // 完了検索
      case '1':
        $webuketuke = $this->_model->getSearchKanryo(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow],
          $request
        );
        $count = $this->_model->getCountSearchKanryo(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd],
          $request
        );
        break;
      // 両方検索
      case '2':
        $webuketuke = $this->_model->getSearchAll(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow],
          $request
        );
        $count = $this->_model->getCountSearchAll(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd],
          $request
        );
        break;
      // キャンセル検索
      case '3':
        $webuketuke = $this->_model->getSearchCancel(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow],
          $request
        );
        $count = $this->_model->getCountSearchCancel(
          ['iraimoto_cd'=>Auth::user()->tokuisaki_cd],
          $request
        );
        break;
      default:
        # code...
        break;
    }
    $total = isset($count[0]->cnt)?$count[0]->cnt:0;
    $parameter = http_build_query($request->all());
    $backUrl = route('webuketuke.showSearch') . '?' . $parameter;
    Session::put('search.preURL',route('webuketuke.search') . '?' . $parameter);
    $pagination = $this->pagination(count($webuketuke),$total, '?' . $parameter);
    $countStart= (count($webuketuke) == 0)  ? count($webuketuke) : ($startrow + 1);
    $countNext = (count($webuketuke) == 10) ? $endrow : $total;
    $countPage = ['countPage'=>$countStart.'〜'.$countNext];
    self::logActivity();
    return view('pages.webuketuke.kekka',[
      'countPage' =>$countPage,
      'webuketuke' => $webuketuke,
      'total' => $total,
      'pagination' => $pagination,
      'backUrl' => $backUrl
    ]);
  }

  /**
   * 詳細アクション開始
   */
  public function detail($webno, Request $request)
  {
    $detail = [];
    $webuketuke = $this->_model->getUeketukeByWebNo(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'web_no'=>$webno]);
    //if(count($webuketuke) == 0){ return abort(501); }

    if(count($webuketuke) == 0){
      $getKakuhou = $this->_model->getkakuhouUketukeNo(['web_no'=>$webno]);
    }else{
      $getKakuhou = $this->_model->getkakuhouUketukeNo(['uketuke_no'=>$webuketuke[0]->uketuke_no]);
    }
    
    // 確報の出力
    if(count($getKakuhou) > 0){
      // 確報の出力
      $detail['data_kbn'] = "確報";
      $kakuhou = $this->_model->getKakuhou(['uc_cd'=>$getKakuhou[0]->uc_cd,'uketuke_no'=>$getKakuhou[0]->uketuke_no]);

      if(count($kakuhou)){

        //依頼情報のセット
        $detail['syuritaisyo_seq'] = "01";//修理対象SEQ
        $detail['uketukebi'] = $kakuhou[0]->uketukebi;//登録日（受付日）
        $detail['uketuke_no'] = $kakuhou[0]->uketuke_no;//受付No
        $detail['ce_sei'] = $kakuhou[0]->ce_sei;//担当者名
        $detail['ce_cd'] = $kakuhou[0]->ce_cd;//担当者コード

        if ($kakuhou[0]->mikanriyu_cd != null && substr($kakuhou[0]->mikanriyu_cd,0,1) == "1") { //結果報告
          $detail['kekka'] = "完了";
        } else if ($kakuhou[0]->mikanriyu_cd != null && substr($kakuhou[0]->mikanriyu_cd,0,1) == "2") {
          $detail['kekka'] = "未完了";
        } else if ($kakuhou[0]->mikanriyu_cd != null && substr($kakuhou[0]->mikanriyu_cd,0,1) == "3") {
          $detail['kekka'] = "キャンセル";
        } else {
          $detail['kekka'] = "";
        }

        if($kakuhou[0]->kaisi_jikoku != null && $kakuhou[0]->kaisi_jikoku != "0000"){
          $detail['houmonbi']= $kakuhou[0]->homonbi . " " . substr($kakuhou[0]->kaisi_jikoku,0,2) . ":" . substr($kakuhou[0]->kaisi_jikoku,2,4);
        } else {
          $detail['houmonbi'] = $kakuhou[0]->homonbi;
        }

        //2018.12.06 MOD START	//グローエ、災害時特別対応
        $detail['sekyu_naiyou'] = $this->setSeikyuNaiyo($kakuhou[0]->yumuryo_kbn,$kakuhou[0]->sekinin_kbn,$kakuhou[0]->mikanriyu_cd,$kakuhou[0]->kanryo_flg,$kakuhou[0]->iraimoto_cd,$kakuhou[0]->seikyusaki_cd,$kakuhou[0]->hisai_kb);//請求内容
        //2018.12.06 MOD END	
        $detail['syuri_naiyou'] = $kakuhou[0]->kanmi_hokoku_bunsyo;//修理内容

        //顧客情報のセット
        $detail['kokyaku_mei'] = $this->setName($kakuhou[0]->kokyaku_sei,$kakuhou[0]->kokyaku_na);
        $detail['kokyaku_jusyo'] = $kakuhou[0]->kokyaku_jusyo;
        $detail['kokyaku_katagaki'] = $kakuhou[0]->kokyaku_katagaki;
        $detail['kokyaku_tel_haihun'] = $kakuhou[0]->kokyaku_tel;

        //商品名の取得・セット
        $meisai = $this->_model->getMesai(['uc_cd'=>$getKakuhou[0]->uc_cd,'uketuke_no'=>$kakuhou[0]->uketuke_no,'uketuke_nendo'=>$kakuhou[0]->uketuke_nendo]);
        if(count($meisai) > 0){
          $detail['hinmei'] = $meisai[0]->hinmei;
          $detail['syohin_hinban'] = $meisai[0]->syohin_hinban;
        } else {
          $detail['hinmei'] = '';
          $detail['syohin_hinban'] = '';
        }

        //依頼元情報のセット
        $detail['irainaiyo'] = $kakuhou[0]->irainaiyo;// ご依頼内容
        $detail['iraimoto_cd'] = $kakuhou[0]->iraimoto_cd;// ご依頼元コード
        $detail['iraimoto_simei'] = $kakuhou[0]->iraimoto_simei;//ご依頼元氏名
        $detail['iraimoto_tanto'] = $this->setName($kakuhou[0]->iraimoto_tanto,'');//ご依頼元担当
        $detail['iraimoto_jusyo'] = $kakuhou[0]->iraimoto_jusyo;// ご依頼元住所
        $detail['iraimoto_tel_haihun'] = $kakuhou[0]->iraimoto_tel;// ご依頼元ＴＥＬ
        $detail['iraimoto_fax'] = $kakuhou[0]->iraimoto_fax;// ご依頼元ＦＡＸ
        $detail['koziten'] = $kakuhou[0]->koziten;// 工事店名
        $detail['tyumon_bango'] = $kakuhou[0]->tyumon_bango;// 注文番号
        $detail['hm_kokyaku_no'] = $kakuhou[0]->hm_kokyaku_no; //31.お得意様顧客Ｎｏ
        $detail['okyakusamakiji'] = $kakuhou[0]->sagyo_naiyo; //38.お客様記事
        $detail['iraimotokiji'] = $kakuhou[0]->iraimoto_kiji; //39.お客様記事

        //請求先情報のセット
        $seikyu = array();
        if(($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041") ||
            $kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995"){

          $detail['seikyusaki_cd'] = '';
          $detail['seikyusaki_simei'] = '';
          $detail['seikyusaki_tanto'] = '';//請求先担当
          $detail['seikyusaki_jusyo'] = '';// ご依頼時請求先住所
          $detail['seikyusaki_tel_haihun'] = '';// ご依頼時請求先ＴＥＬ
          $detail['seikyusaki_fax'] = '';// ご依頼時請求先ＦＡＸ
        } else {
          $detail['seikyusaki_cd'] = $kakuhou[0]->seikyusaki_cd;
          $detail['seikyusaki_simei'] = $this->setName($kakuhou[0]->seikyusaki_simei,'');
          $detail['seikyusaki_tanto'] = $this->setName($kakuhou[0]->seikyusaki_tanto,'');//請求先担当
          $detail['seikyusaki_jusyo'] = $kakuhou[0]->seikyusaki_jusyo;// ご依頼時請求先住所
          $detail['seikyusaki_tel_haihun'] = $kakuhou[0]->seikyusaki_tel;// ご依頼時請求先ＴＥＬ
          $detail['seikyusaki_fax'] = $kakuhou[0]->seikyusaki_fax;// ご依頼時請求先ＦＡＸ
        }
        //部品情報の取得・セット SQL実行
        $buhin = $this->_model->getBuhin(['uketuke_no'=>$kakuhou[0]->uketuke_no,'uketuke_nendo'=>$kakuhou[0]->uketuke_nendo]);
        $recCnt = 0;
        $ta_buhin_dai_kei=0;
        $hinban = '';
        $buhinmei = '';
        $tanka = '';
        $kosuu = '';
        $kingaku = '';

        foreach ($buhin as $value) {

          if($recCnt > 3){
            if ($value->buhindai > 0) {
              if ($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041") {
                # code...

              }elseif ($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995") {
                # code...
              }else {
                $ta_buhin_dai_kei += $value->buhindai;
              }
            }
          } else {
            //品番
            $hinban = $value->hinban;
            //品名
            $buhinmei = $value->hinmei == null ? $value->hinban : $value->hinmei;
            // 数量
            $kosuu = $value->suryo;
            // 単価,部品代
            $kingaku = $value->buhindai;
            if($kingaku > 0) {
              if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041"){
                $tanka = "0";
                $kingaku = "0";
                //「受付依頼元の依頼元CD="10995"でなく、かつ、売上請求先の請求先CD="10995"である」
              } else if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995") {
                $tanka = "0";
                $kingaku = "0";
              } else {
                $tanka = $value->tanka;
              }
            } else {
              $tanka = "0";
              $kingaku = "0";
            }
          }
          if($recCnt <= 3){
            $detail['hinban'.($recCnt+1)] = $hinban;
            $detail['buhinmei'.($recCnt+1)] = $buhinmei;
            $detail['tanka'.($recCnt+1)] = $tanka;
            $detail['kosuu'.($recCnt+1)] = $kosuu;
            $detail['kingaku'.($recCnt+1)] = $kingaku;
          }
          $recCnt++;
        }
        if($recCnt > 4){
          $detail['buhinmei5'] = 'その他部品代';
          $detail['kingaku5'] = $ta_buhin_dai_kei;
        }
      }
      //「受付依頼元の依頼元CD="10995"でなく、かつ、売上請求先の請求先CD="10995"である」
      if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "93041" && $kakuhou[0]->iraimoto_cd != "93041") {
        $detail['kan_mikan'] = '完了（無料）';
        $this->setZero($detail);
      } else if($kakuhou[0]->seikyusaki_cd != null && $kakuhou[0]->iraimoto_cd != null && $kakuhou[0]->seikyusaki_cd == "10995" && $kakuhou[0]->iraimoto_cd != "10995"){
        $detail['kan_mikan'] = '完了（無料）';
        $this->setZero($detail);//部品代計〜合計に０をセット
      } else {
        $goukeigaku = 0;
        $yuryokin = $this->_model->getYuryokin(['uketuke_no'=>$kakuhou[0]->uketuke_no,'uketuke_nendo'=>$kakuhou[0]->uketuke_nendo]);
        if(count($yuryokin)){
          $detail['buhindaikei'] = $yuryokin[0]->buhindai_kei;				// 部品代計
          $detail['gijuturyou'] = $yuryokin[0]->gijuturyo_kei;				// 技術料
          $detail['syutyohi'] = $yuryokin[0]->syutyoryo_kei;				// 出張費
          $detail['syobunryou'] = $yuryokin[0]->syobunryo_kei;				// 処分料
          $detail['nebiki'] = $yuryokin[0]->nebiki_kei;				// 値引き
          $detail['zei'] = $yuryokin[0]->syohizei_kei;				// 消費税
          $goukeigaku = $yuryokin[0]->buhindai_kei + $yuryokin[0]->gijuturyo_kei + $yuryokin[0]->syutyoryo_kei + $yuryokin[0]->syobunryo_kei + $yuryokin[0]->nebiki_kei + $yuryokin[0]->syohizei_kei;
          $detail['goukei'] = $goukeigaku;
        } else {
          $this->setZero($detail);//部品代計〜合計に０をセット
        }
        if($goukeigaku > 0){
          $detail['kan_mikan'] = "完了（有料）";
        } else {
          $detail['kan_mikan'] = "完了（無料）";
        }
      }

      //調整部位情報の取得・セット
      $recCnt2 = 0;
      $tempBui = "";
      $tempSyotiNaiyo = "";
      $bui = "";
      $syotiNaiyo = "";
      $tyosei = $this->_model->getTyosei(['uketuke_no'=>$kakuhou[0]->uketuke_no,'uketuke_nendo'=>$kakuhou[0]->uketuke_nendo]);
      foreach($tyosei as $value) {
        $bui = $value->bui == null ? '' : $value->bui;
        $syotiNaiyo = $value->syoti_naiyo == null ? '' : $value->syoti_naiyo;
        if($recCnt2 > 2){
          break;
        }
        if ($value->syoti_naiyo != null && $value->syoti_naiyo == "共同（２名以上）作業の補助（補助のＣＥが記入）") {
          $syotiNaiyo = " ";
        }
        if($recCnt2 == 0){
          // 1件目の場合
          $detail['tyosei_bui1'] = $bui;
          $detail['syoti_naiyo1'] = $syotiNaiyo;
          $recCnt2++;
        } elseif ($recCnt2 > 0 && ($bui != $tempBui || $syotiNaiyo != $tempSyotiNaiyo)) {
          // 上記以外の場合
          if($recCnt2 == 1){
            $detail['tyosie_bui2'] = $bui;
            $detail['syoti_naiyo2'] = $syotiNaiyo;
          }
          $recCnt2++;
        }
        $tempBui = $bui;
        $tempSyotiNaiyo = $syotiNaiyo;
      }
      $detail['web_uketuke_no'] = $webno;
      $mode = isset($request->mode)?'?mode='.$request->mode:'';
      return view('pages.webuketuke.kakuhou',['detail' => $detail,'mode'=>$mode]);

    } else {
      // SQL実行（速報の判別）

      if(count($webuketuke) == 0){      
        $sokuhou = $this->_model->getSokuhou(['uc_cd'=>$webuketuke[0]->uc_cd, 'web_no'=>$webno]);
      }else{
        $sokuhou = $this->_model->getSokuhou(['uc_cd'=>$webuketuke[0]->uc_cd, 'uketuke_no'=>$webuketuke[0]->uketuke_no]);
      }
      if(count($sokuhou) > 0){
        // 速報の出力

        $detail['data_kbn'] = "速報";

        //依頼情報のセット
        $detail['syuritaisyo_seq'] = $sokuhou[0]->syuritaisyo_seq;//修理対象SEQ
        $detail['uketukebi'] = $sokuhou[0]->uketukebi;//受付日
        $detail['uketuke_no'] = $this->setUketukeNo($sokuhou[0]->uketuke_no);//受付No
        $detail['ce_cd'] = $sokuhou[0]->ce_cd;//担当者コード
        $detail['ce_sei'] = $sokuhou[0]->ce_sei;//担当者名

        if ($sokuhou[0]->mikanriyu_cd != null && substr($sokuhou[0]->mikanriyu_cd,0,1) == "1") { //結果報告
          $detail['kekka'] = "完了";
        } else if ($sokuhou[0]->mikanriyu_cd != null && substr($sokuhou[0]->mikanriyu_cd,0,1) == "2") {
          $detail['kekka'] = "未完了";
        } else if ($sokuhou[0]->mikanriyu_cd != null && substr($sokuhou[0]->mikanriyu_cd,0,1) == "3") {
          $detail['kekka'] = "キャンセル";
        } else {
          $detail['kekka'] = "";
        }

        $detail['houmonbi'] = $sokuhou[0]->syuri_jisekibi;//訪問実施日
        //  2018.12.06 MOD START	//グローエ、災害時特別対応
        $detail['sekyu_naiyou'] = $this->setSeikyuNaiyo($sokuhou[0]->yumuryo_kbn,"",$sokuhou[0]->mikanriyu_cd,$sokuhou[0]->kanryo_flg,$sokuhou[0]->iraimoto_cd,"",$sokuhou[0]->hisai_kb);//請求内容
        //	2018.12.06 MOD END	
        $detail['syuri_naiyou'] = $sokuhou[0]->kanmi_hokoku_bunsyo;//修理内容
        $detail['hm_kokyaku_no'] = $sokuhou[0]->hm_kokyaku_no; // お得意先顧客No
        //顧客情報のセット
        $detail['kokyaku_mei'] = $this->setname($sokuhou[0]->kokyaku_sei,$sokuhou[0]->kokyaku_na);//顧客名
        $detail['kokyaku_jusyo'] = $sokuhou[0]->kokyaku_jusyo;//お客様住所
        $detail['kokyaku_katagaki'] = $sokuhou[0]->kokyaku_katagaki;//マンション名
        $detail['kokyaku_tel_haihun'] = $sokuhou[0]->kokyaku_tel;//顧客TEL

        //商品情報のセット
        $detail['hinmei'] = $sokuhou[0]->hinmei;// 商品名
        $detail['syohin_hinban'] = $sokuhou[0]->syohin_hinban;// 品番

        //依頼元情報のセット
        $detail['irainaiyo'] = $sokuhou[0]->irainaiyo;//ご依頼内容
        $detail['iraimoto_cd'] = $sokuhou[0]->iraimoto_cd;//ご依頼元コード
        $detail['iraimoto_simei'] = $sokuhou[0]->iraimoto_simei;//ご依頼元氏名
        $detail['iraimoto_tanto'] = $this->setname($sokuhou[0]->iraimoto_tanto,"");//ご依頼元担当
        $detail['iraimoto_jusyo'] = $sokuhou[0]->iraimoto_jusyo;//ご依頼元住所
        $detail['iraimoto_tel_haihun'] = $sokuhou[0]->iraimoto_tel;//ご依頼元ＴＥＬ
        $detail['iraimoto_fax'] = $sokuhou[0]->iraimoto_fax;//ご依頼元ＦＡＸ
        $detail['koziten'] = $sokuhou[0]->koziten;//工事店名
        $detail['tyumon_bango'] = $sokuhou[0]->tyumon_bango;//注文番号

        //請求先情報のセット
        $detail['seikyusaki_cd'] = "";
        $detail['seikyusaki_simei'] = "";
        $detail['seikyusaki_tanto'] = "";//請求先担当
        $detail['seikyusaki_jusyo'] = "";// ご依頼時請求先住所
        $detail['seikyusaki_tel_haihun'] = "";// ご依頼時請求先ＴＥＬ
        $detail['seikyusaki_fax'] = "";// ご依頼時請求先ＦＡＸ

        //部品情報のセット
        $detail['kan_mikan'] = "";
        $detail['hinban1'] = "";// 部品品番
        $detail['buhinmei1'] = "";// 部品名
        $detail['tanka1'] = "";// 単価
        $detail['kosuu1'] = "";//個数
        $detail['kingaku1'] = "";//金額

        $this->setZero($detail);// 部品代計〜合計に０をセット

        //調整部位情報のセット
        $detail['tyosei_bui1'] = "";// 処置部位
        $detail['syoti_naiyo1'] = "";// 処理内容
        $mode = isset($request->mode)?'?mode='.$request->mode:'';
        $detail['web_uketuke_no'] = $webno;
        return view('pages.webuketuke.sokuhou',['detail'=>$detail,'mode'=>$mode]);
      } else {
        // 訪問予定の出力
        $yotei = $this->_model->getYotei(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no,'seq'=>$webuketuke[0]->seq]);

        $detail['data_kbn'] = "速報";
        //依頼情報のセット
        $detail['syuritaisyo_seq'] = $yotei[0]->syuritaisyo_seq;//修理対象SEQ
        $detail['uketukebi'] = $yotei[0]->uketukebi;//受付日
        $detail['uketuke_no'] = $yotei[0]->uketuke_no;//受付No
        $detail['ce_cd'] = $yotei[0]->ce_cd;//担当者コード
        $detail['ce_sei'] = $yotei[0]->ce_sei;//担当者名
        $detail['kekka'] = "";//結果報告
        $detail['houmonbi'] = "";//訪問実施日
        $detail['sekyu_naiyou'] = "";//請求内容
        $detail['syuri_naiyou'] = "";//修理内容

        // 訪問予定情報
        $detail['renrakubi'] = $yotei[0]->renrakubi; // 連絡日
        $detail['renraku_naiyo'] = $yotei[0]->renraku_naiyo; // 連絡内容
        $detail['syuri_yoteibi'] = $yotei[0]->syuri_yoteibi; // 連絡予定日
        $detail['yotei_jikan'] = '';// 予定時間
        if($yotei[0]->syuri_yoteibi != null && $yotei[0]->yotei_jikan != null && $yotei[0]->yotei_jikan != ""){
          $yotei_jikan = $yotei[0]->yotei_jikan;
          if($yotei_jikan == "0000"){
            $detail['yotei_jikan'] = "１日";
          } elseif ($yotei_jikan == "1111"){
            $detail['yotei_jikan'] = "午前";
          } elseif ($yotei_jikan == "2222") {
            $detail['yotei_jikan'] = "午後";
          } else {
            $detail['yotei_jikan'] = substr($yotei_jikan,0,2) . ":" . substr($yotei_jikan,2,4);
          }
        }

        //顧客情報のセット
        $detail['kokyaku_mei'] = $this->setName($yotei[0]->kokyaku_sei,$yotei[0]->kokyaku_na);//顧客名
        $detail['kokyaku_jusyo'] = $yotei[0]->kokyaku_jusyo;// お客様住所
        $detail['kokyaku_katagaki'] = $yotei[0]->kokyaku_katagaki;// マンション名
        $detail['kokyaku_tel_haihun'] = $yotei[0]->kokyaku_tel_haihun;// 顧客TEL
        $detail['hinmei'] = $yotei[0]->hinmei;// 商品名
        $detail['syohin_hinban'] = $yotei[0]->syohin_hinban;// 品番

        //請求先情報の取得・セット
        $detail['irainaiyo'] = $yotei[0]->irainaiyo; // ご依頼内容
        $uIraimoto = $this->_model->getUketukeIraimoto(['uc_cd'=>$webuketuke[0]->uc_cd, 'uketuke_no'=>$webuketuke[0]->uketuke_no,'uketuke_nendo'=>$yotei[0]->uketuke_nendo]);
        if(count($uIraimoto) > 0){
          $detail['iraimoto_cd'] = $uIraimoto[0]->iraimoto_cd;// ご依頼元コード
          $detail['iraimoto_simei'] = $uIraimoto[0]->iraimoto_simei;// ご依頼元氏名
          $detail['iraimoto_tanto'] = $this->setName($uIraimoto[0]->iraimoto_tanto,"");
          $detail['iraimoto_jusyo'] = $uIraimoto[0]->iraimoto_jusyo;// ご依頼元住所
          $detail['iraimoto_tel_haihun'] = $uIraimoto[0]->iraimoto_tel_haihun;// ご依頼元ＴＥＬ
          $detail['iraimoto_fax'] = $uIraimoto[0]->iraimoto_fax;// ご依頼元ＦＡＸ
          $detail['koziten'] = $yotei[0]->koziten;// 工事店名
          $detail['tyumon_bango'] = $uIraimoto[0]->tyumon_bango;// 注文番号
        } else {
          $detail['iraimoto_cd'] = '';// ご依頼元コード
          $detail['iraimoto_simei'] = '';// ご依頼元氏名
          $detail['iraimoto_tanto'] = '';
          $detail['iraimoto_jusyo'] = '';// ご依頼元住所
          $detail['iraimoto_tel_haihun'] = '';// ご依頼元ＴＥＬ
          $detail['iraimoto_fax'] = '';// ご依頼元ＦＡＸ
          $detail['koziten'] = '';// 工事店名
          $detail['tyumon_bango'] = '';// 注文番号
        }
        //請求先情報のセット
        $detail['seikyusaki_cd'] = $yotei[0]->seikyusaki_cd;
        $detail['seikyusaki_simei'] = $yotei[0]->seikyusaki_simei;
        $detail['seikyusaki_tanto'] = $yotei[0]->seikyusaki_tanto;// ご依頼時担当者様
        $detail['seikyusaki_jusyo'] = $yotei[0]->seikyusaki_jusyo;// ご依頼時請求先住所
        $detail['seikyusaki_tel_haihun'] = $yotei[0]->seikyusaki_tel_haihun;// ご依頼時請求先ＴＥＬ
        $detail['seikyusaki_fax'] = $yotei[0]->seikyusaki_fax;// ご依頼時請求先ＦＡＸ

        //部品情報のセット
        $detail['kan_mikan'] = '';
        $detail['hinban1'] = '';// 部品品番
        $detail['buhinmei1'] = '';// 部品名
        $detail['tanka1'] = '';// 単価
        $detail['kosuu1'] = '';// 個数
        $detail['kingaku1'] = '';// 金額
        // 部品代計〜合計に０をセット
        $this->setZero($detail);
        //調整部位情報のセット
        $detail['tyosei_bui1'] = '';// 処置部位
        $detail['syoti_naiyo1'] = '';// 処理内容
        $mode = isset($request->mode)?'?mode='.$request->mode:'';
        return view('pages.webuketuke.homonyotei',['detail'=>$detail,'mode'=>$mode]);
      }
    }

    switch ($webuketuke[0]->jyotai) {
      case '未着手':
        $yotei = $this->_model->getYotei(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no,'seq'=>$webuketuke[0]->seq]);
        $iraimoto = $this->_model->getUketukeIraimoto(
          [
            'uc_cd' => $webuketuke[0]->uc_cd,
            'uketuke_no' => $webuketuke[0]->uketuke_no,
            'uketuke_nendo' => $yotei[0]->uketuke_nendo
          ]
        );
        return view('pages.webuketuke.homonyotei',['yotei'=>$yotei[0],'iraimoto'=>$iraimoto[0]]);
        break;
      case '完了':
        $kakuhou = $this->_model->getKakuhou(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no]);
        return view('pages.webuketuke.kakuhou',['kakuhou'=>isset($kakuhou[0])?$kakuhou[0]:array()]);
        break;
      default:
        $sokuhou = $this->_model->getSokuhou(['uc_cd'=>$webuketuke[0]->uc_cd,'uketuke_no'=>$webuketuke[0]->uketuke_no]);
        return view('pages.webuketuke.sokuhou',['sokuhou'=>isset($sokuhou[0])?$sokuhou[0]:array()]);
        break;
    }
  }

  /**
   * ご依頼中の修理の状況[依頼内容の確認]
   */
  public function kakunin($webno,Request $request)
  {
    $webuketuke = DB::table('WEB_UKETUKE')
                    ->leftJoin('IRAIUKETUKE', 'IRAIUKETUKE.uketuke_no', '=', 'WEB_UKETUKE.uketuke_no')
                    ->select('WEB_UKETUKE.*','IRAIUKETUKE.kanryo_flg')
                    ->where('WEB_UKETUKE.web_uketuke_no','=',$webno)
                    ->where('WEB_UKETUKE.iraimoto_cd', '=', Auth::user()->tokuisaki_cd)
                    ->first();
    $mode = isset($request->mode)?$request->mode:false;
    if($webuketuke == null || count($webuketuke) == 0) {

      //uketuke_iraimotoを参照できる場合は表示する。
      $uketuke_iraimoto = DB::table('WEB_UKETUKE')
      ->join('UKETUKE_IRAIMOTO', 'UKETUKE_IRAIMOTO.uketuke_no', '=', 'WEB_UKETUKE.uketuke_no')
      ->select('WEB_UKETUKE.*')
      ->where('WEB_UKETUKE.web_uketuke_no','=',$webno)
      ->where('UKETUKE_IRAIMOTO.iraimoto_cd', '=', Auth::user()->tokuisaki_cd)
      ->first();

      //web_uketuke.iraimoto_cd, uketuke_iraimoto.iraimoto_cd一致が存在しない場合は、申し込みNo.が見つからないと返す。（パラメタを変えた不正アクセス防止）
      if($uketuke_iraimoto == null || count($uketuke_iraimoto) == 0) {
        $webuketuke = false;
        return view('pages.webuketuke.kakunin',compact('webuketuke','mode'));
      
      } else {

        $webuketuke = DB::table('WEB_UKETUKE')
        ->leftJoin('IRAIUKETUKE', 'IRAIUKETUKE.uketuke_no', '=', 'WEB_UKETUKE.uketuke_no')
        ->select('WEB_UKETUKE.*','IRAIUKETUKE.kanryo_flg')
        ->where('WEB_UKETUKE.web_uketuke_no','=',$webno)
        ->first();
     
      }
    }
    // Get all files by web_uketuke_no
    $files = $this->_model->getAllFiles($webno);
    // ブランド名を取得
    $brand = \App\Brand::find($webuketuke->brand_cd);
    $webuketuke->brand_mei = isset($brand->brand_mei)?$brand->brand_mei:'';
    // 顧客都道府県名を取得
    $ken = \App\YubinBango::find($webuketuke->kokyaku_yubin);
    $webuketuke->kokyaku_ken_mei = isset($ken->ken_mei)?$ken->ken_mei:'';
    // ご請求先都道府県名を取得
    if($webuketuke->yuryo_seikyu_kbn == "2"){
      $seiKen = \App\YubinBango::find($webuketuke->seikyusaki_yubin);
      $webuketuke->seikyusaki_ken_mei = isset($seiKen->ken_mei)?$seiKen->ken_mei:'';
    }
    // メール送信フラグを取得
    $webuketuke_mail = \App\WebUketukeMail::where('WEB_UKETUKE_NO','=',$webno)->first();
    $webuketuke->mail_sousin = (isset($webuketuke_mail->sokuho_mail) && $webuketuke_mail->sokuho_mail == '1') ? '「結果・経過速報」  ' : '';
    $webuketuke->mail_sousin .= (isset($webuketuke_mail->uketuke_mail) && $webuketuke_mail->uketuke_mail == '1') ? '「受付確認」' : '';

    return view('pages.webuketuke.kakunin',compact('webuketuke','mode','files'));
  }

  public function kakuninPrint($webno)
  {
    $webuketuke = $this->_model->where('web_uketuke_no','=',$webno)
                        ->where('iraimoto_cd', '=', Auth::user()->tokuisaki_cd)
                        ->first();
    if($webuketuke == null || count($webuketuke) == 0) {
      $webuketuke = false;
      return view('pages.webuketuke.kakuninPrint',compact('webuketuke'));
    }
    // ブランド名を取得
    $brand = \App\Brand::find($webuketuke->brand_cd);
    $webuketuke->brand_mei = isset($brand->brand_mei)?$brand->brand_mei:'';
    // 顧客都道府県名を取得
    $ken = \App\YubinBango::find($webuketuke->kokyaku_yubin);
    $webuketuke->kokyaku_ken_mei = isset($ken->ken_mei)?$ken->ken_mei:'';
    // ご請求先都道府県名を取得
    if($webuketuke->yuryo_seikyu_kbn == "2"){
      $seiKen = \App\YubinBango::find($webuketuke->seikyusaki_yubin);
      $webuketuke->seikyusaki_ken_mei = isset($seiKen->ken_mei)?$seiKen->ken_mei:'';
    }
    // メール送信フラグを取得
    $webuketuke_mail = \App\WebUketukeMail::where('WEB_UKETUKE_NO','=',$webno)->first();
    $webuketuke->mail_sousin = (isset($webuketuke_mail->sokuho_mail) && $webuketuke_mail->sokuho_mail == '1') ? '「結果・経過速報」  ' : '';
    $webuketuke->mail_sousin .= (isset($webuketuke_mail->uketuke_mail) && $webuketuke_mail->uketuke_mail == '1') ? '「受付確認」' : '';
    // Get all files by web_uketuke_no
    $files = $this->_model->getAllFiles($webno);
    return view('pages.webuketuke.kakuninPrint',compact('webuketuke','files'));
  }

  public function index(Request $request)
  {
    $input = array();
    // get mode from url
    // $input['mode'] = isset($request->mode)?$request->mode:'all';
    $input['mode'] = isset($request->mode)?$request->mode:'mikanryo';
    // get page number
    $input['page'] = isset($request->page)?$request->page:1;
    // set start row
    $startrow = ($input['page']-1) * 10;
    // set end row
    $endrow = $startrow + 10;

    $webuketuke = array();
    $count = 0;
    // 受付中カウント
    $uketukechu = $this->_model->getCountUKETUKECHU(['iraimoto_cd'=>Auth::user()->tokuisaki_cd]);
    // 完了カウント
    $totalKanryo = $this->_model->getCountKanryo(['iraimoto_cd'=>Auth::user()->tokuisaki_cd]);

    switch ($input['mode']) {
      // 未完了部分
      case 'mikanryo':
        $webuketuke = $this->_model->getMikanryoList(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow]);
        $count = $this->_model->getCountMikanryo(['iraimoto_cd'=>Auth::user()->tokuisaki_cd] );
        break;
      // 完了部分
      case 'kanryo':
        $webuketuke = $this->_model->getKanryoList(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow]);
        $count = $totalKanryo;
        break;
      // キャンセル部分
      case 'cancel':
        $webuketuke = $this->_model->getCancelList(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow]);
        $count = $this->_model->getCountCancel(['iraimoto_cd'=>Auth::user()->tokuisaki_cd] );
        break;
      // 全て
      default:
        // $webuketuke = $this->_model->getAllList(['iraimoto_cd'=>Auth::user()->tokuisaki_cd,'startrow'=>$startrow,'endrow'=>$endrow]);
        // $count = $this->_model->getCountAll(['iraimoto_cd'=>Auth::user()->tokuisaki_cd] );
        break;
    }

    // Total records
    $total = isset($count[0]->cnt)?$count[0]->cnt:0;
    $totalUketukechu = isset($uketukechu[0]->cnt)?$uketukechu[0]->cnt:0;
    $totalKanryo = isset($totalKanryo[0]->cnt)?$totalKanryo[0]->cnt:0;
    $pagination = $this->pagination(count($webuketuke),$total,'?mode=' . $input['mode']);
    $countStart= (count($webuketuke) == 0)  ? count($webuketuke) : ($startrow + 1);
    $countNext = (count($webuketuke) == 10) ? $endrow : $total;
    $countPage = ['countPage'=>$countStart.'〜'.$countNext];
    self::logActivity();
    return view('pages.webuketuke.index',compact('countPage','webuketuke','input','pagination','total','totalUketukechu','totalKanryo'));
  }

  private function pagination ($collection,$total,$mode = '')
  {
    // 環境を判断で、HTTPかHTTPSにする
    if(env('APP_ENV') === 'production' || env('APP_ENV') === 'staging'){
      $path = secure_url(\Request::path()) . $mode;
    } else {
      $path = '/' . \Request::path() . $mode;
    }

    $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
        range(1, $collection), //a fake range of total items, you can use range(1, count($collection))
        $total, //Total
        10, //items per page
        \Illuminate\Pagination\Paginator::resolveCurrentPage(), //resolve the path
        ['path' => $path]
    );

    return $paginator->render();
  }

  /**
   * 請求区分の編集
   */
  //2018.12.06 MOD START //グローエ、災害時特別対応
  private function setSeikyuNaiyo($seikyu_kbn,$sekinin_kbn,$kanmi_kbn,$kanryo_kbn,$iraimoto_cd,$seikyusaki_cd,$hisai_kb)
  //2018.12.06 MOD END
  {
		if($seikyu_kbn == null)
		{
			$seikyu_kbn = "";
		}
		if($sekinin_kbn == null)
		{
			$sekinin_kbn = "";
		}
		if($kanmi_kbn == null)
		{
			$kanmi_kbn = "";
		}

		if( $kanryo_kbn != null && ( $kanryo_kbn == "8" || $kanryo_kbn == "9" ) ) {
        //「受付依頼元の依頼元CD="10995"でなく、かつ、売上請求先の請求先CD="10995"である」
      	if($iraimoto_cd != null && $seikyusaki_cd != null && $seikyusaki_cd == "93041" && $iraimoto_cd != "93041"){
					$seikyu_kbn =  "無料" ;
				} else if($iraimoto_cd != null && $seikyusaki_cd != null && $seikyusaki_cd == "10995" && $iraimoto_cd != "10995"){
	        		$seikyu_kbn =  "無料" ;
				} else {

            /*2018.12.06 ADD START	グローエ、災害時特別対応*/
            if($hisai_kb != "0"){
              $seikyu_kbn = $hisai_kb;
            }else{
            /*2018.12.06 ADD START	グローエ、災害時特別対応*/
					if ( $seikyu_kbn != null && $seikyu_kbn =="A" ) {
							$seikyu_kbn =  "有料" ;
					} else if ( $seikyu_kbn != null && $seikyu_kbn == "B" && $kanmi_kbn != "1H" ) {
							$seikyu_kbn =  "無料" ;
					} else if ( $seikyu_kbn != null && $seikyu_kbn == "C") {
						$seikyu_kbn =  "有料直収" ;
					} else if ( $seikyu_kbn != null && $seikyu_kbn == "D") {
						$seikyu_kbn =  "有料施主様請求" ;
					} else if ( $seikyu_kbn != null && $seikyu_kbn == "E") {
						$seikyu_kbn =  "有料施主様以外請求" ;
					} else if ( $seikyu_kbn != null && $seikyu_kbn == "F") {
						$seikyu_kbn =  "有料依頼元請求" ;
					} else if ( $seikyu_kbn != null && $iraimoto_cd != null  && $seikyu_kbn == "G" && substr($iraimoto_cd,0,2) == "18" ) {
						$seikyu_kbn =  "有料施主様以外請求" ;
					} else if ( $seikyu_kbn != null && $iraimoto_cd != null  && $seikyu_kbn == "G" && substr($iraimoto_cd,0,2) != "18" ) {
						$seikyu_kbn =  "無料" ;
					} else if ( $seikyu_kbn != null && $seikyu_kbn == "H") {
						$seikyu_kbn =  "無料" ;
					} else {
						$seikyu_kbn =  "";
					}
				}
			}
		} else {
			$seikyu_kbn =  "";
		}
		return $seikyu_kbn;
	}

  //氏名の編集
	private function setName($sei,$mei)
  {

		if($sei == null)
		{
			$sei = "";
		}
		if($mei == null)
		{
			$mei = "";
		}

		if ( $sei != "" && $mei != "" ) {
			$sei =  $sei . " " . $mei . " 様" ;
		} else if ( $sei != "" && $mei == "" ) {
			$sei =   $sei . " 様" ;
		} else if ( $sei == "" && $mei != "" ) {
			$sei =   $mei . " 様" ;
		} else {
			$sei =   "";
		}

		return $sei;
	}

  //部品代計〜合計に０を編集
	private function setZero(&$detail)
  {
		$detail['buhindaikei'] = "0";				// 部品代計
		$detail['gijuturyou'] = "0";				// 技術料
		$detail['syutyohi'] = "0";				// 出張費
		$detail['syobunryou'] = "0";				// 処分料
		$detail['nebiki'] = "0";				// 値引き
		$detail['zei'] = "0";				// 消費税
		$detail['goukei'] = "0";				// 合計

	}

  //結果の編集
	private function setKekka($kanmi_kbn)
  {

		if ( $kanmi_kbn != null && substr($kanmi_kbn,0,1) == "1") {
			$kanmi_kbn = "完了" ;
		} else if ( $kanmi_kbn != null && substr($kanmi_kbn,0,1) == "2" ) {
			$kanmi_kbn = "未完了" ;
		} else if ( $kanmi_kbn != null && substr($kanmi_kbn,0,1) == "3" ) {
			$kanmi_kbn = "キャンセル" ;
		} else {
			$kanmi_kbn = "" ;
		}
		return $kanmi_kbn;
	}

  //受付Noの編集
	private function setUketukeNo($UketukeNo)
  {

		if ( $UketukeNo != null && strlen($UketukeNo) == 8 ) {
			$UketukeNo =  substr($UketukeNo,0,4) . "-" . substr($UketukeNo,4,8) ;
		} else if ( $UketukeNo != null && strlen($UketukeNo) == 12 ) {
			$UketukeNo =  substr($UketukeNo,0,4) . "-" . substr($UketukeNo,4,8) . "-" . substr($UketukeNo,8,12) ;
		} else {
			//そのまま返す
		}
		return $UketukeNo;
	}

}
