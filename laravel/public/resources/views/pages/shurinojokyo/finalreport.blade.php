<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
<title>LIXILインターネット修理受付センター | 修理の状況[結果報告書（速報）]</title>
<style>
html,body{font-size: 14px;font-weight: 500;background: #fff !important;}
table{font-size: 14px;}
</style>
</head>
<body>
  <!-- * Start section print * -->
  <section class="container-silver">
    <div class="content-report">
      <div class="col-print-btn">
        <ul class="col-nav-bt">
            <li><a href="javascript:void(0)" onclick="window.print()"><span class="icon icon_user"><img src="/assets/images/print.png"></span>印刷</a></li>
            <li class="logout_sv">
             <a href="javascript:void(0)" onclick="window.close();">
                <span class="icon icon_logout"><img src="/assets/images/logout.png"></span>閉じる
             </a>
           </li>
        </ul>
      </div><!--/col-print-btn-->
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="report-title">
            <span><h1>結果報告書（速報）</h1></span>
            <span class="report-date">発行日<span>2017/09/01</span></span>
          </div>
        </div>
      </div>
      <div class="row-sv">
        <div class="col-sv-8">
          <div class="report-note">
            <div class="report-id">コード<span>10143</span></div>
            <div class="report-dear"><span class="dear_set"></span><span>御中</span></div>
            <div class="report-name"><span class="name_set"></span><span>様</span></div>
            <div class="report-des">
              <p>いつもお世話になっております。
                ご依頼いただきました修理について訪問しました状況を、
                下記の通りご連絡申し上げます。</p>
            </div>
          </div>
        </div>
        <div class="col-sv-4">
          <div class="report-logo">
            <img src="/assets/images/lixil-logo-report.png">
          </div>
          <div class="report-contact">
            <div class="report-lixil">LIXIL修理受付センター</div>
            <div class="report-lixil">ナビダイアル　0570-01-1794</div>
            <div class="report-lixil">(電話　011-300-51001）</div>
            <div class="report-lixil">FAX　0120-1794-56</div>
          </div>
          </div>
        </div>
      <div class="row-sv">
        <div class="col-sv-12">
          <div class="center">＜記＞</div>
          <div class="tb-responsive table-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">受付No<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">A338-8423</td>
                  <th class="col-ret-title">貴社注文番号<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">受付年月日<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">2017/03/15</td>
                  <th class="col-ret-title">工事店名<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">お得意先様顧客No<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
              </tbody>
            </table>
          </div><!--table-responsive-->
          <div class="tb-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title ft-bold">
                    <div class="tb-report">お客様住所<span style="text-align:right;">：</span></div>
                  </th>
                  <td class="ft-bold"></td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様氏名<span style="text-align:right;">：</span></th>
                  <td class="ft-bold">xx　xx様</td>
                </tr>
                <tr>
                  <th class="col-ret-title ft-bold">お客様電話番号<span style="text-align:right;">：</span></th>
                  <td class="ft-bold"></td>
                </tr>
              </tbody>
            </table>
          </div><!--/tb-responsive-->
          <div class="tb-responsive table-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">商品品番(名)/ご依頼内容<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">商品品番(名)/ご依頼内容<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">2017/03/15</td>
                </tr>
                <tr>
                  <th class="col-ret-title">商品品番(名)/ご依頼内容<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tb-responsive">
            <table class="table-report-no">
              <tbody>
                <tr>
                  <th class="col-ret-title">訪問日<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">担当者<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result">2017/03/15</td>
                </tr>
                <tr>
                  <th class="col-ret-title">完・未完の別<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">ご請求先住所<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">ご請求先名<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">ご請求先担当者<span style="text-align:right;">：</span></th>
                  <td class="col-ret-result"></td>
                </tr>
                <tr>
                  <th class="col-ret-title">
                    <p>交換部品明細
                      ※交換部品は５種類まで
                      しか表示できませんので
                      ご了承ください</p>
                  </th>
                  <td class="col-ret-result">
                    <table class="table-report-border" style="margin-bottom: 30px;">
                      <thead>
                        <tr>
                          <th style="width:50px"></th>
                          <th style="width:350px">部品明細</th>
                          <th style="width:120px">数量</th>
                          <th style="width:120px">単価</th>
                          <th style="width:200px">部品代</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="center">1</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="center">2</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="center">3</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th class="col-ret-title">交換部品明細</th>
                  <td class="col-ret-result">
                    <table class="table-report-border">
                      <thead>
                        <tr>
                          <th style="width:50px;"></th>
                          <th style="width:395px;">部位部品名称</th>
                          <th style="width:395px;">処置内容</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="center">1</td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div><!--/tb-responsive-->
        </div>
      </div><!--/row-sv-->
      <div class="row-sv">
        <div class="col-sv-7">
          <div class="tb-responsive tb-total">
            <table class="table-report-no">
                <tbody style="border-bottom: 1px solid #000;">
                  <tr>
                    <th class="col-ret-title">部品代計<span style="text-align:right;">：</span></th>
                    <td class="col-result-price">\0</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">技術料<span style="text-align:right;">：</span></th>
                    <td class="col-result-price">\0</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">出張費<span style="text-align:right;">：</span></th>
                    <td class="col-result-price">\0</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">処分料<span style="text-align:right;">：</span></th>
                    <td class="col-result-price">\0</td>
                  </tr>
                  <tr>
                    <th class="col-ret-title">消費税<span style="text-align:right;">：</span></th>
                    <td class="col-result-price">\0</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th class="col-ret-title">合計<span style="text-align:right;">：</span></th>
                    <td class="col-result-price">\0</td>
                  </tr>
                </tfoot>
              </table>
            </div><!--/tb-responsive-->
        </div>
        <div class="col-sv-5">
          <div class="tb-responsive">
            <ul class="work-detail">
              <li>
                <p>作業内容：
                給水ユニットの部品交換にて作業を完了致しました。</p>
              </li>
            </ul>
            <ul class="work-detail">
              <li>
                <p>お客様へのメッセージ：
                  給水ユニットの部品交換にて作業を完了致しました。</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div><!--/content-report-->
  </section>
  <!-- * Start section prind * -->
</body>
</html>
