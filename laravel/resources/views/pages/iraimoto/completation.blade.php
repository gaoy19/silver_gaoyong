@extends('layouts.master')
@section('request_active','active_sv')
@section('title','LIXILインターネット修理受付センター | 修理のご依頼[5修理のご依頼完了]')
@section('content')
  <!-- * Start section 修理のご依頼 * -->
  <div class="row-sv">
    <div class="main-vs-12">
        <div class="box-title_sv">
            <h1>修理のご依頼</h1>
        </div>
    </div>
  </div><!--/row-sv-->
  <!-- * End section 修理のご依頼 * -->
  <!-- * Start section お客さま情報の入力 * -->
  <div class="row-sv">
    <div class="col-sv-12 box-progress-12">
        <ul class="row-box-bar">
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">1</div>
                    <div class="box-text-bar">お客さま情報の入力</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">2</div>
                    <div class="box-text-bar">修理依頼内容の入力</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">3</div>
                    <div class="box-text-bar">ご依頼元情報の入力 </div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">4</div>
                    <div class="box-text-bar">ご依頼内容の確認</div>
                </div>
            </li><!--/box-sv-3-->
            <li class="box-sv-3 active_sv">
                <div class="box-progress-bar">
                    <div class="box-num-bar">5</div>
                    <div class="box-text-bar">修理のご依頼完了 </div>
                </div>
            </li><!--/box-sv-3-->
        </ul><!--/box-bar-->
    </div><!--/box-progress-12-->
  </div><!--/row-sv--><!--/row-sv-->
  <!-- * End section お客さま情報の入力 * -->
  <!-- * Start section 修理のご依頼完了 * -->
  <div class="row-sv">
      <div class="main-vs-12 sv_top20">
         <div class="box-main-form">
              <div class="modal-header_sv">
                  <h5>修理のご依頼完了</h5>
              </div>
              <div class="con-info_sv">
                @if(session('result'))
                <div class="col-received-title">お申し込み NO:{{ session('result') }} にてご依頼内容を登録しました。</div>
                <div class="col-received-des">
                    <p>登録内容に関するお問い合わせの際は、お申し込み No をお申し付けください。</p>
                </div>
                @else
                <div class="col-received-title">お申し込みご依頼内容を登録失敗しました。</div>
                @endif

              </div>
          </div>
          <!--お問い合わせ先-->
          <div class="contact-sv">
              <div class="contact-title">
                  <h5>依頼内容の変更、進捗等の問い合せ先</h5>
              </div>
              <div class="box-sv-12">
                  <div class="box-sv-title">（株）LIXIL 修理受付センター<span class="number_des_sv">お問い合わせ対応時間：9:00 ～ 19:00</span></div>                          
                  <div class="number-sv">
                      <div class="number_cot_sv">
                          <div class="number_brand">INAX・トステム（浴室・洗面）：</div><span class="icon icon-contact"><img src="{{ asset('assets/images/contact.png') }}"></span><span class="phone_num_sv">0570-01-1794</span>
                          <div class="number_brand">サンウェーブ・トステム（キッチン）：</div><span class="phone_num_sv">0120-677-755</span>
                      </div>
                  </div>
              </div>
              <div class="box-sv-12">
                  <div class="contact-title">
                      <h5>システムの問い合せ先</h5>
                  </div>
                  <div class="mail-sv">
                      <div class="mail_cot">
                          <a href="mailto:webinfo-imt@i2.inax.co.jp?subject=LIXILインターネット修理受付センターお問い合わせ"><span class="icon icon_mail"></span>お問い合わせ</a>
                          <div class="mail_des">メールアドレス：webinfo-imt@i2.inax.co.jp</div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-sv-12">
             <div class="btn-center-received">
                 <div class="sv_center">
                    <a href="{{ route('iraimoto.showKokyaku') }}" class="btn-confirm">さらに修理依頼を申し込む</a>
                 </div>
                 <div class="sv_center">
                    <a class="btn-confirm_status" href="{{ route('webuketuke.index') }}">修理依頼の状況を確認する</a>
                 </div>
              </div>
          </div>
      </div><!--main-vs-12 sv_top20-->
  </div><!--/row-sv--><!--/row-sv-->
  <!-- * End section 修理のご依頼完了 * -->
@stop
