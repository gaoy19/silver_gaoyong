<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class WebUketukeCenter extends Model
{
    protected $table = 'WEB_UKETUKECENTER';

    public $timestamps = false;
}
