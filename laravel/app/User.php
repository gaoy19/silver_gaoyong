<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  // use Notifiable;
  /**
   * The database table used by the model.
   *
   * @var string
  */
  protected $table = 'WEB_RCP_USER';
  // protected $table = 'USERS';
  protected $primaryKey = 'user_id';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  public $timestamps = false;
  public $incrementing = false;
  protected $fillable = [
    'user_id', 'user_simei', 'passwd',
    'tokuisaki_cd', 'mail_address', 'user_kbn',
    'yuko_kigen', 'torokubi', 'torokusya_cd',
    'kosinbi', 'kosinsya_cd', 'sakujo_flg', 'biko',
    'info_mail_address', 'passwd_sosinbi', 'passwd_sosin_count', 'alphas_id', 'sokuho_mail', 'uketuke_mail','remember_token','password'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'passwd', 'remember_token'
  ];

  // public function getAuthPassword()
  // {
  //     return $this->passwd;
  // }

  public function getAuthIdentifier()
  {
    return $this->original['user_id'];
  }
  /**
  * Validator Resgister
  */
  public function userRules()
  {
    return  [
      "tokuisaki_sei" => "required|max:40|zenkaku|kinsoku",
      "tokuisaki_sei_kana" => "required|max:40|zenkaku",
      // Validator Post code
      "tokuisakiYuubinNo" => "required|max:8|jp_postcode",
      "tokuisakiTodoufuken" => "required",
      "tokuisakiAddress" => "required|max:45|zenkaku|kinsoku",
      // End Validator Post code
      "user_simei"  => "required|max:40|zenkaku|kinsoku",
      "tokuisaki_tel_haihun" => "required|max:13|jp_phone",
      "torokusaki_fax"  => "required|max:13|jp_phone",
      "mail_address" =>"nullable|email|max:200",
      "info_mail_address" => "required|email|max:200",
      // Validator password
      "passwd" => "required|hankaku|min:4|max:20|lixil_password",
      "password_confirmation" => "required|min:4|max:20|same:passwd",
    ];
  }
  /**
  * トップページ自動ログイン
  * Validator Resgister
  */
  public function userRulesAlphas()
  {
    return  [
      "user_id_Alphas" => "required",
      // Validator password
      "passwd_Alphas" => "required|hankaku|min:4|max:20",
    ];
  }
  /**
  * 修理依頼内容の入力のバリデーションのエラーメッセージ
  */
  public function userErrorsMessage()
  {
    return  [
      'tokuisaki_sei.kinsoku' => 'ご利用いただけない文字列が含まれています。利用ができない文字列の一覧は<a target="_blank" href="'.route('shiyofukamoji').'">こちら</a>',
      'tokuisaki_sei.required' =>'貴社名の入力を行ってください。',
      'tokuisaki_sei.max' =>':max文字まで、指定文字数を超える入力は行えない。',
      'tokuisaki_sei.zenkaku' =>'貴社名は全角での入力を行って下さい。',
      'tokuisaki_sei_kana.required' =>'貴社名（フリガナ）の入力を行ってください。',
      'tokuisaki_sei_kana.max' =>':max指定文字数を超える入力は行えない',
      'tokuisaki_sei_kana.zenkakukana' =>'全角カタカナでの入力を行ってください。',
      // Validator Post code
      'tokuisakiYuubinNo.required' => '郵便番号の入力を行ってください。',
      'tokuisakiYuubinNo.max' => ':max指定文字数を超える入力は行えない。',
      'tokuisakiYuubinNo.jp_postcode' => '郵便番号の形式が不正です。',
      'tokuisakiTodoufuken.required' => '都道府県の選択は必須です。',
      'tokuisakiAddress.required' => '住所の入力は必須です。',
      'tokuisakiAddress.max' => ':max文字まで入力して下さい。',
      'tokuisakiAddress.zenkaku' => 'ご住所は全角での入力を行って下さい。',
      'tokuisakiAddress.kinsoku' => 'ご利用いただけない文字列が含まれています。利用ができない文字列の一覧は<a target="_blank" href="'.route('shiyofukamoji').'">こちら</a>',
      // end Validator Post code
      'user_simei.required' => '担当者名の入力は必須です。',
      'user_simei.max' => ':max指定文字数を超える入力は行えない',
      'user_simei.zenkaku' => '担当者名は全角での入力を行って下さい。',
      'user_simei.kinsoku' => 'ご利用いただけない文字列が含まれています。利用ができない文字列の一覧は<a target="_blank" href="'.route('shiyofukamoji').'">こちら</a>',
      'tokuisaki_tel_haihun.required' => '電話番号の入力は必須です。',
      'tokuisaki_tel_haihun.jp_phone' => '電話番号の形式が不正です。',
      'tokuisaki_tel_haihun.max' => ':max指定文字数を超える入力は行えない。',
      'torokusaki_fax.required' => 'FAX番号の入力は必須です。',
      'torokusaki_fax.jp_phone' => 'FAX番号の形式が不正です。',
      'torokusaki_fax.max' => ':max文字まで入力して下さい。',
      'mail_address.max' => ':max指定文字数を超える入力は行えない。',
      'mail_address.email' => 'メールアドレス2の形式が不正です。',
      'info_mail_address.required' => 'メールアドレス1の入力は必須です。',
      'info_mail_address.max' => ':max指定文字数を超える入力は行えない。',
      'info_mail_address.email' => 'メールアドレス1の形式が不正です。',
      // Validator password
      'passwd.required'  => 'パスワードの入力は必須です。',
      'passwd.hankaku' => 'パスワードは半角で入力してください。',
      'passwd.min'  => 'パスワードは半角、4〜20文字で指定してください。',
      'passwd.max'  => 'パスワードは半角、4〜20文字で指定してください。',
      'password_confirmation.required'  => '新しいパスワード（再入力）は必要です。',
      'password_confirmation.min'  => '新しいパスワード（再入力）は半角、4〜20文字で指定してください。',
      'password_confirmation.max'  => '新しいパスワード（再入力）は半角、4〜20文字で指定してください。',
      'password_confirmation.same' => '入力したパスワードが一致しません。'
    ];
  }
  /**
  * トップページ自動ログインの入力のバリデーションのエラーメッセージ
  */
  public function userErrorsMessageAlphas()
  {
    return  [
      'user_id_Alphas.required'=> 'ユーザーIDを入力してください。',
      // Validator password
      'passwd_Alphas.required'  => 'パスワードの入力は必須です。',
      'passwd_Alphas.hankaku' => 'パスワードは半角で入力してください。',
      'passwd_Alphas.min'  => 'パスワードは半角、4〜20文字で指定してください。',
      'passwd_Alphas.max'  => 'パスワードは半角、4〜20文字で指定してください。'
    ];
  }
  /**
   * User change password validation rules.
   *
   * @return array
  */
  public function changePasswordRules()
  {
    return [
      'current_password'          => 'required|hankaku|min:4|max:20',
      "new_password"              => "required|hankaku|min:4|max:20|lixil_password",
      "new_password_confirmation" => "required|min:4|max:20|same:new_password",
    ];
  }

  /**
    * User change password validation messages.
    *
    * @return array
  */
  public function changePasswordErrorMessage()
  {
    return [
      'current_password.required'          => '現在のパスワードの入力は必須です。',
      'current_password.min'               => '現在のパスワードは半角、4〜20文字で指定してください。',
      'current_password.max'               => '現在のパスワードは半角、4〜20文字で指定してください。',
      'current_password.hankaku'           => '現在のパスワードは半角で入力してください。',

      'new_password.required'                  => '新しいパスワードの入力は必須です。',
      'new_password.hankaku'                   => '新しいパスワードは半角で入力してください。',
      'new_password.min'                       => 'パスワードは半角、4〜20文字で指定してください。',
      'new_password.max'                       => 'パスワードは半角、4〜20文字で指定してください。',

      'new_password_confirmation.required' => '新しいパスワード（再入力）の入力は必要です。',
      'new_password_confirmation.min'      => '新しいパスワード（再入力）は半角、4〜20文字で指定してください。',
      'new_password_confirmation.max'      => '新しいパスワード（再入力）は半角、4〜20文字で指定してください。',
      'new_password_confirmation.same'     => '入力したパスワードが一致しません。',
    ];
  }

  /**
   * User change password validation message if not match.
   *
   * @return array
  */
  public function currentPasswordMsgNotExists()
  {
    return '入力されたパスワードが登録内容と異なっています。';
  }

  /**
   * User change information validation message if user not change information.
   *
   * @return array
  */
  public function notChangeInformation()
  {
    return '入力内容が現在の登録内容と同じです。情報を１つ以上変更してください。';
  }

  //TOKUISAKI:WEB_RCP_USER = 1:N relationship
  public function tokuisaki()
	{
		return $this->hasOne('App\Tokuisaki', 'tokuisaki_cd', 'tokuisaki_cd');
	}

  /**
   * User change information validation rules.
   *
   * @return array
  */
  public function changeInformationRules()
  {
    return [
      //'tokuisaki_sei'           => 'required|max:40|zenkaku|kinsoku',
      //'tokuisaki_sei_kana'      => 'required|max:40|zenkaku',
      //'tokuisaki_yubin'         => 'required|max:8|jp_postcode',
      //'tokuisaki_jusyo'         => 'required',
      //'tokuisaki_jusyo_address' => 'required|max:45|zenkaku|kinsoku',
      'user_simei'              => 'required|max:40|zenkaku|kinsoku',
      //'tokuisaki_tel_haihun'    => 'required|max:13|jp_phone',
      //'torokusaki_fax'          => 'required|max:13|jp_phone',
      'info_mail_address'       => 'required|email|max:200',
      'mail_address'            => 'nullable|email|max:200',
    ];
  }

  /**
   * User change information validation messages.
   *
   * @return array
  */
  public function changeInformationErrorMessage()
  {
    return [
      //'tokuisaki_sei.kinsoku'  => 'ご利用いただけない文字列が含まれています。利用ができない文字列の一覧は<a target="_blank" href="'.route('shiyofukamoji').'">こちら</a>',
      //'tokuisaki_sei.required' =>'貴社名の入力を行ってください。',
      //'tokuisaki_sei.max'      =>':max文字まで入力して下さい。',
      //'tokuisaki_sei.zenkaku'  =>'貴社名は全角での入力を行って下さい。',

      //'tokuisaki_sei_kana.required'    =>'貴社名（フリガナ）の入力を行ってください。',
      //'tokuisaki_sei_kana.max'         =>':max文字まで入力して下さい。',
      //'tokuisaki_sei_kana.zenkakukana' =>'全角カタカナでの入力を行ってください。',

      //'tokuisaki_yubin.required'         => '郵便番号の入力を行ってください。',
      //'tokuisaki_yubin.max'              => ':max文字まで入力して下さい。',
      //'tokuisaki_yubin.jp_postcode'      => '郵便番号の形式が不正です。',

      //'tokuisaki_jusyo.required'         => '都道府県の選択は必須です。',

      //'tokuisaki_jusyo_address.required' => '住所の入力は必須です。',
      //'tokuisaki_jusyo_address.max'      => ':max文字まで入力して下さい。',
      //'tokuisaki_jusyo_address.zenkaku'  => 'ご住所は全角での入力を行って下さい。',
      //'tokuisaki_jusyo_address.kinsoku'  => 'ご利用いただけない文字列が含まれています。利用ができない文字列の一覧は<a target="_blank" href="'.route('shiyofukamoji').'">こちら</a>',

      'user_simei.required' => '担当者名の入力は必須です。',
      'user_simei.max'      => ':max文字まで入力して下さい。',
      'user_simei.zenkaku'  => '担当者名は全角での入力を行って下さい。',
      'user_simei.kinsoku'  => 'ご利用いただけない文字列が含まれています。利用ができない文字列の一覧は<a target="_blank" href="'.route('shiyofukamoji').'">こちら</a>',

      //'tokuisaki_tel_haihun.required'    => '電話番号の入力は必須です。',
      //'tokuisaki_tel_haihun.jp_phone'    => '電話番号の形式が不正です。',
      //'tokuisaki_tel_haihun.max'         => ':max文字まで入力して下さい。',

      //'torokusaki_fax.required'          => 'FAX番号の入力は必須です。',
      //'torokusaki_fax.jp_phone'          => 'FAX番号の形式が不正です。',
      //'torokusaki_fax.max'               => ':max文字まで入力して下さい。',

      'info_mail_address.required'       => 'メールアドレス1の入力は必須です。',
      'info_mail_address.max'            => ':max文字まで入力して下さい。',
      'info_mail_address.email'          => 'メールアドレス1の形式が不正です。',

      'mail_address.max'                 => ':max文字まで入力して下さい。',
      'mail_address.email'               => 'メールアドレス2の形式が不正です。',
    ];
  }
  public function charReplaceRegister($values)
  {
    $values['tokuisaki_sei_kana'] = \App\Custom\CharReplace::hanKanaTozenKana($values['tokuisaki_sei_kana']);
    $values['tokuisaki_tel_haihun'] = \App\Custom\CharReplace::replaceNumber($values['tokuisaki_tel_haihun']);
    $values['torokusaki_fax'] = \App\Custom\CharReplace::replaceNumber($values['torokusaki_fax']);
    $values['tokuisakiYuubinNo'] = \App\Custom\CharReplace::replaceNumber($values['tokuisakiYuubinNo']);
    return $values;
  }

  public static function insertData($values)
  {
    return $result = DB::table('WEB_RCP_USER')->insert($values);
  }

  public static function getUserId($random_user_id)
  {
      $u_id = DB::table('WEB_RCP_USER')
          ->where('user_id', '=', $random_user_id)
          ->pluck('user_id');

      return $u_id;
  }
}
