<?php

namespace App\Http\Controllers\rcpUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\User;
use Redirect;
use Validator;
use Auth;
use DB;
use Input;
use App\RequestData;
use Session;
class ResetPasswordController extends Controller
{
  //Show form to seller where they can reset password
  public function showResetForm($token = null,Request $request)
  {
    //check logo
    if(Auth::check()){
      return Redirect::route('login');
    }
    $changePassword =  \App\Web_change_password::where('hash','=',$token)
                                            ->where('del_flg','=','0')
                                            ->first();
    $user = DB::table('WEB_RCP_USER')
                ->join('WEB_CHANGE_PASSWORD', 'WEB_RCP_USER.user_id', '=', 'WEB_CHANGE_PASSWORD.user_id')
                ->where('hash','=',$token)
                ->select('WEB_RCP_USER.user_simei','WEB_RCP_USER.tokuisaki_cd')
                ->first();
    return view('users.passwords.reset',['changePassword'=>$changePassword,'user'=>$user]);

  }
  public function resetPassword($token = null,  Request $request)
  {
    $this->validate($request, [
        'password' => "required|hankaku|min:4|max:20|lixil_password",
        'password_confirmation' => "required|min:4|max:20|same:password",
    ],[
      'password.required'  => '新しいパスワードの入力は必要です。',
      'password.min'  => 'パスワードは半角、4〜20文字で指定してください。',
      'password.max'  => 'パスワードは半角、4〜20文字で指定してください。',
      'password_confirmation.required'  => '新しいパスワード（再入力）の入力は必要です。',
      'password_confirmation.min'  => '新しいパスワード（再入力）は半角英数字、4〜20文字で指定してください。',
      'password_confirmation.max'  => '新しいパスワード（再入力）は半角英数字、4〜20文字で指定してください。',
      'password_confirmation.same' => '入力したパスワードが一致しません。'
    ]);


    $hash = DB::table('WEB_RCP_USER')
                ->join('WEB_CHANGE_PASSWORD', 'WEB_RCP_USER.user_id', '=', 'WEB_CHANGE_PASSWORD.user_id')
                ->where('hash','=',$request->token)
                ->where('del_flg','=','0')
                ->first();

    $token_sql =  \App\Web_change_password::where('hash','=',$hash->hash);
    if($hash === null || count($hash) == 0){
      return Redirect::route('user_password.email');
    }
    $user = User::where('user_id','=',$hash->user_id);

    // table WEB_RCP_USER
    $value['password'] = bcrypt($request->password);
    $value['passwd'] = $request->password;
    $user->update($value);

    // table WEB_CHANGE_PASSWORD
    $flag['del_flg'] = '1';
    $flag['update_datetime'] = Carbon::now();

    $token_sql->update($flag);

    return view('users.passwords.resetcomplete',['status'=>'success','user'=>$user->first()]);
  }

}
