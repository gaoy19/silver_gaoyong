<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UketukeFileMail extends Mailable
{
    use Queueable, SerializesModels;

    private $_subject = '【添付資料】';

    private $_name = '【LIXIL】インターネット修理受付センター';

    private $_from = null;

    private $_data = array();
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = array())
    {
        $this->_data = $data;
        $this->_from = \Config::get('mail.from.address');
        $this->setSubject("【添付資料】Web受付No.:{$data['web_uketuke_no']}");
        $this->setFrom(\Config::get('mail.username'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $dst = \Storage::disk('incoming')->url($this->_data['web_uketuke_no']);

      $this->from($this->_from, '【LIXIL】インターネット修理受付センター')
                    ->subject($this->_subject)
                    ->text('emails.uketukefile');

      $file1 = $dst . '/' . $this->_data['filename1'];
      if(\File::exists($file1) && $this->_data['filename1'] != ''){
        $this->attach($file1);
      }

      $file2 = $dst . '/' . $this->_data['filename2'];
      if(\File::exists($file2) && $this->_data['filename2'] != ''){
        $this->attach($file2);
      }

      $file3 = $dst . '/' . $this->_data['filename3'];
      if(\File::exists($file3) && $this->_data['filename3'] != ''){
        $this->attach($file3);
      }

      $file4 = $dst . '/' . $this->_data['filename4'];
      if(\File::exists($file4) && $this->_data['filename4'] != ''){
        $this->attach($file4);
      }

      $file5 = $dst . '/' . $this->_data['filename5'];
      if(\File::exists($file5) && $this->_data['filename5'] != ''){
        $this->attach($file5);
      }

      return $this->with($this->_data);
    }

    /**
     *題名を設定
     */
    public function setFrom($from)
    {
      $this->_from = $from;
    }

    /**
     *題名を設定
     */
    public function setSubject($subject)
    {
      $this->_subject = $subject;
    }

    /**
     *題名を設定
     */
    public function setName($name)
    {
      $this->_name = $name;
    }
}
